// More info on Webpack's Node API here: https://webpack.github.io/docs/node.js-api.html
import webpack from 'webpack';
import webpackConfig from '../webpack.prod';
import fs from 'fs';

process.env.NODE_ENV = 'production'; // this assures the Babel dev config (for hot reloading) doesn't apply.

console.log('Generating minified bundle for production via Webpack. This will take a moment...'.blue);

webpack(webpackConfig).run((err, stats) => {
  if (err) { // so a fatal error occurred. Stop here.
    console.log(err.bold.red);
    return 1;
  }
  const jsonStats = stats.toJson();

  if (jsonStats.hasErrors) {
    return jsonStats.errors.map(error => console.log(error.red));
  }

  if (jsonStats.hasWarnings) {
    console.log('Webpack generated the following warnings: '.bold.yellow);
    jsonStats.warnings.map(warning => console.log(warning.yellow));
  }

  console.log(`Webpack stats: ${stats}`);

  const dir = './dist/assets/';
  const subdirs = ['', 'styles/', 'images/', 'vocabularies/'];

  subdirs.forEach((sdir) => {
    if (!fs.existsSync(dir + sdir)) {
      fs.mkdirSync(dir + sdir);
    }
  });

  //fs.createReadStream('src/index.html').pipe(fs.createWriteStream('dist/index.html'));
  fs.createReadStream('src/assets/styles/preLoad.css').pipe(fs.createWriteStream('dist/assets/styles/preLoad.css'));
  fs.createReadStream('src/assets/favicon.ico').pipe(fs.createWriteStream('dist/assets/favicon.ico'));
  fs.createReadStream('src/assets/images/rings.svg').pipe(fs.createWriteStream('dist/assets/images/rings.svg'));
  fs.createReadStream('src/assets/images/bkg-blurred-dsk.jpg').pipe(fs.createWriteStream('dist/assets/images/bkg-blurred-dsk.jpg'));
  fs.createReadStream('src/assets/images/no-image.png').pipe(fs.createWriteStream('dist/assets/images/no-image.png'));
  fs.createReadStream('src/assets/images/es.png').pipe(fs.createWriteStream('dist/assets/images/es.png'));
  fs.createReadStream('src/assets/images/gb.png').pipe(fs.createWriteStream('dist/assets/images/gb.png'));
  fs.createReadStream('src/assets/images/ru.png').pipe(fs.createWriteStream('dist/assets/images/ru.png'));
  fs.createReadStream('src/assets/images/us.png').pipe(fs.createWriteStream('dist/assets/images/us.png'));
  fs.createReadStream('src/assets/vocabularies/vocabulary.US.json').pipe(fs.createWriteStream('dist/assets/vocabularies/vocabulary.US.json'));
  fs.createReadStream('src/assets/vocabularies/vocabulary.ES.json').pipe(fs.createWriteStream('dist/assets/vocabularies/vocabulary.ES.json'));
  fs.createReadStream('src/assets/vocabularies/vocabulary.RU.json').pipe(fs.createWriteStream('dist/assets/vocabularies/vocabulary.RU.json'));

  console.log('index.html and asssets written to /dist'.green);
  console.log('The application has been compiled in production mode and written to /dist.'.green);

  return 0;
});