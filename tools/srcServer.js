import express from 'express';
import webpack from 'webpack';
import path from 'path';
import open from 'open';

const config = require('../webpack.dev');
const port = 3000;
const app = express();
const compiler = webpack(config);

app.use('/assets',express.static(path.join( __dirname, '../src/assets/')));
app.use('/images',express.static(path.join( __dirname, '../src/assets/images/')));
app.use('/less',express.static(path.join( __dirname, '../src/assets/styles/less/')));
app.use('/fonts',express.static(path.join( __dirname, '../src/assets/fonts/')));

app.use(require('webpack-dev-middleware')(compiler, {
  noInfo: true,
  publicPath: config.output.publicPath
}));

app.use(require('webpack-hot-middleware')(compiler));

app.get('*', function(req, res) {
  res.sendFile(path.join( __dirname, '../src/index.html'));
});


app.listen(port, function(err) {
  if (err) {
    console.log(err);
  } else {
    //open(`http://localhost:${port}`);
  }
}); 