let smsTemplates = [
      {
        "id": 2434,
        "hospitalId": 1,
        "type": "Consultation_HoursBefore_Phone",
        "smsText": "You have a(n) @BrandName phone appointment in @HoursBefore hour(s), at @ScheduledTime. @ClinicianFirstName @ClinicianLastName will call you at @EncounterPhoneNumber",
        "isActive": "Y"
      },
      {
        "id": 2433,
        "hospitalId": 1,
        "type": "Consultation_HoursBefore",
        "smsText": "You have a(n) @BrandName @Method appointment with @ClinicianFirstName @ClinicianLastName in @HoursBefore hour(s), at @ScheduledTime today. @Url",
        "isActive": "Y"
      },
      {
        "id": 2432,
        "hospitalId": 1,
        "type": "Clinician_HoursBefore",
        "smsText": "You have a(n) @BrandName @Method appointment in @HoursBefore hour(s), at @ScheduledTime. @Url",
        "isActive": "Y"
      },
      {
        "id": 2431,
        "hospitalId": 1,
        "type": "ClinicianReady_FromMobile",
        "smsText": "@ClinicianFirstName @ClinicianLastName is reviewing your information on @BrandName and will be with you shortly.",
        "isActive": "Y"
      },
      {
        "id": 2430,
        "hospitalId": 1,
        "type": "ClinicianReady_Phone",
        "smsText": "@ClinicianFirstName @ClinicianLastName is reviewing your information on @BrandName and will be with you shortly.",
        "isActive": "Y"
      },
      {
        "id": 2429,
        "hospitalId": 1,
        "type": "PatientWaiting",
        "smsText": "A new patient is in the @BrandName waiting room, ready to be seen.",
        "isActive": "Y"
      },
      {
        "id": 2428,
        "hospitalId": 1,
        "type": "ClinicianReady",
        "smsText": "@ClinicianFirstName @ClinicianLastName is ready to see you now on @BrandName.",
        "isActive": "Y"
      }
    ]

  export{smsTemplates};