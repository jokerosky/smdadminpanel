let getListCodeSetss =
    "[{\"CodeSetId\":1,\"Description\":\"New User Registation Token\",\"IsActive\":\"A\",\"Codes\":[],\"CoUserTokens\":[],\"UserTokens\":[]},{\"CodeSetId\":2,\"Description\":\"Reset Password\",\"IsActive\":\"A\",\"Codes\":[],\"CoUserTokens\":[],\"UserTokens\":[]},{\"CodeSetId\":3,\"Description\":\"Blood Type\",\"IsActive\":\"A\",\"Codes\":[],\"CoUserTokens\":[],\"UserTokens\":[]},{\"CodeSetId\":4,\"Description\":\"Eye Color\",\"IsActive\":\"A\",\"Codes\":[],\"CoUserTokens\":[],\"UserTokens\":[]},{\"CodeSetId\":5,\"Description\":\"Ethnicity\",\"IsActive\":\"A\",\"Codes\":[],\"CoUserTokens\":[],\"UserTokens\":[]},{\"CodeSetId\":6,\"Description\":\"Medical Conditions\",\"IsActive\":\"A\",\"Codes\":[],\"CoUserTokens\":[],\"UserTokens\":[]},{\"CodeSetId\":7,\"Description\":\"Medication Allergies\",\"IsActive\":\"A\",\"Codes\":[],\"CoUserTokens\":[],\"UserTokens\":[]},{\"CodeSetId\":8,\"Description\":\"Medications\",\"IsActive\":\"A\",\"Codes\":[],\"CoUserTokens\":[],\"UserTokens\":[]},{\"CodeSetId\":9,\"Description\":\"Hair Color\",\"IsActive\":\"A\",\"Codes\":[],\"CoUserTokens\":[],\"UserTokens\":[]},{\"CodeSetId\":10,\"Description\":\"Month\",\"IsActive\":\"A\",\"Codes\":[],\"CoUserTokens\":[],\"UserTokens\":[]},{\"CodeSetId\":11,\"Description\":\"Year\",\"IsActive\":\"A\",\"Codes\":[],\"CoUserTokens\":[],\"UserTokens\":[]},{\"CodeSetId\":12,\"Description\":\"Relationship\",\"IsActive\":\"A\",\"Codes\":[],\"CoUserTokens\":[],\"UserTokens\":[]},{\"CodeSetId\":13,\"Description\":\"Consultation Status\",\"IsActive\":\"A\",\"Codes\":[],\"CoUserTokens\":[],\"UserTokens\":[]},{\"CodeSetId\":14,\"Description\":\"Doctor Status\",\"IsActive\":\"A\",\"Codes\":[],\"CoUserTokens\":[],\"UserTokens\":[]},{\"CodeSetId\":15,\"Description\":\"Hospital Type\",\"IsActive\":\"A\",\"Codes\":[],\"CoUserTokens\":[],\"UserTokens\":[]},{\"CodeSetId\":16,\"Description\":\"Patients Primary Concern\",\"IsActive\":\"A\",\"Codes\":[],\"CoUserTokens\":[],\"UserTokens\":[]},{\"CodeSetId\":17,\"Description\":\"Patients Secondary Concern\",\"IsActive\":\"A\",\"Codes\":[],\"CoUserTokens\":[],\"UserTokens\":[]},{\"CodeSetId\":18,\"Description\":\"Co-User Registration Token\",\"IsActive\":\"A\",\"Codes\":[],\"CoUserTokens\":[],\"UserTokens\":[]},{\"CodeSetId\":19,\"Description\":\"Choose\",\"IsActive\":\"A\",\"Codes\":[],\"CoUserTokens\":[],\"UserTokens\":[]},{\"CodeSetId\":20,\"Description\":\"Consultation Notes\",\"IsActive\":\"A\",\"Codes\":[],\"CoUserTokens\":[],\"UserTokens\":[]},{\"CodeSetId\":21,\"Description\":\"States\",\"IsActive\":\"A\",\"Codes\":[],\"CoUserTokens\":[],\"UserTokens\":[]},{\"CodeSetId\":22,\"Description\":\"DoctorConEvent\",\"IsActive\":\"A\",\"Codes\":[],\"CoUserTokens\":[],\"UserTokens\":[]},{\"CodeSetId\":23,\"Description\":\"PatientConEvent\",\"IsActive\":\"A\",\"Codes\":[],\"CoUserTokens\":[],\"UserTokens\":[]},{\"CodeSetId\":24,\"Description\":\"Card Type\",\"IsActive\":\"A\",\"Codes\":[],\"CoUserTokens\":[],\"UserTokens\":[]},{\"CodeSetId\":25,\"Description\":\"DroppedConsultationReason\",\"IsActive\":\"A\",\"Codes\":[],\"CoUserTokens\":[],\"UserTokens\":[]},{\"CodeSetId\":26,\"Description\":\"Scheduling Reason Type\",\"IsActive\":\"A\",\"Codes\":[],\"CoUserTokens\":[],\"UserTokens\":[]},{\"CodeSetId\":27,\"Description\":\"AccountSettings Token\",\"IsActive\":\"A\",\"Codes\":[],\"CoUserTokens\":[],\"UserTokens\":[]},{\"CodeSetId\":28,\"Description\":\"Update User EmailID\",\"IsActive\":\"A\",\"Codes\":[],\"CoUserTokens\":[],\"UserTokens\":[]},{\"CodeSetId\":29,\"Description\":\"Interval\",\"IsActive\":\"A\",\"Codes\":[],\"CoUserTokens\":[],\"UserTokens\":[]},{\"CodeSetId\":30,\"Description\":\"MsgInterval\",\"IsActive\":\"A\",\"Codes\":[],\"CoUserTokens\":[],\"UserTokens\":[]},{\"CodeSetId\":31,\"Description\":\"Patient Weight\",\"IsActive\":\"A\",\"Codes\":[],\"CoUserTokens\":[],\"UserTokens\":[]},{\"CodeSetId\":32,\"Description\":\"Patient Height\",\"IsActive\":\"A\",\"Codes\":[],\"CoUserTokens\":[],\"UserTokens\":[]},{\"CodeSetId\":33,\"Description\":\"New User Onboard Token\",\"IsActive\":\"A\",\"Codes\":[],\"CoUserTokens\":[],\"UserTokens\":[]},{\"CodeSetId\":35,\"Description\":\"DismissReason\",\"IsActive\":\"A\",\"Codes\":[],\"CoUserTokens\":[],\"UserTokens\":[]},{\"CodeSetId\":36,\"Description\":\"Appointment Notification Reason\",\"IsActive\":\"A\",\"Codes\":[],\"CoUserTokens\":[],\"UserTokens\":[]},{\"CodeSetId\":37,\"Description\":\"AdminPatientQueue Message\",\"IsActive\":\"A\",\"Codes\":[],\"CoUserTokens\":[],\"UserTokens\":[]}]";

let getListOrganizationTypess={
    "data": [
        {
          "description": "SnapMD",
          "id": 1,
          "statusCode": 0,
          "createdDate": "2015-11-23T00:00:00Z",
          "modifiedDate": "2015-11-23T00:00:00Z",
          "createdByUserId": 1,
          "modifiedByUserId": 1
        },
        {
          "description": "Client",
          "id": 2,
          "statusCode": 0,
          "createdDate": "2016-04-01T00:44:31.81Z",
          "modifiedDate": "2016-04-01T00:44:31.81Z",
          "createdByUserId": 15,
          "modifiedByUserId": 15
        },
        {
          "description": "Re-Seller",
          "id": 3,
          "statusCode": 0,
          "createdDate": "2016-04-01T00:44:31.81Z",
          "modifiedDate": "2016-04-01T00:44:31.81Z",
          "createdByUserId": 15,
          "modifiedByUserId": 15
        }
      ],
      "total": 3
}
let getListDocumentTypess={
    "data": [
        {
          "defaultDocumentText": "TOC",
          "documentType": 1
        },
        {
          "defaultDocumentText": "CTT",
          "documentType": 2
        }
      ]
}
let getListClientsTypess={
    "data": [
        {
          "key": 77,
          "value": "General"
        },
        {
          "key": 78,
          "value": "Pediatric"
        },
        {
          "key": 131,
          "value": "University Hospital"
        },
        {
          "key": 132,
          "value": "Accountable Care Organizations (ACO)"
        },
        {
          "key": 133,
          "value": "Urgent Care (UC)"
        },
        {
          "key": 134,
          "value": "Retail/Convenient Care Clinics (CC)"
        },
        {
          "key": 135,
          "value": "Allied Health Companies (AHC)"
        },
        {
          "key": 136,
          "value": "Integrated Health Systems/Networks (IHS)"
        },
        {
          "key": 137,
          "value": "Maritime (MT)"
        }
      ],
      "total": 9
}
let getListPartners={
    "data": [
        {
          "partnerId": 17,
          "name": "AllscriptsPM",
          "sustainableTokenType": 1,
          "partnerType": 1,
          "assemblyName": "SnapMD.Integration.ExternalApi.Allscripts",
          "providers": {
            "144": "Azure Hospital"
          },
          "sustainableTokenTypeName": "Per Partner",
          "partnerTypeName": "EHR"
        },
        {
          "partnerId": 8,
          "name": "AthenaHealth",
          "sustainableTokenType": 2,
          "partnerType": 1,
          "assemblyName": "SnapMD.Integration.ExternalApi.AthenaHealth",
          "providers": {
            "140": "Athena"
          },
          "sustainableTokenTypeName": "Per Hospital",
          "partnerTypeName": "EHR"
        },
        {
          "partnerId": 20,
          "name": "AuthorizeNet",
          "sustainableTokenType": 0,
          "partnerType": 7,
          "assemblyName": "",
          "providers": {
            "144": "Azure Hospital",
            "36": "SampleTestClient"
          },
          "sustainableTokenTypeName": "None",
          "partnerTypeName": "Payments"
        },
        {
          "partnerId": 16,
          "name": "AzureBlob",
          "sustainableTokenType": 0,
          "partnerType": 2,
          "assemblyName": "",
          "providers": {
            "144": "Azure Hospital",
            "140": "Athena",
            "141": "Practice Max",
            "180": "Test 2017-04-30 - 2",
            "1": "Emerald Dev"
          },
          "sustainableTokenTypeName": "None",
          "partnerTypeName": "File Sharing"
        },
        {
          "partnerId": 26,
          "name": "Chargify",
          "sustainableTokenType": 2,
          "partnerType": 7,
          "assemblyName": "",
          "providers": {
            "1": "Emerald Dev"
          },
          "sustainableTokenTypeName": "Per Hospital",
          "partnerTypeName": "Payments"
        },
        {
          "partnerId": 9,
          "name": "FolderGrid",
          "sustainableTokenType": 0,
          "partnerType": 2,
          "assemblyName": "SnapMD.Integration.FileSharing.FolderGrid",
          "providers": {
            "0": "SnapMD",
            "36": "SampleTestClient",
            "38": "Prompt Healthcare",
            "102": "PhD Labs",
            "104": "Digital Solutions Warehouse  OldDoNotUse",
            "105": "Prompt Healthcare old",
            "106": "Apollo Hospital",
            "107": "Care HospitalCare HospitalCare HospitalCare Hospit",
            "108": "Generic Hospital",
            "109": "Paradise Hospital",
            "110": "Maritime Hospital",
            "111": "Image Hospital",
            "112": "Apollo Joomla",
            "113": "Variety Hospital",
            "114": "Incredible Hospital",
            "115": "OLDDONOTUSEPrompt Care",
            "116": "Digital Solutions Warehouse OldDoNotUse ",
            "117": "OLDDONOTUSEPreDiabetes Centers",
            "118": "OLDDONOTUSELifelineMD",
            "119": "Connelly OLD SETUP DO NOT USE",
            "120": "OLDDONOTUSEConnelly Dermatology",
            "121": "Connelly Dermatology",
            "122": "LifelineMD",
            "123": "PreDiabetes Centers",
            "124": "Prompt Care",
            "125": "Chubs Children Hospital",
            "126": "Emerald Connected Care",
            "127": "MD Hotline",
            "128": "Healthpointe Medical Group",
            "129": "Quantum Pharmacy Solutions Limited",
            "130": "P and P Medical Solutions",
            "131": "M3 Medical",
            "132": "Executive Medicine of Texas",
            "135": "Test Name1",
            "139": "Bad Health",
            "140": "Athena",
            "141": "Practice Max"
          },
          "sustainableTokenTypeName": "None",
          "partnerTypeName": "File Sharing"
        },
        {
          "partnerId": 28,
          "name": "MDToolbox",
          "sustainableTokenType": 2,
          "partnerType": 3,
          "assemblyName": "",
          "providers": {
            "1": "Emerald Dev"
          },
          "sustainableTokenTypeName": "Per Hospital",
          "partnerTypeName": "ePrescription"
        },
        {
          "partnerId": 25,
          "name": "PokitdokEligibility",
          "sustainableTokenType": 1,
          "partnerType": 4,
          "assemblyName": "",
          "providers": {
            "144": "Azure Hospital"
          },
          "sustainableTokenTypeName": "Per Partner",
          "partnerTypeName": "Insurance Verification"
        },
        {
          "partnerId": 1,
          "name": "PracticeMAX eCW",
          "sustainableTokenType": 2,
          "partnerType": 1,
          "assemblyName": "SnapMD.Integration.ExternalApi.PracticeMax.Ecw",
          "providers": {
            "141": "Practice Max"
          },
          "sustainableTokenTypeName": "Per Hospital",
          "partnerTypeName": "EHR"
        },
        {
          "partnerId": 23,
          "name": "Redox",
          "sustainableTokenType": 2,
          "partnerType": 1,
          "assemblyName": "SnapMD.Integration.Ehr.Redox",
          "providers": {
            "148": "Redox Test Hospital"
          },
          "sustainableTokenTypeName": "Per Hospital",
          "partnerTypeName": "EHR"
        },
        {
          "partnerId": 12,
          "name": "Rxnt",
          "sustainableTokenType": 0,
          "partnerType": 3,
          "assemblyName": "SnapMD.Integration.ePrescription.Rxnt",
          "providers": {
            "0": "SnapMD",
            "36": "SampleTestClient",
            "38": "Prompt Healthcare",
            "102": "PhD Labs",
            "104": "Digital Solutions Warehouse  OldDoNotUse",
            "105": "Prompt Healthcare old",
            "106": "Apollo Hospital",
            "107": "Care HospitalCare HospitalCare HospitalCare Hospit",
            "108": "Generic Hospital",
            "109": "Paradise Hospital",
            "110": "Maritime Hospital",
            "111": "Image Hospital",
            "112": "Apollo Joomla",
            "113": "Variety Hospital",
            "114": "Incredible Hospital",
            "115": "OLDDONOTUSEPrompt Care",
            "116": "Digital Solutions Warehouse OldDoNotUse ",
            "117": "OLDDONOTUSEPreDiabetes Centers",
            "118": "OLDDONOTUSELifelineMD",
            "119": "Connelly OLD SETUP DO NOT USE",
            "120": "OLDDONOTUSEConnelly Dermatology",
            "121": "Connelly Dermatology",
            "122": "LifelineMD",
            "123": "PreDiabetes Centers",
            "124": "Prompt Care",
            "125": "Chubs Children Hospital",
            "126": "Emerald Connected Care",
            "127": "MD Hotline",
            "128": "Healthpointe Medical Group",
            "129": "Quantum Pharmacy Solutions Limited",
            "130": "P and P Medical Solutions",
            "131": "M3 Medical",
            "132": "Executive Medicine of Texas",
            "135": "Test Name1",
            "139": "Bad Health",
            "140": "Athena",
            "141": "Practice Max",
            "142": "SnapMD",
            "143": "Folder Grid Test",
            "144": "Azure Hospital",
            "145": "DismissReason Test Hospital",
            "146": "DismissReason Test Hospital",
            "147": "DismissReason Test Hospital 2",
            "148": "Redox Test Hospital",
            "149": "SnapMD",
            "150": "SnapMD",
            "151": "SnapMD",
            "152": "SnapMD",
            "153": "SnapMD",
            "154": "SnapMD",
            "155": "SnapMD",
            "156": "SnapMD",
            "157": "Dan 113"
          },
          "sustainableTokenTypeName": "None",
          "partnerTypeName": "ePrescription"
        },
        {
          "partnerId": 24,
          "name": "RxntEligibility",
          "sustainableTokenType": 1,
          "partnerType": 4,
          "assemblyName": "",
          "providers": {},
          "sustainableTokenTypeName": "Per Partner",
          "partnerTypeName": "Insurance Verification"
        },
        {
          "partnerId": 10,
          "name": "SnapMD Api",
          "sustainableTokenType": 2,
          "partnerType": 5,
          "assemblyName": "SnapMD.Integration.InternalApi",
          "providers": {
            "140": "Athena",
            "141": "Practice Max",
            "144": "Azure Hospital",
            "148": "Redox Test Hospital"
          },
          "sustainableTokenTypeName": "Per Hospital",
          "partnerTypeName": "SnapMD Api"
        },
        {
          "partnerId": 4,
          "name": "Test",
          "sustainableTokenType": 0,
          "partnerType": 2,
          "assemblyName": "",
          "providers": {},
          "sustainableTokenTypeName": "None",
          "partnerTypeName": "File Sharing"
        },
        {
          "partnerId": 19,
          "name": "Twilio",
          "sustainableTokenType": 0,
          "partnerType": 6,
          "assemblyName": "SnapMD.Integration.SMS.Twilio",
          "providers": {
            "0": "SnapMD",
            "36": "SampleTestClient",
            "38": "Prompt Healthcare",
            "102": "PhD Labs",
            "104": "Digital Solutions Warehouse  OldDoNotUse",
            "105": "Prompt Healthcare old",
            "106": "Apollo Hospital",
            "107": "Care HospitalCare HospitalCare HospitalCare Hospit",
            "108": "Generic Hospital",
            "109": "Paradise Hospital",
            "110": "Maritime Hospital",
            "111": "Image Hospital",
            "112": "Apollo Joomla",
            "113": "Variety Hospital",
            "114": "Incredible Hospital",
            "115": "OLDDONOTUSEPrompt Care",
            "116": "Digital Solutions Warehouse OldDoNotUse ",
            "117": "OLDDONOTUSEPreDiabetes Centers",
            "118": "OLDDONOTUSELifelineMD",
            "119": "Connelly OLD SETUP DO NOT USE",
            "120": "OLDDONOTUSEConnelly Dermatology",
            "121": "Connelly Dermatology",
            "122": "LifelineMD",
            "123": "PreDiabetes Centers",
            "124": "Prompt Care",
            "125": "Chubs Children Hospital",
            "126": "Emerald Connected Care",
            "127": "MD Hotline",
            "128": "Healthpointe Medical Group",
            "129": "Quantum Pharmacy Solutions Limited",
            "130": "P and P Medical Solutions",
            "131": "M3 Medical",
            "132": "Executive Medicine of Texas",
            "135": "Test Name1",
            "139": "Bad Health",
            "141": "Practice Max",
            "142": "SnapMD",
            "143": "Folder Grid Test",
            "144": "Azure Hospital",
            "148": "Redox Test Hospital",
            "149": "SnapMD",
            "150": "SnapMD",
            "151": "SnapMD",
            "152": "SnapMD",
            "153": "SnapMD",
            "154": "SnapMD",
            "155": "SnapMD",
            "1": "Emerald Dev"
          },
          "sustainableTokenTypeName": "None",
          "partnerTypeName": "SMS Sender"
        },
        {
          "partnerId": 27,
          "name": "TwilioCopilot",
          "sustainableTokenType": 0,
          "partnerType": 6,
          "assemblyName": "SnapMD.Integration.SMS.TwilioCopilot",
          "providers": {
            "140": "Athena"
          },
          "sustainableTokenTypeName": "None",
          "partnerTypeName": "SMS Sender"
        }
      ],
      "total": 16
}
let getListPartnerOnlyActives={
    "data": [
        {
          "partnerId": 17,
          "name": "AllscriptsPM",
          "sustainableTokenType": 1,
          "partnerType": 1,
          "assemblyName": "SnapMD.Integration.ExternalApi.Allscripts",
          "providers": {
            "144": "Azure Hospital"
          },
          "sustainableTokenTypeName": "Per Partner",
          "partnerTypeName": "EHR"
        },
        {
          "partnerId": 8,
          "name": "AthenaHealth",
          "sustainableTokenType": 2,
          "partnerType": 1,
          "assemblyName": "SnapMD.Integration.ExternalApi.AthenaHealth",
          "providers": {
            "140": "Athena"
          },
          "sustainableTokenTypeName": "Per Hospital",
          "partnerTypeName": "EHR"
        },
        {
          "partnerId": 20,
          "name": "AuthorizeNet",
          "sustainableTokenType": 0,
          "partnerType": 7,
          "assemblyName": "",
          "providers": {
            "144": "Azure Hospital",
            "36": "SampleTestClient"
          },
          "sustainableTokenTypeName": "None",
          "partnerTypeName": "Payments"
        },
        {
          "partnerId": 16,
          "name": "AzureBlob",
          "sustainableTokenType": 0,
          "partnerType": 2,
          "assemblyName": "",
          "providers": {
            "144": "Azure Hospital",
            "140": "Athena",
            "141": "Practice Max",
            "180": "Test 2017-04-30 - 2",
            "1": "Emerald Dev"
          },
          "sustainableTokenTypeName": "None",
          "partnerTypeName": "File Sharing"
        },
        {
          "partnerId": 26,
          "name": "Chargify",
          "sustainableTokenType": 2,
          "partnerType": 7,
          "assemblyName": "",
          "providers": {
            "1": "Emerald Dev"
          },
          "sustainableTokenTypeName": "Per Hospital",
          "partnerTypeName": "Payments"
        },
        {
          "partnerId": 9,
          "name": "FolderGrid",
          "sustainableTokenType": 0,
          "partnerType": 2,
          "assemblyName": "SnapMD.Integration.FileSharing.FolderGrid",
          "providers": {
            "0": "SnapMD",
            "36": "SampleTestClient",
            "38": "Prompt Healthcare",
            "102": "PhD Labs",
            "104": "Digital Solutions Warehouse  OldDoNotUse",
            "105": "Prompt Healthcare old",
            "106": "Apollo Hospital",
            "107": "Care HospitalCare HospitalCare HospitalCare Hospit",
            "108": "Generic Hospital",
            "109": "Paradise Hospital",
            "110": "Maritime Hospital",
            "111": "Image Hospital",
            "112": "Apollo Joomla",
            "113": "Variety Hospital",
            "114": "Incredible Hospital",
            "115": "OLDDONOTUSEPrompt Care",
            "116": "Digital Solutions Warehouse OldDoNotUse ",
            "117": "OLDDONOTUSEPreDiabetes Centers",
            "118": "OLDDONOTUSELifelineMD",
            "119": "Connelly OLD SETUP DO NOT USE",
            "120": "OLDDONOTUSEConnelly Dermatology",
            "121": "Connelly Dermatology",
            "122": "LifelineMD",
            "123": "PreDiabetes Centers",
            "124": "Prompt Care",
            "125": "Chubs Children Hospital",
            "126": "Emerald Connected Care",
            "127": "MD Hotline",
            "128": "Healthpointe Medical Group",
            "129": "Quantum Pharmacy Solutions Limited",
            "130": "P and P Medical Solutions",
            "131": "M3 Medical",
            "132": "Executive Medicine of Texas",
            "135": "Test Name1",
            "139": "Bad Health",
            "140": "Athena",
            "141": "Practice Max"
          },
          "sustainableTokenTypeName": "None",
          "partnerTypeName": "File Sharing"
        },
        {
          "partnerId": 28,
          "name": "MDToolbox",
          "sustainableTokenType": 2,
          "partnerType": 3,
          "assemblyName": "",
          "providers": {
            "1": "Emerald Dev"
          },
          "sustainableTokenTypeName": "Per Hospital",
          "partnerTypeName": "ePrescription"
        },
        {
          "partnerId": 25,
          "name": "PokitdokEligibility",
          "sustainableTokenType": 1,
          "partnerType": 4,
          "assemblyName": "",
          "providers": {
            "144": "Azure Hospital"
          },
          "sustainableTokenTypeName": "Per Partner",
          "partnerTypeName": "Insurance Verification"
        },
        {
          "partnerId": 1,
          "name": "PracticeMAX eCW",
          "sustainableTokenType": 2,
          "partnerType": 1,
          "assemblyName": "SnapMD.Integration.ExternalApi.PracticeMax.Ecw",
          "providers": {
            "141": "Practice Max"
          },
          "sustainableTokenTypeName": "Per Hospital",
          "partnerTypeName": "EHR"
        },
        {
          "partnerId": 23,
          "name": "Redox",
          "sustainableTokenType": 2,
          "partnerType": 1,
          "assemblyName": "SnapMD.Integration.Ehr.Redox",
          "providers": {
            "148": "Redox Test Hospital"
          },
          "sustainableTokenTypeName": "Per Hospital",
          "partnerTypeName": "EHR"
        },
        {
          "partnerId": 12,
          "name": "Rxnt",
          "sustainableTokenType": 0,
          "partnerType": 3,
          "assemblyName": "SnapMD.Integration.ePrescription.Rxnt",
          "providers": {
            "0": "SnapMD",
            "36": "SampleTestClient",
            "38": "Prompt Healthcare",
            "102": "PhD Labs",
            "104": "Digital Solutions Warehouse  OldDoNotUse",
            "105": "Prompt Healthcare old",
            "106": "Apollo Hospital",
            "107": "Care HospitalCare HospitalCare HospitalCare Hospit",
            "108": "Generic Hospital",
            "109": "Paradise Hospital",
            "110": "Maritime Hospital",
            "111": "Image Hospital",
            "112": "Apollo Joomla",
            "113": "Variety Hospital",
            "114": "Incredible Hospital",
            "115": "OLDDONOTUSEPrompt Care",
            "116": "Digital Solutions Warehouse OldDoNotUse ",
            "117": "OLDDONOTUSEPreDiabetes Centers",
            "118": "OLDDONOTUSELifelineMD",
            "119": "Connelly OLD SETUP DO NOT USE",
            "120": "OLDDONOTUSEConnelly Dermatology",
            "121": "Connelly Dermatology",
            "122": "LifelineMD",
            "123": "PreDiabetes Centers",
            "124": "Prompt Care",
            "125": "Chubs Children Hospital",
            "126": "Emerald Connected Care",
            "127": "MD Hotline",
            "128": "Healthpointe Medical Group",
            "129": "Quantum Pharmacy Solutions Limited",
            "130": "P and P Medical Solutions",
            "131": "M3 Medical",
            "132": "Executive Medicine of Texas",
            "135": "Test Name1",
            "139": "Bad Health",
            "140": "Athena",
            "141": "Practice Max",
            "142": "SnapMD",
            "143": "Folder Grid Test",
            "144": "Azure Hospital",
            "145": "DismissReason Test Hospital",
            "146": "DismissReason Test Hospital",
            "147": "DismissReason Test Hospital 2",
            "148": "Redox Test Hospital",
            "149": "SnapMD",
            "150": "SnapMD",
            "151": "SnapMD",
            "152": "SnapMD",
            "153": "SnapMD",
            "154": "SnapMD",
            "155": "SnapMD",
            "156": "SnapMD",
            "157": "Dan 113"
          },
          "sustainableTokenTypeName": "None",
          "partnerTypeName": "ePrescription"
        },
        {
          "partnerId": 24,
          "name": "RxntEligibility",
          "sustainableTokenType": 1,
          "partnerType": 4,
          "assemblyName": "",
          "providers": {},
          "sustainableTokenTypeName": "Per Partner",
          "partnerTypeName": "Insurance Verification"
        },
        {
          "partnerId": 10,
          "name": "SnapMD Api",
          "sustainableTokenType": 2,
          "partnerType": 5,
          "assemblyName": "SnapMD.Integration.InternalApi",
          "providers": {
            "140": "Athena",
            "141": "Practice Max",
            "144": "Azure Hospital",
            "148": "Redox Test Hospital"
          },
          "sustainableTokenTypeName": "Per Hospital",
          "partnerTypeName": "SnapMD Api"
        },
        {
          "partnerId": 4,
          "name": "Test",
          "sustainableTokenType": 0,
          "partnerType": 2,
          "assemblyName": "",
          "providers": {},
          "sustainableTokenTypeName": "None",
          "partnerTypeName": "File Sharing"
        },
        {
          "partnerId": 19,
          "name": "Twilio",
          "sustainableTokenType": 0,
          "partnerType": 6,
          "assemblyName": "SnapMD.Integration.SMS.Twilio",
          "providers": {
            "0": "SnapMD",
            "36": "SampleTestClient",
            "38": "Prompt Healthcare",
            "102": "PhD Labs",
            "104": "Digital Solutions Warehouse  OldDoNotUse",
            "105": "Prompt Healthcare old",
            "106": "Apollo Hospital",
            "107": "Care HospitalCare HospitalCare HospitalCare Hospit",
            "108": "Generic Hospital",
            "109": "Paradise Hospital",
            "110": "Maritime Hospital",
            "111": "Image Hospital",
            "112": "Apollo Joomla",
            "113": "Variety Hospital",
            "114": "Incredible Hospital",
            "115": "OLDDONOTUSEPrompt Care",
            "116": "Digital Solutions Warehouse OldDoNotUse ",
            "117": "OLDDONOTUSEPreDiabetes Centers",
            "118": "OLDDONOTUSELifelineMD",
            "119": "Connelly OLD SETUP DO NOT USE",
            "120": "OLDDONOTUSEConnelly Dermatology",
            "121": "Connelly Dermatology",
            "122": "LifelineMD",
            "123": "PreDiabetes Centers",
            "124": "Prompt Care",
            "125": "Chubs Children Hospital",
            "126": "Emerald Connected Care",
            "127": "MD Hotline",
            "128": "Healthpointe Medical Group",
            "129": "Quantum Pharmacy Solutions Limited",
            "130": "P and P Medical Solutions",
            "131": "M3 Medical",
            "132": "Executive Medicine of Texas",
            "135": "Test Name1",
            "139": "Bad Health",
            "141": "Practice Max",
            "142": "SnapMD",
            "143": "Folder Grid Test",
            "144": "Azure Hospital",
            "148": "Redox Test Hospital",
            "149": "SnapMD",
            "150": "SnapMD",
            "151": "SnapMD",
            "152": "SnapMD",
            "153": "SnapMD",
            "154": "SnapMD",
            "155": "SnapMD",
            "1": "Emerald Dev"
          },
          "sustainableTokenTypeName": "None",
          "partnerTypeName": "SMS Sender"
        },
        {
          "partnerId": 27,
          "name": "TwilioCopilot",
          "sustainableTokenType": 0,
          "partnerType": 6,
          "assemblyName": "SnapMD.Integration.SMS.TwilioCopilot",
          "providers": {
            "140": "Athena"
          },
          "sustainableTokenTypeName": "None",
          "partnerTypeName": "SMS Sender"
        }
      ],
      "total": 16
}
export {
    getListCodeSetss,
    getListOrganizationTypess,
    getListDocumentTypess,
    getListClientsTypess,
    getListPartners,
    getListPartnerOnlyActives
};