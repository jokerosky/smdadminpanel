import { NewPatientResponseDTO } from '../../src/models/DTO/newPatientResponseDTO';

let mockNewPatientResponseDTO = {
      address: "New York,\n NY, USA",
      dob: "1987-05-25T00:00:00Z",
      timeZoneId: 0,
      email: "v10ky~~~@Ya.ru",
      name: {
        first: "Tony",
        last: "Ya"
      },
      patientId: 23723,
      providerId: 126,
      userLoginId: 6965
    
} as NewPatientResponseDTO;

// {
//   "data": [
//     {
//       "address": "New York,\n NY, USA",
//       "dob": "1987-05-25T00:00:00Z",
//       "timeZoneId": 0,
//       "email": "v10ky~~~@Ya.ru",
//       "name": {
//         "first": "Tony",
//         "last": "Ya"
//       },
//       "patientId": 23723,
//       "providerId": 126,
//       "userLoginId": 6965
//     }
//   ],
//   "total": 1
// }

export default mockNewPatientResponseDTO;