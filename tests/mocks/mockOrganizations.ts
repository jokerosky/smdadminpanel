let organizationType = [
    {
      "description": "SnapMD",
      "id": 1,
      "statusCode": 0,
      "createdDate": "2015-11-23T00:00:00Z",
      "modifiedDate": "2015-11-23T00:00:00Z",
      "createdByUserId": 1,
      "modifiedByUserId": 1
    },
    {
      "description": "Client",
      "id": 2,
      "statusCode": 0,
      "createdDate": "2016-04-01T00:44:31.81Z",
      "modifiedDate": "2016-04-01T00:44:31.81Z",
      "createdByUserId": 15,
      "modifiedByUserId": 15
    },
    {
      "description": "Re-Seller",
      "id": 3,
      "statusCode": 0,
      "createdDate": "2016-04-01T00:44:31.81Z",
      "modifiedDate": "2016-04-01T00:44:31.81Z",
      "createdByUserId": 15,
      "modifiedByUserId": 15
    }
  ];

let organization = [
    {
        "addresses": [],
        "hospitalId": 1,
        "locations": [],
        "name": "Organization",
        "organizationTypeId": 2,
        "id": 5,
        "createdDate": "2017-11-02T08:05:59.41Z",
        "modifiedDate": "2017-11-02T08:05:59.41Z",
        "createdByUserId": 96,
        "modifiedByUserId": 96
      },
      {
        "addresses": [],
        "hospitalId": 1,
        "locations": [],
        "name": "New Organization",
        "organizationTypeId": 2,
        "id": 6,
        "createdDate": "2017-11-03T05:39:25.05Z",
        "modifiedDate": "2017-11-03T05:51:04.567Z",
        "createdByUserId": 96,
        "modifiedByUserId": 96
      },
      {
        "addresses": [],
        "hospitalId": 1,
        "locations": [],
        "name": "Civilization",
        "organizationTypeId": 2,
        "id": 15,
        "createdDate": "2017-11-03T06:14:17.797Z",
        "modifiedDate": "2017-11-03T06:14:17.797Z",
        "createdByUserId": 96,
        "modifiedByUserId": 96
      }
]

export{organizationType, organization};