let integrationLogsMock = [
    {
        IntegrationLogId: 1,
        LogType: 2,
        DateCreated: '2016-12-07T00:20:00Z',
        PatientId: 1,
        HospitalStaffProfileId: 1,
        PartnerId: 1,
        LogMessage: 'message1',
        HospitalId: 1,
        CommandName: 'command1'
    },
    {
        IntegrationLogId: 2,
        LogType: 2,
        DateCreated: '2016-12-07T00:20:00Z',
        PatientId: 2,
        HospitalStaffProfileId: 2,
        PartnerId: 2,
        LogMessage: 'message2',
        HospitalId: 36,
        CommandName: 'command2'
    },
    {
        IntegrationLogId: 3,
        LogType: 3,
        DateCreated: '2016-12-07T00:20:00Z',
        PatientId: 3,
        HospitalStaffProfileId: 3,
        PartnerId: 3,
        LogMessage: 'message3',
        HospitalId: 38,
        CommandName: 'command3'
    }
];

export { integrationLogsMock };