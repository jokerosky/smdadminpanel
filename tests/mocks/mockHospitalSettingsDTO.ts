let hospitalSettings = [
  {
    "id": 4575,
    "hospitalId": 1,
    "key": "adminConsultEndUrl",
    "value": ""
  },
  {
    "id": 4576,
    "hospitalId": 1,
    "key": "adminSSO",
    "value": ""
  },
  {
    "id": 4577,
    "hospitalId": 1,
    "key": "adminSSOButtonText",
    "value": ""
  },
  {
    "id": 4585,
    "hospitalId": 1,
    "key": "androidPlayStoreUrl",
    "value": ""
  },
  {
    "id": 4583,
    "hospitalId": 1,
    "key": "androidSchemaUrl",
    "value": ""
  },
  {
    "id": 2017,
    "hospitalId": 1,
    "key": "AuthorizeNet_BaseUrl",
    "value": "https://apitest.authorize.net"
  },
  {
    "id": 23,
    "hospitalId": 1,
    "key": "AuthorizeNet_LoginID",
    "value": "867hTNWkm8"
  },
  {
    "id": 22,
    "hospitalId": 1,
    "key": "AuthorizeNet_PostURL",
    "value": "https://test.authorize.net/gateway/transact.dll"
  },
  {
    "id": 24,
    "hospitalId": 1,
    "key": "AuthorizeNet_TransactionKey",
    "value": "7824m2DxJw3gGx3k"
  },
  {
    "id": 1900,
    "hospitalId": 1,
    "key": "BrandBackgroundColor",
    "value": "#fff"
  },
  {
    "id": 1899,
    "hospitalId": 1,
    "key": "BrandBackgroundImage",
    "value": ""
  },
  {
    "id": 2411,
    "hospitalId": 1,
    "key": "BrandBackgroundLoginImage",
    "value": "https://snap.local/api/v2.1/images/8cd358d8-60ec-44a5-8d8a-8fa7c0a1ed02"
  },
  {
    "id": 1898,
    "hospitalId": 1,
    "key": "BrandTextColor",
    "value": "#fff"
  },
  {
    "id": 4581,
    "hospitalId": 1,
    "key": "CbMinClinicansGood",
    "value": "4"
  },
  {
    "id": 3303,
    "hospitalId": 1,
    "key": "Chargify_Subdomain",
    "value": "snapdev"
  },
  {
    "id": 3304,
    "hospitalId": 1,
    "key": "Chargify_V1Key",
    "value": "GZlGPDEACkKjTR5FzgD9gjvq6y2SGUIDrA100qrs"
  },
  {
    "id": 3305,
    "hospitalId": 1,
    "key": "Chargify_V2ApiId",
    "value": "22da1ee0-2ce6-0135-53c5-0aa88d71309c"
  },
  {
    "id": 3307,
    "hospitalId": 1,
    "key": "Chargify_V2Password",
    "value": "saZ1cXixdNdIuLnp9PorV8XkekBGvl3StqH3UDE3W0"
  },
  {
    "id": 3306,
    "hospitalId": 1,
    "key": "Chargify_V2SharedSecret",
    "value": "bXzF5r81d5Df0UhjHigAbvPvbkJw76069TkyEzzMqI"
  },
  {
    "id": 4572,
    "hospitalId": 1,
    "key": "clinicianSSO",
    "value": ""
  },
  {
    "id": 4573,
    "hospitalId": 1,
    "key": "clinicianSSOButtonText",
    "value": ""
  },
  {
    "id": 1217,
    "hospitalId": 1,
    "key": "customerSSO",
    "value": "Optional"
  },
  {
    "id": 1218,
    "hospitalId": 1,
    "key": "customerSSOButtonText",
    "value": "Go to Test Login"
  },
  {
    "id": 1254,
    "hospitalId": 1,
    "key": "DefaultAvailabilityBlockDuration",
    "value": "240"
  },
  {
    "id": 4584,
    "hospitalId": 1,
    "key": "iOSAppStoreUrl",
    "value": ""
  },
  {
    "id": 4582,
    "hospitalId": 1,
    "key": "iOSSchemaUrl",
    "value": ""
  },
  {
    "id": 1219,
    "hospitalId": 1,
    "key": "jwtIssuer",
    "value": ""
  },
  {
    "id": 4328,
    "hospitalId": 1,
    "key": "MDToolbox_AccountId",
    "value": "5204"
  },
  {
    "id": 4330,
    "hospitalId": 1,
    "key": "MDToolbox_AuthenticationKey",
    "value": "N6YH-WZA4-V92M-A43E-M9LB"
  },
  {
    "id": 4329,
    "hospitalId": 1,
    "key": "MDToolbox_PracticeId",
    "value": "3460"
  },
  {
    "id": 4326,
    "hospitalId": 1,
    "key": "MDToolbox_ServiceUrl",
    "value": "https://test.mdtoolboxrx.net/rxws/rx.asmx"
  },
  {
    "id": 4327,
    "hospitalId": 1,
    "key": "MDToolbox_SsoUrl",
    "value": "https://test.mdtoolboxrx.net/rxtest/access1.aspx"
  },
  {
    "id": 1214,
    "hospitalId": 1,
    "key": "oAuthCallback",
    "value": "google.com"
  },
  {
    "id": 1211,
    "hospitalId": 1,
    "key": "oAuthClientId",
    "value": ""
  },
  {
    "id": 1212,
    "hospitalId": 1,
    "key": "oAuthClientSecret",
    "value": ""
  },
  {
    "id": 1213,
    "hospitalId": 1,
    "key": "oAuthUrl",
    "value": "google.com"
  },
  {
    "id": 4580,
    "hospitalId": 1,
    "key": "patientForgotPasswordApi",
    "value": ""
  },
  {
    "id": 508,
    "hospitalId": 1,
    "key": "PatientProfile.IsBloodTypeRequired",
    "value": "false"
  },
  {
    "id": 507,
    "hospitalId": 1,
    "key": "PatientProfile.IsEthnicityRequired",
    "value": "true"
  },
  {
    "id": 506,
    "hospitalId": 1,
    "key": "PatientProfile.IsEyeColorRequired",
    "value": "true"
  },
  {
    "id": 505,
    "hospitalId": 1,
    "key": "PatientProfile.IsHairColorRequired",
    "value": "true"
  },
  {
    "id": 4579,
    "hospitalId": 1,
    "key": "patientRegistrationApi",
    "value": ""
  },
  {
    "id": 4578,
    "hospitalId": 1,
    "key": "patientTokenApi",
    "value": ""
  },
  {
    "id": 413,
    "hospitalId": 1,
    "key": "RxNTEPrescriptionSingleSignonUrl",
    "value": "https://stage.rxnt.com/dr/oneTimeUseLogin.asp?"
  },
  {
    "id": 4574,
    "hospitalId": 1,
    "key": "ssoAdminExternalLoginUrl",
    "value": ""
  },
  {
    "id": 4570,
    "hospitalId": 1,
    "key": "ssoClinicianExternalLoginUrl",
    "value": ""
  },
  {
    "id": 4571,
    "hospitalId": 1,
    "key": "ssoClinicianReturnUrl",
    "value": ""
  },
  {
    "id": 1215,
    "hospitalId": 1,
    "key": "ssoExternalLoginUrl",
    "value": "google.com"
  },
  {
    "id": 1216,
    "hospitalId": 1,
    "key": "ssoReturnUrl",
    "value": "yahoo.com"
  }
];

let hospitalModulesSettings =
  [
    {
      "id": 2613,
      "hospitalId": 36,
      "key": "GenericMobileAppEnabled",
      "value": "False"
    },
    {
      "id": 4503,
      "hospitalId": 36,
      "key": "mAddressValidation",
      "value": "False"
    },
    {
      "id": 3235,
      "hospitalId": 36,
      "key": "mAnnotation",
      "value": "False"
    },
    {
      "id": 682,
      "hospitalId": 36,
      "key": "mClinicianSearch",
      "value": "False"
    },
    {
      "id": 4508,
      "hospitalId": 36,
      "key": "mConsultationVideoArchive",
      "value": "False"
    },
    {
      "id": 4507,
      "hospitalId": 36,
      "key": "mDisablePhoneConsultation",
      "value": "False"
    },
    {
      "id": 670,
      "hospitalId": 36,
      "key": "mECommerce",
      "value": "False"
    },
    {
      "id": 674,
      "hospitalId": 36,
      "key": "mEPerscriptions",
      "value": "False"
    },
    {
      "id": 675,
      "hospitalId": 36,
      "key": "mEPSchedule1",
      "value": "False"
    },
    {
      "id": 689,
      "hospitalId": 36,
      "key": "mFileSharing",
      "value": "False"
    },
    {
      "id": 4505,
      "hospitalId": 36,
      "key": "mHideDrToDrChat",
      "value": "False"
    },
    {
      "id": 4504,
      "hospitalId": 36,
      "key": "mHideOpenConsultation",
      "value": "False"
    },
    {
      "id": 4502,
      "hospitalId": 36,
      "key": "mHidePaymentBeforeWaiting",
      "value": "False"
    },
    {
      "id": 678,
      "hospitalId": 36,
      "key": "mIFOnDemand",
      "value": "False"
    },
    {
      "id": 679,
      "hospitalId": 36,
      "key": "mIFScheduled",
      "value": "False"
    },
    {
      "id": 671,
      "hospitalId": 36,
      "key": "mInsVerification",
      "value": "False"
    },
    {
      "id": 4499,
      "hospitalId": 36,
      "key": "mInsVerificationBeforeWaiting",
      "value": "False"
    },
    {
      "id": 672,
      "hospitalId": 36,
      "key": "mInsVerificationDummy",
      "value": "False"
    },
    {
      "id": 677,
      "hospitalId": 36,
      "key": "mIntakeForm",
      "value": "False"
    },
    {
      "id": 4506,
      "hospitalId": 36,
      "key": "mKubi",
      "value": "False"
    },
    {
      "id": 4501,
      "hospitalId": 36,
      "key": "mMedicalCodes",
      "value": "False"
    },
    {
      "id": 676,
      "hospitalId": 36,
      "key": "mOnDemand",
      "value": "False"
    },
    {
      "id": 4500,
      "hospitalId": 36,
      "key": "mOnDemandHourly",
      "value": "True"
    },
    {
      "id": 690,
      "hospitalId": 36,
      "key": "mOrganizationLocation",
      "value": "False"
    },
    {
      "id": 684,
      "hospitalId": 36,
      "key": "mRxNTEHR",
      "value": "False"
    },
    {
      "id": 685,
      "hospitalId": 36,
      "key": "mRxNTPM",
      "value": "False"
    },
    {
      "id": 688,
      "hospitalId": 36,
      "key": "mShowCTTOnScheduled",
      "value": "False"
    },
    {
      "id": 683,
      "hospitalId": 36,
      "key": "mTextMessaging",
      "value": "False"
    },
    {
      "id": 681,
      "hospitalId": 36,
      "key": "mVideoBeta",
      "value": "False"
    }
  ];
export { hospitalSettings, hospitalModulesSettings };