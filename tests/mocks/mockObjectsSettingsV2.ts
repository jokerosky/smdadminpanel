let mockClientData = {
    appointmentsContactNumber: "5555551212",
    brandColor: "#c3bd4a",
    brandName: "Emerald SQL Azure 1",
    brandTitle: "Virtual Telemedicine",
    children: 0,
    cliniciansCount: "",
    consultationCharge: 120,
    contactNumber: "1234567891",
    email: "",
    ePrescriptionGateWay: "",
    hospitalCode: "Emerald",
    hospitalDomainName: "hospital1.dev2.snapvcm.com",
    hospitalId: 1,
    hospitalImage: "https://snap.local/api/v2.1/images/af42ce39-9900-46b1-a0ae-d0ef3b0020d4",
    hospitalName: "Emerald Azure SQL 2",
    hospitalType: 78,
    insuranceValidationGateWay: "",
    isActive: "A",
    iTDeptContactNumber: undefined,
    nPINumber: "",
    paymentGateWay: "",
    smsGateway: "",
    totalUsersCount: ""
};

let mockSettings = {
    GenericMobileAppEnabled: "False",
    mAddressValidation: "False",
    mAdminMeetingReport: "False",
    mAnnotation: "True",
    mClinicianSearch: "False",
    mConsultationVideoArchive: "False",
    mDisablePhoneConsultation: "",
    mECommerce: "True",
    mEncounterGeoLocation: "False",
    mEPerscriptions: "True",
    mEPSchedule1: "False",
    mFileSharing: "True",
    mHideDrToDrChat: "False",
    mHideForgotPasswordLink: "False",
    mHideOpenConsultation: "False",
    mHidePaymentBeforeWaiting: "False",
    mIFOnDemand: "True",
    mIFScheduled: "True",
    mIncludeDirections: "False",
    mInsVerification: "True",
    mInsVerificationBeforeWaiting: "False",
    mInsVerificationDummy: "True",
    mIntakeForm: "False",
    mKubi: "False",
    mMedicalCodes: "True",
    mOnDemand: "True",
    mOnDemandHourly: "False",
    mOrganizationLocation: "True",
    mRxNTEHR: "True",
    mRxNTPM: "True",
    mShowCTTOnScheduled: "True",
    mTextMessaging: "True",
    mVideoBeta: "False",
    mShowAppointmentWizard: "False",
    mShowNowButton: "False"
};

let mockAdditionalSettings = {
    BrandBackgroundColor: "#fff",
    BrandBackgroundImage: "https://snap.local/api/v2.1/images/460d2c81-9280-4a82-81e6-53237b316c14",
    BrandBackgroundLoginImage: "https://snap.local/api/v2.1/images/34061455-4272-44aa-9927-44bf9da6427a",
    BrandTextColor: "#fff",
    ContactUsImage: "https://snap.local/api/v2.1/images/e1216500-8974-4e9f-ba4d-a5bfdfa0bf7b",
    language: "en"
};

let mockHospitalAddress = {
    addressObject: {
        addressText:"",
        city: "",
        country: "",
        countryCode: "",
        line1: "",
        line2: "",
        postalCode: "",
        state: "",
        stateCode: ""
    },
    hospitalId: 1
};

let mockClientInLists = {
    children: 0,
    hospitalId: 1,
    hospitalName: "Emerald Azure SQL 2",
    hospitalType: 78,
    hospitalTypeName: "Pediatric",
    isActive: "A",
    seats: ""
};

let saveAdditionalSettingsSSO = {
    androidPlayStoreUrl: "",
    androidSchemaUrl: "",
    iOSAppStoreUrl: "",
    iOSSchemaUrl: ""
}

let saveAdditionalSettingsSSO2 = {
    AuthorizeNet_BaseUrl: "https://apitest.authorize.net",
    AuthorizeNet_LoginID: "4Q2ymT2H",
    AuthorizeNet_PostURL: "https://test.authorize.net/gateway/transact.dll",
    AuthorizeNet_TransactionKey: "7pG2aVK68b7d22Wj"
}

export {
    mockClientData,
    mockSettings,
    mockAdditionalSettings,
    mockHospitalAddress,
    mockClientInLists,
    saveAdditionalSettingsSSO,
    saveAdditionalSettingsSSO2
};