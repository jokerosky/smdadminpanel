let codeSets = [
    {
        "CodeId":15995,
        "Description":"New User Registration Token",
        "CodeSetId":1,
        "IsActive":true,
        "HospitalId":1,
        "PartnerCode":"",
        "DisplayOrder":1
    },
    {
        "CodeId":15996,
        "Description":"New User muser",
        "CodeSetId":1,
        "IsActive":true,
        "HospitalId":1,
        "PartnerCode":"",
        "DisplayOrder":2
    },
    {
        "CodeId":15997,
        "Description":"New Test User",
        "CodeSetId":1,
        "IsActive":true,
        "HospitalId":1,
        "PartnerCode":"",
        "DisplayOrder":3
    }
]

export {codeSets};