import {ValidationSet, ValidationError , ValidationResult} from '../../src/utilities/validators';

let emptyChecker =  (value) => { return !!value ? "" : "the value is empty!"; };

let mockValidationSet =  {
    elementId : 'email',
    element: {value:''},
    rules: [ emptyChecker ]
} as ValidationSet;

let mockValidationSetArray = [
    mockValidationSet,
    {
        elementId: 'password',
        element: {value:'123'},
        rules: [ 
            emptyChecker,
            (value) => { return value.length > 4 ? "" : "should be more than 4 symbols!"; }
        ]
    } as ValidationSet,
    {
        elementId:"length",
        element:{value:'223'},
        rules: [
            emptyChecker,
            ( value ) => { return value < 1999 && value > 1001 ? "" : "value should be inside bounds 1001..1999!"}
        ]
    } as ValidationSet,
] as ValidationSet[];

let mockErrors = [
      {
        elementId:'email',
        errorClass:'err',
        messages:['Empty email', 'Value is not Email', 'Should be nice and shiny']
      } 
    ] as ValidationError[] ;


export  { mockValidationSet };
export  { mockValidationSetArray };
export  { mockErrors }


