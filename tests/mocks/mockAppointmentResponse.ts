let mockAppointmentResponse = {
  "data": [
    {
      "appointmentId": "10126b61-b23f-40a7-a562-104b6e8f261f",
      "patientId": 1900,
      "clinicianId": 1937,
      "appointmentStatusCode": "Waiting",
      "participants": [
        {
          "participantId": "9750b7ee-55f4-471c-bb07-554ab864d39a",
          "person": {
            "id": "9ff4701b-a814-497d-b203-84a2b4c89cbe",
            "name": {
              "given": "Tony",
              "prefix": "",
              "suffix": "",
              "family": "Yakovets"
            },
            "photoUrl": "https://snap.local/api/v2.1/images/c8d29201-aa65-4cee-8bf1-75058d167b5a",
            "providerId": 1,
            "statusCode": 1,
            "contactTypeCode": 1,
            "phones": [
              {
                "id": "c2734c9a-8138-4ebe-a60b-591af2275da5",
                "use": "mobile",
                "value": "+79081040656"
              }
            ]
          },
          "status": 1,
          "attendenceCode": 1,
          "personId": "9ff4701b-a814-497d-b203-84a2b4c89cbe",
          "participantTypeCode": 1
        },
        {
          "participantId": "263be0de-9425-4ab3-a350-9735c782b4c4",
          "person": {
            "id": "524aaf2f-f48d-4632-b311-63a7529d3117",
            "name": {
              "given": "Tony",
              "prefix": "",
              "suffix": "",
              "family": "Yakovets"
            },
            "photoUrl": "https://snap.local/api/v2.1/images/15a76a75-04cc-4239-88fa-f961fdabe726",
            "providerId": 1,
            "statusCode": 1,
            "contactTypeCode": 2,
            "phones": [
              {
                "id": "10cb3057-1a56-490b-816f-4abed7f3c206",
                "use": "mobile",
                "value": "+79659846524"
              },
              {
                "id": "7391afbf-94de-457d-8666-548ce4fae218",
                "use": "home",
                "value": "+79081040656"
              }
            ]
          },
          "status": 1,
          "attendenceCode": 1,
          "personId": "524aaf2f-f48d-4632-b311-63a7529d3117",
          "participantTypeCode": 2
        }
      ],
      "consultationId": 6250,
      "dismissed": false,
      "serviceTypeName": "Self-Scheduling",
      "availabilityBlockId": "68f291d6-1cc5-498f-8f0d-e500faaec36f",
      "waiveFee": false,
      "startTime": "2017-06-27T15:45:00+07:00",
      "endTime": "2017-06-27T16:00:00+07:00",
      "patientQueueId": "798e5947-e220-41d1-ae10-c1d469d36f3e",
      "appointmentTypeCode": 3,
      "intakeMetadata": {
        "concerns": [
          {
            "customCode": {
              "code": 82,
              "description": "Cough"
            },
            "isPrimary": true
          }
        ],
        "additionalNotes": ""
      },
      "encounterTypeCode": 3,
      "where": "",
      "whereUse": "3",
      "serviceTypeId": 9
    }
  ],
  "total": 1
};

export { mockAppointmentResponse };