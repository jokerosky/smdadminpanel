import { IMappedSetting  } from '../../src/models/site/settingsMapping';
import { EditorType } from '../../src/enums/editorTypes';


let mappedKeys = [
    { key: 'mECommerce', editorType: EditorType.toggle },
    { key: 'mInsVerification', 
        action: (value: any, item: IMappedSetting, section:IMappedSetting[])=>{
        },
        children: [ 
            { key: "mInsVerificationDummy", editorType: EditorType.toggle },
            { key: "mInsVerificationBeforeWaiting", editorType: EditorType.toggle }
        ]
    },

] as IMappedSetting[];

export { mappedKeys };