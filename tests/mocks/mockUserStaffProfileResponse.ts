let mockStaffUserProfile = {
      "medicalLicense": "",
      "medicalSchool": "",
      "medicalSpeciality": "FullStack",
      "roles": "Doctor, Heart Surgeon, Hospital Admin #1, Master Admin, My Role, Naveen, New Role, Test Empty",
      "statesLicenced": "[{\"countryCode\":\"RU\"},{\"countryCode\":\"US\",\"regions\":[{\"regionCode\":\"AL\"},{\"regionCode\":\"CA\"}]},{\"countryCode\":\"RS\"},{\"countryCode\":\"CY\"},{\"countryCode\":\"DE\"},{\"countryCode\":\"AU\"},{\"countryCode\":\"AT\"},{\"countryCode\":\"PL\"},{\"countryCode\":\"IL\"},{\"countryCode\":\"BE\"},{\"countryCode\":\"UM\"}]",
      "subSpeciality": "",
      "email": "jokerosky@gmail.com",
      "firstName": "Tony",
      "fullName": "Tony Yakovets",
      "gender": "M",
      "lastName": "Yakovets",
      "profileId": 172,
      "profileImage": "https://snap.local/api/v2.1/images/15a76a75-04cc-4239-88fa-f961fdabe726",
      "profileImagePath": "https://snap.local/api/v2.1/images/15a76a75-04cc-4239-88fa-f961fdabe726",
      "timeZone": "(UTC+07:00) Новосибирск",
      "timeZoneId": 35,
      "timeZoneSystemId": "N. Central Asia Standard Time",
      "userId": 1937,
      "hasRequiredFields": false,
      "personId": "524aaf2f-f48d-4632-b311-63a7529d3117",
      "isDependent": false
    }

let mockStaffUserProfileResponse = 
{
  "data": [
    { ...mockStaffUserProfile }
  ],
  "total": 1
}

export { mockStaffUserProfile, mockStaffUserProfileResponse };