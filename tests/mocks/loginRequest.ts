 import * as userTypes from '../../src/enums/userTypes';
 import LoginRequestDTO from '../../src/models/DTO/loginRequestDTO';
 
 var mockLoginRequest = {
            email: 'test@test.com',
            hospitalId: 1,
            password: 'password',
            userTypeId: userTypes.patient,
            redirectUrl: ''
        } as LoginRequestDTO;

export default mockLoginRequest;
