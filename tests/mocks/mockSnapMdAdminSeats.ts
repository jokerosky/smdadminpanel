let seats = {
    "data": [
        {
          "snUnRestricted": 0,
          "snRestricted": 0,
          "snEnterprise": 2000,
          "snAdmin": 2000,
          "snClSubTotalSeats": 2000,
          "snTotalSeats": 4000,
          "modEHRERx": 0,
          "modAdminEHRERx": 0,
          "modAdminPMFull": 0,
          "modAdminPMPart": 0,
          "modClSubTotalSeats": 0,
          "modTotalSeats": 0
        }
      ],
      "total": 1
}

let seatsAssigned ={
    "data": [
      {
        "snUnRestricted": 0,
        "snRestricted": 0,
        "snEnterprise": 52,
        "snAdmin": 0,
        "snClSubTotalSeats": 52,
        "snTotalSeats": 52,
        "modEHRERx": 0,
        "modAdminEHRERx": 0,
        "modAdminPMFull": 0,
        "modAdminPMPart": 0,
        "modClSubTotalSeats": 0,
        "modTotalSeats": 0
      }
    ],
    "total": 1
  }

  let operatingHours = [
      {
        "startDayOfWeek": 1,
        "endDayOfWeek": 1,
        "operatingHoursId": 132,
        "startTime": "14:30",
        "endTime": "14:13",
        "hospitalId": 0
      },
      {
        "startDayOfWeek": 1,
        "endDayOfWeek": 1,
        "operatingHoursId": 133,
        "startTime": "14:30",
        "endTime": "15:20",
        "hospitalId": 0
      },
      {
        "startDayOfWeek": 3,
        "endDayOfWeek": 3,
        "operatingHoursId": 150,
        "startTime": "14:14",
        "endTime": "15:55",
        "hospitalId": 0
      }
    ]

export{seats, seatsAssigned, operatingHours};