import { ClientSummaryDTO } from '../../src/models/DTO/clientSummaryDTO';

var mockClientSummaryDTOs: ClientSummaryDTO[] =
    [{
        "hospitalId": 0,
        "hospitalType": 77,
        "hospitalTypeName": "General",
        "hospitalName": "SnapMD",
        "children": 0,
        "isActive": "A",
        "seats": 1
    },
    {
        "hospitalId": 1,
        "hospitalType": 77,
        "hospitalTypeName": "General",
        "hospitalName": "Emerald Dev",
        "children": 0,
        "isActive": "A",
        "seats": 4000
    },
    {
        "hospitalId": 36,
        "hospitalType": 77,
        "hospitalTypeName": "General",
        "hospitalName": "SampleTestClient",
        "children": 0,
        "isActive": "A",
        "seats": 20
    },
    {
        "hospitalId": 38,
        "hospitalType": 77,
        "hospitalTypeName": "General",
        "hospitalName": "Prompt Healthcare",
        "children": 0,
        "isActive": "A",
        "seats": 5
    },
    {
        "hospitalId": 145,
        "hospitalType": 77,
        "hospitalTypeName": "General",
        "hospitalName": "DismissReason Test Hospital",
        "children": 0,
        "isActive": "N",
        "seats": 0
    },
    {
        "hospitalId": 146,
        "hospitalType": 77,
        "hospitalTypeName": "General",
        "hospitalName": "DismissReason Test Hospital",
        "children": 0,
        "isActive": "N",
        "seats": 0
    },
    {
        "hospitalId": 147,
        "hospitalType": 77,
        "hospitalTypeName": "General",
        "hospitalName": "DismissReason Test Hospital 2",
        "children": 0,
        "isActive": "N",
        "seats": 0
    },
    {
        "hospitalId": 102,
        "hospitalType": 375,
        "hospitalTypeName": "General",
        "hospitalName": "PhD Labs",
        "children": 0,
        "isActive": "A",
        "seats": 10
    }];
    
var mockClientSummaryDTOsAll =
{
  data:[{
      "hospitalId": 0,
      "hospitalType": 77,
      "hospitalTypeName": "General",
      "hospitalName": "SnapMD",
      "children": 0,
      "isActive": "A",
      "seats": 1
    },
    {
      "hospitalId": 1,
      "hospitalType": 131,
      "hospitalTypeName": "University Hospital",
      "hospitalName": "Emerald Dev",
      "children": 0,
      "isActive": "A",
      "seats": 4000
    },
    {
      "hospitalId": 36,
      "hospitalType": 77,
      "hospitalTypeName": "General",
      "hospitalName": "SampleTestClient",
      "children": 0,
      "isActive": "A",
      "seats": 20
    },
    {
      "hospitalId": 38,
      "hospitalType": 77,
      "hospitalTypeName": "General",
      "hospitalName": "Prompt Healthcare",
      "children": 0,
      "isActive": "A",
      "seats": 5
    },
    {
      "hospitalId": 102,
      "hospitalType": 375,
      "hospitalTypeName": "General",
      "hospitalName": "PhD Labs",
      "children": 0,
      "isActive": "A",
      "seats": 10
    },
    {
      "hospitalId": 104,
      "hospitalType": 77,
      "hospitalTypeName": "General",
      "hospitalName": "Digital Solutions Warehouse  OldDoNotUse",
      "children": 0,
      "isActive": "A",
      "seats": 1
    },
    {
      "hospitalId": 105,
      "hospitalType": 822,
      "hospitalTypeName": "Urgent Care (UC)",
      "hospitalName": "Prompt Healthcare old",
      "children": 0,
      "isActive": "A",
      "seats": 20
    },
    {
      "hospitalId": 106,
      "hospitalType": 766,
      "hospitalTypeName": "General",
      "hospitalName": "Apollo Hospital",
      "children": 0,
      "isActive": "A",
      "seats": 10
    },
    {
      "hospitalId": 107,
      "hospitalType": 821,
      "hospitalTypeName": "Accountable Care Organizations (ACO)",
      "hospitalName": "Care HospitalCare HospitalCare HospitalCare Hospit",
      "children": 0,
      "isActive": "A",
      "seats": 10
    },
    {
      "hospitalId": 108,
      "hospitalType": 826,
      "hospitalTypeName": "Maritime (MT)",
      "hospitalName": "Generic Hospital",
      "children": 0,
      "isActive": "A",
      "seats": 7
    },
    {
      "hospitalId": 109,
      "hospitalType": 767,
      "hospitalTypeName": "Pediatric",
      "hospitalName": "Paradise Hospital",
      "children": 0,
      "isActive": "A",
      "seats": 6
    },
    {
      "hospitalId": 110,
      "hospitalType": 820,
      "hospitalTypeName": "University Hospital",
      "hospitalName": "Maritime Hospital",
      "children": 0,
      "isActive": "A",
      "seats": 8
    },
    {
      "hospitalId": 111,
      "hospitalType": 824,
      "hospitalTypeName": "Allied Health Companies (AHC)",
      "hospitalName": "Image Hospital",
      "children": 0,
      "isActive": "A",
      "seats": 5
    },
    {
      "hospitalId": 112,
      "hospitalType": 822,
      "hospitalTypeName": "Urgent Care (UC)",
      "hospitalName": "Apollo Joomla",
      "children": 0,
      "isActive": "A",
      "seats": 4
    },
    {
      "hospitalId": 113,
      "hospitalType": 820,
      "hospitalTypeName": "University Hospital",
      "hospitalName": "Variety Hospital",
      "children": 0,
      "isActive": "A",
      "seats": 9
    },
    {
      "hospitalId": 114,
      "hospitalType": 822,
      "hospitalTypeName": "Urgent Care (UC)",
      "hospitalName": "Incredible Hospital",
      "children": 0,
      "isActive": "A",
      "seats": 5
    },
    {
      "hospitalId": 115,
      "hospitalType": 77,
      "hospitalTypeName": "General",
      "hospitalName": "OLDDONOTUSEPrompt Care",
      "children": 0,
      "isActive": "A",
      "seats": 4
    },
    {
      "hospitalId": 116,
      "hospitalType": 77,
      "hospitalTypeName": "General",
      "hospitalName": "Digital Solutions Warehouse OldDoNotUse ",
      "children": 0,
      "isActive": "A",
      "seats": 1
    },
    {
      "hospitalId": 117,
      "hospitalType": 77,
      "hospitalTypeName": "General",
      "hospitalName": "OLDDONOTUSEPreDiabetes Centers",
      "children": 0,
      "isActive": "A",
      "seats": 6
    },
    {
      "hospitalId": 118,
      "hospitalType": 77,
      "hospitalTypeName": "General",
      "hospitalName": "OLDDONOTUSELifelineMD",
      "children": 0,
      "isActive": "A",
      "seats": 7
    },
    {
      "hospitalId": 119,
      "hospitalType": 77,
      "hospitalTypeName": "General",
      "hospitalName": "Connelly OLD SETUP DO NOT USE",
      "children": 0,
      "isActive": "A",
      "seats": 1
    },
    {
      "hospitalId": 120,
      "hospitalType": 77,
      "hospitalTypeName": "General",
      "hospitalName": "OLDDONOTUSEConnelly Dermatology",
      "children": 0,
      "isActive": "A",
      "seats": 3
    },
    {
      "hospitalId": 121,
      "hospitalType": 77,
      "hospitalTypeName": "General",
      "hospitalName": "Connelly Dermatology",
      "children": 0,
      "isActive": "A",
      "seats": 11
    },
    {
      "hospitalId": 122,
      "hospitalType": 77,
      "hospitalTypeName": "General",
      "hospitalName": "LifelineMD",
      "children": 0,
      "isActive": "A",
      "seats": 7
    },
    {
      "hospitalId": 123,
      "hospitalType": 77,
      "hospitalTypeName": "General",
      "hospitalName": "PreDiabetes Centers",
      "children": 0,
      "isActive": "A",
      "seats": 16
    },
    {
      "hospitalId": 124,
      "hospitalType": 77,
      "hospitalTypeName": "General",
      "hospitalName": "Prompt Care",
      "children": 0,
      "isActive": "A",
      "seats": 4
    },
    {
      "hospitalId": 125,
      "hospitalType": 77,
      "hospitalTypeName": "General",
      "hospitalName": "Chubs Children Hospital",
      "children": 0,
      "isActive": "A",
      "seats": 6
    },
    {
      "hospitalId": 126,
      "hospitalType": 77,
      "hospitalTypeName": "General",
      "hospitalName": "Emerald Connected Care",
      "children": 0,
      "isActive": "A",
      "seats": 19
    },
    {
      "hospitalId": 127,
      "hospitalType": 77,
      "hospitalTypeName": "General",
      "hospitalName": "MD Hotline",
      "children": 0,
      "isActive": "A",
      "seats": 25
    },
    {
      "hospitalId": 128,
      "hospitalType": 77,
      "hospitalTypeName": "General",
      "hospitalName": "Healthpointe Medical Group",
      "children": 0,
      "isActive": "A",
      "seats": 11
    },
    {
      "hospitalId": 129,
      "hospitalType": 77,
      "hospitalTypeName": "General",
      "hospitalName": "Quantum Pharmacy Solutions Limited",
      "children": 0,
      "isActive": "A",
      "seats": 5
    },
    {
      "hospitalId": 130,
      "hospitalType": 77,
      "hospitalTypeName": "General",
      "hospitalName": "P and P Medical Solutions",
      "children": 0,
      "isActive": "A",
      "seats": 5
    },
    {
      "hospitalId": 131,
      "hospitalType": 77,
      "hospitalTypeName": "General",
      "hospitalName": "M3 Medical",
      "children": 0,
      "isActive": "A",
      "seats": 6
    },
    {
      "hospitalId": 132,
      "hospitalType": 77,
      "hospitalTypeName": "General",
      "hospitalName": "Executive Medicine of Texas",
      "children": 0,
      "isActive": "A",
      "seats": 7
    },
    {
      "hospitalId": 135,
      "hospitalType": 77,
      "hospitalTypeName": "General",
      "hospitalName": "Test Name1",
      "children": 0,
      "isActive": "A",
      "seats": 0
    },
    {
      "hospitalId": 136,
      "hospitalType": 77,
      "hospitalTypeName": "General",
      "hospitalName": "Bad Hospital136 Azure",
      "children": 0,
      "isActive": "I",
      "seats": 19
    },
    {
      "hospitalId": 137,
      "hospitalType": 77,
      "hospitalTypeName": "General",
      "hospitalName": "Bad Hospital137 Azure",
      "children": 0,
      "isActive": "I",
      "seats": 19
    },
    {
      "hospitalId": 138,
      "hospitalType": 77,
      "hospitalTypeName": "General",
      "hospitalName": "Bad Hospital138 Azure",
      "children": 0,
      "isActive": "I",
      "seats": 19
    },
    {
      "hospitalId": 139,
      "hospitalType": 77,
      "hospitalTypeName": "General",
      "hospitalName": "Bad Health",
      "children": 0,
      "isActive": "A",
      "seats": 20
    },
    {
      "hospitalId": 140,
      "hospitalType": 77,
      "hospitalTypeName": "General",
      "hospitalName": "Athena",
      "children": 0,
      "isActive": "A",
      "seats": 0
    },
    {
      "hospitalId": 141,
      "hospitalType": 77,
      "hospitalTypeName": "General",
      "hospitalName": "Practice Max",
      "children": 0,
      "isActive": "A",
      "seats": 20
    },
    {
      "hospitalId": 142,
      "hospitalType": 77,
      "hospitalTypeName": "General",
      "hospitalName": "SnapMD",
      "children": 0,
      "isActive": "A",
      "seats": 1
    },
    {
      "hospitalId": 143,
      "hospitalType": 77,
      "hospitalTypeName": "General",
      "hospitalName": "Folder Grid Test",
      "children": 0,
      "isActive": "A",
      "seats": 40
    },
    {
      "hospitalId": 144,
      "hospitalType": 77,
      "hospitalTypeName": "General",
      "hospitalName": "Azure Hospital",
      "children": 0,
      "isActive": "A",
      "seats": 5
    },
    {
      "hospitalId": 145,
      "hospitalType": 77,
      "hospitalTypeName": "General",
      "hospitalName": "DismissReason Test Hospital",
      "children": 0,
      "isActive": "N",
      "seats": 0
    },
    {
      "hospitalId": 146,
      "hospitalType": 77,
      "hospitalTypeName": "General",
      "hospitalName": "DismissReason Test Hospital",
      "children": 0,
      "isActive": "N",
      "seats": 0
    },
    {
      "hospitalId": 147,
      "hospitalType": 77,
      "hospitalTypeName": "General",
      "hospitalName": "DismissReason Test Hospital 2",
      "children": 0,
      "isActive": "N",
      "seats": 0
    },
    {
      "hospitalId": 148,
      "hospitalType": 77,
      "hospitalTypeName": "General",
      "hospitalName": "Redox Test Hospital",
      "children": 0,
      "isActive": "A",
      "seats": 51
    },
    {
      "hospitalId": 149,
      "hospitalType": 77,
      "hospitalTypeName": "General",
      "hospitalName": "SnapMD",
      "children": 0,
      "isActive": "A",
      "seats": 1
    },
    {
      "hospitalId": 150,
      "hospitalType": 77,
      "hospitalTypeName": "General",
      "hospitalName": "SnapMD",
      "children": 0,
      "isActive": "A",
      "seats": 1
    },
    {
      "hospitalId": 151,
      "hospitalType": 77,
      "hospitalTypeName": "General",
      "hospitalName": "SnapMD",
      "children": 0,
      "isActive": "A",
      "seats": 1
    },
    {
      "hospitalId": 152,
      "hospitalType": 77,
      "hospitalTypeName": "General",
      "hospitalName": "SnapMD",
      "children": 0,
      "isActive": "A",
      "seats": 1
    },
    {
      "hospitalId": 153,
      "hospitalType": 77,
      "hospitalTypeName": "General",
      "hospitalName": "SnapMD",
      "children": 0,
      "isActive": "A",
      "seats": 1
    },
    {
      "hospitalId": 154,
      "hospitalType": 77,
      "hospitalTypeName": "General",
      "hospitalName": "SnapMD",
      "children": 0,
      "isActive": "A",
      "seats": 1
    },
    {
      "hospitalId": 155,
      "hospitalType": 77,
      "hospitalTypeName": "General",
      "hospitalName": "SnapMD",
      "children": 0,
      "isActive": "A",
      "seats": 1
    },
    {
      "hospitalId": 156,
      "hospitalType": 77,
      "hospitalTypeName": "General",
      "hospitalName": "SnapMD",
      "children": 0,
      "isActive": "A",
      "seats": 1
    },
    {
      "hospitalId": 157,
      "hospitalType": 77,
      "hospitalTypeName": "General",
      "hospitalName": "Dan 113",
      "children": 0,
      "isActive": "A",
      "seats": 0
    },
    {
      "hospitalId": 179,
      "hospitalType": 77,
      "hospitalTypeName": "General",
      "hospitalName": "Test 2017-04-30 - 1",
      "children": 0,
      "isActive": "A",
      "seats": 0
    },
    {
      "hospitalId": 180,
      "hospitalType": 77,
      "hospitalTypeName": "General",
      "hospitalName": "Test 2017-04-30 - 2",
      "children": 0,
      "isActive": "A",
      "seats": 0
    },
    {
      "hospitalId": 181,
      "hospitalType": 77,
      "hospitalTypeName": "General",
      "hospitalName": "Test 2017-04-30 - 3",
      "children": 0,
      "isActive": "A",
      "seats": 0
    }],
    total: 60
  }
export { mockClientSummaryDTOs, mockClientSummaryDTOsAll }