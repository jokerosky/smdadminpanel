let mockAddressValidationResponseSuccess = {
    "data": [
    {
      "isAvailable": true,
      "message": "{[ruleSet:Patient Registration:Yes: Allowed to register]}[@]",
      "category": 1,
      "hospitalId": 1
    }
  ],
  "total": 1
};

let mockAddressValidationResponseFailure = {
    "data": [
    {
      "isAvailable": false,
      "message": "{[ruleSet:Patient Registration:No: Not allowed to register]}[@]",
      "category": 1,
      "hospitalId": 1
    }
  ],
  "total": 1
}

export {
    mockAddressValidationResponseSuccess,
    mockAddressValidationResponseFailure
}