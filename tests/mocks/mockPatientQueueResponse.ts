let mockPatientQueueResponse = {
  "data": [
    {
      "appointmentId": "72577e00-734e-463f-8dbd-6a89b76bb714",
      "patientId": 2,
      "clinicianId": 1858,
      "appointmentStatusCode": "Waiting",
      "participants": [
        {
          "participantId": "25b1cc14-b922-4708-b346-ab3432856aa9",
          "person": {
            "id": "5b9ba4fc-3fba-4d20-abe0-6f5a4eb1344b",
            "name": {
              "given": "Todd",
              "prefix": "",
              "suffix": "",
              "family": "Goulding"
            },
            "photoUrl": "https://snap.local/api/v2.1/images/85989595-fd6e-4341-915a-d1a063e2b81d",
            "providerId": 1,
            "statusCode": 1,
            "contactTypeCode": 1,
            "phones": [
              {
                "id": "e6690e4f-ccd5-435d-a143-9b25a0be5ede",
                "use": "mobile",
                "value": "+15554567"
              }
            ]
          },
          "status": 1,
          "attendenceCode": 1,
          "personId": "5b9ba4fc-3fba-4d20-abe0-6f5a4eb1344b",
          "participantTypeCode": 1
        },
        {
          "participantId": "f062152a-ef75-47f7-b760-d4f844ece0ab",
          "person": {
            "id": "fee2f135-80fe-45a0-9fe9-171be54c4406",
            "name": {
              "given": "dan2Dr2",
              "prefix": "",
              "suffix": "",
              "family": "p<script>alert(hi)</>"
            },
            "photoUrl": "https://snap.local/api/v2.1/images/6c6af8ff-7af1-4418-97b8-a773befcc803",
            "providerId": 1,
            "statusCode": 1,
            "contactTypeCode": 2,
            "phones": [
              {
                "id": "8b391323-36c8-41ea-8296-aca8606948cb",
                "use": "home",
                "value": "+1203200039"
              },
              {
                "id": "1e1130ed-8e64-46ae-80ec-ce8f39985578",
                "use": "mobile",
                "value": "+1203200039"
              }
            ]
          },
          "status": 1,
          "attendenceCode": 1,
          "personId": "fee2f135-80fe-45a0-9fe9-171be54c4406",
          "participantTypeCode": 2
        }
      ],
      "consultationId": 5527,
      "dismissed": false,
      "serviceTypeName": "Admin Scheduled",
      "availabilityBlockId": "95ad0003-43c1-45d0-8cf9-08a4d96f3490",
      "waiveFee": false,
      "startTime": "2017-11-05T18:00:00+07:00",
      "endTime": "2017-01-05T18:30:00+07:00",
      "appointmentTypeCode": 1,
      "intakeMetadata": {
        "concerns": [
          {
            "customCode": {
              "code": 84,
              "description": "Pink Eye"
            },
            "isPrimary": true
          }
        ],
        "additionalNotes": ""
      },
      "encounterTypeCode": 2,
      "where": "+61451354355",
      "whereUse": "0",
      "serviceTypeId": 10,
      "zonedTime": {
        "timeZoneId": 51,
        "startTime": "2017-11-05T22:00:00Z",
        "endTime": "2017-01-05T22:30:00Z"
      }
    },
    {
      "appointmentId": "36a4be9a-c66f-412f-9637-0afcb7b29af8",
      "patientId": 2,
      "clinicianId": 1858,
      "appointmentStatusCode": "Waiting",
      "participants": [
        {
          "participantId": "8861ed0f-c71d-4126-85ab-1d2faf416e1e",
          "person": {
            "id": "5b9ba4fc-3fba-4d20-abe0-6f5a4eb1344b",
            "name": {
              "given": "Todd",
              "prefix": "",
              "suffix": "",
              "family": "Goulding"
            },
            "photoUrl": "https://snap.local/api/v2.1/images/85989595-fd6e-4341-915a-d1a063e2b81d",
            "providerId": 1,
            "statusCode": 1,
            "contactTypeCode": 1,
            "phones": [
              {
                "id": "e6690e4f-ccd5-435d-a143-9b25a0be5ede",
                "use": "mobile",
                "value": "+15554567"
              }
            ]
          },
          "status": 1,
          "attendenceCode": 1,
          "personId": "5b9ba4fc-3fba-4d20-abe0-6f5a4eb1344b",
          "participantTypeCode": 1
        },
        {
          "participantId": "597673c8-bb13-4c8a-add3-6055c2482e38",
          "person": {
            "id": "fee2f135-80fe-45a0-9fe9-171be54c4406",
            "name": {
              "given": "dan2Dr2",
              "prefix": "",
              "suffix": "",
              "family": "p<script>alert(hi)</>"
            },
            "photoUrl": "https://snap.local/api/v2.1/images/6c6af8ff-7af1-4418-97b8-a773befcc803",
            "providerId": 1,
            "statusCode": 1,
            "contactTypeCode": 2,
            "phones": [
              {
                "id": "8b391323-36c8-41ea-8296-aca8606948cb",
                "use": "home",
                "value": "+1203200039"
              },
              {
                "id": "1e1130ed-8e64-46ae-80ec-ce8f39985578",
                "use": "mobile",
                "value": "+1203200039"
              }
            ]
          },
          "status": 1,
          "attendenceCode": 1,
          "personId": "fee2f135-80fe-45a0-9fe9-171be54c4406",
          "participantTypeCode": 2
        }
      ],
      "consultationId": 5526,
      "dismissed": false,
      "serviceTypeName": "Admin Scheduled",
      "availabilityBlockId": "95ad0003-43c1-45d0-8cf9-08a4d96f3490",
      "waiveFee": false,
      "startTime": "2017-11-05T17:30:00+07:00",
      "endTime": "2017-01-05T18:00:00+07:00",
      "appointmentTypeCode": 1,
      "intakeMetadata": {
        "concerns": [
          {
            "customCode": {
              "code": 84,
              "description": "Pink Eye"
            },
            "isPrimary": true
          }
        ],
        "additionalNotes": ""
      },
      "encounterTypeCode": 2,
      "where": "+61451354355",
      "whereUse": "0",
      "serviceTypeId": 10,
      "zonedTime": {
        "timeZoneId": 51,
        "startTime": "2017-11-05T21:30:00Z",
        "endTime": "2017-01-05T22:00:00Z"
      }
    },
    {
      "appointmentId": "366f336f-dde5-442c-a7fe-7049c924bd5e",
      "patientId": 1900,
      "clinicianId": 1937,
      "appointmentStatusCode": "Waiting",
      "participants": [
        {
          "participantId": "2d63a1fe-6fdd-4461-9851-8f8e17292bfd",
          "person": {
            "id": "9ff4701b-a814-497d-b203-84a2b4c89cbe",
            "name": {
              "given": "Tony",
              "prefix": "",
              "suffix": "",
              "family": "Yakovets"
            },
            "photoUrl": "https://snap.local/api/v2.1/images/c8d29201-aa65-4cee-8bf1-75058d167b5a",
            "providerId": 1,
            "statusCode": 1,
            "contactTypeCode": 1,
            "phones": [
              {
                "id": "c2734c9a-8138-4ebe-a60b-591af2275da5",
                "use": "mobile",
                "value": "+79081040656"
              }
            ]
          },
          "status": 1,
          "attendenceCode": 1,
          "personId": "9ff4701b-a814-497d-b203-84a2b4c89cbe",
          "participantTypeCode": 1
        },
        {
          "participantId": "1275524f-a8f6-40c5-aaac-de418fe78949",
          "person": {
            "id": "524aaf2f-f48d-4632-b311-63a7529d3117",
            "name": {
              "given": "Tony",
              "prefix": "",
              "suffix": "",
              "family": "Yakovets"
            },
            "photoUrl": "https://snap.local/api/v2.1/images/15a76a75-04cc-4239-88fa-f961fdabe726",
            "providerId": 1,
            "statusCode": 1,
            "contactTypeCode": 2,
            "phones": [
              {
                "id": "10cb3057-1a56-490b-816f-4abed7f3c206",
                "use": "mobile",
                "value": "+79659846524"
              },
              {
                "id": "7391afbf-94de-457d-8666-548ce4fae218",
                "use": "home",
                "value": "+79081040656"
              }
            ]
          },
          "status": 1,
          "attendenceCode": 1,
          "personId": "524aaf2f-f48d-4632-b311-63a7529d3117",
          "participantTypeCode": 2
        }
      ],
      "consultationId": 6249,
      "dismissed": false,
      "serviceTypeName": "Self-Scheduling",
      "availabilityBlockId": "1fe0b24f-35ac-45d3-a251-6a2c652826b7",
      "waiveFee": false,
      "startTime": "2017-06-26T21:15:00+07:00",
      "endTime": "2017-06-26T21:30:00+07:00",
      "patientQueueId": "0f84a073-c302-4a13-9ed6-1b76e00d6114",
      "appointmentTypeCode": 3,
      "intakeMetadata": {
        "concerns": [
          {
            "customCode": {
              "code": 82,
              "description": "Cough"
            },
            "isPrimary": true
          }
        ],
        "additionalNotes": ""
      },
      "encounterTypeCode": 3,
      "where": "",
      "whereUse": "3",
      "serviceTypeId": 9
    }
  ],
  "total": 3
}

export {
    mockPatientQueueResponse
}