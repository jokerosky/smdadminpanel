import { NewPatientRequestDTO } from '../../src/models/DTO/newPatientRequestDTO';

let mockNewPatientRequestDTO:NewPatientRequestDTO = { 
    email:'test@test.com',
    name:{
            first:'Tiesto',
            last: 'DJ'
        },
    password:'@123',
    providerId: 0,
    userTypeId: 1,
    redirectUrl: '/login',
    address:'Montgomery, Alabama',
    dob:'25/05/1987'
};

export default  mockNewPatientRequestDTO;
