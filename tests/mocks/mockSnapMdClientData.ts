let clientData ={
    data:[{
        appointmentsContactNumber:"5555551212",
        brandColor:"#8f00bb",
        brandName:"Emerald Dev&&",
        brandTitle:"Virtual Telemedicine",
        children:0,
        cliniciansCount:2000,
        consultationCharge:120,
        contactNumber:"1234567891",
        email:"",
        hospitalCode:"Emerald",
        hospitalDomainName:"snap.local",
        hospitalId:1,
        hospitalImage:"http://snap.local/api/v2.1/images/66a9c2b5-8f45-4705-9e57-84932a8aeb8e",
        hospitalName:"Emerald Dev",
        hospitalType:131,
        isActive:"A",
        itDeptContactNumber:"5555551212",
        nPINumber:1,
        seats:0,
        totalUsersCount:4000
    }
],
    total:1
};
export {clientData};
