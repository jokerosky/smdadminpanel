let hospitalType = [
    {
        "key": 77,
        "value": "General"
      },
      {
        "key": 78,
        "value": "Pediatric"
      },
      {
        "key": 131,
        "value": "University Hospital"
      },
      {
        "key": 132,
        "value": "Accountable Care Organizations (ACO)"
      },
      {
        "key": 133,
        "value": "Urgent Care (UC)"
      },
      {
        "key": 134,
        "value": "Retail/Convenient Care Clinics (CC)"
      },
      {
        "key": 135,
        "value": "Allied Health Companies (AHC)"
      },
      {
        "key": 136,
        "value": "Integrated Health Systems/Networks (IHS)"
      },
      {
        "key": 137,
        "value": "Maritime (MT)"
      }
];

export{ hospitalType };