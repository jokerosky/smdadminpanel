let mockLeftMenuNoSort = [
        {
            hospitalId: 0,
            hospitalName:'SnapMD',
            seats: 1
        },
        {
            hospitalId: 102,
            hospitalName:'PhD Labs',
            seats: 10
        },
        {
            hospitalId: 38,
            hospitalName:'Prompt Healthcare',
            seats: 5
        },
        {
            hospitalId: 104,
            hospitalName:'Digital Solutions Warehouse OldDoNotUse',
            seats: 1
        },
        {
            hospitalId: 36,
            hospitalName:'SampleTestClient',
            seats: 20
        },];

let mockLeftMenuIdSortAscending = [
        {
            hospitalId: 0,
            hospitalName:'SnapMD',
            seats: 1
        },
        {
            hospitalId: 36,
            hospitalName:'SampleTestClient',
            seats: 20
        },
        {
            hospitalId: 38,
            hospitalName:'Prompt Healthcare',
            seats: 5
        },
        {
            hospitalId: 102,
            hospitalName:'PhD Labs',
            seats: 10
        },
        {
            hospitalId: 104,
            hospitalName:'Digital Solutions Warehouse OldDoNotUse',
            seats: 1
        },];

let mockLeftMenuIdSortWaning = [
        {
            hospitalId: 104,
            hospitalName:'Digital Solutions Warehouse OldDoNotUse',
            seats: 1
        },
        {
            hospitalId: 102,
            hospitalName:'PhD Labs',
            seats: 10
        },
        {
            hospitalId: 38,
            hospitalName:'Prompt Healthcare',
            seats: 5
        },
        {
            hospitalId: 36,
            hospitalName:'SampleTestClient',
            seats: 20
        },
        {
            hospitalId: 0,
            hospitalName:'SnapMD',
            seats: 1
        },];

let mockLeftMenuNameSortAscending = [
        {
            hospitalId: 104,
            hospitalName:'Digital Solutions Warehouse OldDoNotUse',
            seats: 1
        },
        {
            hospitalId: 102,
            hospitalName:'PhD Labs',
            seats: 10
        },
        {
            hospitalId: 38,
            hospitalName:'Prompt Healthcare',
            seats: 5
        },
        {
            hospitalId: 36,
            hospitalName:'SampleTestClient',
            seats: 20
        },
        {
            hospitalId: 0,
            hospitalName:'SnapMD',
            seats: 1
        },];

let mockLeftMenuNameSortWaning = [
        {
            hospitalId: 0,
            hospitalName:'SnapMD',
            seats: 1
        },
        {
            hospitalId: 36,
            hospitalName:'SampleTestClient',
            seats: 20
        },
        {
            hospitalId: 38,
            hospitalName:'Prompt Healthcare',
            seats: 5
        },
        {
            hospitalId: 102,
            hospitalName:'PhD Labs',
            seats: 10
        },
        {
            hospitalId: 104,
            hospitalName:'Digital Solutions Warehouse OldDoNotUse',
            seats: 1
        },];

let mockLeftMenuSeatsSortAscending = [
        {
            hospitalId: 0,
            hospitalName:'SnapMD',
            seats: 1
        },
        {
            hospitalId: 104,
            hospitalName:'Digital Solutions Warehouse OldDoNotUse',
            seats: 1
        },
        {
            hospitalId: 38,
            hospitalName:'Prompt Healthcare',
            seats: 5
        },
        {
            hospitalId: 102,
            hospitalName:'PhD Labs',
            seats: 10
        },
        {
            hospitalId: 36,
            hospitalName:'SampleTestClient',
            seats: 20
        },];

let mockLeftMenuSeatsSortWaning = [
        {
            hospitalId: 36,
            hospitalName:'SampleTestClient',
            seats: 20
        },
        {
            hospitalId: 102,
            hospitalName:'PhD Labs',
            seats: 10
        },
        {
            hospitalId: 38,
            hospitalName:'Prompt Healthcare',
            seats: 5
        },
        {
            hospitalId: 0,
            hospitalName:'SnapMD',
            seats: 1
        },
        {
            hospitalId: 104,
            hospitalName:'Digital Solutions Warehouse OldDoNotUse',
            seats: 1
        },];

export {
    mockLeftMenuNoSort,
    mockLeftMenuIdSortAscending,
    mockLeftMenuIdSortWaning,
    mockLeftMenuNameSortAscending,
    mockLeftMenuNameSortWaning,
    mockLeftMenuSeatsSortAscending,
    mockLeftMenuSeatsSortWaning
};