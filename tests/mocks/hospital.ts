import { AdditionalHospitalSettings, Hospital } from '../../src/models/hospital';
var hospital:Hospital = {
    brandName:'Test hospital',
    brandColor: '#8f00bb',
    contactNumber: '322 232'
}; 

export function getMockedHospital():Hospital {
    return hospital;
}