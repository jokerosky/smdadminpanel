import SiteNotification from '../../src/models/site/siteNotification';
import { ValidationError } from '../../src/utilities/validators';
import * as types from '../../src/enums/actionTypes';
import {EventType} from '../../src/enums/eventType';

let mockValidationResultActionAdd = {
            type: types.VALIDATION_RESULT_ADD,
            notifications:[
                {
                    message:"Something is wrong",
                    type:EventType.error
                },
                {
                    message:"Everything goes as it should be",
                    type:EventType.regular
                }
            ] as SiteNotification[],
            validationErrors:[
                {
                    component:'whatever',
                    elementId:'login',
                    errorClass:'is-error',
                    messages:["one", "two", "three"]
                }
            ] as ValidationError[],
            cmpntClassName: 'whatever'
        };

export { mockValidationResultActionAdd };