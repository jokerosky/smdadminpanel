import SiteNotification from  '../../src/models/site/siteNotification'; 
import { EventType } from  '../../src/enums/eventType'; 

let mockSiteNotifications = [
    {
        message:'One',
        notificationId:0,
        comment:"whatever#login",
        type:EventType.regular
    },
    {
        message:'Two',
        notificationId:1,
        comment:"whatever#password",
        type:EventType.error
    },
    {
        message:'Three',
        notificationId:2,
        type:EventType.info
    }
] as SiteNotification[];

export { mockSiteNotifications }
