let countries = [
    {
        alpha2: "AF",
        areas: Array[0],
        name: "Afghanistan"
    },
    {
        alpha2: "AX",
        areas: Array[0],
        name: "Åland Islands"
    },
    {
        alpha2: "AL",
        areas: Array[0],
        name: "Albania"
    },
    {
        alpha2: "DZ",
        areas: Array[0],
        name: "Algeria"
    },
    {
        alpha2: "AS",
        areas: Array[0],
        name: "American Samoa"
    },
    {
        alpha2: "AD",
        areas: Array[0],
        name: "Andorra"
    }
];

export {countries};