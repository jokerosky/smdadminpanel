let organizations = [
      {
        "addresses": [],
        "hospitalId": 1,
        "locations": [
          {
            "name": "Test location 1 Updated",
            "organizationId": 2,
            "id": 1,
            "statusCode": 1,
            "createdDate": "2015-12-02T00:00:00Z",
            "modifiedDate": "2017-11-03T11:32:43.967Z",
            "createdByUserId": 1,
            "modifiedByUserId": 96
          },
          {
            "name": "New Test Location1",
            "organizationId": 2,
            "id": 4,
            "statusCode": 2,
            "createdDate": "2017-11-09T06:05:35Z",
            "modifiedDate": "2017-11-09T08:39:03.46Z",
            "createdByUserId": 96,
            "modifiedByUserId": 96
          },
          {
            "name": "Test3",
            "organizationId": 2,
            "id": 5,
            "statusCode": 2,
            "createdDate": "2017-11-09T07:19:47.727Z",
            "modifiedDate": "2017-11-09T07:44:05.453Z",
            "createdByUserId": 96,
            "modifiedByUserId": 96
          },
          {
            "name": "Test4",
            "organizationId": 2,
            "id": 6,
            "statusCode": 1,
            "createdDate": "2017-11-09T07:52:55.19Z",
            "modifiedDate": "2017-11-09T07:52:55.19Z",
            "createdByUserId": 96,
            "modifiedByUserId": 96
          },
          {
            "name": "test5",
            "organizationId": 2,
            "id": 7,
            "statusCode": 1,
            "createdDate": "2017-11-09T07:53:56.64Z",
            "modifiedDate": "2017-11-09T07:53:56.64Z",
            "createdByUserId": 96,
            "modifiedByUserId": 96
          },
          {
            "name": "test6",
            "organizationId": 2,
            "id": 8,
            "statusCode": 2,
            "createdDate": "2017-11-09T07:56:10.36Z",
            "modifiedDate": "2017-11-09T09:19:50.843Z",
            "createdByUserId": 96,
            "modifiedByUserId": 96
          },
          {
            "name": "test7",
            "organizationId": 2,
            "id": 9,
            "statusCode": 2,
            "createdDate": "2017-11-09T07:57:23.87Z",
            "modifiedDate": "2017-11-09T09:19:38.22Z",
            "createdByUserId": 96,
            "modifiedByUserId": 96
          }
        ],
        "name": "Test1",
        "organizationTypeId": 1,
        "id": 2,
        "createdDate": "2015-11-23T00:00:00Z",
        "modifiedDate": "2017-11-03T06:58:58.613Z",
        "createdByUserId": 1,
        "modifiedByUserId": 96
      },
      {
        "addresses": [],
        "hospitalId": 1,
        "locations": [],
        "name": "Organization",
        "organizationTypeId": 2,
        "id": 5,
        "createdDate": "2017-11-02T08:05:59.41Z",
        "modifiedDate": "2017-11-02T08:05:59.41Z",
        "createdByUserId": 96,
        "modifiedByUserId": 96
      },
      {
        "addresses": [],
        "hospitalId": 1,
        "locations": [],
        "name": "New Organization",
        "organizationTypeId": 2,
        "id": 6,
        "createdDate": "2017-11-03T05:39:25.05Z",
        "modifiedDate": "2017-11-03T05:51:04.567Z",
        "createdByUserId": 96,
        "modifiedByUserId": 96
      },
      {
        "addresses": [],
        "hospitalId": 1,
        "locations": [],
        "name": "Test2",
        "id": 7,
        "createdDate": "2017-11-03T06:09:27.23Z",
        "modifiedDate": "2017-11-03T06:09:27.23Z",
        "createdByUserId": 96,
        "modifiedByUserId": 96
      },
      {
        "addresses": [],
        "hospitalId": 1,
        "locations": [],
        "name": "test2",
        "id": 8,
        "createdDate": "2017-11-03T06:10:50.723Z",
        "modifiedDate": "2017-11-03T06:10:50.723Z",
        "createdByUserId": 96,
        "modifiedByUserId": 96
      },
      {
        "addresses": [],
        "hospitalId": 1,
        "locations": [],
        "name": "Test2",
        "organizationTypeId": 1,
        "id": 9,
        "createdDate": "2017-11-03T06:11:04.783Z",
        "modifiedDate": "2017-11-03T11:25:41.013Z",
        "createdByUserId": 96,
        "modifiedByUserId": 96
      },
      {
        "addresses": [],
        "hospitalId": 1,
        "locations": [],
        "name": "test3",
        "organizationTypeId": 1,
        "id": 10,
        "createdDate": "2017-11-03T06:12:14.22Z",
        "modifiedDate": "2017-11-03T06:12:14.22Z",
        "createdByUserId": 96,
        "modifiedByUserId": 96
      },
      {
        "addresses": [],
        "hospitalId": 1,
        "locations": [],
        "name": "test4",
        "organizationTypeId": 1,
        "id": 11,
        "createdDate": "2017-11-03T06:13:07.187Z",
        "modifiedDate": "2017-11-03T06:13:07.187Z",
        "createdByUserId": 96,
        "modifiedByUserId": 96
      },
      {
        "addresses": [],
        "hospitalId": 1,
        "locations": [],
        "name": "test5",
        "organizationTypeId": 1,
        "id": 12,
        "createdDate": "2017-11-03T06:13:08.883Z",
        "modifiedDate": "2017-11-03T07:00:02.74Z",
        "createdByUserId": 96,
        "modifiedByUserId": 96
      },
      {
        "addresses": [],
        "hospitalId": 1,
        "locations": [],
        "name": "test6",
        "organizationTypeId": 1,
        "id": 13,
        "createdDate": "2017-11-03T06:13:11.447Z",
        "modifiedDate": "2017-11-03T07:00:06.35Z",
        "createdByUserId": 96,
        "modifiedByUserId": 96
      },
      {
        "addresses": [],
        "hospitalId": 1,
        "locations": [],
        "name": "test7",
        "organizationTypeId": 1,
        "id": 14,
        "createdDate": "2017-11-03T06:13:11.673Z",
        "modifiedDate": "2017-11-03T07:00:09.927Z",
        "createdByUserId": 96,
        "modifiedByUserId": 96
      },
      {
        "addresses": [],
        "hospitalId": 1,
        "locations": [],
        "name": "Civilization",
        "organizationTypeId": 2,
        "id": 15,
        "createdDate": "2017-11-03T06:14:17.797Z",
        "modifiedDate": "2017-11-03T06:14:17.797Z",
        "createdByUserId": 96,
        "modifiedByUserId": 96
      },
      {
        "addresses": [],
        "hospitalId": 1,
        "locations": [],
        "name": "test8",
        "organizationTypeId": 1,
        "id": 16,
        "createdDate": "2017-11-03T06:35:16.54Z",
        "modifiedDate": "2017-11-03T07:00:13.397Z",
        "createdByUserId": 96,
        "modifiedByUserId": 96
      },
      {
        "addresses": [],
        "hospitalId": 1,
        "locations": [],
        "name": "test9",
        "organizationTypeId": 1,
        "id": 17,
        "createdDate": "2017-11-03T06:35:20.607Z",
        "modifiedDate": "2017-11-03T06:59:57.92Z",
        "createdByUserId": 96,
        "modifiedByUserId": 96
      }
    ]

export{organizations};