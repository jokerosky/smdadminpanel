import 'mocha';
import { assert } from 'chai';

import { SignalRService } from '../../../src/services/signalR/signalRService';

var hubConnection = function() {
        this.received = () => {};
        this.error = () => {};
        
        this.stateChanged = () => {};
        this.reconnected = () => {};
        this.starting = () => {};

        this.createHubProxy = (name) => {
            return {
                     name,
                     on:()=>{},
                    }
                };

        this.start = ()=>{
            let defferred = {
                then:()=>{return defferred},
                fail:()=>{return defferred}
            };
            return defferred;
        };         

        return this;
    };

var $ = {
    hubConnection
};

describe('SignalR service specs', () => {
    let spy;
    let dispatch = (data) => { spy = data };

    it('should init connection and have 1 hubs', () => {
        let signalRSrv = new SignalRService(dispatch, $);
        let token = 'token';
        signalRSrv.initSignalR(token);

        assert.equal(Object.keys(signalRSrv.hubs).length, 1);
    });
})