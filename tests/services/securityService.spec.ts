import 'mocha';
import { assert } from 'chai';
import * as configureMockStore from 'redux-mock-store';
import thunk from 'redux-thunk';

import { SecurityService } from '../../src/services/securityService';
import * as userTypes from '../../src/enums/userTypes';

import { NextState } from '../mocks/onEnterCallbackMocks'

describe('SecurityService tests', () => {
    const middlewares = [thunk];
    const getMockStore = configureMockStore(middlewares);

    let mockStore: any;
    let securityService: SecurityService;

    beforeEach(() => {
        mockStore = getMockStore({user:{accessToken:''}});
        securityService = new SecurityService(mockStore);
    });

    it('Should determin userType', () => {
        let type = securityService.determineUserType("/patient/inner/space/shuttle/");
        assert.equal(type, userTypes.patient);

        type = securityService.determineUserType("/");
        assert.equal(type, userTypes.guest);
    });

    it('Should set userType in global state', () => {
        let nextState = new NextState();
        securityService.checkLogin(nextState, () => { }, () => { });
        let actions = mockStore.getActions();
        assert.equal(actions.length, 1);
        assert.equal(actions[0].userType, userTypes.guest);
    });

    it('Should not call callback if not initialized with store, and return false',()=>{
        securityService = new SecurityService(null);
        let nextState = new NextState();

        let callsNumber = 0;
        let callback  = ()=>{callsNumber++};

        let isAllowed = securityService.checkLogin(nextState, ()=>{}, callback);

        assert.equal(callsNumber, 0);
        assert.equal(isAllowed, false);
    });

    it('Should determine if passed location is public page',()=>{
        let location =  "/login/provider/";
        let result = SecurityService.isPublicPage(location);
        assert.isTrue(result);

        location = "/provider/dashboard";
        result = SecurityService.isPublicPage(location);
        assert.isFalse(result);
    });
});
