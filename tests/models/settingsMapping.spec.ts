import { assert } from 'chai';

import { EditorType } from '../../src/enums/editorTypes';
import { IMappedSetting ,  SettingsMapping, ToggleMappedSetting} from '../../src/models/site/settingsMapping';
import { hospitalModulesSettings, hospitalSettings } from '../mocks/mockHospitalSettingsDTO';
import { MODULE_KEYS, SETTINGS_KEYS } from '../../src/enums/clientSettingsKeys';

import { mappedKeys } from '../mocks/mockMappedSettings';

let settings = {};
        [...hospitalModulesSettings, ...hospitalSettings].forEach(x=>{
            settings[x.key] = x;
        })

describe("SettingsMappings tests",()=>{
    it("Should map keys to loaded values array",()=>{
        let settingsMapping = new SettingsMapping(mappedKeys, settings);
        let setting = settingsMapping.mappedSettings.find(x=>x.key == 'mECommerce');

        assert.equal(setting.value, "False");
    });

    it("Should return array of key:value objects to save",()=>{
        let settingsMapping = new SettingsMapping(mappedKeys, settings);

        let result = settingsMapping.getSettingsArrayToSave();

        assert.equal(result.length, 4);
        assert.isObject(result[0]);
        assert.equal(result[0].key, "mECommerce");
        assert.equal(result[0].value, "False");
    });

    it("Should accept empty settings object",()=>{
        let settingsMapping = new SettingsMapping(mappedKeys, undefined);

        assert.equal(settingsMapping.mappedSettings.length, 2);
    });

    it('Should create toggle mapping',()=>{
        let key = "testKey";
        let label = "This is Key Test";
        let mapping = new ToggleMappedSetting(key, label, [{} as any,{} as any]);

        assert.equal(mapping.editorType, EditorType.toggle );
        assert.equal(mapping.key, key );
        assert.equal(mapping.children.length , 2);
    });
});