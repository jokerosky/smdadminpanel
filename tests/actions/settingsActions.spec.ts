import 'mocha';
import * as _ from 'lodash';
import * as configureMockStore from 'redux-mock-store';
import thunk from 'redux-thunk';
import axios from 'axios';
import * as router from 'react-router';
import { assert } from 'chai';
import * as moxios from 'moxios';

import { Endpoints } from '../../src/enums/endpoints';
import * as SettingsActions from '../../src/actions/snapMdAdminClientActions/settingsActions';
import { IServiceLocator, SnapMDServiceLocator } from '../../src/dependenciesInjectionConfiguration';
import { HospitalImagesDTO } from '../../src/models/DTO/hospitalSettingDTO';
import { mockClientData, mockSettings, mockAdditionalSettings, mockHospitalAddress, mockClientInLists } from '../mocks/mockObjectsSettingsV2';
import { ISnapMdHttpService, SnapMdHttpService } from '../../src/services/api/httpServices';
import * as types from '../../src/enums/actionTypes';

const middlewares = [thunk];
const mockStore = configureMockStore(middlewares);

let file: FormData = new FormData();
var imageObjects: HospitalImagesDTO[] = [
    {
        fileImage: file,
        pathSaveImage: Endpoints.snapmdAdmin.brandBackgroundImage,
        fieldNameToSave: 'BrandBackgroundImage'
    },
    {
        fileImage: file,
        pathSaveImage: Endpoints.snapmdAdmin.brandBackgroundLoginImage,
        fieldNameToSave: 'BrandBackgroundLoginImage'
    }
];

var settings: any = {
    clientInLists: mockClientInLists,
    settings: mockSettings,
    additionalSettings: mockAdditionalSettings,
    hospitalAddress: mockHospitalAddress,
    client: mockClientData
};

describe('Settings actions tests: ', () => {
    let store;

    beforeEach(() => {
        moxios.install();
        store = mockStore({});
    });

    afterEach(() => {
        moxios.uninstall()
    });

    it('Test save images and client data', (testDone) => {
        let hospitalId = 1;
        let mockResponse1 = {
            id: "04fc49d7-9d09-4c14-b4de-96b7d2dcef95",
            uri: "https://snap.local/api/v2.1/images/460d2c81-9280-4a82-81e6-53237b316c14"
        };
        let mockResponse2 = {
            id: "04fc49d7-9d09-4c14-b4de-96b7d2dcef94",
            uri: "https://snap.local/api/v2.1/images/34061455-4272-44aa-9927-44bf9da6427a"
        }
        moxios.stubRequest(Endpoints.snapmdAdmin.brandBackgroundImage.replace('%id%', hospitalId.toString()),
            {
                status: 200,
                response: mockResponse1
            });

        moxios.stubRequest(Endpoints.snapmdAdmin.brandBackgroundLoginImage.replace('%id%', hospitalId.toString()),
            {
                status: 200,
                response: mockResponse2
            });
        moxios.stubRequest(Endpoints.snapmdAdmin.contactUsImage.replace('%id%', hospitalId.toString()),
            {
                status: 200,
                response: {
                    id: "04fc49d7-9d09-4c14-b4de-96b7d2dcef93",
                    uri: "https://snap.local/api/v2.1/images/e1216500-8974-4e9f-ba4d-a5bfdfa0bf7b"
                }
            });
        moxios.stubRequest(Endpoints.snapmdAdmin.providerLogo.replace('%id%', hospitalId.toString()),
            {
                status: 200,
                response: {
                    id: "04fc49d7-9d09-4c14-b4de-96b7d2dcef91",
                    uri: "https://snap.local/api/v2.1/images/af42ce39-9900-46b1-a0ae-d0ef3b0020d4"
                }
            });
        let clientPath = Endpoints.snapmdAdmin.clients.replace('%id%', hospitalId.toString());
        moxios.stubRequest(clientPath,
            {
                status: 200,
                response: {
                    id: "30",
                    uri: "Client Data"
                }
            });

        let response = store.dispatch(SettingsActions.saveImagesAndClientData(SnapMDServiceLocator, imageObjects, settings, hospitalId))
        response
            .then((results) => {
                let actns = store.getActions();
                assert.equal(actns.length, 3);
                assert.equal(actns[1].fieldNameToSave, 'BrandBackgroundImage');
                assert.equal(actns[1].imageUrl, mockResponse1.uri);
                assert.equal(actns[2].fieldNameToSave, 'BrandBackgroundLoginImage');
                assert.equal(actns[2].imageUrl, mockResponse2.uri);
                testDone();
            });
    });

    it('Test Conversion of image objects', () => {
        let response = {
            id: "04fc49d7-9d09-4c14-b4de-96b7d2dcef95",
            uri: mockClientData.hospitalImage,
            fieldNameToSave: 'hospitalImage'
        };

        let result = SettingsActions.conversionOfImageObjects(settings, [response]);

        assert.equal(result.client.hospitalImage, response.uri);
    });

    it('Test save images and client data Exception', (testDone) => {
        let hospitalId = 1;
        let mockResponse1 = {
            id: "04fc49d7-9d09-4c14-b4de-96b7d2dcef95",
            uri: "https://snap.local/api/v2.1/images/460d2c81-9280-4a82-81e6-53237b316c14"
        };
        let mockResponse2 = {
            error: 'Server is not available'
        }
        moxios.stubRequest(Endpoints.snapmdAdmin.brandBackgroundImage.replace('%id%', hospitalId.toString()),
            {
                status: 200,
                response: mockResponse1
            });
        moxios.stubRequest(Endpoints.snapmdAdmin.brandBackgroundLoginImage.replace('%id%', hospitalId.toString()),
            {
                status: 503,
                response: mockResponse2
            });

        let response = store.dispatch(SettingsActions.saveImagesAndClientData(SnapMDServiceLocator, imageObjects, settings, hospitalId))
        response
            .then((results) => {
                let actns = store.getActions();
                assert.equal(actns.length, 3);
                assert.equal(actns[1].type, types.SNAPMDADMIN_SAVE_IMAGE);
                assert.equal(actns[2].type, types.AJAX_CALL_ERROR);
                testDone();
            });
    });

});