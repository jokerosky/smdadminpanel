import 'mocha';
import { assert } from 'chai';
import * as configureMockStore from 'redux-mock-store';
import thunk from 'redux-thunk';
import * as axios from 'axios';
import * as moxios from 'moxios';
import * as router from 'react-router';
import * as _ from 'lodash';


import { Endpoints } from '../../src/enums/endpoints';
import * as pqActions from '../../src/actions/patientQueueActions';

import { mockPatientQueueResponse } from '../mocks/mockPatientQueueResponse';

let urls = [];
const middlewares = [thunk];
const getMockStore = configureMockStore(middlewares);

describe('Patient queue actions tests',()=>{

 let mockStore;

    beforeEach(() => {
        mockStore = getMockStore({});
        moxios.install();
    });

    afterEach(() => {
        moxios.uninstall();
    });

    it('should return Action object when corresponding event function called ',()=>{
        for(let item in pqActions.actionNames){
            let action = pqActions[pqActions.actionNames[item]]('data');

            assert.isOk(action.type);
            assert.equal(action.data, 'data');
        }
    });
});