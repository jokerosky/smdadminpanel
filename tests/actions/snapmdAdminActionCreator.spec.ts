import 'mocha';
import * as _ from 'lodash';
import * as configureMockStore from 'redux-mock-store';
import thunk from 'redux-thunk';
import axios from 'axios';
import * as router from 'react-router';
import { assert } from 'chai';
import * as moxios from 'moxios';

import { Endpoints } from '../../src/enums/endpoints';
import * as snapMdAdminActions from '../../src/actions/snapMdAdminActions';
import { IServiceLocator, SnapMDServiceLocator } from '../../src/dependenciesInjectionConfiguration';

import {getListCodeSetss, getListClientsTypess,
    getListDocumentTypess, getListOrganizationTypess,
    getListPartners, getListPartnerOnlyActives} from '../mocks/mockSnapmdAdminActionCreator';

const middlewares = [thunk];
const mockStore = configureMockStore(middlewares);

describe('snapmdAdmin actions tests', () => {
    let store;
  
    beforeEach(() => {
      moxios.install();
      store = mockStore({});
    });
  
    afterEach(() => {
      moxios.uninstall()
    });

    it('snapmdAdmin Should call endpoint and load api-keys and data with hospitalDataService producing 3 actions', (testDone) => {
        moxios.stubRequest(Endpoints.snapmdAdmin.clients.replace('/%id%',''), {
            status: 200,
            response: {
              data: [{ 
                  hospitalId: 1, 
                  hospitalType: 77, 
                  hospitalTypeName: 'General', 
                  hospitalName: 'Emerald Dev',
                  children: 0,
                  isActive: 'A',
                  seats: 4000
            }]
            }
        })
         store.dispatch(snapMdAdminActions.loadClients(SnapMDServiceLocator))
            .then((results) => {
              let actns = store.getActions();

              assert.equal(actns.length, 4);
              testDone();
        });
    });

  /*  function moxiosResponse(str:string){
        let response = '';
        moxios.stubRequest(str,response)
    }*/

    it('snapmdAdmin Should call endpoint and load lists',(testDone)=>{
        moxios.stubRequest(Endpoints.snapmdAdmin.codeSets, {
            status: 200,
            responseText: 
                getListCodeSetss
        });
        moxios.stubRequest(Endpoints.snapmdAdmin.organizationTypes, {
            status: 200,
            response: {
                ...getListOrganizationTypess
            }
        });
        moxios.stubRequest(Endpoints.snapmdAdmin.documentTypes, {
            status: 200,
            response: {
                ...getListDocumentTypess
            }
        });
        moxios.stubRequest(Endpoints.snapmdAdmin.clientsTypes, {
            status: 200,
            response: {
                ...getListClientsTypess
            }
        });
        moxios.stubRequest(Endpoints.snapmdAdmin.partner, {
            status: 200,
            response: {
                ...getListPartners
            }
        });
        moxios.stubRequest(Endpoints.snapmdAdmin.partnerOnlyActive, {
            status: 200,
            response: {
                ...getListPartnerOnlyActives
            }
        });
        //moxiosResponse(Endpoints.backendRequest.getListCodeSets);
        store.dispatch(snapMdAdminActions.loadLists(SnapMDServiceLocator))
            .then((results) => {
                let actns = store.getActions();
                //debugger;
                assert.equal(actns.length, 3);
                testDone();
            });

    });


});