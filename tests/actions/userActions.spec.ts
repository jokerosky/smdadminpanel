// also they are userService tests
import 'mocha';
import { assert } from 'chai';
import * as configureMockStore from 'redux-mock-store';
import thunk from 'redux-thunk';
import * as axios from 'axios';
import * as moxios from 'moxios';
import * as router from 'react-router';
import * as _ from 'lodash';

import * as userTypes from '../../src/enums/userTypes';
import * as actionTypes from '../../src/enums/actionTypes';
import { Endpoints } from '../../src/enums/endpoints';
import UserService from '../../src/services/api/userService';
import * as userActions from '../../src/actions/userActions';
import * as siteActions from '../../src/actions/siteActions';

import mockLoginResponse from '../mocks/loginResponse';
import mockLoginRequest from '../mocks/loginRequest';
import mockNewPatientRequestDTO from '../mocks/mockNewPatientRequestDTO';
import mockNewPatientResponseDTO from '../mocks/mockNewPatientResponseDTO';

import { mockAddressValidationResponseFailure, mockAddressValidationResponseSuccess } from '../mocks/addressValidationResponse';
import LoginRequestDTO from '../../src/models/DTO/loginRequestDTO';
import { NewPatientRequestDTO } from '../../src/models/DTO/newPatientRequestDTO';

const middlewares = [thunk];
const getMockStore = configureMockStore(middlewares);

describe('UserActions tests', () => {

    let mockStore;

    beforeEach(() => {
        mockStore = getMockStore({});
        moxios.install();
    });


    afterEach(() => {
        moxios.uninstall();
    });

    it('should produce 5 actions when Login, call Token endpoint and return token and expiration date, and call save in session store action', (done) => {
        moxios.stubRequest(Endpoints.user.login, {
            status: 200,
            response: mockLoginResponse
        })

        mockStore.dispatch(userActions.logIn(mockLoginRequest))
            .then((results) => {
                let actns = mockStore.getActions();
                assert.equal(actns.length, 6);
                
                let type = _.findKey(userTypes, x => { return x == mockLoginRequest.userTypeId });
                assert.equal(actns[5].payload.args[0], Endpoints.site.dashboard.replace(/%type%/, type));

                let saveActionIndex = _.findIndex(actns, (x:any) => { return x.type == actionTypes.SAVE_SESSION_STORAGE_DATA });
                assert.isTrue(saveActionIndex > 0);
                done();
            });
    });

    it('should produce 5 actions when fails to log in or network error', (done) => {
        moxios.stubRequest(/./, {
            status: 404,
            responseText: "Not found"
        });
        mockStore.dispatch(userActions.logIn(mockLoginRequest))
            .then((results) => {
                let actns = mockStore.getActions();
                assert.equal(actns.length, 5);
                let errorActionIndex = _.findIndex(actns, (x: any) => { return x.type === actionTypes.LOGIN_FAILURE });
                assert.equal(errorActionIndex > -1, true);
                done();
            });
    });

    it('should return 3 actions when validate registration address', (done) => {
        moxios.stubRequest(RegExp(Endpoints.user.validateRegistrationAddress + "?."), {
            status: 200,
            response: mockAddressValidationResponseFailure
        });

        mockStore.dispatch(userActions.validateRegistrationAddress("Omsk", 1))
            .then((results) => {
                let actns = mockStore.getActions();
                assert.equal(actns.length, 3, "actual " + actns.length);
                let validationAction = _.findIndex(actns, (x: any) => { return x.type === actionTypes.REGISTRATION_ADDRESS_VALIDATED });
                assert.equal(validationAction > -1, true);
                done();
            });

    });

    it('should not return address validation result if network error', (done) => {
        moxios.stubRequest(/./, {
            status: 404,
            responseText: "Not found"
        });

        mockStore.dispatch(userActions.validateRegistrationAddress("Omsk", 1))
            .then((results) => {
                let actns = mockStore.getActions();
                assert.equal(actns.length, 2, "actual " + actns.length);
                let validationAction = _.findIndex(actns, (x: any) => { return x.type === actionTypes.REGISTRATION_ADDRESS_VALIDATED });
                assert.equal(validationAction == -1, true);
                done();
            });
    });

    it('should return 3 actions when register new patient and got bad response', (done) => {

        moxios.stubRequest(Endpoints.user.registerNewPatient, {
            status: 400,
            response: {data:{
                    "message": "Email address already registered."
            }}
        });

        mockStore.dispatch(userActions.registerPatient(mockNewPatientRequestDTO))
            .then((results) => {
                let actns = mockStore.getActions();
                assert.equal(actns.length, 3, "actual " + actns.length);
                let validationAction = _.findIndex(actns, (x: any) => { return x.type === actionTypes.REGISTRATION_NEW_PATIENT_ERROR });
                assert.equal(validationAction != -1, true);
                done();
            });
    });

    it('should return 2 actions when register new patient and got network error', (done) => {
        moxios.stubRequest(Endpoints.user.registerNewPatient, {
            status: 404
        });

        mockStore.dispatch(userActions.registerPatient(mockNewPatientRequestDTO))
            .then((results) => {
                let actns = mockStore.getActions();
                assert.equal(actns.length, 2, "actual " + actns.length);
                let errAction = _.findIndex(actns, (x: any) => { return x.type === actionTypes.REGISTRATION_NEW_PATIENT_ERROR });
                assert.equal(errAction == -1, true);
                done();
            });
    });


    it('should return 3 actions when register new patient and got good response', (done) => {

        moxios.stubRequest(Endpoints.user.registerNewPatient, {
            status: 200,
            response: {
                    data:[ mockNewPatientResponseDTO ],
                    total:1
            },
        });

        mockStore.dispatch(userActions.registerPatient(mockNewPatientRequestDTO))
            .then((results) => {
                let actns = mockStore.getActions();
                assert.equal(actns.length, 3, "actual " + actns.length);
                let regAction = _.findIndex(actns, (x: any) => { return x.type === actionTypes.REGISTRATION_NEW_PATIENT_REGISTERED });
                assert.equal(regAction != -1, true);
                done();
            });
    });

    it('should clear user data on logout and redirect to select login page', ()=>{
        mockStore.dispatch(userActions.logOut());
            
        let actns = mockStore.getActions();
        let regAction = _.findIndex(actns, (x: any) => { return x.type === actionTypes.LOGIN_CLEAR });

        assert.equal(actns.length, 4, "actual " + actns.length);
        assert.equal(regAction != -1, true);
        
        assert.equal(actns[3].payload.args[0], Endpoints.site.root);
    });
});