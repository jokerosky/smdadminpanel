import 'mocha';
import * as _ from 'lodash';
import * as configureMockStore from 'redux-mock-store';
import thunk from 'redux-thunk';
import axios from 'axios';
import * as router from 'react-router';
import { assert } from 'chai';
import * as moxios from 'moxios';

import HospitalDataService from '../../src/services/api/hospitalDataService';
import * as siteActions from '../../src/actions/siteActions';
import { Endpoints } from '../../src/enums/endpoints';
import { ValidationError } from '../../src/utilities/validators';
import { mockErrors } from '../mocks/validation';

import { IStorageService } from '../../src/services/storageService';

import { SiteConstants } from '../../src/enums/siteConstants';

const middlewares = [thunk];
const mockStore = configureMockStore(middlewares);

describe('Site actions tests', () => {
  let store;

  beforeEach(() => {
    moxios.install();
    store = mockStore({});
  });

  afterEach(() => {
    moxios.uninstall()
  });

  it('loadHospitalIdAndApiKeys Should call endpoint and load api-keys and data with hospitalDataService producing 4 actions', (testDone) => {
      moxios.stubRequest(Endpoints.hospital.getApiSessionKey,
          {
              status: 200,
              response: {
                  data: [{ apiSessionId: 1 }]
              }
          });

      moxios.stubRequest(Endpoints.hospital.getHospitalIdWithKeys,
          {
              status: 200,
              response: {
                  data: [{ id: 1, key: 2, hospitalId: 3 }]
              }
          });

      moxios.stubRequest(Endpoints.hospital.getHospitalInfo.replace(/%id%/, '3'),
          {
              status: 200,
              response: {
                  data: [{ id: 3, name: "TetsHospital" }]
              }
          });
    store.dispatch(siteActions.loadHospitalIdAndApiKeys())
      .then((results) => {
        let actns = store.getActions();
        assert.equal(actns.length, 4);
        testDone();
      });
  });

  it('loadHospitalIdAndApiKeys Should return 2 actions and redirect to error if it fails', (testDone) => {
    moxios.stubRequest(/./, {
      status: 404,
      responseText: "not found"
    });

    store.dispatch(siteActions.loadHospitalIdAndApiKeys())
      .then((result) => {
        let actns = store.getActions();
        assert.equal(actns.length, 4);
        assert.equal(actns[3].payload.args[0], Endpoints.site.siteError);

        testDone();
      });
  });

  it('showValidationErrors should convert ValidationErrors to notifications', () => {
    store.dispatch(siteActions.showValidationErrors(mockErrors, "whatever"));
    let actns = store.getActions();
    assert.equal(actns.length, 1);
    assert.equal(actns[0].notifications.length, 3);
    assert.equal(actns[0].validationErrors.length, 1);
  });

  it('should save data to session storage', () => {
    let spy = 0;
    let data: any = 'test';
    let sstSrv = {
      add: (key: any, val: any) => { spy = val },
      del: () => { },
      get: () => { },
      clear: () => { }
    } as IStorageService;

    store.dispatch(siteActions.saveSessionData('key', data, sstSrv));

    assert.equal(spy, data);

  });


  it('should clear session data', () => {
    let spy = 0;

    let sstSrv = {
      add: (key: any, val: any) => { },
      del: () => { },
      get: () => { },
      clear: () => { spy++ }
    } as IStorageService;

    store.dispatch(siteActions.deleteSessionData(null, sstSrv));
    assert.equal(spy, 1);
  });

  it('should load data (according to session keys) from storage after page refresh', () => {
    let spy = 0;

    let sstSrv = {
      add: (key: any, val: any) => { },
      del: () => { },
      get: (key: string) => {
        if (_.find(SiteConstants.session, (skey) => { return skey == key })) {
          return key;
        }

      },
      clear: () => { spy++ }
    } as IStorageService;

    let action = siteActions.reloadStateAfterRefresh(sstSrv);
    _.each(SiteConstants.session, (key) => {
      assert.isOk(action.data[key]);
    })
  });
});