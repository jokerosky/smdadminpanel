import 'mocha';
import { assert } from 'chai';
import thunk from 'redux-thunk';
import * as configureMockStore from 'redux-mock-store';
import axios from 'axios';
import * as router from 'react-router';
import * as moxios from 'moxios';

import { Endpoints } from '../../src/enums/endpoints';
import * as providerActions from '../../src/actions/providerActions';
import ProviderService from '../../src/services/api/providerService';

import { mockStaffUserProfileResponse } from '../mocks/mockUserStaffProfileResponse';

let urls = [];
const middlewares = [thunk];
const getMockStore = configureMockStore(middlewares);

describe('Provider service tests. ', () => {
    let mockStore;

    beforeEach(() => {
        mockStore = getMockStore({});
        moxios.install();
    });


    afterEach(() => {
        moxios.uninstall();
    });

    it('should get profile data from server', (done) => {
        moxios.stubRequest(Endpoints.provider.userStaffProfile, {
            status: 200,
            response: {
                ...mockStaffUserProfileResponse
            }
        });
        
    mockStore.dispatch(providerActions.loadStaffProfile())
        .then((results)=>{
                let actns = mockStore.getActions();
                assert.equal(actns.length, 4, "there should be 3 actions after loadStaffProfile");
                done();
            });
    });
});