import 'mocha';
import { assert } from 'chai';

import { ObjectHelper } from '../../src/utilities/objectHelper';

describe('Object helper specs', ()=>{
    it('should lowercase first letter of properties',()=>{
        let testObj  = ObjectHelper.decapitalizeProperties(4);
        assert.equal(testObj, 4 );

        testObj = ObjectHelper.decapitalizeProperties('mockObj');
        assert.equal(testObj, 'mockObj' );

        testObj = ObjectHelper.decapitalizeProperties(true);
        assert.equal(testObj, true );

        testObj = ObjectHelper.decapitalizeProperties(null);
        assert.equal(testObj, null );
        
        let mockObj = {
            First:{
                Deep:[1,2,3,4],
                DeepAgain:'123'
            },
            Second:{},
            Third: 34,
            Fourth: [{A:2,B:'s'}, {}, {C:null, D:false}]
        }

        let newObj = ObjectHelper.decapitalizeProperties(mockObj);
        
        assert.isTrue(Object.keys(newObj).length > 0);

        for(let key in newObj){
            //no deep equal comparison
            assert.isNotTrue(key[0] === key[0].toUpperCase());
        }

    });

    it('test boolFromDataBaseString',()=>{
        let testObject = ObjectHelper.boolFromDataBaseString('True');
        assert.isTrue(testObject);

        testObject = ObjectHelper.boolFromDataBaseString('true');
        assert.isTrue(testObject);

        testObject = ObjectHelper.boolFromDataBaseString('False');
        assert.isFalse(testObject);

        testObject = ObjectHelper.boolFromDataBaseString('');
        assert.isFalse(testObject);

        testObject = ObjectHelper.boolFromDataBaseString('dwfrgfer');
        assert.isFalse(testObject);
    });

    it('test dataBaseStringFromBool',()=>{
        let testObject = ObjectHelper.dataBaseStringFromBool(true);
        assert.equal(testObject, 'True');

        testObject = ObjectHelper.dataBaseStringFromBool(false);
        assert.equal(testObject, 'False');

    });

    it('shuld remove trailing slash',()=>{
        let path = ObjectHelper.removeTrailingSlash('http://someurl.com/');
        assert.equal(path, 'http://someurl.com');  

        path = ObjectHelper.removeTrailingSlash('http://someotherurl.com');
        assert.equal(path, 'http://someotherurl.com');  

    })
});