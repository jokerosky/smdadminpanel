import { assert } from 'chai';
import { getNext, getPrevious, SortOrder } from '../../src/enums/siteEnums'; 


describe('Site Helpers test', ()=>{
    it('Should shift enum value to next one or the first one', ()=>{
                let state = SortOrder.asc;
        
                state =  getNext<SortOrder>(SortOrder, state);
                assert.equal(state, SortOrder.desc);
        
                state =  getNext<SortOrder>(SortOrder, state);
                assert.equal(state, SortOrder.no);
        
                state =  getNext<SortOrder>(SortOrder, state);
                assert.equal(state, SortOrder.asc);
    });

    it('Should shift enum value to previous one or the last', ()=>{
        let state = SortOrder.asc;

        state =  getPrevious<SortOrder>(SortOrder, state);
        assert.equal(state, SortOrder.no);

        state =  getPrevious<SortOrder>(SortOrder, state);
        assert.equal(state, SortOrder.desc);

        state =  getPrevious<SortOrder>(SortOrder, state);
        assert.equal(state, SortOrder.asc);
    });
})

