import 'mocha';
import { assert } from 'chai';

import * as types from '../../src/enums/actionTypes';
import { validateSet, validateAll, Validator, getCssFlags } from '../../src/utilities/validators';
import { mockValidationSet, mockValidationSetArray, mockErrors} from '../mocks/validation';

describe('Validation tests', () => {
    it('Should return false if value is empty or whitespace', () => {
        let value = "  ";
        let result = Validator.isNotEmptyOrWhitespace(value);
        assert.equal(result, false);

        value = "abc";
        result = Validator.isNotEmptyOrWhitespace(value);
        assert.equal(result, true);
    });

    it('Should return false if Password less then 5 symbols', () => {
        let result = Validator.isMinPassLength("1234");
        assert.equal(result, false);

        result = Validator.isMinPassLength("12345");
        assert.equal(result, true);
    });

    it('Should return false if value is not email', () => {
        let result = Validator.isEmail("12345");
        assert.equal(result, false);

        //for now using very simple email pattern, so not all values are valid emails
        result = Validator.isEmail("123@123.123");
        assert.equal(result, true);
    });

    it('Should return validation result according to validation rules', () => {
        let result = validateSet(mockValidationSet);

        assert.equal(result.isValid, false);
        assert.equal(result.errors.length, 1);
    });

    it('Should return validation result according to array of validation sets', () => {
        let result = validateAll(mockValidationSetArray);

        assert.equal(result.isValid, false);
        assert.equal(result.errors.length, 3);
    });

    it('Should return CssStyles object from SiteNotifications',()=>{
        let cssFlags = getCssFlags('whatever','is-error', mockErrors);

        assert.isObject(cssFlags);
        assert.equal(Object.keys(cssFlags).length, 1);
        
        assert.isOk(cssFlags["email"]);
    });


    it('Should validate Date time in different formats',()=>{
        let result = Validator.isDate('123');
        assert.isTrue(result);

        result = Validator.isDate('123','MM/DD/YYYY');
        assert.isFalse(result);
    });
});