import { assert } from 'chai';

import { EditorCreator } from '../../src/utilities/editorCreator';
import { EditorType } from '../../src/enums/editorTypes';
import { IMappedSetting } from '../../src/models/site/settingsMapping';


describe('Editor creator tests', ()=>{
    it('should create edit component according to passed type', ()=>{
        let setting =  {
            editorType: EditorType.edit,
            label: 'test',
            key: 'test',
            value: 'test'
        } as IMappedSetting;
        
        let editor = EditorCreator.getEditor(setting);

        assert.isObject(editor);
    });

    it('should return empty string if editor type is unknown',()=>{
        let setting = {
        } as IMappedSetting;

        let editor = EditorCreator.getEditor(setting);

        assert.equal(editor, '');
    });

});