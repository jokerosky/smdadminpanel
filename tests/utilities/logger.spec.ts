import 'mocha';
import {assert} from 'chai';
import * as _ from 'lodash';


import { Logger, ConsoleAppender, LogLevel, ILog, IAppender } from '../../src/utilities/logger';


describe('Logger tests', ()=>{
    it('should return default logger with name "Site logger" and "info" log level',()=>{
        let logger = Logger.getLogger();
        logger.setLogLevel(LogLevel.info);

        let info = LogLevel.info.toString();
        let infoKey = _.findKey(LogLevel, (value)=>{ return value == LogLevel.info});

        assert.isObject(logger);
        assert.isTrue((logger as Logger).name === 'Site logger');
        assert.isTrue((logger as Logger).logLevel === LogLevel.info);
        assert.equal((logger as Logger).appenders.length, 1);
    });

    describe('should log messages via reportes, has date time and log level with message',()=>{
        let spyMsg ;
        let spy;
        let logger;
        Logger.getLogger('Test').setLogLevel(LogLevel.info);

        beforeEach(()=>{
            spyMsg = '';
            spy = {
                logMessage(msg, level){
                    spyMsg = level + ' - ' + msg;
                },
                layout: {
                    formatMessage(msg){ return msg;}
                },
            } as IAppender ;

            logger = Logger.getLogger('Test') as Logger;
            logger.appenders = [ spy ];
        });

        it('info',()=>{
            logger.info('this is info');
            assert.equal(spyMsg,  LogLevel.info + ' - this is info');
        })

        it('debug',()=>{
            logger.debug('this is debug');
            assert.notEqual(spyMsg,  LogLevel.debug + ' - this is debug');

            logger.logLevel = LogLevel.debug;

            logger.debug('this is debug');
            assert.equal(spyMsg,  LogLevel.debug + ' - this is debug');

        })

        it('warning',()=>{
            logger.warn('this is warn');
            assert.equal(spyMsg,  LogLevel.warn + ' - this is warn');
        })

        it('error',()=>{
            logger.error('this is error');
            assert.equal(spyMsg,  LogLevel.error + ' - this is error');
        })

        it('critical',()=>{
            logger.critical('this is critical');
            assert.equal(spyMsg,  LogLevel.critical + ' - this is critical');
        })
    });

    it('should has date time and log level with message');
});