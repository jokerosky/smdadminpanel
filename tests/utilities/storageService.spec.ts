import 'mocha';
import { assert } from 'chai';
import { IStorageService, LocalStorageService, SessionStorageService, StorageService } from '../../src/services/storageService';

describe('StorageService tests', () => {
    let checkStorage = (srv:IStorageService ) => {
            let val = Math.random();

            assert.isNull(srv.get("key"));
            srv.add("key", val);
            assert.equal(val, srv.get("key"));
            srv.del("key");
            assert.isNull(srv.get("key"));
    };
    
    describe('Not extended class of Storage service', () => {
        it('should return null when get object by key', () => {
            let storageService = new StorageService();
            let val = storageService.get("key");
            assert.isNull(val, `it returned a value ${val} but should return null!`);
        });

        it('should take storage object as constructor parameter', () => {
            let  mockStore =  {
                getItem: (key:string) => { return `\"${key}\"`; },
                removeItem: () => { },
                setItem: () => { }
                };
            
            let storageService = new StorageService(mockStore);
            let val = storageService.get("key");
            assert.equal(val, "key");
        });
    });

    //actualy test should be invariant, but here, we use global! state. and sequence, so it is mini integration test
    describe('Local StorageService',()=>{
        it('should set, get, and remove value from global mocked Local storage',()=>{
            checkStorage(new LocalStorageService());
        })
    });

    describe('Session StorageService',()=>{
        it('should set, get, and remove value from global mocked Session storage',()=>{
            checkStorage(new SessionStorageService());
        })
    });


});