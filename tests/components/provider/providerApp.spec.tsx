import 'mocha';
import {assert} from 'chai';

import { ProviderApp } from '../../../src/components/provider/ProviderApp';
import { setup } from '../../setupComponentViaEnzyme';

import { getMockedHospital } from '../../mocks/hospital';
import { mockStaffUserProfile } from '../../mocks/mockUserStaffProfileResponse';

function getMockedProps () {
    return {
        loadProviderProfile: ()=>{},
        refreshPatientQueue: ()=>{},
        logout: ()=>{},
        hospital: getMockedHospital(),
        profile: mockStaffUserProfile,
        waitingListCount: 0
    }
}

describe('ProviderApp component tests',()=>{
    it('should toggle main menu by changing body element classes',()=>{

        let cmpnt = setup(ProviderApp, getMockedProps());

        (cmpnt.instance() as ProviderApp).toggleMainMenu();

        assert.isTrue(document.body.className.indexOf('is-main-nav') > 0);
    });
});