import 'core-js/modules/es6.object.assign';
import * as React from 'react';
import { mount, shallow } from 'enzyme';
import 'react-dom/test-utils';

import { assert } from 'chai';
import 'mocha';

import { BaseSnapComponent } from '../../../src/components/baseSnapComponent';
import { IValidatable } from '../../../src/utilities/validators';
import { IStorageService, StorageService } from '../../../src/services/storageService';
import storage from '../../mocks/browserStorage';


function setup(externalProps?: any, isMount?: boolean): any {

  let props: any = {
    dependencies: { localStorageService: new StorageService() },
    cssFlags: { shake: '' }
  };

  Object.assign(props, externalProps);
  if (isMount)
    return mount(<BaseSnapComponent {...props} />);
  else
    return shallow(<BaseSnapComponent {...props} />);
}

describe('Base Snap component via Enzyme', () => {
  beforeEach(() => {

  });

  it('should be Ivalidatable',()=>{
    let cmpnt = setup();
    assert.isFunction((cmpnt.instance() as IValidatable).validate, "no validation function");
    assert.isFunction((cmpnt.instance() as IValidatable).prepareValidationRules, "no validation rules preparation");

    assert.equal(Object.keys((cmpnt.instance() as IValidatable).validationErrors).length, 0);
    assert.equal(Object.keys((cmpnt.instance() as IValidatable).validationSets).length, 0);
  });

  it('should be rendered as null',()=>{
        let cmpnt = setup();
        let html = cmpnt.instance().render();
        assert.equal(html, null);
  });

  xit('should call validate input',()=>{

  });
});