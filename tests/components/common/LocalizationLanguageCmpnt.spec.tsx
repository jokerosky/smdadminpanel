import * as React from 'react';
import { assert, expect } from 'chai';

import { setup } from '../../setupComponentViaEnzyme';
import { LanguageCmpnt } from '../../../src/components/common/LocalizationLanguageCmpnt';

import { language } from '../../../src/enums/languages';

let props = {
   // selectedLanguage: 'us',
   changeLanguage: (lang: string) => { languageSelected = lang; }
}

let languageSelected = '';

describe('Localization Language test:', ()=>{
    
    it(' Change the language and get the data to load a new dictionary ', ()=>{
        let cmpnt = setup(LanguageCmpnt, props);
        let lang = cmpnt.find('[name="Language"]');

        assert.equal(lang.node.props.options.length, language.length);
        // default should be 'us'
        assert.equal(lang.node.props.value, language[0].value);

        lang.simulate('change', language[2]);
        lang = cmpnt.find('[name="Language"]');

        assert.equal(lang.node.props.value, language[2].value);
    });
});