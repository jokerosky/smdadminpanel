import 'core-js/modules/es6.object.assign';
import * as React from 'react';
import * as router from 'react-router';
import { mount, shallow } from 'enzyme';
import 'react-dom/test-utils';

import { assert } from 'chai';
import 'mocha';

import { LoginCmpnt, mapStateToProps } from '../../../src/components/common/LoginCmpnt';
import { BaseSnapComponent } from '../../../src/components/BaseSnapComponent';
import { ValidationError } from '../../../src/utilities/validators';
import * as userTypes from '../../../src/enums/userTypes';
import { Endpoints } from '../../../src/enums/endpoints';

import { IStorageService, StorageService } from '../../../src/services/storageService';
import { getMockedHospital } from '../../mocks/hospital';
import storage from '../../mocks/browserStorage';
import { vocabularyUS } from '../../mocks/mockVocabularyLanguage';

const mockedHospital = getMockedHospital();
let urls = [];

function setup(externalProps?: any, isMount?: boolean): any {

  let props: any = {
    dependencies: { localStorageService: new StorageService() },
    hospital: mockedHospital,
    userType: userTypes.guest,
    cssFlags: { shake: '' },
    language: vocabularyUS,
    showValidationErrors: (errs) => { },
    params: {userType: Object.keys(userTypes)[1]}, // set as patient;
    goToRoute: (action)=>{ urls.push(action.payload.args[0])}
  };

  Object.assign(props, externalProps);
  if (isMount)
    return mount(<LoginCmpnt {...props} />);
  else
    return shallow(<LoginCmpnt {...props} />);
}

describe('Login TS page via Enzyme', () => {
  
  beforeEach(() => {
    urls = [];
  });

  it('renders three inputs inside form ', () => {
    let loginCmpmt = setup();
    assert.equal(loginCmpmt.find('form input').length, 3);
  });

  it('should has hospital title', () => {
    let loginCmpmt = setup();
    assert.equal(loginCmpmt.find('h1.title').text().trim(), mockedHospital.brandName);
  });

  it('should not set error after it lost focus if value is empty', () => {
    let cmpnt = setup({}, true);
    cmpnt.find('#txtloginemail').node.value = '';
    cmpnt.find('#txtloginemail').simulate('blur');
    assert.equal(cmpnt.instance().props.cssFlags['txtloginemail'], undefined);
    assert.equal(Object.keys(cmpnt.instance().validationErrors).length, 0);
  });

  it('should validate input after it lost focus (blur event)', () => {
    let cmpnt = setup({}, true);
    //bad input
    cmpnt.find('#txtloginemail').node.value = 'mail.com';
    cmpnt.find('#txtloginemail').simulate('blur');
    assert.equal(cmpnt.instance().cssFlags['txtloginemail'], BaseSnapComponent.errClass);
    assert.equal(Object.keys(cmpnt.instance().validationErrors).length, 1, "no errors in array");
    //good input
    cmpnt.find('#txtloginemail').node.value = 'box@mail.com';
    cmpnt.find('#txtloginemail').simulate('change');
    cmpnt.find('#txtloginemail').simulate('blur');
    assert.equal(cmpnt.instance().cssFlags['txtloginemail'], undefined);
    assert.equal(Object.keys(cmpnt.instance().validationErrors).length, 0, "still have errors in array");
  });

  it('should validate all inputs and shake form if there are any errors', () => {
    let cmpnt = setup({}, true);
    cmpnt.instance().validate();

    assert.equal(Object.keys(cmpnt.instance().validationErrors).length, 2, "no errors in array");
    assert.equal(Object.keys(cmpnt.instance().cssFlags).length, 3);
  });

  it('should clean error css class when input starts againg', () => {
    let cmpnt = setup({}, true);
    cmpnt.instance().validate();
    assert.equal(cmpnt.instance().cssFlags['txtloginemail'], BaseSnapComponent.errClass);
    cmpnt.find('input#txtloginemail').simulate('change', { target: { id: 'txtloginemail', value: "aaaa" } });
    assert.equal(cmpnt.instance().cssFlags['txtloginemail'], undefined);
  });

  it('should validate login and password', () => {
    let cmpnt = setup({}, true);
    let isValid = true;
    isValid = cmpnt.instance().validate();
    assert.equal(isValid, false);

    //no validation for password strength

    assert.equal(Object.keys(cmpnt.instance().validationErrors).length, 2);

    cmpnt.find('input#txtloginemail').node.value = 'sample@mail.com';
    cmpnt.find('input#txtPassword').node.value = 'Password';
    isValid = cmpnt.instance().validate();
    assert.equal(isValid, true);
    assert.equal(Object.keys(cmpnt.instance().validationErrors).length, 0);
  });

  it('show error if login or password were incorrect', () => {
    let cmpt = setup({
      cssFlags: { shake: 'shake' }
    });
    assert.equal(cmpt.find('div.box').hasClass('shake'), true);
  });

  it('show Login title according to passed userType ', () => {
    let cmpt = setup({
      userType: userTypes.patient
    });

    assert.equal(cmpt.find('form h2').text().trim(), "Customer Login");

    cmpt = setup({
      userType: userTypes.provider
    });
    assert.equal(cmpt.find('form h2').text().trim(), "Provider Login");

    cmpt = setup({
      userType: userTypes.admin
    });
    assert.equal(cmpt.find('form h2').text().trim(), "Admin Login");

    cmpt = setup({
      userType: userTypes.guest
    });
    assert.equal(cmpt.find('form h2').text().trim(), "");
  });

  it('show Login title according to login route ', () => {
    let state = {
      hospital: {},
      user: {
        userTypeName: '',
        userType: 0
      },
      site: { 
        validation: { validations: [] },
        misc: { vocabulary: {} }
      }
    };

    let ownProps = {
      params: {
        userType: ''
      }
    };

    let result = mapStateToProps(state, ownProps);
    assert.equal(result.userType, userTypes.guest);

    ownProps = {
      params: {
        userType: 'provider'
      }
    };
    result = mapStateToProps(state, ownProps);
    assert.equal(result.userType, userTypes.provider)

    ownProps = {
      params: {
        userType: 'admin'
      }
    };
    result = mapStateToProps(state, ownProps);
    assert.equal(result.userType, userTypes.admin)
  });

  it('should has registration link for patient login type', () => {
    let loginCmpmt = setup({ userType: userTypes.patient });
    assert.equal(loginCmpmt.find('Link[to="' + Endpoints.site.patientSignup + '"]').length, 1);

    loginCmpmt = setup({ userType: userTypes.admin });
    assert.equal(loginCmpmt.find('Link[to="' + Endpoints.site.patientSignup + '"]').length, 0);
  });

  // where is an actual email value check? :) todo: rewrite test
  it('should be able to set "remember" password flag, and show it if was set ', () => {
    let mockStore: IStorageService = {
      get: (val) => {
        if (val === 'rememberLogin') return true;
        if (val === 'loginEmail') return 'email';
      },
      add: () => { },
      del: () => { },
      clear: () => { }
    };

    // remember login checked
    let cmpt = setup({
      dependencies: { localStorageService: mockStore }
    }, true);

    let attr = cmpt.find('div.checkbox input').node.checked;
    let email = cmpt.find('div.inputs__email input').node.value;
    assert.equal(attr, true);
    assert.equal(email, 'email');

    // not checked
    let store = new StorageService(storage);

    cmpt = setup({
      dependencies: { localStorageService: store }
    }, true);

    attr = cmpt.find('div.checkbox input').node.checked;
    assert.equal(attr, false);

    // checked and saved after click
    cmpt.instance().rememberMyEmail();
    attr = cmpt.find('div.checkbox input').node.checked;
    assert.equal(attr, true);
    assert.equal(store.get('rememberLogin'), true);
  });

  it('shoule map global validations to css flags', () => {
    let state = {
      user: {
        loginError: false,
        userType: 0,
        userTypeName: ''
      },
      site: {
        validation: {
          validations: [
            {
              elementId: 'txtloginemail',
              component: LoginCmpnt.name,
              errorClass: 'is-error',
              messages: ["many errors", "it is not email at all"]
            }
          ] as ValidationError[]
        },
        misc: { vocabulary: {} }
      }
    };
    let mappedProps = mapStateToProps(state, {params:{}});

    assert.equal(mappedProps.cssFlags['txtloginemail'], 'is-error');
  });

  it('should replace hospital is and user type id for snapMdAdmin',()=>{
    let spy:any= {};

    let cmpnt = setup({
      sendCredentials:(data)=>{ spy = data;},
      userType: userTypes.snapMdAdmin
    }, true);

    cmpnt.find('input#txtloginemail').node.value = 'sample@mail.com';
    cmpnt.find('input#txtPassword').node.value = 'Password';

    cmpnt.instance().logIn({preventDefault:()=>{}});

    assert.equal(spy.hospitalId, 0, 'hospitalId is not 0');
    assert.equal(spy.userTypeId, 3, 'user type is  not 0');
  })

  it('should redirect to site root if login user type not allowed',()=>{
    let cmpnt = setup({
      userType: userTypes.guest,
      params: {userType: 'whatever'}
    }, true);

    assert.equal(urls[0], Endpoints.site.root);
  })
});


