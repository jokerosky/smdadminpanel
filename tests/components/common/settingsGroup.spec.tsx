import { assert } from 'chai';
import { setup, setupConnected } from '../../setupComponentViaEnzyme';
import { SettingsGroup, ISettingsGroupProps } from '../../../src/components/common/SettingsGroupCmpnt'
import { mappedKeys } from '../../mocks/mockMappedSettings';
import { IMappedSetting } from '../../../src/models/site/settingsMapping';

describe('SettingsGroup component tests',()=>{
    var props = {
        title: 'Test',
        settings: mappedKeys.map(x=>Object.assign({},x))
    } as ISettingsGroupProps;

    it('should render all settings elements with children',()=>{
        let cmpnt = setup(SettingsGroup, props, false);
        let items = cmpnt.find('li');
        let childrenItems = cmpnt.find('.smd-setting-children .smd-full-width');
        
        assert.equal(items.length, 2);
        assert.equal(childrenItems.length, 2);
    });

    it('should call replaced onCahnge with settingsGroup as parameter',()=>{
        let a = 0;
        let b = 0;
        let callCount = 0;

        let obj = {
            change: ()=>{ 
                console.log('1');
                a = 1;
                callCount++;
            }
        }

        let func1 = obj.change;

        obj.change = ()=>{ 

            func1();
            console.log('2');
            b = 2;
            callCount++;
        };

        obj.change();

        assert.equal(a, 1);
        assert.equal(b, 2);
        assert.equal(callCount, 2);
    });

    it('should call props change function on setting changed',()=>{
        let spy = '';
        const testVal = 'A';
        let onSettingChanged = ()=>{ 
            spy = testVal;
            return spy;
        }
            
        var localProps = Object.assign({}, {
            onSettingChanged,
            title:'Test',
            settings: mappedKeys.map(x=>Object.assign({},x))
        } as ISettingsGroupProps);
        
        var cmpnt = setup(SettingsGroup, localProps, true);

        cmpnt.find('.smd-toggle#mECommerce input').simulate('click');

        assert.equal(spy, testVal);
    })
});