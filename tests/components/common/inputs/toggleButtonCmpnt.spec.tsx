import { assert } from 'chai';

import { setup } from '../../../setupComponentViaEnzyme';
import { ToggleButton, IToggleButtonProps } from '../../../../src/components/common/inputs/ToggleButtonCmpnt';

var spy = 0;

var props = {
    onChange:()=>{ spy++; },

} as IToggleButtonProps;

describe("ToggleButton tests",()=>{
    it('should call passed onChange function, change on/off state, and set focus to inner button',()=>{
        let cmpnt = setup(ToggleButton, props, true);

        let state = cmpnt.instance().state.isOn;
        cmpnt.find('input').simulate('click');

        let nextState = cmpnt.instance().state.isOn;
        let hasFocus = document.activeElement == cmpnt.find('button').node;

        assert.equal(spy, 1);
        assert.notEqual(state, nextState);
        assert.isTrue(hasFocus);
    });

    
});


