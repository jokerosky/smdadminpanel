import * as React from 'react';
import { assert, expect } from 'chai';
import { getFormattedDate, secondsToMinutesString } from '../../../src/utilities/converters'

describe('SnapMdAdmin Consultation Meetings top component View:', () => {
    it('test function secondsToMinutesString', () => {
        assert.equal(secondsToMinutesString(180), '3:0');
        assert.equal(secondsToMinutesString(30), '0:30');
        assert.equal(secondsToMinutesString(38), '0:38');
        assert.equal(secondsToMinutesString(61), '1:1');
    });

    it('test function getFormattedDate', () => {
        assert.equal(getFormattedDate('2016-12-07T00:20:00Z'), '7/12/2016 0:20');
        assert.equal(getFormattedDate('2016-12-07T07:15:00Z'), '7/12/2016 7:15');
        assert.equal(getFormattedDate('2016-12-07T09:30:00Z'), '7/12/2016 9:30');
        assert.equal(getFormattedDate('2016-12-07T10:45:00Z'), '7/12/2016 10:45');
    });
});