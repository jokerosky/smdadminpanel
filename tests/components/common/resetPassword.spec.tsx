import 'core-js/modules/es6.object.assign';
import * as React from 'react';
import { mount, shallow } from 'enzyme';
import 'react-dom/test-utils';

import { assert } from 'chai';
import 'mocha';

import { ResetPasswordCmpnt } from '../../../src/components/common/ResetPasswordCmpnt';
import { Hospital } from '../../../src/models/hospital';
import * as userTypes from '../../../src/enums/userTypes';

import { IStorageService, StorageService } from '../../../src/services/storageService';
import { getMockedHospital } from '../../mocks/hospital';
import storage from '../../mocks/browserStorage';

const mockedHospital = getMockedHospital();

function setup(externalProps?: any, isMount?: boolean): any {

  let props: any = {
    dependencies: { localStorageService: new StorageService() },
    hospital: mockedHospital,
    userType: userTypes.guest,
    cssFlags: { shake: '' }
  };

  Object.assign(props, externalProps);
  if (isMount)
    return mount(<ResetPasswordCmpnt {...props} />);
  else
    return shallow(<ResetPasswordCmpnt {...props} />);
}

describe('Reset password page via Enzyme', () => {
  beforeEach(() => {
  });

  it('should have input, button, "back" link, and register link if patient user type',()=>{
    let cmpnt = setup(
      {
        userType:userTypes.patient
      }
    );

    assert.equal(cmpnt.find('input.k-input').nodes.length,1);
    assert.equal(cmpnt.find('button.button__brand--verify').nodes.length,1);
    assert.equal(cmpnt.find('a.back').nodes.length,1);
    assert.equal(cmpnt.find('a.registration').nodes.length,1);
  });

  it('should show message when email with reset link was sent ',()=>{
    let props = {
        passwordResetSuccess: true,
    }
    let cmpnt = setup(props);

    assert.equal(cmpnt.find('input.k-input').nodes.length,0,'input still there');
    assert.equal(cmpnt.find('button.button__brand--verify').nodes.length,0,'button still there');
  });
});