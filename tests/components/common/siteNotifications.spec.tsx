import 'core-js/modules/es6.object.assign';
import * as React from 'react';
import { mount, shallow } from 'enzyme';
import 'react-dom/test-utils';

import { assert } from 'chai';
import 'mocha';

import { SiteNotificationsCmpnt } from '../../../src/components/common/SiteNotificationsCmpnt';
import { EventType } from '../../../src/enums/eventType';
import { mockSiteNotifications } from '../../mocks/mockSiteNotifications';

let functionWasCalled: boolean;

function setup(externalProps?: any, isMount?: boolean): any {

  let props: any = {
    notifications: mockSiteNotifications,
    removeNotification: ()=>{ functionWasCalled = true; }
  };

  Object.assign(props, externalProps);
  if (isMount)
    return mount(<SiteNotificationsCmpnt {...props} />);
  else
    return shallow(<SiteNotificationsCmpnt {...props} />);
}

describe('Site Notifications component via Enzyme', () => {
  beforeEach(() => {
  });

  it('Should  return name of event type as class string', () => {
    let cmpnt = setup();
    let className = (cmpnt.instance() as SiteNotificationsCmpnt).getClassName(EventType.regular);
    assert.equal(className, 'regular');
  });

  it('Should call remove action when user clicks on notification bar', () => {
    let cmpnt = setup();
    functionWasCalled = false;
    (cmpnt.instance() as SiteNotificationsCmpnt).removeNotification(mockSiteNotifications[0].notificationId);
    assert.equal(functionWasCalled, true);
  });
});