import { assert } from 'chai';
import 'mocha';
import * as _ from 'lodash';

import { setup } from '../../setupComponentViaEnzyme';

import { BaseSnapComponent } from '../../../src/components/BaseSnapComponent';
import Registration1StepCmpnt, {IRegistrationFirstStepData} from '../../../src/components/patient/Registration1StepCmpnt';
import { IValidatable } from '../../../src/utilities/validators';
import { IStorageService, StorageService } from '../../../src/services/storageService';
import storage from '../../mocks/browserStorage';


describe('Register patient 1 step component via Enzyme', () => {
  let prepareProps = function(spy:any){
    return {
      userTypeName: 'patient',
      onChange: ((val) => {Object.assign(spy, val) }),
      submit: (data)=>{ 
        Object.assign(spy, data)},
      setErrors: (errs)=>{Object.assign(spy, errs)},
      formData: {},
      language:{}
    };
  }

  beforeEach(() => {

  });


  it('should rendered with 3 inputs', () => {
    let spy = {};
    let cmpnt = setup(Registration1StepCmpnt, prepareProps(spy), true);
    assert.isOk(cmpnt);
    assert.equal(cmpnt.find('input').length, 3);
  });

  it('should change input value with onChange and google autocomplete place_changed', () => {
    let spy = {};
    let cmpnt = setup(Registration1StepCmpnt, prepareProps(spy), true);
    let addrsId = Registration1StepCmpnt.addressId;
    cmpnt.find(`#${addrsId}`).node.value = 'LA';
    cmpnt.find(`#${addrsId}`).simulate('change');
    
    assert.equal(cmpnt.instance().address, 'LA');
  });

  it('should validate inputs after "Get started" pressed and return validation errors if not valid',()=>{
    let spy = {};
    let cmpnt = setup(Registration1StepCmpnt, prepareProps(spy), true);
    //cmpnt.find(`button`).simulate('click'); //doesnt work for some reasone (validate undefined)
    cmpnt.instance().submit();
    assert.equal(Object.keys(spy).length, 2);
  })

  it('should return Registration first step data if valid',()=>{
    let spy = {};
    let cmpnt = setup(Registration1StepCmpnt, prepareProps(spy), true);
    let fname = 'Test';
    let lname = 'Testos';
    let addr = 'Testorio';

    cmpnt.find(`#${Registration1StepCmpnt.firstNameId}`).node.value = fname;
    cmpnt.find(`#${Registration1StepCmpnt.lastNameId}`).node.value = lname;
    cmpnt.find(`#${Registration1StepCmpnt.addressId}`).node.value = addr;
    cmpnt.find(`#${Registration1StepCmpnt.addressId}`).simulate('change');

    cmpnt.instance().submit();
    
    assert.equal((spy as IRegistrationFirstStepData).name.first, fname);
    assert.equal((spy as IRegistrationFirstStepData).name.last, lname);
    assert.equal((spy as IRegistrationFirstStepData).address, addr);
  });

  it('should render inputs with default values if they present', ()=>{
    let testVal = "Test";
    let spy = {};
    let newProps = Object.assign({}, prepareProps(spy), {formData:{
              name:{
                first:testVal+'1',
                last:testVal+'2'
              },
              address:testVal+'3' }} );
    let cmpnt = setup(Registration1StepCmpnt, newProps, true);

    assert.equal(cmpnt.find(`#${Registration1StepCmpnt.firstNameId}`).node.value, testVal+'1') ;
    assert.equal(cmpnt.find(`#${Registration1StepCmpnt.lastNameId}`).node.value, testVal+'2') ;
    assert.equal(cmpnt.find(`#${Registration1StepCmpnt.addressId}`).node.value, testVal+'3') ;
  });
});