import { assert } from 'chai';
import 'mocha';
import * as _ from 'lodash';

import { setup } from '../../setupComponentViaEnzyme';

import { BaseSnapComponent } from '../../../src/components/BaseSnapComponent';
import Registration2StepCmpnt from '../../../src/components/patient/Registration2StepCmpnt';
import { IValidatable } from '../../../src/utilities/validators';



describe('Register patient 2 step component via Enzyme', () => {
  let spy: any;
  let props = {
    userTypeName: 'patient',
    setStep: ((val) => { spy = val }),
    submit: (data) => { spy = data },
    setErrors: (errs) => { spy = errs },
    formData: { firstName: 'Test' },
    language:{
      VALIDATION_INVALID_EMIAL:"Email is invalid",
      VALIDATION_INVALID_PASSWORD: "Password is invalid",
      VALIDATION_EMPTY_STRING: "Value can't be empty",
      VALIDATION_INVALID_DATE: "Value is not a date",
      VALIDATION_NOT_DATE: "Value is not date",
      VALIDATION_FLAG_NOT_CHECKED: "Flag should be checked",
      VALIDATION_PASSWORDS_NOT_EQUAL: "Password and confirmation are not equal",
      ALIDATION_PLEASE_CORRECT_ERRORS: "Please correct the fields below",
      VALIDATION_ACCEPT_TERMS: "Please accept the terms & conditions",
      VALIDATION_SHOULD_BE_OLDER: "You should be at least %AGE% years old to register",
    }
  };

  beforeEach(() => {

  });


  it('should rendered with 5 inputs and recived props', () => {
    let cmpnt = setup(Registration2StepCmpnt, props, true);
    assert.isOk(cmpnt);
    assert.equal(cmpnt.find('input').length, 5);
    assert.equal(cmpnt.find('span.box__firstname').text(), "Test");
  });

  it('should set previous step (1) when "Back" button pressed', () => {
    let cmpnt = setup(Registration2StepCmpnt, props, true);

    cmpnt.find('.row button.one').simulate('click');

    assert.equal(spy, 1);
  });

  function verifyComponent(cmpnt: any, val:string, id: string) {

    cmpnt.find('input#' + id).node.value = val;
    cmpnt.find('input#' + id).simulate('blur');

    let elm = cmpnt.find('input#' + id);
    assert.isTrue(_.includes(elm.node.className, BaseSnapComponent.errClass), " for " + id);
  }

  it('should validate email and birthday inputs', () => {
    let cmpnt = setup(Registration2StepCmpnt, props, true);

    verifyComponent(cmpnt, "asd", Registration2StepCmpnt.emailId);
    verifyComponent(cmpnt, "123", Registration2StepCmpnt.birthDayId);
  });


  it('should validate all inputs when "Continue" button clicked',()=>{
    let cmpnt = setup(Registration2StepCmpnt, props, true);

    // very strange don`t ypu think so https://github.com/airbnb/enzyme/issues/308
    cmpnt.find('.button__brand--finish').simulate('submit');
    
    let errosCount = _.keys(cmpnt.instance().validationErrors).length;
    assert.equal(errosCount, 5);
  })

  it('should validate if password and confiramtion are equal',()=>{
    let cmpnt = setup(Registration2StepCmpnt, props, true);
    
    cmpnt.find('input#' + Registration2StepCmpnt.passwordId).node.value = "val";
    cmpnt.find('input#' + Registration2StepCmpnt.passConfiramtionId).node.value = "val1";

    cmpnt.find('.button__brand--finish').simulate('submit');

    let err = cmpnt.instance().validationErrors[Registration2StepCmpnt.passConfiramtionId];
    assert.equal(err.messages.length, 1);
  });
});