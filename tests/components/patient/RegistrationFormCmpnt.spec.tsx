import 'core-js/modules/es6.object.assign';
import * as React from 'react';

import { assert } from 'chai';
import 'mocha';

import { RegistrationFormCmpnt } from '../../../src/components/patient/RegistrationFormCmpnt';
import { IValidatable } from '../../../src/utilities/validators';
import { IStorageService, StorageService } from '../../../src/services/storageService';

import { mockErrors } from '../../mocks/validation';

import {setup, setupConnected} from '../../setupComponentViaEnzyme';

 let props: any = {
    dependencies: { localStorageService: new StorageService() },
    cssFlags: { shake: '' },
    hospital: { brandName: 'Test' },
    step: 1,
    registrationData: {}
  };

describe('Register patient form component via Enzyme', () => {
  beforeEach(() => {

  });

  it('should start with step equals 1 and show only one box__content div', () => {
    let cmpnt = setupConnected(RegistrationFormCmpnt, props, {user:{ userTypeName:'patient'}});
    assert.equal(cmpnt.find('RegistrationFormCmpnt').node.props.step, 1);
    assert.equal(cmpnt.find('div.box__content').length, 1);
  });

  it('should have three "tabs" (ul→li and div elements)', () => {
    let cmpnt = setup(RegistrationFormCmpnt, props);
    //google.maps.places.Autocomplete

    assert.equal(cmpnt.find('div > ul li').nodes.length, 3);
    //render nothing if step not setted up
    assert.equal(cmpnt.find('div.box__content').nodes.length, 0);

  });

  it('should render first error after first step validation fails', () => {
    let rootCmpnt = setupConnected(RegistrationFormCmpnt, props);
    let cmpnt = rootCmpnt.find('RegistrationFormCmpnt').node;
    cmpnt.setErrors({firstname: mockErrors[0]});
    
    assert.equal(Object.keys(cmpnt.validationErrors).length,1);
    assert.equal(cmpnt.cssFlags['hasErrors'], 'is-active');
    assert.equal(cmpnt.notificationText, mockErrors[0].messages[0]);
  });
  
});