import {assert} from 'chai';

import * as React from 'react';
import { setup } from '../../setupComponentViaEnzyme';

import { ISmdaClientSettingsCmpntProps, SmdaClientSettingsCmpnt, mapStateToProps, mapDispatchToProps } from '../../../src/components/snapMdAdmin/snapContentMenuCmpnts/SmdaClientSettings';
import { IMappedSetting } from '../../../src/models/site/settingsMapping';
import { hospitalModulesSettings, hospitalSettings } from '../../mocks/mockHospitalSettingsDTO';

let loadedSettings = {};
[...hospitalModulesSettings, ...hospitalSettings].forEach(x=>{loadedSettings[x.key] = x});
let clientDetails ;


let props = {
    hospitalName:'Test Hospital',
    loadedSettings: loadedSettings,
    clientDetails: {
        address: {
            address:'TEst adderess',
            addressObject:{
                addressText:'addressText',
                city:'city',
                country:'country',
                countryCode:'countryCode',
                line1:'line1',
                line2:'line2',
                postalCode:'postalCode',
                state:'state',
                stateCode:'stateCode'
            }
        }
    },

    settingChanged: (setting)=>{ },
    loadAllSettingsData:(id)=>{},
    language: {}
} as ISmdaClientSettingsCmpntProps;


describe('SnapMdAdminApp Settings View',()=>{
    it('Should remap loaded settings (for modules settings) on mounting',()=>{
        let instance = new SmdaClientSettingsCmpnt(props);
        instance.remapSettings(props);

    });

    it('Should remap loaded settings (for modules settings) on mounting',()=>{
        let cmpnt = setup( SmdaClientSettingsCmpnt, props, true);
        let instance = cmpnt.instance() as SmdaClientSettingsCmpnt;
        let iseCommerceOn = instance.state.modulesSettings.mappedSettings.find(x=>x.key ='mECommerce').value;
        
        assert.equal(iseCommerceOn, "False");
    });

    it('Should expand all groups by button click, and all collapsed by default',()=>{
        let cmpnt = setup( SmdaClientSettingsCmpnt, props, false);
        let instance = cmpnt.instance() as SmdaClientSettingsCmpnt;
        for(let key in instance.state.collapsedMap){
            assert.isTrue(instance.state.collapsedMap[key]);
        }
        cmpnt.find('.pull-left .btn-outline-secondary').simulate('click');
        
        for(let key in instance.state.collapsedMap){
            assert.isFalse(instance.state.collapsedMap[key]);
        }

    });

    it('Should collapse all groups by button click',()=>{
        let cmpnt = setup( SmdaClientSettingsCmpnt, props, false);
        cmpnt.find('.pull-left .btn-outline-dark').simulate('click');
        for(let key in cmpnt.instance().state.collapsedMap){
            assert.isTrue(cmpnt.instance().state.collapsedMap[key]);
        }
    });

    it('Should collect data for save action',()=>{
        let cmpnt = setup( SmdaClientSettingsCmpnt, props, true);
        let data = cmpnt.instance().collectSettingsToSave();

        assert.equal(data.length, 25);
        for(let i in data){
            assert.property(data[i],'key');
            assert.property(data[i],'value');    
        }
    });
});