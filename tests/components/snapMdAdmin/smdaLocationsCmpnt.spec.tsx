import * as React from 'react';
import { assert, expect } from 'chai';
import * as _ from 'lodash';

import { setup } from '../../setupComponentViaEnzyme';
import {ISmdaLocationsCmpntProps, SmdaLocationsCmpnt} from '../../../src/components/snapMdAdmin/snapContentMenuCmpnts/SmdaLocations';
import {organizations} from '../../mocks/mockLocationsOrganizations';
import { vocabularyUS } from '../../mocks/mockVocabularyLanguage';

let props = {
    hospitalId:1,
    hospitalName:'Emerald Dev',
    organizationsWithLocations:organizations,
    locationsForSelect: createObjectCodeSetsTypeForSelect(organizations),
    language:{vocabularyUS},

    loadLocations:(locationId:number)=>{},
    updateLocations:(locationId:number, organizationId:number, locationData:any, editorAction:string)=>{updateLocations = locationData},
    deleteLocation:(locationId:number, organizationId:number)=>{deleteLocations.locationId = locationId; deleteLocations.organizationId=organizationId;},
};
let updateLocations={
    hospitalId:-1,
    name:'',
    organizationId:-1,
    id:-1,
};
let deleteLocations={
    locationId:-1,
    organizationId:-1
};

function createObjectCodeSetsTypeForSelect(props){ 
    let locations = [];
    
    if(props.length === 0){
        return locations;
    }

    for(let index in props){
        locations.push({
            value:+index,
            label:props[index].name
        });
    }
    return locations;
}

describe('SnapMdAdminApp Locations View', ()=>{

    it('Should displaying records in a table',()=>{
        let cmpnt = setup(SmdaLocationsCmpnt, props, true);
        let local = cmpnt.instance() as SmdaLocationsCmpnt;
        let records = local.props.organizationsWithLocations[local.state.selectedOrganization].locations;
        let count = 0;

        for(let index in records){
            if(records[index].statusCode === 1){
                count++;
            }
        }

        assert.equal(count, cmpnt.find('tbody tr').length);
    });

    it('Should Modal window show', ()=>{
        let stateStart={
            modalVisible:false
        };
        let stateEnd={
            modalVisible:true
        };
        let cmpnt = setup(SmdaLocationsCmpnt, props, true);
        let local = cmpnt.instance() as SmdaLocationsCmpnt;

        assert.equal(local.state.modalVisible, stateStart.modalVisible);

        cmpnt.find('[name="Record"]').simulate('click');

        assert.equal(local.state.modalVisible, stateEnd.modalVisible);
    });

    it('Should click Add new record btn and add new record',()=>{
        let inputText = {
            currentTarget:{
                value: 'Organizations Locations'
            }
        };

        let cmpnt = setup(SmdaLocationsCmpnt, props, true);
        let local = cmpnt.instance() as SmdaLocationsCmpnt;

        cmpnt.find('[name="Record"]').simulate('click');

        let textInput = cmpnt.find('[name="Locations"]');
        textInput.props().onChange(inputText);

        cmpnt.find('[name="Success"]').simulate('click');
        
        assert.equal(updateLocations.name, inputText.currentTarget.value);
        assert.equal(updateLocations.hospitalId, props.hospitalId);

        assert.equal(updateLocations.organizationId, props.organizationsWithLocations[local.state.selectedOrganization].id);
    });

    it('Should click Edit btn and edit record',()=>{
        let inputText = {
            currentTarget:{
                value: 'Organizations Locations'
            }
        };

        let cmpnt = setup(SmdaLocationsCmpnt, props, true);
        let local = cmpnt.instance() as SmdaLocationsCmpnt;

        cmpnt.find('[name="0Edit"]').simulate('click');

        let textInput = cmpnt.find('[name="Locations"]');
        textInput.props().onChange(inputText);

        cmpnt.find('[name="Success"]').simulate('click');

        assert.equal(updateLocations.name, inputText.currentTarget.value);
        assert.equal(updateLocations.hospitalId, props.hospitalId);
        assert.equal(updateLocations.organizationId, props.organizationsWithLocations[local.state.selectedOrganization].id);
        assert.equal(updateLocations.id, local.state.locationId);
    });

    it('Should click Delete btn and delete record', ()=>{
        let cmpnt = setup(SmdaLocationsCmpnt, props, true);
        let local = cmpnt.instance() as SmdaLocationsCmpnt;

        cmpnt.find('[name="0Delete"]').simulate('click');
        
        assert.isTrue(deleteLocations.locationId > 0);
    });
});