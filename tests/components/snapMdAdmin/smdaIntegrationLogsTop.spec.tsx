import * as React from 'react';
import { assert, expect } from 'chai';
import * as _ from 'lodash';

import { setup } from '../../setupComponentViaEnzyme';
import { vocabularyUS } from '../../mocks/mockVocabularyLanguage';
import { ViewIntegrationLogsTopCmpnt } from '../../../src/components/snapMdAdmin/snapMdAdminMenuCmpnts/ViewIntegrationLogsTop';
import { integrationLogsMock } from '../../mocks/mockIntegrationLogs';

let props = {
    language: vocabularyUS,
    integrationLogs: integrationLogsMock,

    getIntegrationLogs: (fieldQuery: any[]) => { fields = fieldQuery; }
}

let fields = [];
let fieldsMock = [
    { key: 'messageTypes[]', value: 'Internal' },
    { key: 'messageTypes[]', value: 'Admin' },
    { key: 'messageTypes[]', value: 'Public' },
    { key: 'searchPhrase', value: '' },
    { key: 'sortAscending', value: true },
    { key: 'maxCount', value: null },
    { key: 'fromDate', value: 'Tue Dec 26 2017 11:40:27 GMT+0600' }
]

describe('SnapMdAdmin Integration Logs Top:', () => {

    it('test to enter and save a search phrase', () => {
        let cmpnt = setup(ViewIntegrationLogsTopCmpnt, props);
        let search = cmpnt.find('[name="SearchPhrase"]');
        let valueSearch = {
            currentTarget: {
                value: 'Info'
            }
        };
        assert.equal(search.props().value, '');
        search.simulate('change', valueSearch);
        search = cmpnt.find('[name="SearchPhrase"]');
        assert.equal(search.props().value, valueSearch.currentTarget.value);
    });

    it('test for selecting the sorting method', () => {
        let cmpnt = setup(ViewIntegrationLogsTopCmpnt, props);
        let logs = cmpnt.instance() as ViewIntegrationLogsTopCmpnt;
        let asc = cmpnt.find('[value="asc"]');
        let desc = cmpnt.find('[value="desc"]');

        assert.isTrue(logs.state.sortAsceding);
        desc.simulate('change');
        assert.isFalse(logs.state.sortAsceding);
        asc.simulate('change');
        assert.isTrue(logs.state.sortAsceding);
    });

    it('switch test Message types', () => {
        let cmpnt = setup(ViewIntegrationLogsTopCmpnt, props);
        let logs = cmpnt.instance() as ViewIntegrationLogsTopCmpnt;
        let internal = cmpnt.find('[name="messageTypesInternal"]');
        let admin = cmpnt.find('[name="messageTypeAdmin"]');
        let pub = cmpnt.find('[name="messageTypePublic"]');

        assert.isTrue(logs.state.messageTypesInternal);
        assert.isTrue(logs.state.messageTypeAdmin);
        assert.isTrue(logs.state.messageTypePublic);

        internal.simulate('change');
        admin.simulate('change');
        pub.simulate('change');

        assert.isFalse(logs.state.messageTypesInternal);
        assert.isFalse(logs.state.messageTypeAdmin);
        assert.isFalse(logs.state.messageTypePublic);
    });

    it('collecting data into an object, for querying and sending', () => {
        let cmpnt = setup(ViewIntegrationLogsTopCmpnt, props);
        let query = cmpnt.find('[name="query"]');

        query.simulate('click');

        let isTrue = false;
        for (let index in fieldsMock) {
            if (index !== '6') {
                if (_.isEqual(fieldsMock[index], fields[index])) {
                    isTrue = true;
                } else {
                    isTrue = false;
                    break;
                }
            }
        }

        assert.isTrue(isTrue);
    });

});