import { assert } from 'chai';
import * as _ from 'lodash';

import { setup } from '../../setupComponentViaEnzyme'

import { mockClientSummaryDTOs, mockClientSummaryDTOsAll } from '../../mocks/mockClientSummaryDTO';
import {mockLeftMenuNoSort, mockLeftMenuIdSortAscending, mockLeftMenuIdSortWaning, 
        mockLeftMenuNameSortAscending, mockLeftMenuNameSortWaning,
        mockLeftMenuSeatsSortAscending, mockLeftMenuSeatsSortWaning} from '../../mocks/mockLeftMenuCmpntSort';

import { SmdaLeftMenuCmpnt, ISmdaLeftMenuProps } from '../../../src/components/snapMdAdmin/SmdaLeftMenuCmpnt';

import { SortOrder, getNext } from '../../../src/enums/siteEnums'; 


let spy = null;

let props:ISmdaLeftMenuProps = {
    hospitalItems: mockClientSummaryDTOs,
    selectedId:null,
    seats:null,
    seatsAssigned:null,
    selectItem: (id:number)=>{ spy = id;},
    filters:{},
    filtersChanged:(filters: any)=>{ },
    getClientData:(filters: any)=>{ },
    language:{},
};

describe('SnapMdAdminApp Left Menu specifications',()=>{
    it('Should render menu items',()=>{
        let cmpnt = setup(SmdaLeftMenuCmpnt, props);
        let menu = cmpnt.instance() as SmdaLeftMenuCmpnt;
        let items = menu.renderItems(mockClientSummaryDTOs);
        
        assert.isArray(items);

    })

    it('Should set selected item on row click',()=>{
        let cmpnt = setup(SmdaLeftMenuCmpnt, props, true);
        cmpnt.find(`tbody tr`).first().simulate('click');

        assert.isTrue(spy>=0);
        
    });

    it('SHould filter items on filter cahnged',(done)=>{

        let cmpnt = setup(SmdaLeftMenuCmpnt, props, true);
        let menu = cmpnt.instance() as SmdaLeftMenuCmpnt;
        let filter = {id:'1', name:'snap'};
        let before = menu.state.filteredItems.length;
        menu.filterItems(filter, mockClientSummaryDTOs);
        let after = menu.state.filteredItems.length;
        assert.isTrue(before > after);

        done();
    });

    function sortBy(hospitalSort: any[], stateSort: string, sort: string){
      return  _.orderBy(hospitalSort, [stateSort],[sort]);
    }

    function sortEqals(arr1:any[], arr2:any[]){
        if(arr1.length != arr2.length) 
            return false
        let bool = false;
        for(let item in arr1){
            if(arr1[item].hospitalId === arr2[item].hospitalId && 
                arr1[item].hospitalName === arr2[item].hospitalName &&
                arr1[item].seats === arr2[item].seats)
                    bool = true;
            else
                return false;
        }
        return bool;
    }

    it('Should sort items: ID',()=>{
        let cmpnt = setup(SmdaLeftMenuCmpnt, props, true);
        let menu = cmpnt.instance() as SmdaLeftMenuCmpnt;
        let sort={ asc: 'asc', desc: 'desc', no: 'no'};
        let stateSort={ id:'hospitalId', name: 'hospitalName', seats: 'seats'};

        let filter={ sortKey:stateSort.id, sortOrder:sort.asc};
        menu.filterItems(filter, mockClientSummaryDTOs);
        let after = menu.state.filteredItems;
        let sortAscId = sortBy(mockClientSummaryDTOs, stateSort.id, sort.asc);
        assert.isTrue(sortEqals(after, sortAscId));
 
        filter={ sortKey:stateSort.id, sortOrder:sort.desc};
        menu.filterItems(filter, mockClientSummaryDTOs);
        let afterDesc = menu.state.filteredItems;
        let sortDescId = sortBy(mockClientSummaryDTOs, stateSort.id, sort.desc);
        assert.isTrue(sortEqals(afterDesc, sortDescId));
    });

    it('Should sort items: NAME',()=>{
        let cmpnt = setup(SmdaLeftMenuCmpnt, props, true);
        let menu = cmpnt.instance() as SmdaLeftMenuCmpnt;
        let sort={ asc: 'asc', desc: 'desc', no: 'no'};
        let stateSort={ id:'hospitalId', name: 'hospitalName', seats: 'seats'};

        let filter={ sortKey:stateSort.name, sortOrder:sort.asc};
        menu.filterItems(filter, mockClientSummaryDTOs);
        let afterNameAsc = menu.state.filteredItems;
        let sortAscName = sortBy(mockClientSummaryDTOs, stateSort.name, sort.asc);
        assert.isTrue(sortEqals(afterNameAsc, sortAscName));

        filter={ sortKey:stateSort.name, sortOrder:sort.desc};
        menu.filterItems(filter, mockClientSummaryDTOs);
        let afterDescName = menu.state.filteredItems;
        let sortDescName = sortBy(mockClientSummaryDTOs, stateSort.name, sort.desc);
        assert.isTrue(sortEqals(afterDescName, sortDescName));
    });

    it('Should sort items: SEATS',()=>{
        let cmpnt = setup(SmdaLeftMenuCmpnt, props, true);
        let menu = cmpnt.instance() as SmdaLeftMenuCmpnt;
        let sort={ asc: 'asc', desc: 'desc', no: 'no'};
        let stateSort={ id:'hospitalId', name: 'hospitalName', seats: 'seats'};

        let filter={ sortKey:stateSort.seats, sortOrder:sort.asc};
        menu.filterItems(filter, mockClientSummaryDTOs);
        let afterSeatsAsc = menu.state.filteredItems;
        let sortAscSeats = sortBy(mockClientSummaryDTOs, stateSort.seats, sort.asc);
        assert.isTrue(sortEqals(afterSeatsAsc, sortAscSeats));

        filter={ sortKey:stateSort.seats, sortOrder:sort.desc};
        menu.filterItems(filter, mockClientSummaryDTOs);
        let afterDescSeats = menu.state.filteredItems;
        let sortDescSeats = sortBy(mockClientSummaryDTOs, stateSort.seats, sort.desc);
        assert.isTrue(sortEqals(afterDescSeats, sortDescSeats));
    });

    
})