import * as React from 'react';
import { assert, expect } from 'chai';
import * as _ from 'lodash';

import { setup } from '../../setupComponentViaEnzyme';
import {ISmdaHospitalDocumentProps, SmdaHospitalDocumentCmpnt} from '../../../src/components/snapMdAdmin/snapContentMenuCmpnts/SmdaHospitalDocument';
import {doc, docType} from '../../mocks/hospitalDocument';
import RichEdit from '../../../src/components/common/RichEditCmpnt';
import { vocabularyUS } from '../../mocks/mockVocabularyLanguage';

let props ={
    hospitalId:1,
    hospitalName:'Emerald Dev',
    documentType:docType,
    hospitalDocument:doc,
    selectedHospitalDocumentId:1,
    language:{vocabularyUS},

    getDocument:(selectDocument:number, hospitalId:number)=>{selectDoc = selectDocument, isGetDocument = true;},
    saveDocument:(document:any)=>{saveDocument=document},
    deleteDocument:(document:any)=>{deleteDocument=document},
    selectDocument:(documentId:number)=>{},
}

let propsEdit = {
    text: '',
    textEditChange: (text: string)=>{txt=text}
}

let selectDoc;
let isGetDocument = false ;
let saveDocument = {
    documentText: '',
    documentType: 0,
    hospitalId: props.hospitalId
};
let deleteDocument = {
    documentText: 'default-text',
    documentType: 0,
    hospitalId: props.hospitalId
};
let txt;

let textRichEdit = '<span style="font-size:medium;color:#00cc00;background-color:#ffccff;"><strong>(No Consent to Treat agreement on file.) vbnmvbmn</strong></span>'
let textRishEditEnd = '&lt;span&nbsp;style=&quot;font-size:medium;color:#00cc00;background-color:#ffccff;&quot;&gt;&lt;strong&gt;(No&nbsp;Consent&nbsp;to&nbsp;Treat&nbsp;agreement&nbsp;on&nbsp;file.)&nbsp;vbnmvbmn&lt;/strong&gt;&lt;/span&gt;1234';

describe('SnapMdAdminApp Hospital Document View', ()=>{

    it('Should displaying components',()=>{
        let cmpntDocument = setup(SmdaHospitalDocumentCmpnt, props);
        let document = cmpntDocument.instance() as SmdaHospitalDocumentCmpnt;
        let cmpntRich = setup(RichEdit, propsEdit);

        let select = cmpntDocument.find('[name="Select"]');
        let richEdit = cmpntRich.find('[name="RichEdit"]');
        assert.equal( select.length, 1);
        assert.equal( richEdit.length, 1);
    });

    it('Should Select component ',()=>{
        let cmpntDocument = setup(SmdaHospitalDocumentCmpnt, props);
        let document = cmpntDocument.instance() as SmdaHospitalDocumentCmpnt;
        let doc = [];

        for(let index in docType){
            let input = {
                value: docType[index].documentType,
                label: docType[index].defaultDocumentText
            }
            doc.push(input);
        }
        document.setState({
            documentType:doc
        })
        
        let select = cmpntDocument.find('[name="Select"]');
        assert.equal(select.props().options.length, docType.length)
    });

    it('Should RichEdit components',()=>{
        let cmpntDocument = setup(SmdaHospitalDocumentCmpnt, props);
        let document = cmpntDocument.instance() as SmdaHospitalDocumentCmpnt;
        
        let docum = [];

        for(let index in docType){
            let input = {
                value: docType[index].documentType,
                label: docType[index].defaultDocumentText
            }
            docum.push(input);
        }
        document.setState({
            documentType:docum
        })

        let inputText = {
            value: docType[1].documentType,
            label: docType[1].defaultDocumentText
        };

        let select = cmpntDocument.find('[name="Select"]');

        assert.equal(document.state.selectedDocumentType, 1);
        select.props().onChange(inputText);
        assert.equal(document.state.selectedDocumentType, docType[1].documentType);
        assert.equal(document.state.selectedDocumentType, selectDoc);
        propsEdit.text = doc[0];

        let cmpntRich = setup(RichEdit, propsEdit);
        let richEdit = cmpntRich.find('[name="RichEdit"]');

        assert.equal(richEdit.props().children.props.value, textRichEdit);
    });

    it('Should Save text',()=>{
        let cmpntDocument = setup(SmdaHospitalDocumentCmpnt, props);
        let document = cmpntDocument.instance() as SmdaHospitalDocumentCmpnt;
        
        let docum = [];

        for(let index in docType){
            let input = {
                value: docType[index].documentType,
                label: docType[index].defaultDocumentText
            }
            docum.push(input);
        }
        document.setState({
            documentType:docum
        })

        let inputText = {
            value: docType[1].documentType,
            label: docType[1].defaultDocumentText
        };

        let select = cmpntDocument.find('[name="Select"]');

        select.props().onChange(inputText);

        propsEdit.text = doc[0];

        let cmpntRich = setup(RichEdit, propsEdit);
        let richEdit = cmpntRich.find('[name="RichEdit"]');

        richEdit.props().children.props.onChange(doc[0] + '1234');

        document.setState({
            textEdit:txt
        })
        
        let buttonSave = cmpntDocument.find('[name="Save"]');
        buttonSave.props().onClick();

        assert.equal(saveDocument.documentText, textRishEditEnd);
    });

    it('Should Delete text',()=>{
        let cmpntDocument = setup(SmdaHospitalDocumentCmpnt, props);
        let document = cmpntDocument.instance() as SmdaHospitalDocumentCmpnt;
        
        let docum = [];

        for(let index in docType){
            let input = {
                value: docType[index].documentType,
                label: docType[index].defaultDocumentText
            }
            docum.push(input);
        }
        document.setState({
            documentType:docum
        })

        let inputText = {
            value: docType[1].documentType,
            label: docType[1].defaultDocumentText
        };

        let select = cmpntDocument.find('[name="Select"]');

        select.props().onChange(inputText);

        propsEdit.text = doc[0];

        let cmpntRich = setup(RichEdit, propsEdit);
        let richEdit = cmpntRich.find('[name="RichEdit"]');

        richEdit.props().children.props.onChange(doc[0] + '1234');

        document.setState({
            textEdit:txt
        })
        
        let buttonSave = cmpntDocument.find('[name="Delete"]');
        buttonSave.props().onClick();

        assert.equal(deleteDocument.documentType, docType[1].documentType);
    });

});
