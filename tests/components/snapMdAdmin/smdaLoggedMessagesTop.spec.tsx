import * as React from 'react';
import { assert, expect } from 'chai';

import { setup } from '../../setupComponentViaEnzyme';
import { ViewLoggedMessagesTopCmpnt, countRecord } from '../../../src/components/snapMdAdmin/snapMdAdminMenuCmpnts/ViewLoggedMessagesTop';
import { vocabularyUS } from '../../mocks/mockVocabularyLanguage';

let props = {
    language: {vocabularyUS},
    sendingObjectForRequest: (queryProps) => { request = queryProps; }
}
let request;

let requestTest = [
    { key: "messageTypes[]", value: "Info" },
    { key: "messageTypes[]", value: "Error" },
    { key: "searchPrase", value: "" },
    { key: "sortAscending", value: true },
    { key: "maxCount", value: "Chose logs count" },
    { key: "fromDate", value: "Fri Dec 08 2017 11:58:07 GMT+0600" }
]

describe('SnapMdAdmin Logged Messages top component View:', () => {

    it('customer input test', () => {
        let cmpnt = setup(ViewLoggedMessagesTopCmpnt, props);
        let consultation = cmpnt.instance() as ViewLoggedMessagesTopCmpnt;
        let valueSearch = {
            currentTarget: {
                value: 'Info'
            }
        };

        let input = cmpnt.find('[name="SearchInput"]');
        assert.equal(input.props().value, '');

        input.simulate('change', valueSearch);
        input = cmpnt.find('[name="SearchInput"]');
        assert.equal(input.props().value, valueSearch.currentTarget.value);
        assert.equal(consultation.state.searchText, valueSearch.currentTarget.value);
    });

    it('max records count selection test', () => {
        let cmpnt = setup(ViewLoggedMessagesTopCmpnt, props);

        let select = cmpnt.find('[name="maxCount"]');
        assert.equal(select.props().options.length, countRecord.length);
    });

    it('check flag switching for sorting', () => {
        let cmpnt = setup(ViewLoggedMessagesTopCmpnt, props);
        let consultation = cmpnt.instance() as ViewLoggedMessagesTopCmpnt;

        let radio1 = cmpnt.find('[value="asc"]');
        let radio2 = cmpnt.find('[value="desc"]');

        assert.isTrue(consultation.state.dateSort);
        radio2.simulate('change');
        assert.isFalse(consultation.state.dateSort);
        radio1.simulate('change');
        assert.isTrue(consultation.state.dateSort);
        radio2.simulate('change');
        assert.isFalse(consultation.state.dateSort);
    });

    it('check flagging for Message types', () => {
        let cmpnt = setup(ViewLoggedMessagesTopCmpnt, props);
        let consultation = cmpnt.instance() as ViewLoggedMessagesTopCmpnt;
        let check1 = cmpnt.find('[name="info"]');
        let check2 = cmpnt.find('[name="error"]');

        assert.isTrue(consultation.state.info);
        check1.simulate('change');
        assert.isFalse(consultation.state.info);
        check1.simulate('change');
        assert.isTrue(consultation.state.info);

        assert.isTrue(consultation.state.error);
        check2.simulate('change');
        assert.isFalse(consultation.state.error);
        check2.simulate('change');
        assert.isTrue(consultation.state.error);
    });

    function equals(arr1, arr2) {
        let equal = false;
        if (arr1.length > 0 && arr2.length > 0) {
            for (let index in arr1) {
                if (arr1[index].key !== 'fromDate') {
                    if (arr1[index].key === arr2[index].key && arr1[index].value === arr2[index].value) {
                        equal = true;
                    } else {
                        return false;
                    }
                }
            }
        }
        return equal;
    }

    it('checking the creation of an object for the query', () => {
        let cmpnt = setup(ViewLoggedMessagesTopCmpnt, props);
        let query = cmpnt.find('[name="query"]');

        query.simulate('click');

        assert.isTrue(equals(request, requestTest));
    });

});