import { assert } from 'chai';

import { setup } from '../../setupComponentViaEnzyme'
import { SnapMdDashboardCmpnt, mapDispatchToProps } from '../../../src/components/snapMdAdmin/SnapMdDashboardCmpnt';
import * as Action from '../../../src/actions/snapMdAdminActions';


var arr =[];
var mockDispatch = (action)=>{
    arr.push(action)
};

describe('SnapMdAdminApp Dashboard specifications',()=>{

    it('SnapMdAdminApp if id and view then show the selected view',()=>{
            let result = mapDispatchToProps(mockDispatch);
            result.selectView(1, 'settings');

            let calledUrl = '/snapmdadmin/dashboard/1/settings';
            assert.equal(arr[1].payload.args[0], calledUrl);
    });

    
})