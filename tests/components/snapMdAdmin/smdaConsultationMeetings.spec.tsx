import * as React from 'react';
import { assert, expect } from 'chai';
import * as _ from 'lodash';

import { setup } from '../../setupComponentViaEnzyme';

import { IViewConsultationMeetingsProps, ViewConsultationMeetingsCmpnt,  } from '../../../src/components/snapMdAdmin/snapMdAdminMenuCmpnts/ViewConsultationMeetings';
import { mockClientSummaryDTOs } from '../../mocks/mockClientSummaryDTO';
import { callText, callType, countRecord } from '../../../src/components/snapMdAdmin/snapMdAdminMenuCmpnts/ViewConsultationMeetingsTop';
import { consultationMeetings } from '../../mocks/consultationMeetings';
import { vocabularyUS } from '../../mocks/mockVocabularyLanguage';

function hospitalName() {
    let client: Array<any> = [];
    for (let index in mockClientSummaryDTOs) {
        client.push({
            value: index,
            label: mockClientSummaryDTOs[index].hospitalName
        });
    }
    return client;
}

let props = {
    hospitalNamesForSelect: hospitalName(),
    consultationMeetingsRecords: consultationMeetings,
    language: {vocabularyUS},
    hospitals: mockClientSummaryDTOs,

    getConsultationMeetings: (requestArrayField: any[]) => {requestField=requestArrayField;}
}

let requestField:Array<any>;

describe('SnapMdAdmin Consultation Meetings table component View:', () => {

    it('should create table and be able to change columns number', ()=> {

        let index = 'callerName';
        let cmpnt = setup(ViewConsultationMeetingsCmpnt, props, true);

        let tableRecords = cmpnt.find('[id="tableRecords"]');
        let tableButtons = cmpnt.find('[id="tableButtons"]');
        assert.equal(tableRecords.props().children[0].props.children.props.children.length, 15);
        assert.equal(tableButtons.props().children.props.children.props.children.length, 15);

        cmpnt.find('[id="'+index+ '"]').simulate('click');
        let columns = _.findIndex(cmpnt.find('[id="tableRecords"]').props().children[0].props.children.props.children, {key: index});

        assert.equal(columns, -1);
    });

    it('should render rows items', ()=>{
        let cmpnt = setup(ViewConsultationMeetingsCmpnt, props, true);

        let tableRecords = cmpnt.find('[id="tableRecords"]');
        assert.equal(tableRecords.props().children[1].props.children.length, consultationMeetings.length);
    });

});