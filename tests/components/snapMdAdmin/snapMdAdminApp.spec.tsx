import { setupConnected, setup } from '../../setupComponentViaEnzyme'
import { SnapMdAdminApp, mapDispatchToProps } from '../../../src/components/snapMdAdmin/SnapMdAdminApp';

import { assert } from 'chai';
import { vocabularyUS } from '../../mocks/mockVocabularyLanguage';

let flag = true;
let getClientsCalledFlag = false;
let getListsCalledFlag = false;

let paths = '';

let props = {
    params:{id:''},//like routing works
    routes:[{},{},{path:'dashboard(/:id)'},{path:'details'}],
    language: {vocabularyUS},

    selectClient:(id:number)=>{},
    selectView:(view:string)=>{},
    goTo:(path: string)=>{paths = path;},
    goToClientsPage:(e?:any)=>{},
    

    toggleHospitalStyles:(val)=>{ flag = val;},
    getClients: ()=>{ getClientsCalledFlag = true;},
    getLists: ()=>{getListsCalledFlag = true},
    logOut: ()=>{},
    changeLanguage :(symbolOfLanguage: string) => {}

};

let state = {
    site:{
        misc:{
            enableHospitalStyles:true,
            language: 'es'
        }
    },
    hospital:{
        settings:{
            language: {}
        }
    }
};

describe('SnapMdAdminApp specifications',()=> {
    it('Should disable hospitall styles on component mounting and enable on unmounting, and load snapmdadmin data (clients, lists...)',
        () => {
            props.params.id = '0';

            var cmpnt = setupConnected(SnapMdAdminApp, props, state);
            assert.isFalse(flag);

            assert.isTrue( getClientsCalledFlag);
            assert.isTrue( getListsCalledFlag);

            cmpnt.unmount();
            assert.isTrue(flag);            
        });
    
    it('Invalid URL redirect to dashboard',()=>{
        props.params.id = 'fsdfsd'
        var cmpnt = setupConnected(SnapMdAdminApp, props, state);

        assert.equal(paths, '/snapmdadmin/dashboard');
    });

    it('When in URL only id redirect to details',()=>{
        props.params.id = '1'
        var cmpnt = setupConnected(SnapMdAdminApp, props, state);
        assert.equal(paths, '/snapmdadmin/dashboard/1/details');
    });

})