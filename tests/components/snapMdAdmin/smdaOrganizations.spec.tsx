import * as React from 'react';
import { assert, expect } from 'chai';
import * as _ from 'lodash';

import { setup } from '../../setupComponentViaEnzyme';
import { ISmdaOrganizationsCmpntProps, SmdaOrganizationsCmpnt } from '../../../src/components/snapMdAdmin/snapContentMenuCmpnts/SmdaClientOrganizations';
import { organizationType, organization } from '../../mocks/mockOrganizations';
import {EditorActions} from '../../../src/enums/siteEnums';
import { vocabularyUS } from '../../mocks/mockVocabularyLanguage';

let props={
    hospitalId:1,
    hospitalName: 'Emerald Dev',
    organizationTypes: organizationType,
    organization: organization,
    language:{vocabularyUS},

    loadOrganizations:(typeOrganization:number, hospitalId:number)=>{},
    addAndUpdateOrganization:(organization:any, methodAddOrUpdate:string)=>{organizations = organization; method = methodAddOrUpdate;}
};

let organizations={
    hospitalId: -1,
    name: '',
    organizationTypeId: -1,
    id: -1
};
let method='';

describe('SnapMdAdminApp Organization View', ()=>{

    it('Should Displaying the list in select',()=>{
        let cmpnt = setup(SmdaOrganizationsCmpnt, props);
        let org = cmpnt.instance() as SmdaOrganizationsCmpnt;
        let orgType=[];
        for(let index in organizationType){
            let iption ={
                value: organizationType[index].id,
                label: organizationType[index].description
            }
            orgType.push(iption);
        }
        org.setState({
            organizations: orgType,
            selectedOption: 1
        })

        let select = cmpnt.find('[name="Select"]');
        assert.equal(select.props().options.length, org.state.organizations.length);
        assert.equal(select.props().value.description, org.state.organizations[0].label);
    });

    it('Should displaying records in a table', ()=>{
        let cmpnt = setup(SmdaOrganizationsCmpnt, props, true);
        let org = cmpnt.instance() as SmdaOrganizationsCmpnt;

        assert.equal(org.props.organization.length, cmpnt.find('tbody tr').length);
    });

    it('Should click Add new record btn and add new record', ()=>{
        let inputText = {
            currentTarget:{
                value: 'Clinica'
            }
        };

        let cmpnt = setup(SmdaOrganizationsCmpnt, props, true);
        let org = cmpnt.instance() as SmdaOrganizationsCmpnt;

        cmpnt.find('[name="Record"]').simulate('click');

        let textInput = cmpnt.find('[name="Organization"]');
        textInput.props().onChange(inputText);


        cmpnt.find('[name="Success"]').simulate('click');

        assert.equal(organizations.name, inputText.currentTarget.value);
        assert.equal(method, EditorActions.add);
    });

    it('Should click Edit btn and update record', ()=>{
        let inputText = {
            currentTarget:{
                value: 'Clinica'
            }
        };

        let cmpnt = setup(SmdaOrganizationsCmpnt, props, true);
        let org = cmpnt.instance() as SmdaOrganizationsCmpnt;

        cmpnt.find('#0Edit').simulate('click');

        assert.equal(cmpnt.find('[name="Organization"]').node.value, organization[0].name);

        let textInput = cmpnt.find('[name="Organization"]');
        textInput.props().onChange(inputText);

        cmpnt.find('[name="Success"]').simulate('click');
        
        assert.equal(organizations.name, inputText.currentTarget.value);
        assert.equal(method, EditorActions.edit);
    });

    it('Should Modal window show', ()=>{
        let stateStart={
            modalVisible:false
        };
        let stateEnd={
            modalVisible:true
        };
        let cmpnt = setup(SmdaOrganizationsCmpnt, props, true);
        let org = cmpnt.instance() as SmdaOrganizationsCmpnt;

        assert.equal(org.state.modalVisible, stateStart.modalVisible);

        cmpnt.find('[name="Record"]').simulate('click');

        assert.equal(org.state.modalVisible, stateEnd.modalVisible);
    });
});