import * as React from 'react';
import { assert, expect } from 'chai';
import * as _ from 'lodash';

import { setup } from '../../setupComponentViaEnzyme';
import { SmdaAddNewClientCmpnt, createObjectHospitalTypesForSelect } from '../../../src/components/snapMdAdmin/SmdaAddNewClient';
import { vocabularyUS } from '../../mocks/mockVocabularyLanguage';
import { mockClientSummaryDTOsAll } from '../../mocks/mockClientSummaryDTO';
import { hospitalType } from '../../mocks/mockHospitalType';

let props = {
    language: vocabularyUS,
    hospitalType: hospitalType,
    hospitalTypeForSelect: createObjectHospitalTypesForSelect(hospitalType),
    hospitals: mockClientSummaryDTOsAll.data,

    addNewClient: (newClient: any, hospitalSet: any, additionalSet: any) => {
        response = newClient;
        hospitalSettings = hospitalSet;
        additionalSettings = additionalSet;
    }
}

let phoneEdit = {
    currentTarget: {
        value: '+7 (999) 999-99-99'
    }
}

let emailEdit = {
    currentTarget: {
        value: 'Email'
    }
}

let nameEdit = {
    currentTarget: {
        value: 'Name Client'
    }
};

let response, hospitalSettings, additionalSettings, hospitalId;

let queriFieldMock = {
    appointmentsContactNumber: "",
    brandColor: "#d14a43",
    brandName: "",
    brandTitle: "",
    children: 0,
    cliniciansCount: 0,
    consultationCharge: 0,
    contactNumber: phoneEdit.currentTarget.value,
    ePrescriptionGateWay: "",
    email: emailEdit.currentTarget.value,
    hospitalCode: "",
    hospitalDomainName: "",
    hospitalId: null,
    hospitalImage: "",
    hospitalName: nameEdit.currentTarget.value,
    hospitalType: 77,
    iTDeptContactNumber: "",
    insuranceValidationGateWay: "",
    isActive: null,
    nPINumber: 0,
    paymentGateWay: "",
    smsGateway: "",
    totalUsersCount: 0
}

let mockHospitalSettings = {
    GenericMobileAppEnabled: false,
    mAddressValidation: false,
    mAdminMeetingReport: false,
    mAnnotation: false,
    mClinicianSearch: false,
    mConsultationVideoArchive: false,
    mDisablePhoneConsultation: false,
    mECommerce: false,
    mEncounterGeoLocation: false,
    mEPerscriptions: false,
    mEPSchedule1: false,
    mFileSharing: false,
    mHideDrToDrChat: false,
    mHideForgotPasswordLink: false,
    mHideOpenConsultation: false,
    mHidePaymentBeforeWaiting: false,
    mIFOnDemand: false,
    mIFScheduled: false,
    mIncludeDirections: false,
    mInsVerification: false,
    mInsVerificationBeforeWaiting: false,
    mInsVerificationDummy: false,
    mIntakeForm: false,
    mKubi: false,
    mMedicalCodes: false,
    mOnDemand: false,
    mOnDemandHourly: false,
    mOrganizationLocation: false,
    mRxNTEHR: false,
    mRxNTPM: false,
    mShowCTTOnScheduled: false,
    mTextMessaging: false,
    mVideoBeta: false,
    mShowAppointmentWizard: false,
    mShowNowButton: false
}

let mockAdditionalSettings = {
    BrandBackgroundColor: "#fff",
    BrandBackgroundImage: "",
    BrandBackgroundLoginImage: "",
    BrandTextColor: "#fff",
    ContactUsImage: "",
    language: "en"
}

describe('SnapMdAdmin AddNewClient View: ', () => {

    it('Checking the drawing of components', () => {
        let cmpnt = setup(SmdaAddNewClientCmpnt, props);

        assert.equal(cmpnt.find('#nameInput').length, 1);
        assert.equal(cmpnt.find('#emailInput').length, 1);
        assert.equal(cmpnt.find('#phoneInput').length, 1);
        assert.equal(cmpnt.find('#hospitalType').length, 1);
    });

    it('Checking the formation and sending of the request object to create a client', () => {
        let cmpnt = setup(SmdaAddNewClientCmpnt, props);
        let nameInput = cmpnt.find('#nameInput');
        let emailInput = cmpnt.find('#emailInput');
        let phoneInput = cmpnt.find('#phoneInput');
        let hospitalType = cmpnt.find('#hospitalType');

        nameInput.props().onChange(nameEdit);
        assert.equal(cmpnt.find('#nameInput').node.props.value, nameEdit.currentTarget.value);        

        emailInput.props().onChange(emailEdit);
        assert.equal(cmpnt.find('#emailInput').node.props.value, emailEdit.currentTarget.value);

        phoneInput.props().onChange(phoneEdit);
        assert.equal(cmpnt.find('#phoneInput').node.props.value, phoneEdit.currentTarget.value);
        cmpnt.find('#RegisterUser').simulate('click');

        assert.isTrue(_.isEqual(response, queriFieldMock));
        assert.isTrue(_.isEqual(hospitalSettings, mockHospitalSettings));
        assert.isTrue(_.isEqual(additionalSettings, mockAdditionalSettings));
    });

    it('Validation testing, when the name field is empty, the styles change', ()=>{
        let cmpnt = setup(SmdaAddNewClientCmpnt, props);
        let divInput = cmpnt.find('.smd-validate-border');
        let label = cmpnt.find('.smd-validate-color-text');

        assert.equal(divInput.length, 0);
        assert.equal(label.length, 0);
        cmpnt.find('#RegisterUser').simulate('click');

        divInput = cmpnt.find('.smd-validate-border');
        label = cmpnt.find('.smd-validate-color-text');
        assert.equal(divInput.length, 1);
        assert.equal(label.length, 1);
    });

    it('Validation testing, if the email field is not correctly populated, the styles change', ()=>{
        let cmpnt = setup(SmdaAddNewClientCmpnt, props);
        let divInput = cmpnt.find('.smd-validate-border');
        let label = cmpnt.find('.smd-validate-color-text');
        let emailInput = cmpnt.find('#emailInput');

        assert.equal(divInput.length, 0);
        assert.equal(label.length, 0);

        emailInput.props().onChange(emailEdit);
        assert.equal(cmpnt.find('#emailInput').node.props.value, emailEdit.currentTarget.value);
        
        cmpnt.find('#emailInput').simulate('blur');
        divInput = cmpnt.find('.smd-validate-border');
        label = cmpnt.find('.smd-validate-color-text');
        assert.equal(divInput.length, 1);
        assert.equal(label.length, 1);

        emailEdit.currentTarget.value = 'email@mail.to';
        emailInput.props().onChange(emailEdit);
        assert.equal(cmpnt.find('#emailInput').node.props.value, emailEdit.currentTarget.value);

        cmpnt.find('#emailInput').simulate('blur');
        divInput = cmpnt.find('.smd-validate-border');
        label = cmpnt.find('.smd-validate-color-text');
        assert.equal(divInput.length, 0);
        assert.equal(label.length, 0);
    });

});