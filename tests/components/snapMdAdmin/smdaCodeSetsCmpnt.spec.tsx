import * as React from 'react';
import { assert, expect } from 'chai';
import * as _ from 'lodash';

import { setup } from '../../setupComponentViaEnzyme';
import {ISmdaCodeSetsProps, SmdaCodeSetsCmpnt} from '../../../src/components/snapMdAdmin/snapContentMenuCmpnts/SmdaCodeSets';
import {CodeSetsDTO} from '../../../src//models/DTO/CodeSetsDTO';
import {getListCodeSetss} from '../../mocks/mockSnapmdAdminActionCreator';
import {codeSets} from '../../mocks/mockSnapMdAdminCodeSets';
import {Endpoints} from '../../../src/enums/endpoints';
import { vocabularyUS } from '../../mocks/mockVocabularyLanguage';

let getListCodeSets = JSON.parse(getListCodeSetss);
let props ={
    hospitalId:1,
    hospitalName:'Emerald Dev',
    codeSetsTypeForSelect:createObjectCodeSetsTypeForSelect(getListCodeSets),
    codeSetsType:getListCodeSets,
    codeSetsTypeId:1,
    codeSets:codeSets,
    language:{vocabularyUS},

    loadCodes:(hospitalId:number, codeSetsTypeId:number)=>{},
    codesAddOrUpdate:(codeSet:CodeSetsDTO, type:string)=>{ codesAdd = codeSet, methodType = type},
    saveSelectedCodeSetsTypeId:(codeSetsTypeId:number)=>{},
}

let methodType = '';

let codesAdd: CodeSetsDTO={
    CodeId:null,
    CodeSetId:-1,
    Description:"",
    DisplayOrder:-1,
    hospitalId:-1,
    HospitalId:0,
    IsActive:false,
    PartnerCode:"",
};

let codesUpdate={
    
};

function createObjectCodeSetsTypeForSelect(props){ 
    let codes=[];
    for(let index in props){
        codes.push({
            value:+index+1,
            label:props[index].Description
        });
    }
    return codes;
}

describe('SnapMdAdmin CodeSets View', ()=>{

    it('Should displaying components', ()=>{
        let cmpntDocument = setup(SmdaCodeSetsCmpnt, props);
        let document = cmpntDocument.instance() as SmdaCodeSetsCmpnt;

        let select = cmpntDocument.find('[name="Select"]');
        let table = cmpntDocument.find('table');
        let button = cmpntDocument.find('[name="Select"]');

        assert.equal( select.length, 1);
        assert.equal( table.length, 1);
        assert.equal( button.length, 1);
    });

    it('Should Select component',()=>{
        let cmpntDocument = setup(SmdaCodeSetsCmpnt, props);
        let codeSet = cmpntDocument.instance() as SmdaCodeSetsCmpnt;

        let code = [];
        for(let index in getListCodeSets){
            let input = {
                value: getListCodeSets[index].documentType,
                label: getListCodeSets[index].defaultDocumentText
            }
            code.push(input);
        }
        codeSet.setState({
            codeSetType: code
        });

        let select = cmpntDocument.find('[name="Select"]');
        assert.equal(select.props().options.length, getListCodeSets.length);
    });

    it('Should displaying records in a table',()=>{
        let cmpntDocument = setup(SmdaCodeSetsCmpnt, props);
        let codeSet = cmpntDocument.instance() as SmdaCodeSetsCmpnt;

        let table = cmpntDocument.find('table');
        assert.equal(table.props().children[1].props.children.length, props.codeSets.length);
    });

    it('Should show Modal window ',()=>{
        let stateStart={
            modalVisible:false
        };
        let stateEnd={
            modalVisible:true
        };
        let cmpnt = setup(SmdaCodeSetsCmpnt, props, true);
        let codeSet = cmpnt.instance() as SmdaCodeSetsCmpnt;

        assert.equal(codeSet.state.modalVisible, stateStart.modalVisible);

        cmpnt.find('[name="Record"]').simulate('click');

        assert.equal(codeSet.state.modalVisible, stateEnd.modalVisible);

        cmpnt.find('[name="Cancel"]').simulate('click');

        assert.equal(codeSet.state.modalVisible, stateStart.modalVisible);

    });

    it('Should click Add new record btn and add new record',()=>{
        let inputText = {
            currentTarget:{
                value: 'Organizations Code Sets'
            }
        };

        let cmpnt = setup(SmdaCodeSetsCmpnt, props, true);
        let codeSet = cmpnt.instance() as SmdaCodeSetsCmpnt;

        cmpnt.find('[name="Record"]').simulate('click');

        let input = cmpnt.find('[name="Description"]');
        input.props().onChange(inputText);
        
        let check = cmpnt.find('#check');
        check.props().onChange(true);

        cmpnt.find('[name="Success"]').simulate('click');

        assert.equal(codesAdd.Description, inputText.currentTarget.value);
        assert.isTrue(codesAdd.IsActive);
        assert.equal(codesAdd.DisplayOrder, codeSet.props.codeSets.length+1);
        assert.equal(codeSet.state.methodType, methodType);
    });

    it('Should click Edit btn and edit record',()=>{
        let inputText = {
            currentTarget:{
                value: 'Organizations Code Sets'
            }
        };

        let cmpnt = setup(SmdaCodeSetsCmpnt, props, true);
        let codeSet = cmpnt.instance() as SmdaCodeSetsCmpnt;

        cmpnt.find('[name="0Edit"]').simulate('click');

        assert.equal(codeSet.state.Description, props.codeSets[0].Description);

        let input = cmpnt.find('[name="Description"]');
        input.props().onChange(inputText);
        
        cmpnt.find('[name="Success"]').simulate('click');

        assert.equal(codesAdd.Description, inputText.currentTarget.value);
        assert.equal(codesAdd.CodeId, props.codeSets[0].CodeId);
        assert.equal(codesAdd.DisplayOrder, props.codeSets[0].DisplayOrder);
    });
});