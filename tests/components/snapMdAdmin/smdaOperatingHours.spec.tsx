import * as React from 'react';
import { assert, expect } from 'chai';
import * as _ from 'lodash';
import { mount, shallow } from 'enzyme';

import { setup } from '../../setupComponentViaEnzyme';
import { IOpHoursProps, SmdaClientOpHoursCmpnt } from '../../../src/components/snapMdAdmin/snapContentMenuCmpnts/SmdaContentOpHoursCmpnt';
import {operatingHours} from '../../mocks/mockSnapMdAdminSeats';
import { IModalWindow, ModalWindow } from '../../../src/components/common/ModalWindow';
import { vocabularyUS } from '../../mocks/mockVocabularyLanguage';

let stateToggle={
    mOnDemandHourly:''
};
let updateOperatingHours={};
let deleteOperatingHours={
    dayId:0,
    hospitalId:0,
};
let toggle = {
    "id": 915,
    "hospitalId": 1,
    "key": "mOnDemandHourly",
    "value": "False"
  };
let props:IOpHoursProps ={
    hospitalId:1,
    hospitalName:'Emerald Dev',
    operatingHours:operatingHours,
    toggleState:false,
    language:{vocabularyUS},

    updateOperatingHours:(id:number, data:any, method:string)=>{updateOperatingHours = data},
    deleteOperatingHours:(dayId:number, hospitalId:number)=>{deleteOperatingHours.dayId = dayId; deleteOperatingHours.hospitalId=hospitalId},
    toggleOperatingHours:(state:any, id:number, field:string)=>{ stateToggle = state;},
    getSettings: (hospitalId: number, fields: string[]) => {}
};
let propsModal:IModalWindow ={
    title:'Edit',
    visible: false,

    okBtnText:'update',
    cancaleBtnText:'close',
    state:{},

    success:()=>{},
    cancel:()=>{},
    
};
let stateStart = {
    endDay:1,
    endTime:"",
    method:"",
    modalVisible:false,
    operatingHoursId:0,
    startDay:1,
    startTime:"",
    toggle:""
};
let stateEnd ={
    endDay:1,
    endTime:"",
    method:"Add",
    modalVisible:true,
    operatingHoursId:0,
    startDay:1,
    startTime:"",
    toggle:""
};

describe('SnapMdAdminApp Operationg Hours View',()=>{

    it('Should Displaying data in a table',()=>{
        let cmpnt = setup(SmdaClientOpHoursCmpnt, props);
        let opHours = cmpnt.instance() as SmdaClientOpHoursCmpnt;
        let table = opHours.renderItems(operatingHours);
        assert.equal(table.length, 3);
    });

    it('Should Displaying Toggle On', ()=>{
        let cmpnt = setup(SmdaClientOpHoursCmpnt, props);
        let opHours = cmpnt.instance() as SmdaClientOpHoursCmpnt;
        let toggleComponent = cmpnt.find('[name="toggle"]').simulate('change', true);
        
        assert.equal(stateToggle.mOnDemandHourly, 'True');
        toggleComponent.props().onChange(false);
        assert.equal(stateToggle.mOnDemandHourly, 'False');
        
    });

    function sortEqals(arr1:any, arr2:any){
        if(arr1.length != arr2.length) 
            return false
        let bool = false;
        for(let item in arr1){
            if( arr1[item] === arr2[item])
                    bool = true;
            else
                return false;
        }
        return bool;
    };

    it('should edit state Modal window', ()=>{        
        props.operatingHours = operatingHours;
        props.toggleState =toggle;
        let cmpntSmdaClientOpHours = setup(SmdaClientOpHoursCmpnt, props);
        let opHours = cmpntSmdaClientOpHours.instance() as SmdaClientOpHoursCmpnt;

        assert.isTrue(sortEqals(opHours.state, stateStart));
        cmpntSmdaClientOpHours.find('[name="Record"]').simulate('click');
        assert.isTrue(sortEqals(opHours.state,stateEnd));

        propsModal.visible=opHours.state.modalVisible;
        let cmpntWindowModal = setup(ModalWindow, propsModal);
        let windowModal = cmpntWindowModal.instance() as ModalWindow;

        cmpntWindowModal.find('[name="Cancel"]').simulate('click');
        assert.isTrue(sortEqals(opHours.state, stateEnd));

    });

    it('Should add new record button',()=>{
        props.operatingHours = operatingHours;
        props.toggleState =toggle;
        let cmpnt = setup(SmdaClientOpHoursCmpnt, props);
        let opHours = cmpnt.instance() as SmdaClientOpHoursCmpnt;

        propsModal.success = opHours.updateOperatingHours;
        let cmpntWindowModal = setup(ModalWindow, propsModal);
        let windowModal = cmpntWindowModal.instance() as ModalWindow;

        let changeStartDay={
            value: 2,
            label: 'Monday'
        };
        let changeEndDay={
            value: 2,
            label: 'Monday'
        };
        let valueStartTime={
            currentTarget:{
                value:'14:30'
            }
        };
        let valueEndTime={
            currentTarget:{
                value:'15:40'
            }
        };

        let response ={
            endDayOfWeek:2,
            endTime:"15:40",
            hospitalId:1,
            operatingHoursId:0,
            startDayOfWeek:2,
            startTime:"14:30",
        };

        cmpnt.find('[name="Record"]').simulate('click');
        cmpnt.find('[name="firstSelect"]').simulate('change',changeStartDay);
        assert.equal(opHours.state.startDay, changeStartDay.value);
        cmpnt.find('[name="secondSelect"]').simulate('change',changeEndDay);
        assert.equal(opHours.state.endDay, changeEndDay.value);

        cmpnt.find('[name="startTime"]').simulate('change', valueStartTime);
        cmpnt.find('[name="endTime"]').simulate('change',valueEndTime);
        cmpntWindowModal.find('[name="Success"]').simulate('click');

        assert.isTrue(sortEqals(updateOperatingHours, response));
    });

    it('Should edit record button',()=>{
        props.operatingHours = operatingHours;
        props.toggleState = toggle;
        let cmpnt = setup(SmdaClientOpHoursCmpnt, props);
        let opHours = cmpnt.instance() as SmdaClientOpHoursCmpnt;

        propsModal.success = opHours.updateOperatingHours;
        let cmpntWindowModal = setup(ModalWindow, propsModal);
        let windowModal = cmpntWindowModal.instance() as ModalWindow;

        let valueStartTime={
            currentTarget:{
                value:'14:30'
            }
        };
        let valueEndTime={
            currentTarget:{
                value:'15:40'
            }
        };
        let response ={
            endDayOfWeek:2,
            endTime:"15:40",
            hospitalId:1,
            operatingHoursId:0,
            startDayOfWeek:2,
            startTime:"14:30",
        };

        cmpnt.find('[name="1Edit"]').simulate('click');
        cmpnt.find('[name="startTime"]').simulate('change', valueStartTime);
        cmpnt.find('[name="endTime"]').simulate('change',valueEndTime);

        assert.equal(opHours.state.endTime, response.endTime);
    });

    it('Should edit record button',()=>{
        props.operatingHours = operatingHours;
        props.toggleState = toggle;
        let cmpnt = setup(SmdaClientOpHoursCmpnt, props);
        let opHours = cmpnt.instance() as SmdaClientOpHoursCmpnt;

        propsModal.success = opHours.updateOperatingHours;
        let cmpntWindowModal = setup(ModalWindow, propsModal);
        let windowModal = cmpntWindowModal.instance() as ModalWindow;

        cmpnt.find('[name="1Delete"]').simulate('click');

        assert.equal(opHours.props.operatingHours[1].operatingHoursId, deleteOperatingHours.dayId);
    });
});