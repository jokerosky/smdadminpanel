import * as React from 'react';
import { assert, expect } from 'chai';
import * as _ from 'lodash';

import { setup } from '../../setupComponentViaEnzyme';
import { vocabularyUS } from '../../mocks/mockVocabularyLanguage';
import { ViewIntegrationLogsCmpnt } from '../../../src/components/snapMdAdmin/snapMdAdminMenuCmpnts/ViewIntegrationLogs';
import { integrationLogsMock } from '../../mocks/mockIntegrationLogs';

let props = {
    language: vocabularyUS,
    integrationLogs: integrationLogsMock,

    getIntegrationLogs: (fieldQuery: any[]) => {integrationLgs = fieldQuery}
}

let integrationLgs = [];

describe('SnapMdAdmin Integration Logs:', () => {

    it('should create table and be able to change columns number', () => {
        let index = 'DateCreated';
        let cmpnt = setup(ViewIntegrationLogsCmpnt, props, true);

        let tableRecords = cmpnt.find('[id="tableRecords"]');
        let tableButtons = cmpnt.find('[id="tableButtons"]');

        assert.equal(tableRecords.props().children[0].props.children.props.children.length, 9);
        assert.equal(tableButtons.props().children.props.children.props.children.length, 9);

        cmpnt.find('[id="'+index+ '"]').simulate('click');
        let columns = _.findIndex(cmpnt.find('[id="tableRecords"]').props().children[0].props.children.props.children, {key: index});

        assert.equal(columns, -1);
    });

    it('should render rows items', ()=>{
        let cmpnt = setup(ViewIntegrationLogsCmpnt, props, true);

        let tableRecords = cmpnt.find('[id="tableRecords"]');
        assert.equal(tableRecords.props().children[1].props.children.length, integrationLogsMock.length);
    });

});