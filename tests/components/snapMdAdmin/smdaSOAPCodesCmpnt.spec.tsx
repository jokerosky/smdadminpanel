import * as React from 'react';
import { assert, expect } from 'chai';
import * as _ from 'lodash';

import { setup } from '../../setupComponentViaEnzyme';
import { SmdaClientSOAPCodesProps, SmdaClientSOAPCodesCmpnt } from '../../../src/components/snapMdAdmin/snapContentMenuCmpnts/SmdaClientSOAPCodes';
import { vocabularyUS } from '../../mocks/mockVocabularyLanguage';

let hospitalName = {
    name:'Emerald Dev'
};
let props ={
    hospitalId:0,
    hospitalName: hospitalName.name,
    soapDiagnosticCodingSystem:'',
    language:{vocabularyUS},

    saveDiagnosticCodingSettings:(hospitalId:number, diagnosticCodingSystem:string)=>{saveDiagnosticCodingSettings = diagnosticCodingSystem},
    deleteDiagnosticCodingSettings:(hospitalId:number)=>{deleteDiagnosticCodingSettings = hospitalId}
};
let saveDiagnosticCodingSettings;
let deleteDiagnosticCodingSettings;
describe('SnapMdAdminApp SOAP Codes View',()=>{

    it('Should Radio button is checked',()=>{
        props.soapDiagnosticCodingSystem='SNOMED-CT';
        let cmpnt = setup(SmdaClientSOAPCodesCmpnt, props);
        let soap = cmpnt.instance() as SmdaClientSOAPCodesCmpnt;

        let radio = cmpnt.find('#optionsRadios3');
        assert.isTrue(radio.node.props.checked);
        
        radio = cmpnt.find('#optionsRadios2').simulate('change',2);
        assert.equal(soap.state.selectedSoapDCS, 'ICD-10-DX');
        assert.isTrue(soap.state.showRemoveDCSButton);

        let name = cmpnt.find('#name');
        assert.equal(name.node.props.children[0], props.hospitalName);
        cmpnt.setProps({hospitalName: 'SnapMD'});
        name = cmpnt.find('#name');
        assert.equal(name.node.props.children[0], 'SnapMD');
    });

    it('Should Button Remove Diagnostic Coding System is visible',()=>{
        props.soapDiagnosticCodingSystem='SNOMED-CT';
        let cmpnt = setup(SmdaClientSOAPCodesCmpnt, props);
        let soap = cmpnt.instance() as SmdaClientSOAPCodesCmpnt;

        let radio = cmpnt.find('#RemoveDiagnostic');
        assert.equal(radio.node.props.id, 'RemoveDiagnostic');

        radio.simulate('click');
        radio = cmpnt.find('#RemoveDiagnostic');
        
        assert.equal(deleteDiagnosticCodingSettings, props.hospitalId);
        assert.isFalse(soap.state.showRemoveDCSButton);
    });

    it('Should Button Save is click',()=>{
        props.soapDiagnosticCodingSystem='SNOMED-CT';
        let cmpnt = setup(SmdaClientSOAPCodesCmpnt, props);
        let soap = cmpnt.instance() as SmdaClientSOAPCodesCmpnt;

        cmpnt.find('#Save').simulate('click');
        assert.equal(saveDiagnosticCodingSettings, props.soapDiagnosticCodingSystem);
    });

});