import * as React from 'react';
import { assert, expect } from 'chai';
import * as _ from 'lodash';

import { setup } from '../../setupComponentViaEnzyme';
import { SmdaClientSmsTemplateCmpntProps, SmdaClientSmsTemplateCmpnt } from '../../../src/components/snapMdAdmin/snapContentMenuCmpnts/SmdaClientSMSTemplate';
import {smsTemplates} from '../../mocks/mockSnapmdAdminSMSTemplates';
import { vocabularyUS } from '../../mocks/mockVocabularyLanguage';

let props = {
    hospitalId:1,
    hospitalName:'Emerald Dev',
    smsTemplates:smsTemplates,
    language:{vocabularyUS},

    updateSMSText:(messageId:number, message:any, method:string)=>{updateSMSText = message; updateSMSTextMethod = method},
    deleteSMSText:(messageId:number)=>{deleteSMSText = messageId},
    loadSMSTemplate:(hospitalId:number)=>{},
}

let updateSMSText = {  
    hospitalId:-1,
    id:"",
    isActive:"",
    smsText:"",
    type:""
};
let updateSMSTextMethod = '';
let deleteSMSText = -1;

describe('SnapMdAdminApp SMS Template View', ()=>{

    it('Should displaying records in a table', ()=>{
        let cmpnt = setup(SmdaClientSmsTemplateCmpnt, props, true);
        let sms = cmpnt.instance() as SmdaClientSmsTemplateCmpnt;

        assert.equal(sms.props.smsTemplates.length, cmpnt.find('tbody tr').length);
    });

    it('Should click Add new record btn and add new record', ()=>{
        let inputText = {
            currentTarget:{
                value: 'Clinica'
            }
        };
        let textArea = {
            currentTarget:{
                value: 'New Clinica'
            }
        };

        let cmpnt = setup(SmdaClientSmsTemplateCmpnt, props, true);
        let sms = cmpnt.instance() as SmdaClientSmsTemplateCmpnt;

        cmpnt.find('[name="Record"]').simulate('click');

        let textInput = cmpnt.find('[type="text"]');
        textInput.props().onChange(inputText);

        let areaText = cmpnt.find('textarea');
        areaText.props().onChange(textArea);

        cmpnt.find('[name="Success"]').simulate('click');

        assert.equal(updateSMSText.smsText, textArea.currentTarget.value);
        assert.equal(updateSMSText.type, inputText.currentTarget.value);
    });

    it('Should click Edit btn and update record', ()=>{
        let inputText = {
            currentTarget:{
                value: 'Clinica'
            }
        };
        let textArea = {
            currentTarget:{
                value: 'New Clinica'
            }
        };

        let cmpnt = setup(SmdaClientSmsTemplateCmpnt, props, true);
        let sms = cmpnt.instance() as SmdaClientSmsTemplateCmpnt;

        cmpnt.find('#Edit0').simulate('click');

        assert.equal(cmpnt.find('[type="text"]').node.value, smsTemplates[0].type);
        assert.equal(cmpnt.find('textarea').node.value, smsTemplates[0].smsText);

        let textInput = cmpnt.find('[type="text"]');
        textInput.props().onChange(inputText);

        let areaText = cmpnt.find('textarea');
        areaText.props().onChange(textArea);

        cmpnt.find('[name="Success"]').simulate('click');

        assert.equal(updateSMSText.smsText, textArea.currentTarget.value);
        assert.equal(updateSMSText.type, inputText.currentTarget.value);
    });

    it('Should click Delete btn and delete record', ()=>{
        let cmpnt = setup(SmdaClientSmsTemplateCmpnt, props, true);
        let sms = cmpnt.instance() as SmdaClientSmsTemplateCmpnt;

        cmpnt.find('#Delete0').simulate('click');

        assert.equal(updateSMSText.id, smsTemplates[0].id);
    });

    it('Should Modal window show', ()=>{
        let stateStart={
            modalVisible:false
        };
        let stateEnd={
            modalVisible:true
        };
        let cmpnt = setup(SmdaClientSmsTemplateCmpnt, props, true);
        let sms = cmpnt.instance() as SmdaClientSmsTemplateCmpnt;

        assert.equal(sms.state.modalVisible, stateStart.modalVisible);

        cmpnt.find('[name="Record"]').simulate('click');

        assert.equal(sms.state.modalVisible, stateEnd.modalVisible);
    });
});