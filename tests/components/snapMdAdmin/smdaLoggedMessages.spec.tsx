import * as React from 'react';
import { assert, expect } from 'chai';
import * as _ from 'lodash';

import { setup } from '../../setupComponentViaEnzyme';
import { ViewLoggedMessagesCmpnt } from '../../../src/components/snapMdAdmin/snapMdAdminMenuCmpnts/ViewLoggedMessages';

import { loggedMessages } from '../../mocks/loggedMessages';
import { vocabularyUS } from '../../mocks/mockVocabularyLanguage';

let props = {
    logMessages: loggedMessages,
    language: {vocabularyUS},
    locale: 'en',

    getLoggedMessages: (fieldQuery: any[]) => {response=fieldQuery;}
}

let response;

describe('SnapMdAdmin Logged Messages table component View:', () => {

    it('should create table and be able to change columns number', () => {
        let index = 'messageType';
        let cmpnt = setup(ViewLoggedMessagesCmpnt, props);
        let tableRecords = cmpnt.find('[id="tableRecords"]');
        let tableButtons = cmpnt.find('[id="tableButtons"]');

        assert.equal(tableRecords.props().children[0].props.children.props.children.length, 8);
        assert.equal(tableButtons.props().children.props.children.props.children.length, 8);

        cmpnt.find('[id="' + index + '"]').simulate('click');

        let columns = _.findIndex(cmpnt.find('[id="tableRecords"]').props().children[0].props.children.props.children, { key: index });

        assert.equal(columns, -1);
    });

    it('should render rows items', ()=>{
        let cmpnt = setup(ViewLoggedMessagesCmpnt, props);
        let consultation = cmpnt.instance() as ViewLoggedMessagesCmpnt;
        let record = consultation.renderItems(loggedMessages);
        assert.equal(record.length, loggedMessages.length);
    });

});