import * as React from 'react';
import { assert, expect } from 'chai';
import * as _ from 'lodash';

import { setup } from '../../setupComponentViaEnzyme';
import { vocabularyUS } from '../../mocks/mockVocabularyLanguage';
import { SmdaServicesCmpnt } from '../../../src/components/snapMdAdmin/snapMdAdminMenuCmpnts/ViewServicesCmpnt';
import { services } from '../../mocks/mockServices';
import { ServicesTableHeaders } from '../../../src/enums/tableHeaders';

let props = {
    services: services,
    language: vocabularyUS,

    getSystemServices: () => { serviceRefrech = true},
    getServiceLogRecords: (id: number, gparameters: any) => { serviceId= id;  logParameters = gparameters;},
    updateStateServices: (status: any, loglevel: any) => { servicesStatus = status; loggingLevel = loglevel; },
}

let serviceRefrech = false;
let servicesStatus = {}
let loggingLevel = {}
let servicesStatusMock = {
    CycleMS: 3000,
    ServiceId: 3,
    Status: "Off"
}

let loggingLevelMock = {
    LoggingLevel: 2,
    ServiceId: 3
}

let serviceId = -1;
let logParameters = {}
let logParametersMock = {
    ascending: false,
    level: 0,
    take: 100
};

describe('SnapMdAdmin Services component View:', () => {

    let cmpnt = setup(SmdaServicesCmpnt, props, true);
    let servicesCmpnt = cmpnt.instance() as SmdaServicesCmpnt;

    it('We test the table, create and print the required number of records', () => {
        let cmpnt = setup(SmdaServicesCmpnt, props);
        let tableRecords = cmpnt.find('[id="tableRecords"]');

        assert.equal(tableRecords.node.props.children[1].props.children.length, services.length);
        assert.equal(tableRecords.node.props.children[0].props.children.props.children.length, ServicesTableHeaders.length);
    });

    it('creating and displaying a modal window', () => {
        let testIndex = 2;
        
        assert.isFalse(servicesCmpnt.state.modalVisibleDetails);
        assert.isFalse(servicesCmpnt.state.modalVisibleLog);

        cmpnt.find(`[name="details${testIndex}"]`).simulate('click');
        assert.isTrue(servicesCmpnt.state.modalVisibleDetails);
        assert.equal(cmpnt.find('[id="ModalDetails"]').length, 1);
        cmpnt.find(`[name="Cancel"]`).simulate('click');
        assert.isFalse(servicesCmpnt.state.modalVisibleDetails);
        assert.equal(cmpnt.find('[id="ModalDetails"]').length, 0);

        cmpnt.find(`[name="log${testIndex}"]`).simulate('click');
        assert.isTrue(servicesCmpnt.state.modalVisibleLog);
        assert.equal(cmpnt.find('[id="ModalLogs"]').length, 1);
        cmpnt.find(`[name="Cancel"]`).simulate('click');
        assert.isFalse(servicesCmpnt.state.modalVisibleLog);
        assert.equal(cmpnt.find('[id="ModalLogs"]').length, 0);
    });

    it('check for filling the fields of the modal window with records Details', () => {
        let testIndex = 2;
        let cmpnt = setup(SmdaServicesCmpnt, props);

        let btnDetails = cmpnt.find(`[name="details${testIndex}"]`);

        btnDetails.simulate('click');
        let modalDetails = cmpnt.find('[id="ModalDetails"]');
        let description = modalDetails.find('[id="Description"]');
        let select = modalDetails.find('[name="Select"]');
        let cycle = modalDetails.find('[name="Cycle"]');
        let toggle = modalDetails.find('[name="toggle"]');

        assert.equal(description.node.props.children, services[testIndex].Description);
        assert.equal(select.node.props.value.label, services[testIndex].LoggingLevelString);
        assert.equal(cycle.node.props.value, services[testIndex].CycleMS);
        assert.equal(toggle.node.props.isChecked, services[testIndex].Status === 'On' ? true : false);
    });

    it('checking the sending of data from the modal window Details', () => {
        let testIndex = 2;
        let btnDetails = cmpnt.find(`[name="details${testIndex}"]`);

        btnDetails.simulate('click');
        let modalDetails = cmpnt.find('[id="ModalDetails"]');
        let description = modalDetails.find('[id="Description"]');
        let select = modalDetails.find('[name="Select"]');
        let cycle = modalDetails.find('[name="Cycle"]');
        let toggle = modalDetails.find('[name="toggle"]');

        cmpnt.find('[name="Success"]').simulate('click');
        assert.isTrue(_.isEqual(servicesStatus, servicesStatusMock));
        assert.isTrue(_.isEqual(loggingLevel, loggingLevelMock));
    });

    it('checking the sending of data from the modal window Log', () => {
        let testIndex = 2;
        let btnLog = cmpnt.find(`[name="log${testIndex}"]`);

        btnLog.simulate('click');

        let modalLogs = cmpnt.find('[id="ModalLogs"]');
        let select = modalLogs.find('[name="Select"]');
        assert.isTrue(_.isEqual(logParameters, logParametersMock));
        assert.equal(serviceId, services[testIndex].ServiceId);

        modalLogs.find('[name="Refresh"]').simulate('click');
        assert.isTrue(_.isEqual(logParameters, logParametersMock));
        assert.equal(serviceId, services[testIndex].ServiceId);
    });

    it('button testing Common Log, sending object for request', () => {
        let btnLog = cmpnt.find(`[name="CommonLog"]`);

        btnLog.simulate('click');
        assert.isTrue(_.isEqual(logParameters, logParametersMock));
        assert.equal(serviceId, -1);
    });

    it('button testing Refresh, service refresh', ()=>{
        let btnLog = cmpnt.find(`[name="RefreshServices"]`);

        btnLog.simulate('click');
        assert.isTrue(serviceRefrech);
    });
});