import * as React from 'react';
import { assert, expect } from 'chai';
import * as _ from 'lodash';

import { setup } from '../../setupComponentViaEnzyme';
import { vocabularyUS } from '../../mocks/mockVocabularyLanguage';
import {hospitalType} from '../../mocks/mockHospitalType';
import {getDefaultState, getHospitalSeatsKeys, getHospitalSeatsAssignedKeys} from '../../../src/models/DTO/hospitalSettingDTO';
import {seats, seatsAssigned} from '../../mocks/mockSnapMdAdminSeats';
import {getListPartners} from '../../mocks/mockSnapmdAdminActionCreator';
import {countries} from '../../mocks/countries';
import {settings} from '../../mocks/mockSettingsV2';
import {mockAdditionalSettings, mockClientData, mockClientInLists, mockHospitalAddress, mockSettings,
    saveAdditionalSettingsSSO, saveAdditionalSettingsSSO2} from '../../mocks/mockObjectsSettingsV2';
import {ISmdaClientSettingsV2Props, SmdaClientSettingsV2} from '../../../src/components/snapMdAdmin/snapContentMenuCmpnts/SmdaClientSettingsV2';

function objectFormationSeats(seats, seatsAssigned) {
    let tempSeats: any = {};
    for (let index in getHospitalSeatsKeys()) {
        tempSeats[index] = seats[index];
    }
    for (let index in getHospitalSeatsAssignedKeys()) {
        tempSeats[index] = seatsAssigned[index.replace('Assigned', '')];
    }
    return tempSeats;
}

let props = {
    language: vocabularyUS,
    countries: countries,
    clientTypes: hospitalType,
    hospitalId: 1,
    settings: settings,
    seats: objectFormationSeats(seats, seatsAssigned),
    partners: getListPartners,
    hospitalAddress: mockHospitalAddress,

    getCountries: () => {},
    getSettings: (hospitalId: number, fields: string[]) => {},
    loadSeats: (hospitalId: number) => {},
    saveSettings: (settings: any, hospitalId: number) => {
        settingsSaved = settings;
        hospitalIdSaved = hospitalId;
    },
    integrationsHospitalsPaymentGateway: (data: any, hospitalId: number) => {},
    integrationsHospitalsPartnerTypes: (data: any, hospitalId: number, partnerTypes: number) => {},
    deleteHospitalPartnerType: (data: any, hospitalId: number, partnerTypes: number) => {},
    postHospitalPartnerType: (data: any, hospitalId: number) => {},
    saveClientData: (image: any[], settings: any, hospitalId: number) => {
        imageSaved = image;
        settingsSaved = settings;
        hospitalIdSaved = hospitalId;
    },
    loadHospitalAddress: (hospitalId: number) => {}
}

let imageSaved, settingsSaved, hospitalIdSaved;

describe('Test Settings v2 :', ()=>{

    it('Test save client data', ()=>{
        let component = setup(SmdaClientSettingsV2, props);

        component.find('#showComponentClientSettings').simulate('click');
        let btnSave = component.find('#btnSave');
        btnSave.simulate('click');
        assert.isTrue(_.isEqual(settingsSaved.client, mockClientData));
        assert.isTrue(_.isEqual(settingsSaved.settings, mockSettings));
        assert.isTrue(_.isEqual(settingsSaved.hospitalAddress, mockHospitalAddress));
        assert.isTrue(_.isEqual(settingsSaved.clientInLists, mockClientInLists));
        assert.isTrue(_.isEqual(settingsSaved.additionalSettings, mockAdditionalSettings));
        
    });

    it('Test save settings', ()=>{
        let component = setup(SmdaClientSettingsV2, props, true);
        component.find('#showComponentCustomAppLinks').simulate('click');

        let btnSave = component.find('#customAppLinksSave');
        btnSave.simulate('click');
        
        assert.isTrue(_.isEqual(settingsSaved, saveAdditionalSettingsSSO));
        assert.equal(hospitalIdSaved, props.hospitalId);

        btnSave = component.find('#AuthorizeNetSave');
        btnSave.simulate('click');

        assert.isTrue(_.isEqual(settingsSaved, saveAdditionalSettingsSSO2));
        assert.equal(hospitalIdSaved, props.hospitalId);
    });

});