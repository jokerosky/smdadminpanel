import * as React from 'react';
import { assert, expect } from 'chai';
import * as _ from 'lodash';

import { setup } from '../../setupComponentViaEnzyme';

import { ViewConsultationMeetingsTopCmpnt,
     countRecord} from '../../../src/components/snapMdAdmin/snapMdAdminMenuCmpnts/ViewConsultationMeetingsTop';
import { mockClientSummaryDTOs } from '../../mocks/mockClientSummaryDTO';
import { vocabularyUS } from '../../mocks/mockVocabularyLanguage';

function hospitalName() {
    let client: Array<any> = [];
    for (let index in mockClientSummaryDTOs) {
        client.push({
            value: index,
            label: mockClientSummaryDTOs[index].hospitalName
        });
    }
    return client;
}

let props = {
    hospitalNamesForSelect: hospitalName(),
    consultationMeetingsRecords: countRecord,
    callData: {
        hospitalId: true,
        hospitalName: true,
        calleeUserId: true,
        calleeName: true,
        patientUserId: true,
        callerUserId: true,
        callerName: true,
        type: true,
        status: true,
        wihoutCharge: true,
        startTime: true,
        callDuration: true,
        messageInReply: true,
        replyLength: true,
        chatDuration: true,
    },
    language: vocabularyUS,

    sendingObjectForRequest: (queryProps: any) => {response = queryProps}
}

let response:any;

describe('SnapMdAdmin Consultation Meetings top component View:', () => {

    it('customer selection test', () => {
        let cmpnt = setup(ViewConsultationMeetingsTopCmpnt, props);
        let consultation = cmpnt.instance() as ViewConsultationMeetingsTopCmpnt;

        let select = cmpnt.find('[name="SelectClient"]');
        assert.equal(select.props().options.length, mockClientSummaryDTOs.length);
    });

    it('max records count selection test', () => {
        let cmpnt = setup(ViewConsultationMeetingsTopCmpnt, props);
        let consultation = cmpnt.instance() as ViewConsultationMeetingsTopCmpnt;

        let select = cmpnt.find('[name="SelectRecords"]');
        assert.equal(select.props().options.length, countRecord.length);
    });

    it('checked Call type test', ()=>{
        let cmpnt = setup(ViewConsultationMeetingsTopCmpnt, props);
        let consultation = cmpnt.instance() as ViewConsultationMeetingsTopCmpnt;

        let check1 = cmpnt.find('[name="check1"]');
        let check2 = cmpnt.find('[name="check2"]');
        let check3 = cmpnt.find('[name="check3"]');
        let check4 = cmpnt.find('[name="check4"]');

        assert.isTrue(check1.props().checked);
        assert.isTrue(check2.props().checked);
        assert.isTrue(check3.props().checked);
        assert.isTrue(check4.props().checked);

        check1.simulate('change');
        check2.simulate('change');
        check3.simulate('change');
        check4.simulate('change');

        assert.isFalse(cmpnt.find('[name="check1"]').props().checked);
        assert.isFalse(cmpnt.find('[name="check2"]').props().checked);
        assert.isFalse(cmpnt.find('[name="check3"]').props().checked);
        assert.isFalse(cmpnt.find('[name="check4"]').props().checked);
    });

    it('button query test', ()=>{
        let res = {
            openConsultation:true,
            providerAudio:true,
            providerPatient:true,
            providerVideo:true,
            recordSelectedValue:0,
            selectedHospitalId:-1
        }
        let cmpnt = setup(ViewConsultationMeetingsTopCmpnt, props);
        let consultation = cmpnt.instance() as ViewConsultationMeetingsTopCmpnt;
        
        let query = cmpnt.find('[name="ButtonQuery"]').simulate('click');

        assert.equal(response.openConsultation, res.openConsultation);
        assert.equal(response.recordSelectedValue, res.recordSelectedValue);
        assert.equal(response.selectedHospitalId, res.selectedHospitalId);
    });

    it('create column table in Excel', ()=>{
        let cmpnt = setup(ViewConsultationMeetingsTopCmpnt, props);
        let consultation = cmpnt.instance() as ViewConsultationMeetingsTopCmpnt;

        let work = consultation.workbookColumn(props);
        assert.equal(work.length, Object.keys(props.callData).length);
    });
});