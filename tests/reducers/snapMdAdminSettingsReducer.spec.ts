import 'mocha';
import { assert } from 'chai';

import initialState from '../../src/reducers/initialState';
import * as types from '../../src/enums/actionTypes';
import { SettingsDTO } from '../../src/models/DTO/settingsDTO';

import snapAdminSettingsReducer from '../../src/reducers/snapMdAdminClientReducers/snapAdminSettingsReducer';
import {hospitalSettings , hospitalModulesSettings} from '../mocks/mockHospitalSettingsDTO';
import { mappedKeys } from '../mocks/mockMappedSettings';
import {getDefaultState} from '../../src/models/DTO/hospitalSettingDTO';

describe('snapMdAdminSettingReducer tests',()=>{
    it('should add new items into state object',()=>{
       let state = initialState.snapmdAdmin.client.settings;

       let action = {type: types.SNAPMDADMIN_CLIENT_SETTINGS_LOADED, settings: hospitalModulesSettings};
        
       let newState = snapAdminSettingsReducer(state, action);

       assert.equal(Object.keys(newState).length, Object.keys(getDefaultState()).length);
    });

    it('should replace items with the same key in state object',()=>{
        let state = initialState.snapmdAdmin.client.settings;
        let action = {type: types.SNAPMDADMIN_CLIENT_SETTINGS_LOADED, settings: [
            {hospitalId:1, id:1, key:'one', value:'1'},
            {hospitalId:1, id:2, key:'two', value:'2'},
        ] as SettingsDTO[]};
         
        let newState = snapAdminSettingsReducer(state, action);

        assert.equal(Object.keys(newState).length, Object.keys(getDefaultState()).length + action.settings.length);
        assert.equal(newState['one'].value, 1);
        assert.equal(newState['two'].value, 2);

        action = {type: types.SNAPMDADMIN_CLIENT_SETTINGS_LOADED, settings: [
            {hospitalId:1, id:1, key:'one', value:'3'},
            {hospitalId:1, id:2, key:'two', value:'4'},
        ] as SettingsDTO[]};

        newState = snapAdminSettingsReducer(state, action);

        assert.equal(Object.keys(newState).length, Object.keys(getDefaultState()).length + action.settings.length);
        assert.equal(newState['one'].value, 3);
        assert.equal(newState['two'].value, 4);
     });

     it('should update setting in state on change ',()=>{
        let state = initialState.snapmdAdmin.client.settings;
        let action = {
            type: types.SNAPMDADMIN_CLIENT_SETTING_CHANGED,
            setting: mappedKeys[1]
        };

        let newState = snapAdminSettingsReducer(state, action);

        assert.equal(Object.keys(newState).length, Object.keys(getDefaultState()).length);
     });   
});