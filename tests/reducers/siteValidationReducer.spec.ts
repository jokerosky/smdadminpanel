import 'mocha';
import { assert } from 'chai';

import * as types from '../../src/enums/actionTypes';
import {EventType} from '../../src/enums/eventType';
import { ValidationError } from '../../src/utilities/validators';
import siteValidationsReducer from '../../src/reducers/siteValidationsReducer';

import { mockValidationResultActionAdd } from '../mocks/mockValidationResultActionAdd';

describe('Site Validation reducer tests', () => {
    let state:any = {
        validations:[
            {
                component:'whatever',
                elementId:'login',
                errorClass:'is-error'
            },
            {
                component:'whatever',
                elementId:'password',
                errorClass:'is-error'
            }
        ] as ValidationError[],
    };

    it('Should clear validations of component',()=>{
        let action = {
            type: types.VALIDATION_RESULT_ADD,
            cmpntClassName: 'whatever',
            validationErrors: [
            ] as ValidationError[]
        };
        let newState:any = siteValidationsReducer(state, action);

        assert.equal(newState.validations.length, 0, "old component validations were not deleted");
    });

    it('Should add notifications and update siteNotificationCount value ', () => {
        let action = mockValidationResultActionAdd;
        let newState:any = siteValidationsReducer(state, action);

        assert.equal(newState.validations.length, 1, "no validations flags were added");
        //assert.equal(newState.notifications.length, 2, "empty notifications array");
    });

     
});