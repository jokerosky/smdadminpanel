import 'mocha';
import { assert } from 'chai';

import * as types from '../../src/enums/actionTypes';
import errorReducer from '../../src/reducers/siteErrorsReducer';

import loginResponse from '../mocks/loginResponse';

import { mockValidationResultActionAdd } from '../mocks/mockValidationResultActionAdd';
import { mockAddressValidationResponseSuccess, mockAddressValidationResponseFailure } from '../mocks/addressValidationResponse';
import mockNewPatientResponseDTO from '../mocks/mockNewPatientResponseDTO';

describe('User reducer tests', () => {
    let state: any = {};

});