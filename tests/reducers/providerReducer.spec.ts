import 'mocha';
import { assert } from 'chai';

import * as types from '../../src/enums/actionTypes';
import providerReducer from '../../src/reducers/providerReducer';

describe('Provider reducer test',()=>{
    it('should clear profile after logout', ()=>{
        let state = {profile:{id:1}};

        let newState = providerReducer(state ,{type: types.LOGIN_CLEAR});

        assert.isTrue(Object.keys(newState.profile).length == 0);
    });
});