import 'mocha';
import { assert } from 'chai';

import * as types from '../../src/enums/actionTypes';
import { SiteConstants } from '../../src/enums/siteConstants';
import userReducer from '../../src/reducers/userReducer';

import loginResponse from '../mocks/loginResponse';

import { mockValidationResultActionAdd } from '../mocks/mockValidationResultActionAdd';
import { mockAddressValidationResponseSuccess, mockAddressValidationResponseFailure } from '../mocks/addressValidationResponse';
import mockNewPatientResponseDTO from '../mocks/mockNewPatientResponseDTO';

describe('User reducer tests', () => {
    let state: any = {};

    it('Should update state with token and expiration ', () => {
        let newState: any = userReducer(state, {
            type: types.LOGIN_SUCCESS,
            tokenData: {
                access_token: loginResponse.data[0].access_token,
                expires: loginResponse.data[0].expires
            }
        } as any);

        assert.equal(newState.accessToken, loginResponse.data[0].access_token);
        assert.equal(newState.tokenExpires, loginResponse.data[0].expires);
    });

    it('Should update state with positive address validation result', () => {
        let newState: any = userReducer(state, {
            type: types.REGISTRATION_ADDRESS_VALIDATED,
            response: mockAddressValidationResponseSuccess.data[0]
        });

        assert.equal(newState.registrationStep, 2);
    });

    it('Should update state with negative address validation result', () => {
        let newState: any = userReducer(state, {
            type: types.REGISTRATION_ADDRESS_VALIDATED,
            response: mockAddressValidationResponseFailure.data[0]
        });

        assert.equal(newState.registrationStep, 4);
    });

    it('Should set user registration form step', () => {
        let newState: any = userReducer(state, {
            type: types.SET_REGISTRATION_FORM_STEP,
            step: 3
        });

        assert.equal(newState.registrationStep, 3);
    });

    it('should set an error message and second step if registration',()=>{
        let newState: any = userReducer(state,{
            type: types.REGISTRATION_NEW_PATIENT_ERROR,
            message: 'Something is wrong'
        });

        assert.equal(newState.registrationStep, 2);        
    });

    it('should set step to 3 and add response data to state',() => {
        let newState: any = userReducer(state, {
            type:types.REGISTRATION_NEW_PATIENT_REGISTERED,
            response: mockNewPatientResponseDTO
        })

        assert.equal(newState.registrationStep, 3);
    });

    it('should set state form loaded data',()=>{
        let access_token = "1";
        let data = {};
        data[SiteConstants.session.sessionData] = {
                access_token,
                expires:'someday'
            };
        let newState: any = userReducer(state, {
            type:types.LOAD_SESSION_STORAGE_DATA,
            data
        })

        assert.equal(newState.accessToken, "1");
    });


});