import 'mocha';
import { assert } from 'chai';

import * as types from '../../src/enums/actionTypes';
import {EventType} from '../../src/enums/eventType';
import siteNotificationsReducer from '../../src/reducers/siteNotificationsReducer';

import { mockValidationResultActionAdd } from '../mocks/mockValidationResultActionAdd';

describe('Site Notifications reducer tests', () => {
    let state:any = {
        notifications:[],
        notificationsCount:0
    };

    
    it('Should add notifications and update siteNotificationCount value ', () => {
        let action = mockValidationResultActionAdd;

        let newState:any = siteNotificationsReducer(state, action);

        assert.equal(newState.notificationsCount, 2, "site notification counter has not been changed");
        assert.equal(newState.notifications.length, 2, "empty notifications array");
    });

    it('Should remove notifications and leave siteNotificationCount value the same', () => {
        let newState = siteNotificationsReducer(state, mockValidationResultActionAdd);
        
        let action = {
            type: types.GLOBAL_NOTIFICATION_REMOVE,
            ids:[0,1]
        };
        newState = siteNotificationsReducer(newState, action);

        assert.equal(newState.notificationsCount, 2, "site notification counter has been changed!");
        assert.equal(newState.notifications.length, 0, "notifications were not rmoved");
    });

    
});