import 'mocha';
import { assert } from 'chai';

import * as types from '../../src/enums/actionTypes';

import snapAdminListReducer from '../../src/reducers/snapMdAdminRestReducerc/snapAdminListReducer';
import snapAdminMiscReducer from '../../src/reducers/snapMdAdminRestReducerc/snapAdminMiscReducer';
import snapAdminDetailsReducer from '../../src/reducers/snapMdAdminClientReducers/snapAdminDetailsReducer';
import snapAdminOpHoursReducer from '../../src/reducers/snapMdAdminClientReducers/snapAdminOpHoursReducer';

import {mockClientSummaryDTOsAll} from '../mocks/mockClientSummaryDTO';
import {getListCodeSetss, getListClientsTypess,
        getListDocumentTypess, getListOrganizationTypess, 
        getListPartnerOnlyActives, getListPartners} from '../mocks/mockSnapmdAdminActionCreator';
import {clientData} from '../mocks/mockSnapMdClientData';
import {seats, seatsAssigned, operatingHours} from '../mocks/mockSnapMdAdminSeats';
import {hospitalSettings , hospitalModulesSettings} from '../mocks/mockHospitalSettingsDTO';
import { ObjectHelper } from '../../src/utilities/objectHelper';
import { SiteConstants } from '../../src/enums/siteConstants';

describe('SnapAdmin reducer tests', () => {
    let state:any = {
        snapmdAdmin: {
            misc: {
              selectedId:null,
              clientView:'',
              filters:{},
            },
            client:{
              details: {
                clientData:{},
                seats:seats,
                seatsAssigned:seats,
              },
              organizations: [],
              settings: {},
              smsTemplates: [],
              soapCodes: '',
              codeSets: {
                codeSetsRecords:[],
                codeSetsTypeId:0,
              },
              operatingHours: [],
              hospitalDocument: {
                selectedDocument:0,
                textDocument: ''
              },
              locations: {}
            },
            lists:{
              clients:[],
              clientsTypes: [],
              codeSets: [],
              documentTypes: [],
              organizationTypes: [],
              partner: [],
              partnerOnlyActive: [],
              hospitalSettings: [],
            },
          }
    };

    it('snapAdmin client loaded',()=>{
        let newState: any = snapAdminListReducer(state.snapmdAdmin.lists, {
            type: types.SNAPMDADMIN_CLIENTS_LOADED,
            data: mockClientSummaryDTOsAll
        }) as any;

        assert.equal(newState.clients.total, mockClientSummaryDTOsAll.total);
    });

    it('snapAdmin client selected',()=>{
        let newState: any = snapAdminMiscReducer(state.snapmdAdmin.misc, {
            type: types.SNAPMDADMIN_CLIENT_SELECTED,
            id: 1
        }) as any;

        assert.equal(newState.selectedId, 1);
    });

    it('snapAdmin client filters',()=>{
        let filter={
            id:"122",
            name:"",
            sortKey:"",
            sortOrder:"asc"
        };
        let newState: any = snapAdminMiscReducer(state.snapmdAdmin.misc, {
            type: types.SNAPMDADMIN_FILTERS_CHANGED,
            filters: filter
        }) as any;

        assert.equal(newState.filters, filter);
    });

    it('snapAdmin view selected',()=>{
        let newState: any = snapAdminMiscReducer(state.snapmdAdmin.misc, {
            type: types.SNAPMDADMIN_VIEW_SELECTED,
            view: 'details'
        }) as any;

        assert.equal(newState.clientView, 'details');
    });

    it('snapAdmin view selected',()=>{
        let data={
            clientsTypes: getListClientsTypess,
            codeSets: getListCodeSetss,
            documentTypes: getListDocumentTypess,
            organizationTypes: getListOrganizationTypess,
            partner: getListPartners,
            partnerOnlyActive: getListPartnerOnlyActives
        }
        let newState: any = snapAdminListReducer(state.snapmdAdmin.lists, {
            type: types.SNAPMDADMIN_LISTS_LOADED,
            listData: data
        }) as any;

        assert.equal(newState.clientsTypes, data.clientsTypes);
        assert.equal(newState.codeSets, data.codeSets);
        assert.equal(newState.documentTypes, data.documentTypes);
        assert.equal(newState.organizationTypes, data.organizationTypes);
        assert.equal(newState.partner, data.partner);
        assert.equal(newState.partnerOnlyActive, data.partnerOnlyActive);
    });

    it('snapAdmin view selected',()=>{
        let newState: any = snapAdminDetailsReducer(state.snapmdAdmin.client.details, {
            type: types.SNAPMDADMIN_CLIENTS_DATA_LOADED,
            data: clientData
        }) as any;

        assert.equal(newState.clientData, clientData);
    });

    it('snapAdmin cliets seats loaded',()=>{
        let newState: any = snapAdminDetailsReducer(state.snapmdAdmin.client.details, {
            type: types.SNAPMDADMIN_CLIENTS_SEATS_LOADED,
            seats: seats
        }) as any;

        assert.equal(newState.seats, seats);
    });

    it('snapAdmin cliets seatsAssigned loaded',()=>{
        let newState: any = snapAdminDetailsReducer(state.snapmdAdmin.client.details, {
            type: types.SNAPMDADMIN_CLIENTS_SEATS_ASSIGNED_LOADED,
            seatsAssigned: seatsAssigned
        }) as any;
        assert.equal(newState.seatsAssigned, seatsAssigned);
    });

    it('snapAdmin cliets operating hours',()=>{
        let newState: any = snapAdminOpHoursReducer(state.snapmdAdmin.client.operatingHours, {
            type: types.SNAPMDADMIN_OPERATING_HOURS_LOADED,
            hours: operatingHours,
        }) as any;

        assert.equal(newState.length, operatingHours.length);
    });
});