import * as axios from 'axios';
import {AxiosInstance, AxiosStatic } from 'axios';

import { SnapMDServiceLocator } from '../src/dependenciesInjectionConfiguration';
import { ISnapMdClientService } from '../src/services/api/snapMdClientService';
import { ISnapMdClientDataService, SnapMdClientDataService } from '../src/services/api/snapMdClientDataService';
import {ISnapMdHttpService, SnapMdHttpService} from '../src/services/api/httpServices';

export function configureDependencies(){
    
    SnapMDServiceLocator.addObject("TestService", { getString:()=>{return 'AAAA!'; }} );
    SnapMDServiceLocator.addObject('ISnapMdClientService', { getClientsAsync(){ return new Promise((resolve,reject)=>{ resolve()}) } } as ISnapMdClientService);

    var snapMdHttpService = new SnapMdHttpService(axios.default as AxiosStatic);
    SnapMDServiceLocator.addObject('ISnapMdHttpService', snapMdHttpService);

    SnapMDServiceLocator.addObject('ISnapMdClientDataService', SnapMdClientDataService);
}