import 'mocha';
import { assert } from 'chai';

import { mapStateToProps } from '../src/components/app';

let state = {
    site: {
        ajax: {
            dataLoaded: false,
            stylesLoaded: false,
            hospitalDataLoaded: false,
            ajaxCallsInProgress: 0
        },
        errors: {
            isCriticalError: false
        },
        misc:{
            enableHospitalStyles: true,
        }
    }
};

let mockAppOwnState = {
    location: {
        pathname: '/login/provider/'
    }
};

describe('Splash screen should', () => {
    it('be shown when api keys, hospital data or styles not loaded', () => {
        let testState = JSON.parse(JSON.stringify(state));
        let props = mapStateToProps(testState, mockAppOwnState);
        assert.isTrue(props.showSplashScreen);

        testState.site.ajax.hospitalDataLoaded = true;
        props = mapStateToProps(testState, mockAppOwnState);
        assert.isTrue(props.showSplashScreen);

        testState.site.ajax.stylesLoaded = true;
        testState.site.ajax.hospitalDataLoaded = false;
        props = mapStateToProps(testState, mockAppOwnState);
        assert.isTrue(props.showSplashScreen);
    });

    it('be hidden for public pages when styles and other data loaded',()=>{
        let testState = JSON.parse(JSON.stringify(state));
        testState.site.ajax.stylesLoaded = true;
        testState.site.ajax.hospitalDataLoaded = true;

        let props = mapStateToProps(testState, mockAppOwnState);
        assert.isFalse(props.showSplashScreen);
    });

    it('be shown when dataLoaded flag is false',()=>{
        let testState = JSON.parse(JSON.stringify(state));
        let testOwnState = JSON.parse(JSON.stringify(mockAppOwnState));
        testState.site.ajax.stylesLoaded = true;
        testState.site.ajax.hospitalDataLoaded = true;
        testOwnState.location.pathname = '/provider/dashboard/';

        let props = mapStateToProps(testState, testOwnState);
        assert.isTrue(props.showSplashScreen);

        testState.site.ajax.dataLoaded = true;
        props = mapStateToProps(testState, testOwnState);
        assert.isFalse(props.showSplashScreen);
    });

    it('be hidden when critical error flag was set',()=>{
        let testState = JSON.parse(JSON.stringify(state));
        let testOwnState = JSON.parse(JSON.stringify(mockAppOwnState));

        let props = mapStateToProps(testState, mockAppOwnState);
        assert.isTrue(props.showSplashScreen);

        testState.site.errors.isCriticalError =  true;
        props = mapStateToProps(testState, mockAppOwnState);
        assert.isFalse(props.showSplashScreen);
    });
});

