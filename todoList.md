# Things to do:

* add Dependency injector or service locator instead of importing stuff from global scope
* refactor navigation logic: replace react-router with react-router-redux, *browserHistory* with dispatching *push* 
* refactor navigation logic: replace endpoints or endpoints generation to absolute path