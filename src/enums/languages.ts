var flags = {
    'us':  "assets/images/us.png",
    'es':  "assets/images/es.png",
    'ru':  "assets/images/ru.png",
    //'gb': "../../../assets/images/gb.png"
};

const language = [
    {
        value: 'us',
        label: 'US'
    },
    {
        value: 'es',
        label: 'ES'
    },
    {
        value: 'ru',
        label: 'RU'
    },
    // {
    //     value: 'gb',
    //     label: 'GB'
    // },
    
];

export {language, flags};