import * as _ from 'lodash';

/// Get previous enum value (or the last one)
export function getPrevious<T>(enumType:any, current:string):T {
    let index =  _.indexOf(_.keys(enumType), current) ;
    index = index == 0 ? _.keys(enumType).length - 1 : --index;
    let previous = SortOrder[_.keys(enumType)[index]];
    return previous;
}

/// Get next enum value (or the first one)
export function getNext<T>(enumType:any, current:string):T {
    let index =  _.indexOf(_.keys(enumType), current) ;
    index = index == _.keys(enumType).length - 1 ? 0 : ++index;
    let next = SortOrder[_.keys(enumType)[index]];
    return next;
}

export enum SortOrder{
    no = 'no',
    asc = 'asc',
    desc = 'desc'
}


export enum DaysOfWeek{
    mon = 'Mon',
    tus = 'Tue',
    wed = 'Wed',
    thu = 'Thu',
    fri = 'Fri',
    sat = 'Sat',
    sun = 'Sun'
}

export enum EditorActions{
    empty = '',
    add = 'Add',
    edit = 'Edit',
    archive = 'Archive',
    activate = 'Activate',
}