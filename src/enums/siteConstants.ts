export const SiteConstants = {
    session:{
        //site
        sessionData: "session_data",

        //legacy
        uiq: "_uiq_ct",
        chatUnreadCounter: "snap_chatunreadcounter_session",
        hospital: "snap_hospital_session",
        hospitalSettings: "snap_hospital_settings",
        staffprofile: "snap_staffprofile_session",
        patientprofile:"snap_patientprofile_session",
        user: "snap_user_session",
        logoutError: "snap_logoutError",
        locationWasChecked: "snap_locationWasChecked"
    } ,
    css:{ 
        isActive:'is-active'
    },

    daysOfWeek:[
    { value:0, label:'Sunday'},
    { value:1, label: 'Monday'},
    { value:2, label: 'Tuesday'},
    { value:3, label: 'Wednesday'},
    { value:4, label: 'Thursday'},
    { value:5, label: 'Friday'},
    { value:6, label: 'Saturday'}],
   
}