export enum EditorType{
    toggle = 'ToggelButton',
    edit = 'Edit',
    dropdown = 'Dropdown',
    radio = 'RadioButton',
    textarea = 'TextArea',
    address = 'PlacesAutocompleteCmpnt',
    dateTime = 'DateTime',
    select = 'Select',
    button = 'Button',
    container = 'Container',
    check = 'Check',
    excel = 'Excel'
}