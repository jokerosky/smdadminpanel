let encodeHTML={
    quot: '&quot;',
    apos: '&apos;',
    amp: '&amp;',
    lt: '&lt;',
    gt: '&gt;',
    nbsp: '&nbsp;',
}

let decodeHTML ={
    quot: '"',
    apos: "'",
    amp: '&',
    lt: '<',
    gt: '>',
    nbsp: ' ',
}

export{encodeHTML, decodeHTML};