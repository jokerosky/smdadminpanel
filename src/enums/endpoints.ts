//vars
var login = 'login';
var dashboard = 'dashboard';
var forgotPassword = 'forgotpassword';
var account = 'account';
var client = 'client';

var settings = 'settings';
var settingsv2 = 'settingsv2';
var details = 'details';
var opHours = 'ophours';
var smsTemplate = 'smstemplate';
var codeSets = 'codesets';
var soapCodes = 'soapcodes';
var hospitalDoc = 'hospitaldoc';
var admin = 'admin';
var locations = 'locations';
var organizations = 'organizations';
var communications = 'communications';

var snapmdAdmin = 'snapmdadmin' ;

var consultationMeetings = 'consultationmeetings';
var loggedMessages = 'loggedmessages';
var integrationLogs = 'integrationlogs';
var services = 'services';
var addNewClient = 'addnewclient';

var consultationMeetingsV2 = 'consultationmeetingsv2';

export function toAbsolutePath(localUrl:string):string {
    return Endpoints.site.root + localUrl;
}
export const baseUrl = 'https://snapv2.ruteco.com/';
export const baseApiUrl = 'https://snapmd.ruteco.com/api/';
// export const baseUrl = 'https://emerald.qa2.snapvcm.com/';
// export const baseApiUrl = 'https://emerald.qa2.snapvcm.com/api/';
export var Endpoints = {
    site: {
        root: '/',
        rootSnapLocal: 'http://snap.local/',
        siteError: '/error',
        //tony.y: i know, this is kind of maniac :)
        loginPostfix: login,
        dashboardPostfix: dashboard,
        forgotPasswordPostfix: forgotPassword,
        accountPostfix: account,
        clientPostfix: client,     

        patientSignup: '/patientsignup',
        snapMdAdmin: {
            root: snapmdAdmin,
            dashboard: snapmdAdmin +'/'+ dashboard,
            consultationmeetings: snapmdAdmin +'/'+ consultationMeetings,
            loggedmessages: snapmdAdmin +'/'+ loggedMessages,
            integrationlogs: snapmdAdmin +'/'+ integrationLogs,
            services: snapmdAdmin +'/'+ services, 

            consultationMeetingsV2: snapmdAdmin +'/'+ consultationMeetingsV2,
            
            settings: snapmdAdmin +'/'+ dashboard + '/%id%/' + settings,
            settingsv2: snapmdAdmin +'/'+ dashboard + '/%id%/' + settingsv2,
            details:  snapmdAdmin +'/'+ dashboard + '/%id%/' + details,
            opHours:  snapmdAdmin +'/'+ dashboard + '/%id%/' + opHours,
            smsTemplate: snapmdAdmin +'/'+ dashboard + '/%id%/' + smsTemplate,
            codeSets: snapmdAdmin +'/'+ dashboard + '/%id%/' + codeSets,
            soapCodes: snapmdAdmin +'/'+ dashboard + '/%id%/' + soapCodes,
            hospitalDoc: snapmdAdmin +'/'+ dashboard + '/%id%/' + hospitalDoc,
            admin: snapmdAdmin +'/'+ dashboard + '/%id%/' + admin,
            locations: snapmdAdmin +'/'+ dashboard + '/%id%/' + locations,
            organizations: snapmdAdmin +'/'+ dashboard + '/%id%/' + organizations,
            communications: snapmdAdmin +'/'+ dashboard + '/%id%/' + communications,

            addnewclient: snapmdAdmin +'/'+  dashboard +'/'+ addNewClient,

            settingsPostfix: settings,
            settingsv2Postfix: settingsv2,
            detailsPostfix: details,
            opHoursPostfix: opHours,
            smsTemplatePostfix: smsTemplate,
            codeSetsPostfix: codeSets,
            soapCodesPostfix: soapCodes,
            hospitalDocPostfix: hospitalDoc,
            adminPostfix: admin,
            locationsPostfix: locations,
            organizationsPostfix: organizations,
            communicationsPostfix: communications,

            consultationmeetingsPostfix: consultationMeetings,
            loggedMessagesPostfix: loggedMessages,
            integrationLogsPostfix: integrationLogs,
            servicesPostfix: services,
            addNewClientPostfix: addNewClient,

            consultationmeetingsv2Postfix: consultationMeetingsV2
            
        },

        login: login + '/%type%/',
        forgotPassword: forgotPassword + '/%type%/',
        dashboard: '/%type%/' + dashboard
        
    },

    hospital: {
        getHospitalIdWithKeys: baseApiUrl + 'v2.1/hello/api-keys',
        getApiSessionKey:  baseUrl + 'snapmd-api-session',
        getHospitalInfo: baseApiUrl + 'v2/hospital/%id%', //'v2/hospitals/within-users-limit'//
        hospitalSettings: baseApiUrl + 'v2/admin/hospitals/settings/%id%',
        diagnosticCodingSystem: baseApiUrl + 'v2/snapmd/hospital/diagnosticcodingsystem/%id%',
        smstemplatesLoad: baseApiUrl + 'v2/smstemplates/hospitals/%id%',
        smsTemplates: baseApiUrl + 'v2/smstemplates/%id%',
        organizationTypes: baseApiUrl + 'v2/organizationtypes',
        organizations: baseApiUrl + 'v2/admin/hospitals/%id%/organizations',
        addAndUpdateOrganization: baseApiUrl + 'v2/organizations',
        organizationsLocations: baseApiUrl + 'v2/admin/hospitals/%id%/locations',
        locations: baseApiUrl + 'v2/locations/%id%',
        hospitalDocument: baseApiUrl + 'v2/documents/%docId%/hospitals/%id%',
        hospitalDocuments: baseApiUrl + 'snapadmin/documents',
        codeSetLoad: baseApiUrl + 'code/%codeSetsId%/%id%',
        codeSetAdd: baseApiUrl + 'code/add',
        codeSetUpdate: baseApiUrl + 'code/update',
    },

    user: {
        login: baseApiUrl + 'v2/Account/Token', //userTypeId: 2, hospitalId: "126", email: "", password: ""
        resetPassword: baseApiUrl + 'v2/mail/resetPassword', //userTypeId: 2, hospitalId: "126", email: "" 
        validateRegistrationAddress: baseApiUrl + 'v2.1/patients/registrations/availability', // addressText=Omsk&hospitalId=1
        registerNewPatient: baseApiUrl + 'v2/patients/single-trip-registration' // NewPatientRequest.cs → class for DTO
    },

    patient: {
        userProfile: baseApiUrl + 'v2/patients/userprofile'
    },

    provider: {
        userStaffProfile: baseApiUrl + 'v2/admin/userstaffprofile'
    },

    appointments: {
        clinitiansAppointments: baseApiUrl + 'v2.1/clinicians/appointments',
        patientAppointments: baseApiUrl + 'v2.1/patients/filtered-appointments'
    },

    snapmdAdmin:{
        clients: baseApiUrl + 'v2/admin/clients/%id%',
        codeSets: baseApiUrl + 'CodeSets',
        organizationTypes: baseApiUrl + 'v2/organizationtypes',
        documentTypes: baseApiUrl + 'snapadmin/documenttypes',
        clientsTypes: baseApiUrl + 'v2/admin/clients/types',
        partner: baseApiUrl + 'v2/integrations/partner',
        partnerOnlyActive: baseApiUrl + 'v2/integrations/partner?onlyActive=true',
        seats: baseApiUrl + 'v2/admin/clients/%id%/seats',
        seatsAssigned: baseApiUrl + 'v2/admin/clients/%id%/seats?assigned=true',
        operatinghHours: baseApiUrl + 'v2/hospital/%id%/operatinghours',
        operatinghHoursDelete: baseApiUrl + 'v2/hospital/operatinghours/%id%',
        hospitalAddress:  baseApiUrl + 'v2/hospitaladdress/%id%',

        diagnosticcodingsystem: baseApiUrl + 'v2/snapmd/hospital/diagnosticcodingsystem/%id%',
        communications: baseApiUrl + 'v2/admin/hospitals/%id%/communications',
        consultationMeetingsPath: baseApiUrl + 'v2.1/admin/consultation-meetings',
        loggedMessagesPath:baseApiUrl + 'v2/admin/log-messages',
        servicesPath: baseApiUrl + 'systemservice/all',
        servicesLogs: baseApiUrl + 'v2/admin/snapservices/logs/%id%',
        updateLoggingLevel: baseApiUrl + 'systemservice/updatelogginglevel',
        updateStatus: baseApiUrl + 'systemservice/updatestatus',
        integrationLogsPath: baseApiUrl + 'v2/admin/integration-logs',

        settingsCountries: baseApiUrl + 'v2.1/addresses/countries',
        integrationsHospitalsPaymentGateway: baseApiUrl + 'v2.1/integrations/hospitals/%id%/payment-gateway',
        integrationsHospitalsPartnerTypes: baseApiUrl + 'v2.1/integrations/hospitals/%id%/partner-types/%partnerType%',
        integrationsHospitalPartner: baseApiUrl + 'v2/integrations/hospitalpartner/%id%/partnertype/%partnerType%',

        brandBackgroundImage: baseApiUrl + 'v2/admin/hospitals/%id%/settings/BrandBackgroundImage',
        brandBackgroundLoginImage: baseApiUrl + 'v2/admin/hospitals/%id%/settings/BrandBackgroundLoginImage',
        contactUsImage: baseApiUrl + 'v2/admin/hospitals/%id%/settings/ContactUsImage',
        providerLogo: baseApiUrl + 'v2.1/providers/profile-images?hospitalId=%id%'
    },

    

};