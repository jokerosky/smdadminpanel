import * as Vocabulary from '../localization/vocabulary';

export const ConsultationMeetingsTableHeaders = [
    { id: 'hospitalId', label: Vocabulary.TABLE_HOSPITAL_ID },
    { id: 'hospitalName', label: Vocabulary.TABLE_HOSPITAL_NAME },
    { id: 'callerUserId', label: Vocabulary.TABLE_CALLER_USER_ID },
    { id: 'callerName', label: Vocabulary.TABLE_CALLER_NAME },
    { id: 'patientUserId', label: Vocabulary.TABLE_PATIENT_USER_ID },
    { id: 'calleeUserId', label: Vocabulary.TABLE_CALLEE_USER_ID },
    { id: 'calleeName', label: Vocabulary.TABLE_CALLEE_NAME },
    { id: 'type', label: Vocabulary.TABLE_CALL_TYPE },
    { id: 'status', label: Vocabulary.TABLE_STATUS },
    { id: 'wihoutCharge', label: Vocabulary.TABLE_WITHOUT_CHARGE },
    { id: 'startTime', label: Vocabulary.TABLE_START_TIME },
    { id: 'callDuration', label: Vocabulary.TABLE_CALL_DURATION_MIN_SECS },
    { id: 'messageInReply', label: Vocabulary.TABLE_MESSAGES_IN_REPLY },
    { id: 'replyLength', label: Vocabulary.TABLE_REPLY_LENGTH_CHARACTERS },
    { id: 'chatDuration', label: Vocabulary.TABLE_CHAT_DURATION_MINS_SECS }
];

export let LoggedMessagesTableHeaders = [
    { id: 'id', label: Vocabulary.TABLE_ID },
    { id: 'dateStamp', label: Vocabulary.TABLE_DATE_STAMP },
    { id: 'messageOrigin', label: Vocabulary.TABLE_MESSAGE_ORIGIN },
    { id: 'messageType', label: Vocabulary.TABLE_MESSAGE_TYPE },
    { id: 'message', label: Vocabulary.TABLE_MESSAGE },
    { id: 'recommendation', label: Vocabulary.TABLE_RECOMENDATION },
    { id: 'logLevel', label: Vocabulary.TABLE_LOG_LEVEL },
    { id: 'ipAddres', label: Vocabulary.TABLE_IP_ADDRES }
];

export let ServicesTableHeaders = [
    Vocabulary.TABLE_DESCRIPTION,
    Vocabulary.TABLE_SERVICE_STATUS,
    Vocabulary.TABLE_LAST_RUN_TIME,
    Vocabulary.TABLE_VERSION,
    Vocabulary.TABLE_CYCLE_SEC,
    Vocabulary.TABLE_LOGGING_LEVEL,
    ' '
];

export const IntegrationLogsTableHeaders = [
    { id: 'IntegrationLogId', label: Vocabulary.TABLE_INTEGRATION_LOG_ID },
    { id: 'LogType', label: Vocabulary.TABLE_LOG_TYPE },
    { id: 'DateCreated', label: Vocabulary.TABLE_DATE_CREATED },
    { id: 'PatientId', label: Vocabulary.TABLE_PATIENT_ID },
    { id: 'HospitalStaffProfileId', label: Vocabulary.TABLE_HOSPITAL_STAFF_PROFILE_ID },
    { id: 'PartnerId', label: Vocabulary.TABLE_PARTNER_ID },
    { id: 'LogMessage', label: Vocabulary.TABLE_LOG_MESSAGE },
    { id: 'HospitalId', label: Vocabulary.TABLE_HOSPITAL_ID },
    { id: 'CommandName', label: Vocabulary.TABLE_COMMAND_NAME }
];