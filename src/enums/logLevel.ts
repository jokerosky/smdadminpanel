export enum LogLevel {
    debug = 0,
    info = 1,
    warn = 2,
    error = 3,
    critical = 4,
    off = 5
}

export let serverLogingLevel =[
    {value: 0, label: 'Fatal'},
    {value: 1, label: 'Warning'},
    {value: 2, label: 'Info'},
    {value: 3, label: 'Debug'},
    {value: 4, label: 'Trace'}
];
