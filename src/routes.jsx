import React from 'react';
import { Route, IndexRoute, Redirect } from 'react-router';
//import { Route, IndexRoute, Redirect } from 'react-router-dom';


import { Endpoints } from './enums/endpoints';
import * as userTypes from './enums/userTypes';
import _ from 'lodash';

import App from './components/app';
import LoginCmpnt from './components/common/LoginCmpnt';
import ResetPasswordCmpnt from './components/common/ResetPasswordCmpnt';
import SelectLoginCmpnt from './components/common/SelectLoginCmpnt';
import PageNotFoundCmpnt from './components/common/PageNotFoundCmpnt';
import ErrorPageCmpnt from './components/common/ErrorPageCmpnt';

import PatientApp from './components/patient/PatientApp';
import ProviderApp from './components/provider/ProviderApp';
import ClientAdminApp from './components/clientAdmin/ClientAdminApp';

import ProviderDashboardCmpnt from './components/provider/ProviderDashboardCmpnt';
import { ProviderAccountPageCmpnt } from './components/provider/ProviderAccountPageCmpnt';

import RegistrationFormCmpnt from './components/patient/RegistrationFormCmpnt';
import PatientDashboardCmpnt from './components/patient/PatientDashboardCmpnt';
import SmdaAddNewClientCmpnt from './components/snapMdAdmin/SmdaAddNewClient';

import SnapMdAdminApp from './components/snapMdAdmin/SnapMdAdminApp';
import SnapMdDashboardCmpnt from './components/snapMdAdmin/SnapMdDashboardCmpnt';
import SmdaClientSettingsCmpnt from './components/snapMdAdmin/snapContentMenuCmpnts/SmdaClientSettings';
import SmdaClientDetailsCmpnt from './components/snapMdAdmin/snapContentMenuCmpnts/SmdaClientDetails';
import SmdaClientOpHoursCmpnt from './components/snapMdAdmin/snapContentMenuCmpnts/SmdaContentOpHoursCmpnt';
import SmdaClientSMSTemplate from './components/snapMdAdmin/snapContentMenuCmpnts/SmdaClientSMSTemplate';
import SmdaClientSOAPCodes from './components/snapMdAdmin/snapContentMenuCmpnts/SmdaClientSOAPCodes';
import SmdaClientOrganizations from './components/snapMdAdmin/snapContentMenuCmpnts/SmdaClientOrganizations';
import SmdaLocations from './components/snapMdAdmin/snapContentMenuCmpnts/SmdaLocations';
import SmdaHospitalDocument from './components/snapMdAdmin/snapContentMenuCmpnts/SmdaHospitalDocument';
import SmdaCodeSetsCmpnt from './components/snapMdAdmin/snapContentMenuCmpnts/SmdaCodeSets';

import ViewConsultationMeetingsCmpnt from './components/snapMdAdmin/snapMdAdminMenuCmpnts/ViewConsultationMeetings';
import ViewLoggedMessagesCmpnt from './components/snapMdAdmin/snapMdAdminMenuCmpnts/ViewLoggedMessages';
import ViewIntegrationLogsCmpnt from './components/snapMdAdmin/snapMdAdminMenuCmpnts/ViewIntegrationLogs';
import ViewServicesCmpnt from './components/snapMdAdmin/snapMdAdminMenuCmpnts/ViewServicesCmpnt';

import ViewConsultationMeetingsV2 from './components/snapMdAdmin/snapMdAdminMenuCmpnts/ViewConsultationMeetingsV2';
import SmdaClientSettingsV2 from './components/snapMdAdmin/snapContentMenuCmpnts/SmdaClientSettingsV2';

import { TestWrapperCmpnt } from './components/testWrapper';
//import { rootPath } from '../tools/rootPathSetting';

//change it for production or test deploy to
//Endpoints.site.root = '/v2/dist/';
Endpoints.site.root = "/";

// just for fun, although we type admin word anyway :)
let patient = _.findKey(userTypes, x => x == userTypes.patient);
let provider = _.findKey(userTypes, x => x == userTypes.provider);
let admin = _.findKey(userTypes, x => x == userTypes.admin);
let snapmdadmin = _.findKey(userTypes, x => x == userTypes.snapMdAdmin);

var getRoutes = (securityService, store) => {
  return (
    <Route path={Endpoints.site.root} component={App}  >

      <IndexRoute component={SelectLoginCmpnt} />
      {/* fallback */}
      <Route path={"index"} component={SelectLoginCmpnt} />

      <Route path={patient} component={PatientApp} onEnter={securityService.checkLogin} >
        <IndexRoute component={PatientDashboardCmpnt} />
        <Route path={Endpoints.site.dashboardPostfix} component={PatientDashboardCmpnt} />
      </Route>

      <Route path={provider} component={ProviderApp} onEnter={securityService.checkLogin}>
        <IndexRoute component={ProviderDashboardCmpnt} />
        <Route path={Endpoints.site.dashboardPostfix} component={ProviderDashboardCmpnt} />
        <Route path={Endpoints.site.accountPostfix} component={ProviderAccountPageCmpnt} />
      </Route>

      <Route path={admin} component={ClientAdminApp} onEnter={securityService.checkLogin}>
        <IndexRoute component={LoginCmpnt} />
      </Route>

      <Route path={snapmdadmin} component={SnapMdAdminApp} onEnter={securityService.checkLogin}>
        {/*<IndexRoute component={SnapMdDashboardCmpnt} />*/}
        <Route path={Endpoints.site.dashboardPostfix + '(/:id)'} component={SnapMdDashboardCmpnt} >

          <Route path={Endpoints.site.snapMdAdmin.settingsPostfix} component={SmdaClientSettingsCmpnt} />
          <Route path={Endpoints.site.snapMdAdmin.settingsv2Postfix} component={SmdaClientSettingsV2} />
          <Route path={Endpoints.site.snapMdAdmin.detailsPostfix} component={SmdaClientDetailsCmpnt} />
          <Route path={Endpoints.site.snapMdAdmin.opHoursPostfix} component={SmdaClientOpHoursCmpnt} />
          <Route path={Endpoints.site.snapMdAdmin.smsTemplatePostfix} component={SmdaClientSMSTemplate} />
          <Route path={Endpoints.site.snapMdAdmin.soapCodesPostfix} component={SmdaClientSOAPCodes} />
          <Route path={Endpoints.site.snapMdAdmin.organizationsPostfix} component={SmdaClientOrganizations} />
          <Route path={Endpoints.site.snapMdAdmin.locationsPostfix} component={SmdaLocations} />
          <Route path={Endpoints.site.snapMdAdmin.hospitalDocPostfix} component={SmdaHospitalDocument} />
          <Route path={Endpoints.site.snapMdAdmin.codeSetsPostfix} component={SmdaCodeSetsCmpnt} />
          <Route path={Endpoints.site.snapMdAdmin.addNewClientPostfix} component={SmdaAddNewClientCmpnt} />
        </Route>
        <Route path={Endpoints.site.snapMdAdmin.consultationmeetingsPostfix} component={ViewConsultationMeetingsCmpnt} />
        <Route path={Endpoints.site.snapMdAdmin.loggedMessagesPostfix} component={ViewLoggedMessagesCmpnt} />
        <Route path={Endpoints.site.snapMdAdmin.integrationLogsPostfix} component={ViewIntegrationLogsCmpnt} />
        <Route path={Endpoints.site.snapMdAdmin.servicesPostfix} component={ViewServicesCmpnt} />
        <Route path={Endpoints.site.snapMdAdmin.consultationmeetingsv2Postfix} component={ViewConsultationMeetingsV2} />
      </Route>

      <Route path={Endpoints.site.loginPostfix + "/:userType"} component={LoginCmpnt} />
      <Route path={Endpoints.site.forgotPasswordPostfix + "/:userType"} component={ResetPasswordCmpnt} />


      <Route path={Endpoints.site.patientSignup} component={RegistrationFormCmpnt} />

      <Route path={Endpoints.site.siteError + '(/:code)'} component={ErrorPageCmpnt} />

      <Route path={'test' + '(/:code)'} component={TestWrapperCmpnt} />

      {/*<Redirect from="*" to={Endpoints.site.siteError+'/404'} />*/}
      <Route path="*" component={PageNotFoundCmpnt} />
    </Route>
  )
}

export default getRoutes