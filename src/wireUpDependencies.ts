
import {LocalStorageService, SessionStorageService} from './services/storageService';

export default function wireUpDependencies() {
  return {
      localStorageService: new LocalStorageService(),
      sessionStorageService: new SessionStorageService()
  };
}
