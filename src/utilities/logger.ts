export enum LogLevel {
    debug = 0,
    info = 1,
    warn = 2,
    error = 3,
    critical = 4,
    off = 5
}

export interface ILog {
    debug(msg: string) : any;
    info (msg: string) : any;
    warn(msg: string) : any;
    error(msg: string, err?:any) : any;
    critical(msg: string, err?: any) : any;

    setLogLevel(level:LogLevel):any;
}

export interface ILayout{
    formatMessage:(msg:string, level: LogLevel, err?: any)=>string;
}

export interface IAppender {
    logMessage: (message: string, level: LogLevel, err?: any) => any;
    layout:ILayout;
}

export class Logger implements ILog {
    constructor(name: string, logLevel?: LogLevel) {
        this.name = name;
        this.logLevel = logLevel ? logLevel : LogLevel.info;

        this.appenders = [new ConsoleAppender(this.name)];
    }

    name: string;
    logLevel: LogLevel;
    appenders: IAppender[];

    static defaultLogger: ILog = null;
    static getLogger(name?: string) {
        if (name) {
            return new Logger(name);
        }

        if (!Logger.defaultLogger) {
            Logger.defaultLogger = new Logger('Site logger');
        }
        return Logger.defaultLogger;
    }

    setLogLevel(level:LogLevel){
        this.logLevel = level;
    }

    logMessage(msg: string, level: LogLevel, err?:any) {
        if (level < this.logLevel) return;

        for (let i = 0; i < this.appenders.length; i++) {
            this.appenders[i].logMessage(msg, level, err);
        }
    };

    debug(msg: string) { this.logMessage(msg, LogLevel.debug); };
    info(msg: string) { this.logMessage(msg, LogLevel.info); };
    warn(msg: string) { this.logMessage(msg, LogLevel.warn); };
    error(msg: string, err?: any) { this.logMessage(msg, LogLevel.error, err); };
    critical(msg: string, err?: any) { this.logMessage(msg, LogLevel.critical, err); };
}

export class ConsoleAppender implements IAppender {
    constructor(loggerName:string, logLevel?: LogLevel) {
        this.loggerName = loggerName;
        this.minLogLevel = logLevel ? logLevel : LogLevel.info ;
        this.layout = new StandardLayout(this.loggerName);

    }
    minLogLevel: LogLevel
    loggerName: string;
    layout: ILayout;

    logMessage(message: string, level: LogLevel, err?: any) {
        if (level < this.minLogLevel) {
            return;
        }
        let msg = this.layout.formatMessage(message, level);
        switch (level) {
            case LogLevel.debug: { console.info(msg); break; }
            case LogLevel.info: { console.info(msg); break; }
            case LogLevel.warn: { console.warn(msg); break;}
            case LogLevel.error: { console.error(msg, err); break; }
            case LogLevel.critical: { console.error(msg, err); break; }
            default: {
                console.log(message);
            }
        }
    };
}

export class StandardLayout implements ILayout{
    constructor(loggerName:string, locale?: string){
        this.loggerName = loggerName;
        this.locale = locale ? locale : 'en-us';

    }
    locale: string;
    loggerName: string;

    formatMessage(msg: string, level: LogLevel) {
        let date = new Date().toLocaleString(this.locale);
        let levelString = Object.keys(LogLevel).find((key)=>{ return LogLevel[key] == level});

        let result = `${date}:[${this.loggerName}]:[${levelString}]:[${msg}]`;

        return result;
    };

}