import * as _ from 'lodash';

export class ObjectHelper{


    /// Modifies the object being passed into
    static decapitalizeProperties(obj:any){
        // take all keys, 
        // create new object 
        // add each field with 

        if( typeof obj == 'string' ||
            typeof obj == 'number' ||
            typeof obj == 'boolean' ||
            typeof obj == 'undefined' ||
            obj == null
        )
        return obj;

        if(obj instanceof Array){
            for(let item in obj){
                item = ObjectHelper.decapitalizeProperties(item);
            }
        }

        if(Object.keys(obj).length == 0){
            return obj;
        } else {
            for(let key in obj){
                if(key[0] != key[0].toLowerCase()){
                    let newKey = key[0].toLowerCase() + key.substr(1);
                    obj[newKey] = ObjectHelper.decapitalizeProperties(obj[key]);
                    delete obj[key];
                } else {
                    obj[key] = ObjectHelper.decapitalizeProperties(obj[key]);
                }
            }
        }

        return obj;
    }
    
    static boolFromDataBaseString(strBool:string){
        if(strBool === 'True' || strBool === 'true'){
            return true;
        }
        else  {
            return false
        }
    }

    static dataBaseStringFromBool(value:boolean){
        if(value === true){
            return 'True';
        }
        else if(value === false){
            return 'False';
        } else 
            return value;
    }
    
    static generateRequestPath(fieldQuery:any[]){
        let query = '';
        for(let index in fieldQuery) {
            query = query + fieldQuery[index].key + '=' + fieldQuery[index].value + '&';
        }
        query = query.substring(0,query.length-1);
        query = query.replace(/\[/g, '%5B');
        query = query.replace(/\]/g, '%5D');
        query = query.replace(/\:/g, '%3A');
        return query;
    }

    static generateRequestPathLoggedMessages(fieldQuery: any[]) {
        let query = this.generateRequestPath(fieldQuery);
        query = query.replace(/\+/g, '%2B');
        query = query.replace(/\ /g, '+');
        return query;
    }

    static getLocaleForMomentDateTime(language: string){
        if(language.search(/us/i) === 0){
            return 'en';
        } else if(language.search(/es/i) === 0){
            return 'es';
        } else if(language.search(/ru/i) === 0){
            return 'ru';
        } else {
            return 'en';
        }
    }

    static removeTrailingSlash(path:string):string {
        if(path[path.length-1] == '/'){
            return path.substr(0, path.length - 1);
        }
        else{
            return path;
        }
    }
}