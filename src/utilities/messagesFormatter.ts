
export default class MessagesFormatter {

    static formatHttpDataIsNotAnObject(response:any):string {
        return `status code [${response.status}] but data is not an object. Data [${response.data}]`;
    }
    static    formatUIResponseError(responseError:any):string {
        try {
            let response = responseError.response;
            if (typeof response !== 'undefined') {
                if (response.data) {
                    if (response.data.exceptionMessage)
                        return `${response.data.exceptionMessage}`;
                }
                return `${response.data.message || response.data}`;
            }
            return `${responseError.message}`;
        }
        catch(exp){
            return `Can't parse error response`;
        }

    }
    
    static formatHttpResponseError(responseError:any):string {
        // structure of responseError  { config, message, response, stack }
        try {
            let response = responseError.response;
            if (typeof response !== 'undefined') {
                if (response.data) {
                    if (response.data.exceptionMessage)
                        return `status code [${response.status}] status [${responseError.message}] response [${response.data.exceptionMessage}]`;
                }
                return `status code [${response.status}] status [${responseError.message}] response [${response.data.message || response.data}]`;
            }
            return `status [${responseError.message}] url [${responseError.config.url}] `;
        }
        catch(exp){
            responseError = responseError || { message:'undefined' };
            return `status [${responseError.message}]`;
        }



    }
}