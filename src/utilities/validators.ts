import * as _ from 'lodash';
import * as moment from 'moment';

import SiteNotification from '../models/site/siteNotification';

export interface IValidatable {
    prepareValidationRules();
    validate(event?: any): boolean;
    validationSets: any;
    validationErrors: any;
}

export class Validator {
    static isEmail(email: string): boolean {
        //very simple pattern, can change for more relevant 
        return /^[a-zA-Z0-9_.+-]+@[a-zA-Z0-9-]+\.[a-zA-Z0-9-.]+$/.test(email);
    }

    static isNotEmptyOrWhitespace(str: string): boolean {
        if(str)
            return !!str.trim();
        else 
            return false;
    }

    static isMinPassLength(str: string): boolean {
        return str.length > 4;
    }

    static isDate(str: string, format?:string): boolean {
        if(format){
            return moment(str, format, true).isValid();
        }

        let timestamp = Date.parse(str);
        return !isNaN(timestamp); 
    }

    static isOlderThanTwelve(str: string,  format?:string): boolean {
        let dob = moment(str, format, true);
        let dif = new Date().getFullYear() - dob.toDate().getFullYear();
        return dif > 12;
    }

    static isNumber(number: number): boolean{
        if (!isNaN(Number(number))) {
            return true;
        } else {
            return false;
        }
        
    }
}

export class ValidationError {
    component?: string;
    messages: string[];
    elementId?: string;
    errorClass?: string;
}

export class ValidationSet {
    elementId: string;
    element?: any;
    rules: ((object: any) => string)[];
}

export class ValidationResult {
    isValid: boolean;
    errors?: ValidationError[];
}

export function validateSet(vSet: ValidationSet): ValidationResult {
    let result = {
        isValid: true,
        errors: []
    } as ValidationResult;

    let errorMessages = [];
    for (let i = 0; i < vSet.rules.length; i++) {
        let message;
        if (vSet.element) {
            message = vSet.rules[i](vSet.element.value);
        }
        else{
            message = vSet.rules[i](null);
        }
        if (!!message) {
            errorMessages.push(message);
        }
    }
    if (errorMessages.length > 0) {
        result.isValid = false;
        result.errors.push({
            elementId: vSet.elementId,
            messages: errorMessages
        } as ValidationError);
    }

    return result;
};

export function validateAll(vSets: ValidationSet[]): ValidationResult {
    let result = {
        isValid: true,
        errors: []
    } as ValidationResult;

    for (let i = 0; i < vSets.length; i++) {
        let tempResult = validateSet(vSets[i]);

        result.isValid = result.isValid && tempResult.isValid;
        result.errors = [...result.errors, ...tempResult.errors];
    }

    return result;
};

export function getCssFlags(cmpntClassName: string, cssClass: string, validations: ValidationError[]): object {
    let result = {};
    result = validations.filter((x: ValidationError) => {
        return x.component === cmpntClassName
    }).map((x) => {
        return x.elementId;
    });

    // get component ids from validations where component equals passed value
    result = _.zipObject(result as string[],
        _.fill(Array((result as string[]).length), cssClass));

    return result;
}