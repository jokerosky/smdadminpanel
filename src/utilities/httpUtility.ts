import axios from 'axios';

import * as actions from '../actions/siteActions';

export default class HttpUtility {

    static getResponseData(response:any ):any {
        let result = null;
        if(response.data)
            if(response.data.data)
                if(response.data.data[0] && response.data.total < 2)
                    result = response.data.data[0];
                else
                    result = response.data.data;
            else
                result = response.data;
        else
            result = response;
        
        return result;
    }

    static configureSnapMdHeaders(keys) {
        axios.defaults.headers.common['X-Api-Key'] = keys.apiKey;
        axios.defaults.headers.common['X-Developer-Id'] = keys.developerId;
        keys.apiSessionId ? axios.defaults.headers.common['X-Api-Session-Id'] = keys.apiSessionId : null;
    }

    static configureAuthorization(token) {
        if(token)
            axios.defaults.headers.common['Authorization'] = `Bearer ${token}`;
    }

    static configure401Interceptor( dispatch ) {
        axios.interceptors.response.use( (response) => {
            return response;
        }, (error) => {
            try{
                if(error.response.status == 401)
                {
                    //here we can redirect to error page or to login page
                    dispatch(actions.handleUnauthorizeResponse(error.response));
                }
            }
            catch(err)
            {

            }            

            // Do something with response error
            return Promise.reject(error);
        });
    }
}