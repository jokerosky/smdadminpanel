import * as _ from 'lodash';
import * as React from 'react';
import { IMappedSetting } from '../models/site/settingsMapping';
import { EditorType } from '../enums/editorTypes';

import { ObjectHelper } from '../utilities/objectHelper';

import { ToggleButton } from '../components/common/inputs/ToggleButtonCmpnt';

var noAction = ()=>{};

var textCreator = function(setting: IMappedSetting){
    return (
    <div className='form-row'>
        <input type='text'
                    className="form-control"
                    name={setting.key} 
                    defaultValue={setting.value}
                    onChange = {setting.action || noAction}
                  />
    </div>)
};

var toggleCreator = function(setting: IMappedSetting){
    return <ToggleButton name = {setting.key}
                         isChecked = { ObjectHelper.boolFromDataBaseString(setting.value) } 
                         value = { setting.value } 
                         onChange = {setting.action || noAction}
                         />
}

var addressCreator = function(setting: IMappedSetting){
    return  '';
}

var dropdownCreator = function(setting: IMappedSetting){
    return  '';
}

export class EditorCreator{
    static editorDefinitions = [
        { type: EditorType.edit , creator: textCreator },
        { type: EditorType.toggle , creator: toggleCreator },
        { type: EditorType.dropdown, creator: dropdownCreator }
    ]

    static addCreator(type: EditorType ,creator: (settings:IMappedSetting)=>any){
        this.editorDefinitions.push({type, creator});
    }

    static getEditor(setting: IMappedSetting):any {
        let editorCreatorRecord  = _.find(this.editorDefinitions, ( elm )=>{ return elm.type == setting.editorType });
        if(editorCreatorRecord)
        {
            return editorCreatorRecord.creator(setting);
        } 
        else 
        {
            return '';
        }
    }

}