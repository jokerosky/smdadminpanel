import * as _ from 'lodash';
import * as moment from 'moment';

export function toDate(val:string, format?:string):Date {
    return moment(val, format).toDate();
}

export function secondsToMinutesString(time: number): string {
    let minutes: string = '';
    let seconds: string = '';
    if (time > 0) {
        if ((time % 60) < 60) {
            minutes = Math.floor((time / 60)).toFixed();
            seconds = (time % 60).toString();
        }
        return minutes + ':' + seconds;
    }
    return '';
}

export function getFormattedDate(titles: string): string {
    let date: Date;
    let title: string = '';
    date = new Date(titles);
    title = date.getUTCDate() + '/' + (date.getUTCMonth() + 1) + '/' + date.getUTCFullYear() + ' ' +
        date.getUTCHours() + ':' + date.getUTCMinutes();

    return title;
}