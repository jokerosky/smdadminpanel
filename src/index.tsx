import * as  React from 'react';
import { render } from 'react-dom';
import { Router, browserHistory } from 'react-router';
import { syncHistoryWithStore } from 'react-router-redux';
import { Provider } from 'react-redux';

import getRoutes from './routes';
import configureStore from './configureStore';

import state from './reducers/initialState';
import { SignalRService } from './services/signalR/signalRService';
import { SecurityService } from './services/securityService';

import * as siteActions from './actions/siteActions';
import * as types from './enums/actionTypes';

import HospitalDataService from './services/api/hospitalDataService';
import { Logger } from './utilities/logger';
import HttpUtility from './utilities/httpUtility';

import { configureSnapIoC, SnapMDServiceLocator, MyServiceLocator } from './dependenciesInjectionConfiguration';

// have to add script to index.html file for now
//import  'jquery'; 
declare var $: any;
import 'ms-signalr-client';

//----------- INITIALIZATION ----------------------
Logger.getLogger().warn('SnapMD front-end application is starting');
const store = configureStore(state);
const history = syncHistoryWithStore(browserHistory, store);

configureSnapIoC(store, $);

var securityService = SnapMDServiceLocator.getObject<SecurityService>('SecurityService');
var signalRService = SnapMDServiceLocator.getObject<SignalRService>('SignalRService');

store.dispatch(siteActions.reloadStateAfterRefresh());
store.dispatch(siteActions.loadHospitalIdAndApiKeys());

var token = store.getState().user.accessToken;
if (token) {
  signalRService.initSignalR(token);
}


HttpUtility.configure401Interceptor(store.dispatch);
HttpUtility.configureAuthorization(store.getState().user.accessToken);

//tony.y: very dirty hack. need this for <link onload=''> function  which Helmet can paste only as text, so when it will support dynamic functionality we should remove this code:
if (typeof window !== 'undefined') {
  (window as any).dispatch = store.dispatch;
}

browserHistory.listen((action) => {
  store.dispatch({ type: types.SITE_CLEAR_TEMP_STATE });
})
//---------------------------------------------------

render(
  <Provider store={store}>
    <Router history={history} routes={getRoutes(securityService, store)} />
  </Provider>,
  document.getElementById('app')
);
