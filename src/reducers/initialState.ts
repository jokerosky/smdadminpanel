import * as userTypes from '../enums/userTypes';
import * as _ from 'lodash';
import { ClientDataDTO, SeatsDTO } from '../models/DTO/snapmdAdminStateDTO';
import { Client } from '../models/state/snapMDAdminClient';
import { CodeSets } from '../models/state/snapMDAdminCodeSets';
import { Details } from '../models/state/snapMDAdminDetails';
import { HospitalDocument } from '../models/state/snapMDAdminHospitalDocument';
import { Lists } from '../models/state/snapMDAdminLists';
import { Misc } from '../models/state/snapMDAdminMisc';
import { SnapAdmin } from '../models/state/snapMDAdminSnapAdmin';
import { AddressDTO, AddressObject } from '../models/DTO/addressDTO';
//import { HospitalSettings } from '../models/state/snapMDAdminSettings';
import { getDefaultState} from '../models/DTO/hospitalSettingDTO';

/**SnapAdmin */
var seats: SeatsDTO = {
  modAdminEHRERx: 0,
  modAdminPMFull: 0,
  modAdminPMPart: 0,
  modClSubTotalSeats: 0,
  modEHRERx: 0,
  modTotalSeats: 0,
  snAdmin: 0,
  snClSubTotalSeats: 0,
  snEnterprise: 0,
  snRestricted: 0,
  snTotalSeats: 0,
  snUnRestricted: 0,
};

var clientData: ClientDataDTO = {
  appointmentsContactNumber: '',
  brandName: '',
  brandTitle: '',
  children: 0,
  cliniciansCount: 0,
  consultationCharge: 0,
  contactNumber: '',
  email: '',
  hospitalCode: '',
  hospitalDomainName: '',
  hospitalId: 0,
  hospitalImage: '',
  hospitalName: '',
  hospitalType: 0,
  isActive: '',
  itDeptContactNumber: '',
  nPINumber: '',
  seats: 0,
  totalUsersCount: 0,
};

var address: AddressDTO = {
  address: '',
  hospitalId: 0,
  addressObject: {
    addressText: '',
    city: '',
    country: '',
    countryCode: '',
    line1: '',
    line2: '',
    postalCode: '',
    state: '',
    stateCode: ''
  } as AddressObject
};

var details: Details = {
  clientData: clientData,
  seats: seats,
  seatsAssigned: seats,
  address: address
};

var codeSets: CodeSets = {
  codeSetsRecords: [],
  codeSetsTypeId: 0,
};

var hospitalDocument: HospitalDocument = {
  selectedDocument: 0,
  textDocument: ''
};

var organizations = [];

var settings: any = getDefaultState();

var smsTemplates = [];
var soapCodes = '';
var operatingHours = [];
var locations = [];

var client: Client = {
  details: details,
  organizations: organizations,
  settings: settings,
  smsTemplates: smsTemplates,
  soapCodes: soapCodes,
  codeSets: codeSets,
  operatingHours: operatingHours,
  hospitalDocument: hospitalDocument,
  locations: locations,
};

var lists: Lists = {
  clients: [],
  clientsTypes: [],
  codeSets: [],
  documentTypes: [],
  organizationTypes: [],
  partner: [],
  partnerOnlyActive: [],
  countries: []
};

var misc: Misc = {
  selectedId: null,
  clientView: '',
  filters: {},
  hospitalName: ''
};

var snapAdmin: SnapAdmin = {
  misc: misc,
  client: client,
  lists: lists,
  consultationMeetings: [],
  logMessages: [],
  services: [],
  integrationLogs: []
};
/** */

export default {
  site: {
    ajax: {
      ajaxCallsInProgress: 0,
      hospitalDataLoaded: false,
      stylesLoaded: false,
      dataLoaded: false,
      session: {} as any
    },
    notifications: {
      notifications: [],
      notificationsCount: 0,
    },
    errors: {
      isCriticalError: false,
      lastError: '',
      lastErrorEventName: ''
    },
    validation: {
      validations: [],
      registrationAddressIsValid: null
    },
    misc: {
      language: 'us',
      enableHospitalStyles: true,
      vocabulary: {}
    }
  },
  hospital: {},
  consultations: {
    waitingList: []
  },
  user: {
    userId: 0,
    userType: userTypes.guest,
    userTypeName: _.findKey(userTypes, x => { return x == userTypes.guest }),
    accessToken: '',
    tokenExpires: '',
    passwordResetSuccess: false,
    profile: {},

    loginError: false,
    registrationStep: 1,
    registrationData: {}
  },
  provider: {
    profile: {}
  },
  snapmdAdmin: snapAdmin
};