import * as _ from 'lodash';

import * as types from '../enums/actionTypes';
import initialState from './initialState';
import HttpUtility from '../utilities/httpUtility';

import SiteNotification from '../models/site/siteNotification';
import {EventType} from '../enums/eventType';

export default function siteNotificationsReducer(state = initialState.site.notifications, action) {
    let count = state.notificationsCount;
            
    switch (action.type) {
        case types.VALIDATION_RESULT_ADD:{
            //in this event also should be passed notifications;
            let notifications = action.notifications.map((x:SiteNotification) =>{ x.notificationId = count++ ; return x;})
            return Object.assign({}, state, { notifications:[...state.notifications, ...action.notifications], notificationsCount: count });
        } 

        case types.GLOBAL_NOTIFICATION_ADD:{
            if(action.notification.message){
                action.notification.notificationId = count++;
                return Object.assign({}, state, { notifications:[...state.notifications, action.notification], notificationsCount: count });
            }
            return state;
        }

        case types.GLOBAL_NOTIFICATION_REMOVE:{
            let notifications = state.notifications
                                        .filter((x:SiteNotification)=>{
                                                if( ! _.includes(action.ids, x.notificationId))
                                                     return x;
                                            });
            return Object.assign({}, state, { notifications: notifications});
        } 

        case types.AJAX_CALL_ERROR:{
            let notification;
            if(action.message !== '' || action.error !== ''){
                let msg = '';
                if(action.error && action.message){ msg =  action.error + ' : ' + action.message}
                else if(action.error){ msg =  action.error }
                else if(action.message){ msg =  action.message }
                
                notification = {
                    notificationId: count++,
                    type: EventType.error,
                    message: msg,
                } as SiteNotification;
            }
            
            
            return Object.assign({}, state, { notifications:[...state.notifications, notification], notificationsCount: count });        
        }

        case types.LOGIN_SUCCESS:{
            return { notifications:[], notificationsCount:0 };
        }

        default:
            return state;

    }

}