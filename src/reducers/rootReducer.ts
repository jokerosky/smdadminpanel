import { combineReducers } from 'redux';
import { routerReducer } from 'react-router-redux';

import userReducer from './userReducer';
import hospitalDataReducer from './hospitalDataReducer';
import providerReducer from './providerReducer';
import consultationReducer from './consultationsReducer';

import siteAjaxReducer from './siteAjaxReducer';
import siteNotificationsReducer from './siteNotificationsReducer';
import siteErrorsReducer from './siteErrorsReducer';
import siteValidationsReducer from './siteValidationsReducer';
import siteMiscReducer from './siteMiscReducer';

import snapAdminDetailsReducer from './snapMdAdminClientReducers/snapAdminDetailsReducer';
import snapAdminOrganizationsReducer from './snapMdAdminClientReducers/snapAdminOrganizationsReducer';
import snapAdminSettingsReducer from './snapMdAdminClientReducers/snapAdminSettingsReducer';
import snapAdminSMSTemplateReducer from './snapMdAdminClientReducers/snapAdminSMSTemplateReducer';
import snapAdminSOAPCodesReducer from './snapMdAdminClientReducers/snapAdminSOAPCodesReducer';
import snapAdminCodeSetsReducer from './snapMdAdminClientReducers/snapAdminCodeSetsReducer';
import snapAdminOpHoursReducer from './snapMdAdminClientReducers/snapAdminOpHoursReducer';
import snapAdminHospitalDocumentReducer from './snapMdAdminClientReducers/snapAdminHospitalDocumentReducer';
import snapAdminLocationReducer from './snapMdAdminClientReducers/snapAdminLocationReducer';
import snapAdminListReducer from './snapMdAdminRestReducerc/snapAdminListReducer';
import snapAdminMiscReducer from './snapMdAdminRestReducerc/snapAdminMiscReducer';

import snapAdminConsultationMeetingsReducer from './snapMdAdminMenuReducers/consultationMeetingsReducers';
import snapAdminLoggedMessagesReducer from './snapMdAdminMenuReducers/logMessagesReducers';
import snapAdminServicesReducers from './snapMdAdminMenuReducers/servicesReducers';
import snapAdminIntegrationLogsReducer from './snapMdAdminMenuReducers/integrationLogsReducer';

const rootReducer = combineReducers({
    user: userReducer,
    hospital: hospitalDataReducer,
    consultations: consultationReducer,
    site: combineReducers({
         ajax: siteAjaxReducer,
         notifications: siteNotificationsReducer,
         errors: siteErrorsReducer,
         validation: siteValidationsReducer,
         misc:siteMiscReducer
    }),
    provider: providerReducer,
    routing: routerReducer,
    snapmdAdmin: combineReducers({
        lists: snapAdminListReducer,
        misc: snapAdminMiscReducer,
        client: combineReducers({
            details: snapAdminDetailsReducer,
            organizations: snapAdminOrganizationsReducer,
            settings: snapAdminSettingsReducer,
            smsTemplates: snapAdminSMSTemplateReducer,
            soapCodes: snapAdminSOAPCodesReducer,
            codeSets: snapAdminCodeSetsReducer,
            operatingHours: snapAdminOpHoursReducer,
            hospitalDocument: snapAdminHospitalDocumentReducer,
            locations: snapAdminLocationReducer
        }),
        consultationMeetings: snapAdminConsultationMeetingsReducer,
        logMessages: snapAdminLoggedMessagesReducer,
        services: snapAdminServicesReducers,
        integrationLogs: snapAdminIntegrationLogsReducer
    })
});

export default rootReducer;
