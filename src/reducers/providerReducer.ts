import * as _ from 'lodash';

import * as types from '../enums/actionTypes';
import initialState from './initialState';



export default function providerReducer(state = initialState.provider, action) {
    switch (action.type) {
        case types.LOGIN_CLEAR: {
            return Object.assign({}, state, { profile: {} });
        }

        case types.STAFF_PROFILE_DATA_LOADED:{
            return Object.assign({}, state, { profile: action.data });
        } 

        default:
            return state;

    }

}