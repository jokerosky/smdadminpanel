import * as _ from 'lodash';

import * as types from '../enums/actionTypes';
import initialState from './initialState';

import SiteNotification from '../models/site/siteNotification';

export default function siteErrorsReducer(state = initialState.site.errors, action) {
    switch (action.type) {
        case types.SITE_CRIRICAL_ERROR : {
            return Object.assign({}, state, { lastError:action.error });
        }
        
        case types.AJAX_CALL_ERROR:{
            return Object.assign({}, state, { lastError:action.error });
                 //TODO: make errors array with max elements count or something like this...
        }

        case types.HOSPITAL_ID_AND_DEV_API_KEYS_FAILURE:{
                return Object.assign({}, state, { lastError:action.error , isCriticalError: true });
            } 
 
        case types.REGISTRATION_NEW_PATIENT_ERROR: {
            return Object.assign({}, state, { lastError:action.message, lastErrorEventName: types.REGISTRATION_NEW_PATIENT_ERROR});
        }

        default:
            return state;

    }

}