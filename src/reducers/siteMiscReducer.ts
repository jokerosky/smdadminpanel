import * as _ from 'lodash';

import * as types from '../enums/actionTypes';
import initialState from './initialState';


export default function siteMiscReducer(state = initialState.site.misc, action) {
    switch (action.type) {
        case types.SITE_HOSPITAL_STYLES_TOGGLED: {

            return Object.assign({}, state, {enableHospitalStyles: action.enableHospitalStyles });
        }

        case types.LANGUAGE_CHANGED: {
            return Object.assign({}, state, {language: action.language});
        }

        case types.DICTIONARY_CHANGED: {
            return Object.assign({}, state, {vocabulary: action.dictionary});
        }

        default:
            return state;

    }

}