import * as _   from 'lodash';
import * as types from '../../enums/actionTypes';
import initialState from '../initialState';
import {Misc} from '../../models/state/snapMDAdminMisc';

export default function snapAdminMiscReducer(state = initialState.snapmdAdmin.misc, action){

    switch(action.type){
        case types.SNAPMDADMIN_CLIENT_SELECTED:{
            let id = null;
            if(action.id !== null && action.id > -1){
                id = Number(action.id);
            }
            return Object.assign({},state, {selectedId: id});
        }

        case types.SNAPMDADMIN_SET_CLIENT_NAME:{
            let hospitalName = '';
            if(state.selectedId !== null){
                let index = _.findIndex(action.hospitals, {hospitalId: state.selectedId});
                hospitalName = action.hospitals[index].hospitalName;
            }
            return Object.assign({},state, {hospitalName: hospitalName});
        }

        case types.SNAPMDADMIN_FILTERS_CHANGED:{
            return Object.assign({},state, {filters: action.filters});
        }

        case types.SNAPMDADMIN_VIEW_SELECTED:
        {   
            return Object.assign({}, state, { clientView: action.view });
        }

        case types.SNAPMDADMIN_HOSPITAL_DATA_UPDATE:
        {
            return Object.assign({},state, {hospitalName: action.clientForLists.hospitalName});
        }

        case types.LOGIN_CLEAR:
        {
            let newState: Misc = Object.assign({}, initialState.snapmdAdmin.misc);
            return newState;
        }
        
        default: 
            return state;
    }

}
