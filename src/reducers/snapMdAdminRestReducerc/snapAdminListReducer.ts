import * as _   from 'lodash';
import * as types from '../../enums/actionTypes';
import initialState from '../initialState';
import {ObjectHelper} from '../../utilities/objectHelper';
import {Lists} from '../../models/state/snapMDAdminLists';
import {SiteConstants} from '../../enums/siteConstants';

export default function snapAdminListReducer(state = initialState.snapmdAdmin.lists, action){

    switch(action.type){
        case types.SNAPMDADMIN_CLIENTS_LOADED:{
            return Object.assign({},state, {clients: action.data});
        }

        case types.SNAPMDADMIN_CLIENT_ADDED:
        {
            let newState: any [] = _.map(state.clients);
            newState.push(action.client);
            return Object.assign({}, state, { clients: newState });
        }

        case types.SNAPMDADMIN_HOSPITAL_DATA_UPDATE:
        {
            let newState: any [] = _.map(state.clients);
            let index = _.findIndex(newState, {hospitalId: action.hospitalId});

            newState[index] =  action.clientForLists;
            newState = newState.map((x)=>{return x});

            return Object.assign({}, state, { clients: newState });
        }

        case types.SNAPMDADMIN_LISTS_LOADED:
        {
            return Object.assign({}, state, action.listData );
        }

        case types.LOGIN_CLEAR:
        {
            let newState: Lists = Object.assign({}, initialState.snapmdAdmin.lists);
            return newState;
        }

        case types.SNAPMDADMIN_SETTINGS_COUNTRIES_LOADED:
        {
            return Object.assign({}, state, {countries: action.countries} );
        }

        default: {
            return state;
        }
    }

}
