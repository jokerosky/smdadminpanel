import * as _ from 'lodash';

import * as types from '../enums/actionTypes';
import initialState from './initialState';
import HttpUtility from '../utilities/httpUtility';

import SiteNotification from '../models/site/siteNotification';

export default function siteAjaxReducer(state = initialState.site.ajax, action) {
    switch (action.type) {
        case types.BEGIN_AJAX_CALL: {
            return Object.assign({}, state, { ajaxCallsInProgress: state.ajaxCallsInProgress + 1 });
        }

        case types.END_AJAX_CALL: {
            return Object.assign({}, state, { ajaxCallsInProgress: state.ajaxCallsInProgress - 1 });
        }

        case types.AJAX_CALL_ERROR: {
            return Object.assign({}, state, { ajaxCallsInProgress: state.ajaxCallsInProgress - 1 });
            //TODO: make errors array with max elements count or something like this...
        }

        case types.HOSPITAL_ID_AND_DEV_API_KEYS_LOADED:
            {
                let session: any = {
                    'hospitalId': action.data.hospitalId,
                    'apiKey': action.data.key,
                    'developerId': action.data.id,
                    'apiSessionId': action.data.apiSessionId
                };
                HttpUtility.configureSnapMdHeaders(session);

                return Object.assign({}, state, session);
            }

        case types.LOAD_HOSPITAL_DATA_SUCCESS:
            {
                return Object.assign({}, state, { hospitalDataLoaded: true });
            }

        case types.STYLES_LOADED:
            {
                return Object.assign({}, state, { stylesLoaded: true });
            }

        case types.HOSPITAL_ID_AND_DEV_API_KEYS_FAILURE:
            {
                return Object.assign({}, state, { stylesLoaded: true });
            }
 
        case types.STAFF_PROFILE_DATA_LOADED:
            {
                return Object.assign({}, state, { dataLoaded: true });
            }

        case types.LOGIN_CLEAR:
            {
                return Object.assign({}, state, { dataLoaded: false });
            }
            

        default:
            return state;

    }

}