import * as _ from 'lodash';

import * as types from '../enums/actionTypes';
import { SiteConstants } from '../enums/siteConstants';
import * as userTypes from '../enums/userTypes';
import initialState from './initialState';

export default function userReducer(state: any = initialState.user, action: any) {
    let clearState = {
        loginError: false,
        redirectUrl:'',
        passwordResetSuccess: false,
        registrationStep: 1,
        registrationData: {}
    };

    switch (action.type) {
        case types.LOGIN_TYPE_SELECTED:
            {
                let userTypeName = _.findKey(userTypes, x => { return x == action.userType });
                return Object.assign({}, state, { userType: action.userType, userTypeName: userTypeName, loginError: false, redirectUrl: action.redirectUrl });
            }
        case types.LOGIN_SUCCESS:
            {
                return Object.assign({}, state, { accessToken: action.tokenData.access_token, tokenExpires: action.tokenData.expires, loginError: false });
            }
        case types.LOGIN_FAILURE:
            {
                return Object.assign({}, state, { loginError: true });
            }
        case types.LOGIN_CLEAR:
            {
                return Object.assign({}, state, { accessToken: '', tokenExpires: '', loginError: false });
            }

        case types.RESET_PASSWORD_SUCCESS: {
            return Object.assign({}, state, { loginError: false, passwordResetSuccess: true });
        }

        case types.RESET_PASSWORD_FAILURE: {
            return Object.assign({}, state, { loginError: true });
        }

        case types.SITE_CLEAR_TEMP_STATE:
            {
                return Object.assign({}, state, clearState);
            }

        case types.SET_REGISTRATION_FORM_STEP: {
            return Object.assign({}, state, { registrationStep: action.step });
        }

        case types.SET_REGISTRATION_FORM_DATA: {
            return Object.assign({}, state, { registrationData: action.data });
        }

        case types.REGISTRATION_ADDRESS_VALIDATED: {
            let step = action.response.isAvailable ? 2 : 4;//registrationStep for registration form component
            return Object.assign({}, state, { registrationStep: step });
        }

        case types.REGISTRATION_NEW_PATIENT_ERROR: {
            let response = action.message;

            return Object.assign({}, state, {registrationStep: 2});
        }

        case types.REGISTRATION_NEW_PATIENT_REGISTERED: {
            let response = action.response;

            return Object.assign({}, state, {registrationStep: 3, userId: response.patientId });
        }

        case types.LOAD_SESSION_STORAGE_DATA: {
            let userData = {};
            let data = action.data[SiteConstants.session.sessionData];
            if(data){
                userData = { accessToken:data.access_token, tokenExpires: data.expires }
            }

            return Object.assign({}, state, userData );
        }

        default:
            return state;
    }
}