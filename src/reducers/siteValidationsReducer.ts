import * as _ from 'lodash';
import * as types from '../enums/actionTypes';
import { ValidationError } from '../utilities/validators';
import initialState from './initialState';



export default function siteValidationsReducer(state = initialState.site.validation, action) {
    switch (action.type) {
        case types.VALIDATION_RESULT_ADD:{
            //delete validations of the component
            let validations = state.validations.filter((x:ValidationError)=>{ return x.component != action.cmpntClassName });

            return Object.assign({}, state, {validations:[ ...validations, ...action.validationErrors]});
        } 

        case types.SITE_CLEAR_TEMP_STATE: {
            return Object.assign({}, state, {validations:[ ]});
        }

        default:
            return state;

    }

}