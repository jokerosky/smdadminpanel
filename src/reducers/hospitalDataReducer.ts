import * as types from '../enums/actionTypes';
import initialState from './initialState';

export default function hospitalDataReducer(state = initialState.hospital , action){
    switch(action.type){
        case types.LOAD_HOSPITAL_DATA_SUCCESS:
        {   
            return Object.assign({}, state, action.hospitalData);
        }
        default:
            return state;

    }
}