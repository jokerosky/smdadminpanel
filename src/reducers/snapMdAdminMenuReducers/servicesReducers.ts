import * as _ from 'lodash';
import * as types from '../../enums/actionTypes';
import initialState from '../initialState';

export default function servicesReducer(state = initialState.snapmdAdmin.services, action) {

    switch (action.type) {

        case types.SERVICES_LOADED:
            {
                let newState: any[] = _.map(state);
                newState = action.services.ServiceList;
                return newState;
            }

        case types.SERVICES_STATUS_UPDATE:
            {
                let newState: any[] = _.map(state);
                let index = _.findIndex(newState, { ServiceId: action.serviceStatusParameters.ServiceId });
                newState[index].CycleMS = action.serviceStatusParameters.CycleMS;
                newState[index].Status = action.serviceStatusParameters.Status;
                newState[index].LoggingLevel = action.loggingLevelParameters.LoggingLevel;

                return newState;
            }

        case types.LOGIN_CLEAR:
        {
            return [];
        }    

        default:
            return state;
    }

}