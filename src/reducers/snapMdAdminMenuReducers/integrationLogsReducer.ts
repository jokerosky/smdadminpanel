import * as _ from 'lodash';
import * as types from '../../enums/actionTypes';
import initialState from '../initialState';

export default function snapAdminIntegrationLogsReducer(state = initialState.snapmdAdmin.integrationLogs, action) {

    switch (action.type) {

        case types.INTEGRATION_LOGS_DATA_LOADED:
        {
            let newState: any[] = _.map(state);
                newState = action.logs;
                return newState;
        }

        case types.LOGIN_CLEAR:
            {
                return [];
            }

        default:
            return state;
    }

}