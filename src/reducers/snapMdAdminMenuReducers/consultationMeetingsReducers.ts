import * as _ from 'lodash';
import * as types from '../../enums/actionTypes';
import initialState from '../initialState';

export default function snapAdminConsultationMeetingsReducer(state = initialState.snapmdAdmin.consultationMeetings, action) {

    switch (action.type) {

        case types.CONSULTATION_MEETINGS_DATA_LOADED:
        {
            let consultation = [];
                let newState: any[] = _.map(state);
                for (let index in action.data) {
                    consultation.push(action.data[index]);
                }
                newState = consultation;
                return newState;
            }

        case types.LOGIN_CLEAR:
            {
                let newState: any = Object.assign({}, initialState.snapmdAdmin.consultationMeetings);
                return newState;
            }

        default:
            return state;
    }

}