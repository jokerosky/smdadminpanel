import * as _ from 'lodash';
import * as types from '../../enums/actionTypes';
import initialState from '../initialState';

export default function snapAdminLoggedMessagesReducer(state = initialState.snapmdAdmin.logMessages, action) {

    switch (action.type) {

        case types.LOGGED_MESSAGES_DATA_LOADED:
            {
                let messages = [];
                let newState: any[] = _.map(state);
                for (let index in action.data) {
                    messages.push(action.data[index]);
                }
                newState = messages;
                return newState;
            }

        case types.LOGIN_CLEAR:
            {
                let newState: any = Object.assign({}, initialState.snapmdAdmin.logMessages);
                return newState;
            }

        default:
            return state;
    }

}