import * as _   from 'lodash';
import * as types from '../../enums/actionTypes';
import initialState from '../initialState';

export default function snapAdminSMSTemplateReducer(state = initialState.snapmdAdmin.client.smsTemplates, action){

    switch(action.type){

        case types.SNAPMDADMIN_LOAD_SMS_TEMPLATES:
        {
            let smsTemplates = [];
            let newState: any[] = _.map(state);
            if(action.smsTemplates instanceof Array){
                for(let index in action.smsTemplates){
                    smsTemplates.push(action.smsTemplates[index]);
                }
            } else if(action.smsTemplates instanceof Object){
                smsTemplates.push(action.smsTemplates);
            }
            newState = smsTemplates;
            return newState;
        }

        case types.SNAPMDADMIN_ADD_SMS_TEMPLATES:
        {
            let newState:any[] = _.map(state);
            newState.push(action.smsTemplates);
            newState = newState.map((x)=>{return x});
            return newState
        }


        case types.SNAPMDADMIN_UPDATE_SMS_TEMPLATES:
        {
            let newState:any[] = _.map(state);
            let index = _.findIndex(newState, {id:action.smsTemplates.id});
            newState.splice(index,1, action.smsTemplates);
            newState = newState.map((x)=>{return x});

            return newState;
        }
        
        case types.SNAPMDADMIN_DELETE_SMS_TEMPLATES:
        {
            let newState:any[] = _.map(state);
            let array:Array<any>=[];
            array = _.filter(newState, 
                (tmplt:any)=>{ 
                   return tmplt.id != action.messageId                        
                }
            );
            newState = array;
           return newState;
        }

        case types.LOGIN_CLEAR:
        {
            let newState = Object.assign({}, initialState.snapmdAdmin.client.smsTemplates);
            return newState;
        }
        
        default: 
            return state;
    }

}
