import * as _ from 'lodash';
import * as types from '../../enums/actionTypes';
import initialState from '../initialState';

export default function snapAdminLocationReducer(state = initialState.snapmdAdmin.client.locations, action) {

    switch (action.type) {

        case types.SNAPMDADMIN_LOAD_ORGANIZATIONS_WITH_LOCATIONS:
            {
                let arr: Array<any> = Array.isArray(action.locations) ? action.locations : Array(action.locations);
                return arr;
            }

        case types.SNAPMDADMIN_ADD_LOCATIONS:
            {
                let newState: any[] = _.map(state);
                newState[action.organizationId].locations.push(action.location);
                newState = newState.map((x) => { return x });

                return newState;
            }

        case types.SNAPMDADMIN_UPDATE_LOCATIONS:
            {
                let newState: any[] = _.map(state);
                let index = _.findIndex(state[action.organizationId].locations, { id: action.location.id });

                newState[action.organizationId].locations.splice(index, 1, action.location);
                newState = newState.map((x) => { return x });

                return newState;
            }

        case types.SNAPMDADMIN_DELETE_LOCATIONS:
            {
                let newState: any[] = _.map(state);
                let array: Array<any> = [];

                array = _.filter(newState[action.organizationId].locations,
                    (loc: any) => {
                        if (loc.id != action.locationId) {
                            return loc;
                        }
                    }
                );

                newState[action.organizationId].locations = array;
                newState = newState.map((x) => { return x });

                return newState;
            }

        case types.LOGIN_CLEAR:
            {
                let newState = Object.assign({}, initialState.snapmdAdmin.client.locations);
                return newState;
            }

        default:
            return state;
    }

}
