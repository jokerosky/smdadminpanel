import * as _   from 'lodash';
import * as types from '../../enums/actionTypes';
import initialState from '../initialState';

export default function snapAdminSOAPCodesReducer(state = initialState.snapmdAdmin.client.soapCodes, action){

    switch(action.type){

        case types.SNAPMDADMIN_LOAD_DIAGNOSTIC_CODING_SYSTEM:
        {
            return action.soapDCS;
        }

        case types.LOGIN_CLEAR:
        {
            let newState = Object.assign({}, initialState.snapmdAdmin.client.soapCodes);
            return newState;
        }
        
        default: 
            return state;
    }

}
