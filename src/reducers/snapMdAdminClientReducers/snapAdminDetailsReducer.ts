import * as _   from 'lodash';
import * as types from '../../enums/actionTypes';
import initialState from '../initialState';
import { Details } from '../../models/state/snapMDAdminDetails';

export default function snapAdminDetailsReducer(state = initialState.snapmdAdmin.client.details, action){

    switch(action.type){
        case types.SNAPMDADMIN_CLIENTS_SEATS_LOADED:
        {
            return Object.assign({}, state, { seats: action.seats });
        }

        case types.SNAPMDADMIN_CLIENTS_SEATS_ASSIGNED_LOADED:
        {
            return Object.assign({}, state, { seatsAssigned: action.seatsAssigned });
        }        
        case types.SNAPMDADMIN_CLIENTS_DATA_LOADED:
            {
                return Object.assign({}, state, { clientData: action.data });
            }
        case types.SNAPMDADMIN_CLIENT_ACTIVE:
        {
            let newState = Object.assign({}, state);
            newState.clientData.isActive = action.active;
            return newState;
        }

        case types.SNAPMDADMIN_HOSPITAL_DATA_UPDATE:
        {
            let newState: any = Object.assign({}, state);
            newState.clientData = {};
            newState.clientData = action.clientData;
            return Object.assign({}, state, newState);
        }

        case types.SNAPMDADMIN_HOSPITAL_ADDRESS_LOADED :
        {
            return Object.assign({}, state, { address: action.address });
        }

        case types.LOGIN_CLEAR:
        {
            let newState: Details = Object.assign({}, initialState.snapmdAdmin.client.details);
            return newState;
        }
        case types.SNAPMDADMIN_SETTINGS_CLEAR:
            {
                let newState: Details = Object.assign({}, initialState.snapmdAdmin.client.details);
                return newState;
            }
        
        default: 
            return state;
    }

}
