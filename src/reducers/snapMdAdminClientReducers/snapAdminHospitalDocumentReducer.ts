import * as _   from 'lodash';
import * as types from '../../enums/actionTypes';
import initialState from '../initialState';
import {HospitalDocument} from '../../models/state/snapMDAdminHospitalDocument';

export default function snapAdminHospitalDocumentReducer(state = initialState.snapmdAdmin.client.hospitalDocument, action){

    switch(action.type){
        
        case types.SNAPMDADMIN_LOAD_HOSPITAL_DOCUMENT:
        {
            return Object.assign({}, state, {
                textDocument: action.document,
                selectedDocument: action.selectDocument
            });
        }

        case types.LOGIN_CLEAR:
        {
            let newState: HospitalDocument = Object.assign({}, initialState.snapmdAdmin.client.hospitalDocument);
            return newState;
        }
        
        default: 
            return state;
    }

}
