import * as _   from 'lodash';
import * as types from '../../enums/actionTypes';
import initialState from '../initialState';
import {CodeSets} from '../../models/state/snapMDAdminCodeSets';

export default function snapAdminCodeSetsReducer(state = initialState.snapmdAdmin.client.codeSets, action){

    switch(action.type){
        
        case types.SNAPMDADMIN_LOAD_CODE_SET_SETTINGS:
        {
            return Object.assign({}, state, {codeSetsRecords: action.codes});
        }

        case types.SNAPMDADMIN_ADD_CODE_SET:
        {
            return Object.assign({}, state, { codeSetsRecords:[...state.codeSetsRecords, action.codeSet]});
        }

        case types.SNAPMDADMIN_UPDATE_CODE_SET:
        {
            let newState = Object.assign({}, state);
            let index = _.findIndex(newState.codeSetsRecords, {DisplayOrder: action.codeSet.DisplayOrder});

            newState.codeSetsRecords.splice(index,1, action.codeSet);
            newState.codeSetsRecords = newState.codeSetsRecords.map((x)=>{return x});

            return newState;
        }
        case types.SNAPMDADMIN_SELECTED_CODE_SET_TYPE_ID:
        {
            return Object.assign({}, state, {codeSetsTypeId: action.codeSetsTypeId});
        }

        case types.LOGIN_CLEAR:
        {
            let newState: CodeSets = Object.assign({}, initialState.snapmdAdmin.client.codeSets);
            return newState;
        }
        
        default: 
            return state;
    }

}
