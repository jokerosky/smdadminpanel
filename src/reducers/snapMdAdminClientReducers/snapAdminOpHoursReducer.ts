import * as _   from 'lodash';
import * as types from '../../enums/actionTypes';
import initialState from '../initialState';

export default function snapAdminOpHoursReducer(state = initialState.snapmdAdmin.client.operatingHours, action){

    switch(action.type){
        case types.SNAPMDADMIN_OPERATING_HOURS_LOADED:
        {
            let newState: any[] = _.map(state);
            if(action.hours instanceof Array){
                newState=action.hours;
            } else if(action.hours instanceof Object){
                let arr:any = [action.hours];
                newState = arr;
            }

            return newState;
        }

        case types.LOGIN_CLEAR:
        {
            let newState = Object.assign({}, initialState.snapmdAdmin.client.operatingHours);
            return newState;
        }
        
        default: 
            return state;
    }

}
