import * as _   from 'lodash';
import * as types from '../../enums/actionTypes';
import initialState from '../initialState';

export default function snapAdminOrganizationsReducer(state = initialState.snapmdAdmin.client.organizations, action){

    switch(action.type){
        
        case types.SNAPMDADMIN_LOAD_ORGANIZATION:
        {
            return action.organization;
        }

        case types.SNAPMDADMIN_ADD_ORGANIZATION:
        {
            let newState: any[] = _.map(state);
            newState.push(action.newOrganization);
            newState = newState.map((x)=>{return x});

            return state;
        }

        case types.SNAPMDADMIN_UPDATE_ORGANIZATION:
        {
            let newState: any[] = _.map(state);
            let index = _.findIndex(newState, {id:action.organization.id});

            newState.splice(index,1, action.organization);
            newState = newState.map((x)=>{return x});

            return newState;
        }

        case types.LOGIN_CLEAR:
        {
            let newState = Object.assign({}, initialState.snapmdAdmin.client.organizations);
            return newState;
        }
        
        default: 
            return state;
    }

}