import * as _ from 'lodash';
import * as types from '../../enums/actionTypes';
import initialState from '../initialState';
import { SettingsDTO } from '../../models/DTO/settingsDTO';
import { ObjectHelper } from '../../utilities/objectHelper';
import { IMappedSetting } from '../../models/site/settingsMapping';


export default function snapAdminSettingsReducer(state = initialState.snapmdAdmin.client.settings, action) {

    switch (action.type) {
        case types.SNAPMDADMIN_CLEAR_STATE: {
            return Object.assign({}, initialState.snapmdAdmin.client.settings);
        }


        case types.SNAPMDADMIN_CLIENT_SETTINGS_LOADED:
            {
                let newState = Object.assign({}, state);

                action.settings.forEach((x: SettingsDTO) => {
                    newState[x.key] = x;
                });

                return newState;
            }

        case types.SNAPMDADMIN_CLIENT_SETTING_CHANGED:
            {
                let setting: IMappedSetting = action.setting;
                let newState = Object.assign({}, state);

                let applySettingChanges = (setting: IMappedSetting) => {
                    if (setting.children && setting.children.length > 0) {
                        setting.children.forEach(x => {
                            applySettingChanges(x);
                        });
                    }

                    newState[setting.key] = {
                        key: setting.key,
                        value: setting.value
                    } as SettingsDTO;
                };

                applySettingChanges(setting);

                return newState;
            }

        /****************************************************************************** */

        case types.SNAPMDADMIN_SETTINGS_MODULES_LOADED:
            {
                let tempState: any = Object.assign({}, state);
                let arrModules: Array<any> = Array.isArray(action.modules) ? action.modules : Array(action.modules);
                if (arrModules.length > 0) {
                    for (let index in arrModules) {
                        if ('value' in arrModules[index]) {
                            if (arrModules[index].value.search(/true|false/i) === 0) {
                                tempState[arrModules[index].key.replace('PatientProfile.', '').replace('Integrations.', '').replace('EHR.', '').replace('SnapMD.', '')] = ObjectHelper.boolFromDataBaseString(arrModules[index].value);
                            } else {
                                tempState[arrModules[index].key.replace('PatientProfile.', '').replace('Integrations.', '').replace('EHR.', '').replace('SnapMD.', '')] = arrModules[index].value;
                            }
                        } else {
                            tempState[arrModules[index].key.replace('PatientProfile.', '').replace('Integrations.', '').replace('EHR.', '').replace('SnapMD.', '')] = null;
                        }

                    }
                }
                return Object.assign(tempState);
            }

        case types.SNAPMDADMIN_SETTINGS_CLEAR:
            {
                let tempState: any = Object.assign({}, action.state);

                return Object.assign({}, tempState);
            }

        case types.SNAPMDADMIN_SAVE_IMAGE:
            {
                let tempState: any = Object.assign({}, state);
                tempState[action.fieldNameToSave] = action.imageUrl;

                return Object.assign({}, tempState);
            }

        case types.SNAPMDADMIN_UPDATE_TOGGLE:
            {
                let newState = Object.assign({}, state);

                newState[action.stateToggle.key] = ObjectHelper.boolFromDataBaseString(action.stateToggle.value);
                return newState;
            }

        default:
            return state;
    }

}