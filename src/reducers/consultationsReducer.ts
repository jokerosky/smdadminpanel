import * as types from '../enums/actionTypes';
import initialState from './initialState';
import { PatientQueueResponseDTO } from '../models/DTO/PatientQueueResponseDTO';

export default function consultationReducer(state = initialState.consultations , action){
    switch(action.type){
        case types.PATIENT_QUEUE_LOADED :{
            return Object.assign({}, state, { waitingList: [] } );
        }
        case types.PATIENT_QUEUE_SIGNALR_RECIVED :
        {   
            let data:PatientQueueResponseDTO = action.data;
            return Object.assign({}, state, { waitingList: data.schedulPatientWaitingList || [] } );
        }
        case types.PATIENT_QUEUE_ERROR : {
            return Object.assign({}, state, { waitingList: [] } );
        }

        default:
            return state;

    }

}