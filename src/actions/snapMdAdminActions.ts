import * as ajaxStatusActions from './ajaxStatusActions';
import * as types from '../enums/actionTypes';
import { SettingsDTO } from '../models/DTO/settingsDTO';
import { IServiceLocator, SnapMDServiceLocator } from '../dependenciesInjectionConfiguration';
import { Logger } from '../utilities/logger';
import { Endpoints, toAbsolutePath} from '../enums/endpoints';
import { ISnapMdHttpService, SnapMdHttpService } from '../services/api/httpServices';
import { AddressDTO, AddressObject } from '../models/DTO/addressDTO';
import { push } from 'react-router-redux';


function getClientsHttpAsync(httpService: ISnapMdHttpService): Promise<any> {
    return httpService.getViaHttp(Endpoints.snapmdAdmin.clients.replace('/%id%', ''));
}

function getListCodeSetsHttpAsync(httpService: ISnapMdHttpService): Promise<any> {
    return httpService.getViaHttp(Endpoints.snapmdAdmin.codeSets);
}

function getListOrganizationTypesHttpAsync(httpService: ISnapMdHttpService): Promise<any> {
    return httpService.getViaHttp(Endpoints.snapmdAdmin.organizationTypes);
}

function getListDocumentTypesHttpAsync(httpService: ISnapMdHttpService): Promise<any> {
    return httpService.getViaHttp(Endpoints.snapmdAdmin.documentTypes);
}

function getListClientsTypesHttpAsync(httpService: ISnapMdHttpService): Promise<any> {
    return httpService.getViaHttp(Endpoints.snapmdAdmin.clientsTypes);
}

function getListPartnerHttpAsync(httpService: ISnapMdHttpService): Promise<any> {
    return httpService.getViaHttp(Endpoints.snapmdAdmin.partner);
}

function getListPartnerOnlyActiveHttpAsync(httpService: ISnapMdHttpService): Promise<any> {
    return httpService.getViaHttp(Endpoints.snapmdAdmin.partnerOnlyActive);
}

function loadSettingsHttpAsync(httpService: ISnapMdHttpService, hospitalId: number, fields: string[]): Promise<any> {
    return httpService.getViaHttp(Endpoints.hospital.hospitalSettings, hospitalId, fields);
}

function postAddNewClientHttpAsync(httpService: ISnapMdHttpService, data: any): Promise<any> {
    return httpService.postViaHttp(Endpoints.snapmdAdmin.clients.replace('%id%', ''), data);
}

function putHospitalSettingsHttpAsync(httpService: ISnapMdHttpService, hospitalSettings: any, hospitalId: number): Promise<any>{
    return httpService.putViaHttp(Endpoints.hospital.hospitalSettings, hospitalSettings, hospitalId);
}

function putHospitalAddressHttpAsync(httpService: ISnapMdHttpService, hospitalId: number): Promise<any>{
    return httpService.putViaHttp(Endpoints.snapmdAdmin.hospitalAddress.replace(/v2\//i, '').replace(/\/%id%/i,''), {hospitalId});
}

function getHospitalAddressAsync(httpService: ISnapMdHttpService, hospitalId: number): Promise<any>{
    return httpService.getViaHttp(Endpoints.snapmdAdmin.hospitalAddress, hospitalId);
}


export function loadClients(locator: IServiceLocator) {
    Logger.getLogger().debug('Launched SnapMdAdminActions, function loadClients');
    return function (dispatch) {
        dispatch(ajaxStatusActions.beginAjaxCall());
        var httpService = locator.getObject<ISnapMdHttpService>('ISnapMdHttpService');
        return getClientsHttpAsync(httpService)
            .then((data) => {
                dispatch(loadClientsSuccess(data));
                dispatch(findClientName(data));
                dispatch(ajaxStatusActions.endAjaxCall());
                Logger.getLogger().debug('loadClients function performed work and received data');
            }).catch((err: any) => {
                dispatch(ajaxStatusActions.ajaxCallError(err.message, 'Load clients error'));
                Logger.getLogger().error(err.message, err);
            });
    };
};

export function loadLists(locator: IServiceLocator) {
    Logger.getLogger().debug('Launched SnapMdAdminActions, function loadLists');
    return function (dispatch) {
        dispatch(ajaxStatusActions.beginAjaxCall());
        var httpService = locator.getObject<ISnapMdHttpService>('ISnapMdHttpService');
        let data = {};
        return getListCodeSetsHttpAsync(httpService)
            .then((codeSets) => {
                if (typeof codeSets === "string") {
                    codeSets = JSON.parse(codeSets);
                }
                Object.assign(data, { codeSets: codeSets });
                return getListOrganizationTypesHttpAsync(httpService);
            })
            .then((organizationTypes) => {
                Object.assign(data, { organizationTypes: organizationTypes });
                return getListDocumentTypesHttpAsync(httpService)
            })
            .then((documentTypes) => {
                Object.assign(data, { documentTypes: documentTypes });
                return getListClientsTypesHttpAsync(httpService)
            })
            .then((clientsTypes) => {
                Object.assign(data, { clientsTypes: clientsTypes });
                return getListPartnerHttpAsync(httpService)
            })
            .then((partner) => {
                Object.assign(data, { partner: partner });
                return getListPartnerOnlyActiveHttpAsync(httpService)
            })
            .then((partnerOnlyActive) => {
                Object.assign(data, { partnerOnlyActive: partnerOnlyActive });
                dispatch(getList(data));
                dispatch(ajaxStatusActions.endAjaxCall());
                Logger.getLogger().debug('loadLists function performed work and received data');
            }).catch((err: any) => {
                dispatch(ajaxStatusActions.ajaxCallError(err.message, 'Load lists error'));
                Logger.getLogger().error(err.message, err);
            });
    };
};

export function loadHospitalSettings(locator: IServiceLocator, fields: string[], hospitalId: number) {
    Logger.getLogger().debug('Launched SnapMdAdminActions, function loadHospitalSettings');
    return function (dispatch) {
        dispatch(ajaxStatusActions.beginAjaxCall());
        var httpService = locator.getObject<ISnapMdHttpService>('ISnapMdHttpService');
        return loadSettingsHttpAsync(httpService, hospitalId, fields)
            .then((settings: SettingsDTO[]) => {
                dispatch(getSettings(settings));
                dispatch(ajaxStatusActions.endAjaxCall());
                Logger.getLogger().debug('loadHospitalSettings function performed work and received data');
            })
            .catch((err: any) => {
                dispatch(ajaxStatusActions.ajaxCallError(err.message, 'Load Hospital settings error'));
                Logger.getLogger().error(err.message, err);
            });
    }
};

export function loadHospitalAddress(locator: IServiceLocator, hospitalId: number){
    Logger.getLogger().debug('Launched SnapMdAdminActions, function loadHospitalAddress');
    return function (dispatch) {
        dispatch(ajaxStatusActions.beginAjaxCall());
        var httpService = locator.getObject<ISnapMdHttpService>('ISnapMdHttpService');
        return getHospitalAddressAsync(httpService, hospitalId)
            .then((address: AddressDTO) => {
                dispatch(hospitalAddressLoaded(address));
                dispatch(ajaxStatusActions.endAjaxCall());
                Logger.getLogger().debug('loadHospitalAddress function performed work and received data');
            })
            .catch((err: any) => {
                dispatch(ajaxStatusActions.ajaxCallError(err.message, 'Load Hospital Address error'));
                Logger.getLogger().error(err.message, err);
            });
    }
   
}

export function addNewClient(locator: IServiceLocator, newClient: any, hospitalSettings: any, additionalSettings: any) {
    Logger.getLogger().debug('Launched SnapMdAdminActions, function addNewClient');
    return function (dispatch) {
        dispatch(ajaxStatusActions.beginAjaxCall());
        var httpService = locator.getObject<ISnapMdHttpService>('ISnapMdHttpService');
        let response: any = {};
        return postAddNewClientHttpAsync(httpService, newClient)
            .then((clients) => {          
                response['clients'] = clients;
                return putHospitalSettingsHttpAsync(httpService, hospitalSettings, response.clients.hospitalId);
                
            })
            .then((hospitalSettings) => {
                //TODO: write a response to hospital settings
                return putHospitalSettingsHttpAsync(httpService, additionalSettings, response.clients.hospitalId);
            })
            .then((additionalSettings) => {
                //TODO: write a response to hospital settings
                return putHospitalAddressHttpAsync(httpService, response.clients.hospitalId);
            })
            .then((hospitalAddress) => {
                dispatch(newClientAdded(response.clients));
                //TODO: write the answer to the state, until I know where
                dispatch(ajaxStatusActions.endAjaxCall());
                Logger.getLogger().debug('addNewClient function performed work and received data');
            })
            .catch((err: any) => {
                dispatch(ajaxStatusActions.ajaxCallError(err.message, 'Network Error: request addNewClient in SnapMdAdminActions'));
                Logger.getLogger().error(err.message, err);
            })
    }
}

export function loadClientsSuccess(data: any) {
    return { type: types.SNAPMDADMIN_CLIENTS_LOADED, data };
};

export function selectClient(id: number) {
    return { type: types.SNAPMDADMIN_CLIENT_SELECTED, id };
};

export function findClientName(hospitals: any[]) {
    return { type: types.SNAPMDADMIN_SET_CLIENT_NAME, hospitals }
};

export function filtersChanged(filters: any) {
    return { type: types.SNAPMDADMIN_FILTERS_CHANGED, filters };
};

export function selectView(view: string) {
    return { type: types.SNAPMDADMIN_VIEW_SELECTED, view };
};

export function getList(listData: any) {
    return { type: types.SNAPMDADMIN_LISTS_LOADED, listData };
};

export function getSettings(settings: SettingsDTO[]) {
    return { type: types.SNAPMDADMIN_CLIENT_SETTINGS_LOADED, settings  };
};

export function clearState() {
    return { type: types.SNAPMDADMIN_CLEAR_STATE };
};

function newClientAdded(client: any) {
    return { type: types.SNAPMDADMIN_CLIENT_ADDED, client };
};

function addNewHospitalSettings(){
    //TODO: implement the function
}

export function hospitalAddressLoaded(address: AddressDTO){
    return { type: types.SNAPMDADMIN_HOSPITAL_ADDRESS_LOADED, address}
};