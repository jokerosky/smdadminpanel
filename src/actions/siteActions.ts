import { push } from 'react-router-redux';
import * as _ from 'lodash';

import { Endpoints } from '../enums/endpoints';
import * as types from '../enums/actionTypes';
import { EventType } from '../enums/eventType';
import { SiteConstants } from '../enums/siteConstants';
import { ValidationError } from '../utilities/validators';
import SiteNotification from '../models/site/siteNotification';
import HospitalDataService from '../services/api/hospitalDataService';
import * as hospitalDataActions from './hospitalDataActions';
import * as ajaxStatusActions from './ajaxStatusActions';

import { SessionStorageService, IStorageService } from '../services/storageService';

export function loadHospitalIdAndApiKeys() {
    return function (dispatch) {
        dispatch(ajaxStatusActions.beginAjaxCall());
        return HospitalDataService.getIdAndApiKeys()
            .then((keys: any) => {
                dispatch(loadApiKeysSuccess(keys));
                return HospitalDataService.getData(keys.hospitalId);
            })
            .then(hospitalData => {
                dispatch(hospitalDataActions.loadHospitalDataSuccess(hospitalData));
                dispatch(ajaxStatusActions.endAjaxCall());
            })
            .catch(error => {
                //TODO: add logging service
                dispatch(ajaxStatusActions.ajaxCallError(error));
                dispatch(loadApiKeysFailure(error));

                dispatch(push(Endpoints.site.siteError));
            });
    };
}

export function handleUnauthorizeResponse(response) {
    return { type: types.HANDLE_UNAUTHORIZED_RESPONSE, response };
}

export function loadApiKeysSuccess(data) {
    return { type: types.HOSPITAL_ID_AND_DEV_API_KEYS_LOADED, data };
}

export function loadApiKeysFailure(error) {
    return { type: types.HOSPITAL_ID_AND_DEV_API_KEYS_FAILURE, error };
}

export function stylesLoaded() {
    return { type: types.STYLES_LOADED };
}

export function showValidationErrors(errors: ValidationError[], cmpntClassName: string) {
    let notifications: SiteNotification[] = [];
    let validationErrors: ValidationError[] = [];

    validationErrors = errors.map((x: ValidationError) => {
        x.component = cmpntClassName;
        return x;
    });

    for (let i = 0; i < errors.length; i++) {
        for (let k = 0; k < errors[i].messages.length; k++) {
            let validFlag = cmpntClassName + "#" + errors[i].elementId;
            notifications.push({
                message: errors[i].messages[k],
                comment: validFlag,
                type: EventType.error,
                notificationId: 0
            })
        }
    }

    return { type: types.VALIDATION_RESULT_ADD, notifications, validationErrors, cmpntClassName };
}

export function addNotifications(notification: SiteNotification) {
    return { type: types.GLOBAL_NOTIFICATION_ADD, notification };
}

export function removeNotifications(ids: number[]) {
    return { type: types.GLOBAL_NOTIFICATION_REMOVE, ids: ids };
}


export function reloadStateAfterRefresh(sstSrv?: IStorageService): any {
    if (!sstSrv) {
        sstSrv = new SessionStorageService();
    }
    let data = {};

    _.each(SiteConstants.session, (key)=>{
        let value = sstSrv.get(key);
        if(value)
            data[ key ] = value;
    });
    
    return {type: types.LOAD_SESSION_STORAGE_DATA, data: data};
}

export function saveSessionData(key:string, data: any, sstSrv?: IStorageService) {
    if (!sstSrv) {
        sstSrv = new SessionStorageService();
    }

    sstSrv.add(key, data);

    let storedData = {};

    if(key){
        storedData[key] = data;
    } else {
        storedData = data
    }

    return { type: types.SAVE_SESSION_STORAGE_DATA, key, data: storedData };
}

export function loadSessionData(key?: string, sstSrv?: IStorageService) {
    if (!sstSrv) {
        sstSrv = new SessionStorageService();
    }

    if(!key){
        let userData = sstSrv.get("session_data");

        return { type: types.LOAD_SESSION_STORAGE_DATA, data: { user: userData } };
    }

    
    return { type: types.LOAD_SESSION_STORAGE_DATA, data: sstSrv.get(key) };
}

export function deleteSessionData(key?: string, sstSrv?: IStorageService) {
    if (!sstSrv) {
        sstSrv = new SessionStorageService();
    }

    if(!key){
        //clear all keys
        sstSrv.clear();
    }

    sstSrv.del("session_data");

    return { type: types.DELETE_SESSION_STORAGE_DATA, key };
}


export function logoutUserCleanup(){
    document.body.className = '';
    return { type: types.SITE_LOGOUT_CLEANUP };
}

export function toggleMainMenu(wantClose?:boolean){
    let isMenu = 
    document.body.className.split(' ').findIndex(x=>{return x == 'is-main-nav'}) > -1;

    if(isMenu){
            document.body.className = document.body.className.split(' ')
                .filter(x=>{ return x && x != 'is-main-nav'})
                .join();
        } else {
            document.body.className = document.body.className + " is-main-nav" ;
        }

    return { type: types.SITE_MAIN_MENU_TOGGLED }
}

export function toggleHospitalStyles(isEnabled:boolean){
    return { type: types.SITE_HOSPITAL_STYLES_TOGGLED, enableHospitalStyles: isEnabled }
}