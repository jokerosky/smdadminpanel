import * as types from '../enums/actionTypes';
import hospitalDataService from '../services/api/hospitalDataService';
import * as ajaxStatusActions from './ajaxStatusActions'; 


export function  loadHospitalData(hospitalId){
    return function(dispatch){
        return hospitalDataService.getData(hospitalId).then( hospitalData => {
            dispatch(loadHospitalDataSuccess(hospitalData));
        }).catch( error => {
            dispatch(ajaxStatusActions.ajaxCallError(error));
        });
    };
}

export function  loadHospitalDataSuccess(hospitalData){
    return {type: types.LOAD_HOSPITAL_DATA_SUCCESS, hospitalData};
}