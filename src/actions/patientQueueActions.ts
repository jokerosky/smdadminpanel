import * as types from '../enums/actionTypes';
import { SiteConstants } from '../enums/siteConstants';
import * as ajaxStatusActions from '../actions/ajaxStatusActions';
import { PatientQueueResponseDTO } from '../models/DTO/PatientQueueResponseDTO';

import { AppointmentService } from '../services/api/appointmentService';
import { SignalRService, HubNames} from '../services/signalR/signalRService';
import { SnapWaitingRoomConsultationsHub } from '../services/signalR/snapWaitingRoomConsultationsHub';

import { SnapMDServiceLocator } from '../dependenciesInjectionConfiguration';

// corresponding with signalR event names in SnapWaitingRoomConsultationsHub
let actionNames = [
    'dispatchPatientConsultationInformation',
    'dispatchAdminPatientQueue',
    'dispatchAdminPatientQueuePatientCount',
    'dispatchPatientQueuePatientCount',
    'lockRequest',
    'unlockRequest',
    'appointmentDismissed',
    'appointmentSaveClosed',
    'onUpdateFlag'
];

export { actionNames }

export function dispatchAdminPatientQueue(queueResponse: PatientQueueResponseDTO) {
    return { type: types.PATIENT_QUEUE_SIGNALR_RECIVED, data: queueResponse };
}

export function dispatchPatientConsultationInformation(data) {
    return { type: types.PATIENT_CONSULTATION_INFORMATION_SIGNALR_RECIVED, data };
}

export function dispatchAdminPatientQueuePatientCount(data) {
    return { type: types.ADMIN_PATIENT_QUEUE_PATIENT_COUNT_SIGNALR_RECIVED, data };
}

export function dispatchPatientQueuePatientCount(data) {
    return { type: types.PATIENT_CONSULTATION_INFORMATION_SIGNALR_RECIVED, data };
}

export function lockRequest(data) {
    return { type: types.WAITING_ROOM_LOCK_REQUEST_SIGNALR_RECIVED, data };
}

export function unlockRequest(data) {
    return { type: types.WAITING_ROOM_UNLOCK_REQUEST_SIGNALR_RECIVED, data };
}

export function appointmentDismissed(data) {
    return { type: types.APPOINTMENT_DISMISSED_SIGNALR_RECIVED, data };
}

export function appointmentSaveClosed(data) {
    return { type: types.APPOINTMENT_SAVE_CLOSED_SIGNALR_RECIVED, data };
}

export function onUpdateFlag(data) {
    return { type: types.WAITING_ROOM_ON_UPDATE_FLAG_SIGNALR_RECIVED, data };
}

//------------- Server functions ----------------------------------------*/

export function refreshQueue(){
    return function(dispatch){
        let signalRService = SnapMDServiceLocator.getObject<SignalRService>('SignalRService');
        return (signalRService.hubs[HubNames.snapWaitingRoomConsultationsHub] as SnapWaitingRoomConsultationsHub).refreshAsync()
        .then((data)=>{
            dispatch({type: types.WAITING_ROOM_REFRESHED});
        })
        .catch((err)=>{
            dispatch({type: types.PATIENT_QUEUE_ERROR, error: err});
        });
    }
}


//************* Custom actions (not signalR hub events) ******************/



export function getPatientQueue() {
    return function (dispatch) {
        dispatch(ajaxStatusActions.beginAjaxCall());
        return AppointmentService.getPatientQueueAsync()
            .then((queueResponse) => {
                dispatch(ajaxStatusActions.endAjaxCall());
                dispatch({ type: types.PATIENT_QUEUE_LOADED, data: queueResponse });
            })
            .catch((error) => {
                dispatch(ajaxStatusActions.ajaxCallError());
                dispatch({ type: types.PATIENT_QUEUE_ERROR, error });
            });
    }
}
