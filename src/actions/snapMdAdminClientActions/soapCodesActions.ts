import * as ajaxStatusActions from '.././ajaxStatusActions';
import * as types from '../../enums/actionTypes';
import { Logger } from '../../utilities/logger';
import { Endpoints } from '../../enums/endpoints';
import { IServiceLocator, SnapMDServiceLocator } from '../../dependenciesInjectionConfiguration';
import { ISnapMdHttpService, SnapMdHttpService } from '../../services/api/httpServices';

function getLoadDiagnosticCodingSettingsHttpAsync(httpService:ISnapMdHttpService, hospitalId:number):Promise<any>{
    return httpService.getViaHttp(Endpoints.hospital.diagnosticCodingSystem, hospitalId);
}

function putDiagnosticCodingSettingsHttpAsync(httpService:ISnapMdHttpService, hospitalId:number, diagnosticCodingSystem:string):Promise<any>{
    return httpService.putViaHttp(Endpoints.hospital.diagnosticCodingSystem, '', hospitalId,  diagnosticCodingSystem );
}

function delDiagnosticCodingSettingsHttpAsync(httpService:ISnapMdHttpService, hospitalId:number):Promise<any>{
    return httpService.deleteViaHttp(Endpoints.hospital.diagnosticCodingSystem, hospitalId);
}

export function loadDiagnosticCodingSettings(locator:IServiceLocator, hospitalId:number){
    Logger.getLogger().debug(`Launched SOAPCodesActions, function loadDiagnosticCodingSettings, hospital id ${hospitalId}`);
    return function(dispatch){
        dispatch(ajaxStatusActions.beginAjaxCall());
        var httpService = locator.getObject<ISnapMdHttpService>('ISnapMdHttpService');
        return getLoadDiagnosticCodingSettingsHttpAsync(httpService, hospitalId)
            .then((soapDCS)=>{
                // DCS - Diagnostic Coding System
                if (typeof soapDCS === 'string'){
                    dispatch(loadDiagnosticCoding(soapDCS));
                } else{
                    dispatch(loadDiagnosticCoding(''));
                }                
                dispatch(ajaxStatusActions.endAjaxCall());
                Logger.getLogger().debug('loadDiagnosticCodingSettings function performed work and received data');
            })
            .catch((err:any )=>{
                dispatch(ajaxStatusActions.ajaxCallError(err.message, 'Network Error: load Diagnostic Coding in SOAPCodesActions'));
                dispatch(loadDiagnosticCoding(''));
                Logger.getLogger().error(err.message , err);
            });
    }
}

export function updateDiagnosticCodingSettings(locator:IServiceLocator, hospitalId:number, diagnosticCodingSystem:string){
    Logger.getLogger().debug(`Launched SOAPCodesActions, function deleteDiagnosticCodingSettings, hospital id ${hospitalId}`);
    return function(dispatch){
        dispatch(ajaxStatusActions.beginAjaxCall());
        var httpService = locator.getObject<ISnapMdHttpService>('ISnapMdHttpService');
        return putDiagnosticCodingSettingsHttpAsync(httpService, hospitalId, diagnosticCodingSystem)
            .then((diagnosticCodingSettings)=>{
                dispatch(loadDiagnosticCoding(diagnosticCodingSystem));
                dispatch(ajaxStatusActions.endAjaxCall());
                Logger.getLogger().debug('updateDiagnosticCodingSettings function performed work and received data');
            })
            .catch((err:any )=>{
                dispatch(ajaxStatusActions.ajaxCallError(err.message, 'Network Error: Update Diagnostic Coding in SOAPCodesActions'));
                Logger.getLogger().error(err.message , err);
            });
    }
}

export function deleteDiagnosticCodingSettings(locator:IServiceLocator, hospitalId:number){
    Logger.getLogger().debug(`Launched SOAPCodesActions, function deleteDiagnosticCodingSettings, hospital id ${hospitalId}`);
    return function(dispatch){
        dispatch(ajaxStatusActions.beginAjaxCall());
        var httpService = locator.getObject<ISnapMdHttpService>('ISnapMdHttpService');
        return delDiagnosticCodingSettingsHttpAsync(httpService, hospitalId)
            .then(()=>{
                dispatch(loadDiagnosticCoding(''));
                dispatch(ajaxStatusActions.endAjaxCall());
                Logger.getLogger().debug('deleteDiagnosticCodingSettings function performed work and received data');
            })
            .catch((err:any )=>{
                dispatch(ajaxStatusActions.ajaxCallError(err.message, 'Network Error: delete Diagnostic Coding in SOAPCodesActions'));
                Logger.getLogger().error(err.message , err);
            });
    }
}

function loadDiagnosticCoding(soapDCS:string){
    return {type: types.SNAPMDADMIN_LOAD_DIAGNOSTIC_CODING_SYSTEM, soapDCS};
};