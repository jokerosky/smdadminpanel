import * as ajaxStatusActions from '.././ajaxStatusActions';
import * as types from '../../enums/actionTypes';
import { Logger } from '../../utilities/logger';
import { Endpoints } from '../../enums/endpoints';
import { IServiceLocator, SnapMDServiceLocator } from '../../dependenciesInjectionConfiguration';
import { ISnapMdHttpService, SnapMdHttpService } from '../../services/api/httpServices';
import { SaveHospitalClientDTO, HospitalModulesKeysDTO, AdditionalSettingsKeysDTO, HospitalClientInListsKeyDTO, HospitalImagesDTO } from '../../models/DTO/hospitalSettingDTO';
import { getClientsData } from './clientDataActions';
import { hospitalAddressLoaded } from '../snapMdAdminActions';

function getCountriesHttpAsync(httpService: ISnapMdHttpService): Promise<any> {
    return httpService.getViaHttp(Endpoints.snapmdAdmin.settingsCountries);
}

function getClientSettingsHttpAsync(httpService: ISnapMdHttpService, hospitalId: number, fields: string[]): Promise<any> {
    return httpService.getViaHttp(Endpoints.hospital.hospitalSettings, hospitalId, fields);
}

function saveClientDataHttpAsync(httpService: ISnapMdHttpService, clientData: SaveHospitalClientDTO, hospitalId: number): Promise<any> {
    return httpService.putViaHttp(Endpoints.snapmdAdmin.clients, clientData, hospitalId);
}

function saveClientSettingsHttpAsync(httpService: ISnapMdHttpService, settings: any, hospitalId: number): Promise<any> {
    return httpService.putViaHttp(Endpoints.hospital.hospitalSettings, settings, hospitalId);
}

function saveClientAddressHttpAsync(httpService: ISnapMdHttpService, address: any): Promise<any> {
    return httpService.putViaHttp(Endpoints.snapmdAdmin.hospitalAddress.replace(/v2\//i, '').replace(/\/%id%/i, ''), address);
}

function savePartnerIdPaymentGatewayHttpAsync(httpService: ISnapMdHttpService, data: any, hospitalId: number): Promise<any> {
    return httpService.putViaHttp(Endpoints.snapmdAdmin.integrationsHospitalsPaymentGateway, data, hospitalId);
}

function saveIntegrationsHospitalsPartnerTypesHttpAsync(httpService: ISnapMdHttpService, data: any, hospitalId: number, partnerTypes: number): Promise<any> {
    return httpService.putViaHttp(Endpoints.snapmdAdmin.integrationsHospitalsPartnerTypes.replace('%partnerType%', partnerTypes.toString()), data, hospitalId);
}

function deleteHospitalPartnerTypeHttpAsync(httpService: ISnapMdHttpService, data: any, hospitalId: number, partnerTypes: number): Promise<any> {
    return httpService.deleteViaHttp(Endpoints.snapmdAdmin.integrationsHospitalPartner.replace('%partnerType%', partnerTypes.toString()), hospitalId, data)
}

function postHospitalPartnerTypeHttpAsync(httpService: ISnapMdHttpService, data: any, hospitalId: number): Promise<any> {
    return httpService.postViaHttp(Endpoints.snapmdAdmin.integrationsHospitalPartner.replace('/partnertype/%partnerType%', ''), data, hospitalId);
}

function saveImageHttpAsync(httpService: ISnapMdHttpService, pathSaveImage: string, fileImage: any, hospitalId: number): Promise<any> {
    return httpService.postViaHttp(pathSaveImage, fileImage, hospitalId);
}

export function getCountries(locator: IServiceLocator) {
    Logger.getLogger().debug(`Launched SettingsActions, function getCountries`);
    return function (dispatch) {
        dispatch(ajaxStatusActions.beginAjaxCall());
        var httpService = locator.getObject<ISnapMdHttpService>('ISnapMdHttpService');
        return getCountriesHttpAsync(httpService)
            .then((respons) => {
                dispatch(countriesLoaded(respons));
                dispatch(ajaxStatusActions.endAjaxCall());
            })
            .catch((err: any) => {
                dispatch(ajaxStatusActions.ajaxCallError(err.message, 'Network Error: load Countries in SettingsActions'));
                Logger.getLogger().error(err.message, err);
            });
    }
}

export function getClientSettings(locator: IServiceLocator, hospitalId: number, fields: string[]) {
    Logger.getLogger().debug(`Launched SettingsActions, function getClientSettings`);
    return function (dispatch) {
        dispatch(ajaxStatusActions.beginAjaxCall());
        var httpService = locator.getObject<ISnapMdHttpService>('ISnapMdHttpService');
        return getClientSettingsHttpAsync(httpService, hospitalId, fields)
            .then((response) => {
                dispatch(modulesSettings(response));
                dispatch(ajaxStatusActions.endAjaxCall());
            })
            .catch((err: any) => {
                dispatch(ajaxStatusActions.ajaxCallError(err.message, 'Network Error: load Settings in SettingsActions'));
                Logger.getLogger().error(err.message, err);
            });
    }
}

export function clearSettings(state) {
    Logger.getLogger().debug(`Launched SettingsActions, function clearSettings`);
    return clrSettings(state);
}

export function saveClientData(locator: IServiceLocator, clientData: SaveHospitalClientDTO, settings: HospitalModulesKeysDTO, additionalSettings: AdditionalSettingsKeysDTO, hospitalAddress: any, clientInLists: HospitalClientInListsKeyDTO) {
    Logger.getLogger().debug(`Launched SettingsActions, function saveClientData`);
    return function (dispatch) {
        var httpService = locator.getObject<ISnapMdHttpService>('ISnapMdHttpService');
        return saveClientDataHttpAsync(httpService, clientData, hospitalAddress.hospitalId)
            .then((response) => {
                dispatch(clientNameUpdate(clientInLists, response, hospitalAddress.hospitalId));
                dispatch(getClientsData(response));
                return saveClientSettingsHttpAsync(httpService, settings, hospitalAddress.hospitalId);
            })
            .then((response) => {
                dispatch(modulesSettings(response));
                return saveClientSettingsHttpAsync(httpService, additionalSettings, hospitalAddress.hospitalId);
            })
            .then((response) => {
                dispatch(modulesSettings(response));
                return saveClientAddressHttpAsync(httpService, hospitalAddress);
            })
            .then((response) => {
                dispatch(hospitalAddressLoaded(response));
                dispatch(ajaxStatusActions.endAjaxCall());
            })
            .catch((err: any) => {
                dispatch(ajaxStatusActions.ajaxCallError(err.message, 'Network Error: save ClientData in SettingsActions'));
                Logger.getLogger().error(err.message, err);
            });
    }
}

export function saveSettings(locator: IServiceLocator, settings: any, hospitalId: number) {
    Logger.getLogger().debug(`Launched SettingsActions, function saveSettings`);
    return function (dispatch) {
        dispatch(ajaxStatusActions.beginAjaxCall());
        var httpService = locator.getObject<ISnapMdHttpService>('ISnapMdHttpService');
        return saveClientSettingsHttpAsync(httpService, settings, hospitalId)
            .then((response) => {
                dispatch(modulesSettings(response));
                dispatch(ajaxStatusActions.endAjaxCall());
            })
            .catch((err: any) => {
                dispatch(ajaxStatusActions.ajaxCallError(err.message, 'Network Error: save Settings in SettingsActions'));
                Logger.getLogger().error(err.message, err);
            });
    }
}

export function savePartnerIdPaymentGateway(locator: IServiceLocator, data: any, hospitalId: number) {
    Logger.getLogger().debug(`Launched SettingsActions, function savePartnerIdPaymentGateway`);
    return function (dispatch) {
        dispatch(ajaxStatusActions.beginAjaxCall());
        var httpService = locator.getObject<ISnapMdHttpService>('ISnapMdHttpService');
        return savePartnerIdPaymentGatewayHttpAsync(httpService, data, hospitalId)
            .then((response) => {
                dispatch(ajaxStatusActions.endAjaxCall());
            })
            .catch((err: any) => {
                dispatch(ajaxStatusActions.ajaxCallError(err.message, 'Network Error: save PartnerIdPaymentGateway in SettingsActions'));
                Logger.getLogger().error(err.message, err);
            });
    }
}

export function saveIntegrationsHospitalsPartnerTypes(locator: IServiceLocator, data: any, hospitalId: number, partnerTypes: number) {
    Logger.getLogger().debug(`Launched SettingsActions, function saveIntegrationsHospitalsPartnerTypes`);
    return function (dispatch) {
        dispatch(ajaxStatusActions.beginAjaxCall());
        var httpService = locator.getObject<ISnapMdHttpService>('ISnapMdHttpService');
        return saveIntegrationsHospitalsPartnerTypesHttpAsync(httpService, data, hospitalId, partnerTypes)
            .then((response) => {
                dispatch(ajaxStatusActions.endAjaxCall());
            })
            .catch((err: any) => {
                dispatch(ajaxStatusActions.ajaxCallError(err.message, 'Network Error: save IntegrationsHospitalsPartnerTypes in SettingsActions'));
                Logger.getLogger().error(err.message, err);
            });
    }
}

export function deleteHospitalPartnerType(locator: IServiceLocator, data: any, hospitalId: number, partnerTypes: number) {
    Logger.getLogger().debug(`Launched SettingsActions, function deleteHospitalPartnerType`);
    return function (dispatch) {
        dispatch(ajaxStatusActions.beginAjaxCall());
        var httpService = locator.getObject<ISnapMdHttpService>('ISnapMdHttpService');
        return deleteHospitalPartnerTypeHttpAsync(httpService, data, hospitalId, partnerTypes)
            .then((response) => {
                dispatch(ajaxStatusActions.endAjaxCall());
            })
            .catch((err: any) => {
                dispatch(ajaxStatusActions.ajaxCallError(err.message, 'Network Error: delete Hospital partner type in SettingsActions'));
                Logger.getLogger().error(err.message, err);
            });
    }
}

export function postHospitalPartnerType(locator: IServiceLocator, data: any, hospitalId: number) {
    Logger.getLogger().debug(`Launched SettingsActions, function postHospitalPartnerType`);
    return function (dispatch) {
        dispatch(ajaxStatusActions.beginAjaxCall());
        var httpService = locator.getObject<ISnapMdHttpService>('ISnapMdHttpService');
        return postHospitalPartnerTypeHttpAsync(httpService, data, hospitalId)
            .then((response) => {
                dispatch(ajaxStatusActions.endAjaxCall());
            })
            .catch((err: any) => {
                dispatch(ajaxStatusActions.ajaxCallError(err.message, 'Network Error: post Hospital Partner type in SettingsActions'));
                Logger.getLogger().error(err.message, err);
            });
    }
}

export function saveImagesAndClientData(locator: IServiceLocator, imageObjects: HospitalImagesDTO[], settings: any, hospitalId: number) {
    Logger.getLogger().debug(`Launched SettingsActions, function saveImagesAndClientData`);
    return function (dispatch) {
        dispatch(ajaxStatusActions.beginAjaxCall());
        var httpService = locator.getObject<ISnapMdHttpService>('ISnapMdHttpService');
        let promise: Array<any> = [];
        let responseImages: Array<any> = [];
        let count = 0;
        if (imageObjects.length > 0) {
            for (let index in imageObjects) {
                promise.push(
                    saveImageHttpAsync(httpService, imageObjects[index].pathSaveImage, imageObjects[index].fileImage, hospitalId)
                        .then((response) => {
                            let imageResult = Object.assign({}, response, { fieldNameToSave: imageObjects[index].fieldNameToSave });
                            responseImages.push(imageResult);
                            dispatch(imageSaved(response.uri, imageObjects[index].fieldNameToSave));
                            count++;
                            if (count === imageObjects.length) {
                                let resp = conversionOfImageObjects(settings, responseImages);
                                dispatch(saveClientData(locator, resp.client, resp.settings, resp.additionalSettings,
                                    resp.hospitalAddress, resp.clientInLists));
                            }
                        })
                        .catch((err: any) => {
                            Logger.getLogger().error(err.message, err);
                            dispatch(ajaxStatusActions.ajaxCallError(err.message, 'Network Error: save Images and ClientData in SettingsActions: ' + err.message));
                        })
                );
            }
            return Promise.all(promise);
        } else {
            return dispatch(saveClientData(locator, settings.client, settings.settings, settings.additionalSettings,
                settings.hospitalAddress, settings.clientInLists));
        }
    }
}

function countriesLoaded(countries) {
    return { type: types.SNAPMDADMIN_SETTINGS_COUNTRIES_LOADED, countries };
}

function modulesSettings(modules) {
    return { type: types.SNAPMDADMIN_SETTINGS_MODULES_LOADED, modules };
}

function clrSettings(state) {
    return { type: types.SNAPMDADMIN_SETTINGS_CLEAR, state };
}

function clientNameUpdate(clientForLists, clientData, hospitalId) {
    return { type: types.SNAPMDADMIN_HOSPITAL_DATA_UPDATE, clientForLists, clientData, hospitalId };
}

function imageSaved(imageUrl, fieldNameToSave) {
    return { type: types.SNAPMDADMIN_SAVE_IMAGE, imageUrl, fieldNameToSave };
}

export function conversionOfImageObjects(settings: any, responseImages: any[]) {
    let tempSettings = Object.assign({}, settings);
    for (let index in responseImages) {
        if (responseImages[index].fieldNameToSave === 'hospitalImage') {
            tempSettings.client.hospitalImage = responseImages[index].uri;
        } else {
            tempSettings.additionalSettings[responseImages[index].fieldNameToSave] = responseImages[index].uri;
        }
    }
    return tempSettings;
}