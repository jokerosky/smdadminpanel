import * as ajaxStatusActions from '.././ajaxStatusActions';
import * as types from '../../enums/actionTypes';
import { Logger } from '../../utilities/logger';
import { Endpoints } from '../../enums/endpoints';
import { IServiceLocator, SnapMDServiceLocator } from '../../dependenciesInjectionConfiguration';
import { ISnapMdHttpService, SnapMdHttpService } from '../../services/api/httpServices';
import {CodeSetsDTO} from '../../models/DTO/CodeSetsDTO';
import {EditorActions} from '../../enums/siteEnums';

function getCodeSetsHttpAsync(httpService:ISnapMdHttpService, hospitalId:number, codeSetsTypeId:number){
    return httpService.getViaHttp(Endpoints.hospital.codeSetLoad.replace('%codeSetsId%', codeSetsTypeId.toString()) + '?{}', hospitalId)
}

function addCodeSetsHttpAsync(httpService:ISnapMdHttpService, codeSets:CodeSetsDTO){
    return httpService.postViaHttp(Endpoints.hospital.codeSetAdd, codeSets);
}

function updateCodeSetsHttpAsync(httpService:ISnapMdHttpService, codeSets:CodeSetsDTO){
    return httpService.postViaHttp(Endpoints.hospital.codeSetUpdate, codeSets);
}

export function loadCodeSets(locator:IServiceLocator, hospitalId:number, codeSetsTypeId:number){
    Logger.getLogger().debug(`Launched CodeSetsActions, function loadCodeSets, hospital id ${hospitalId}`);
    return function(dispatch){
        dispatch(ajaxStatusActions.beginAjaxCall());
        var httpService = locator.getObject<ISnapMdHttpService>('ISnapMdHttpService');
        return getCodeSetsHttpAsync(httpService, hospitalId, codeSetsTypeId)
            .then((codes)=>{
                dispatch(loadCodes(codes));
                dispatch(ajaxStatusActions.endAjaxCall());
                Logger.getLogger().debug('loadCodeSets function performed work and received data');
            })
            .catch((err:any )=>{
                let codeSets: CodeSetsDTO = new CodeSetsDTO();
                
                dispatch(ajaxStatusActions.ajaxCallError(err.message, 'Network Error: loading Code Sets  in codeSetsActions'));
                dispatch(loadCodes(codeSets));
                Logger.getLogger().error(err.message , err);
            });
    }
}

export function codeSetsAddOrUpdate(locator:IServiceLocator, codeSet:CodeSetsDTO, methodType:string){
    Logger.getLogger().debug(`Launched CodeSetsActions, function codeSetsAddOrUpdate`);
    return function(dispatch){
        dispatch(ajaxStatusActions.beginAjaxCall());
        var httpService = locator.getObject<ISnapMdHttpService>('ISnapMdHttpService');
        var promise = methodType === EditorActions.add  ? addCodeSetsHttpAsync(httpService, codeSet)
                                                        : updateCodeSetsHttpAsync(httpService, codeSet);
        promise
            .then((codeSet)=>{
                if(methodType === EditorActions.add){
                    dispatch(addCodeSets(codeSet));
                } else if(methodType === EditorActions.edit){
                    dispatch(updateCodeSets(codeSet));
                }
                dispatch(ajaxStatusActions.endAjaxCall());
                Logger.getLogger().debug('codeSetsAddOrUpdate function performed work and received data');
            })
            .catch((err:any )=>{
                let method = '';

                if(methodType === EditorActions.add){
                    method = EditorActions.add;
                } else if(methodType === EditorActions.edit){
                    method = EditorActions.edit;
                }

                dispatch(ajaxStatusActions.ajaxCallError(err.message, `Network Error: ${method} Code Sets  in codeSetsActions`));
                Logger.getLogger().error(err.message , err);
            });

        return promise;
    }
}

function loadCodes(codes:any){
    return {type: types.SNAPMDADMIN_LOAD_CODE_SET_SETTINGS, codes};
}

function addCodeSets(codeSet:CodeSetsDTO){
    return {type: types.SNAPMDADMIN_ADD_CODE_SET, codeSet};
}

function updateCodeSets(codeSet:CodeSetsDTO){
    return {type: types.SNAPMDADMIN_UPDATE_CODE_SET, codeSet};
}

export function saveSelectedCodeSetsTypeId(codeSetsTypeId:number){
    return {type: types.SNAPMDADMIN_SELECTED_CODE_SET_TYPE_ID, codeSetsTypeId};
}