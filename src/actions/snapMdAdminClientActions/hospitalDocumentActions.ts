import * as ajaxStatusActions from '.././ajaxStatusActions';
import * as types from '../../enums/actionTypes';
import { Logger } from '../../utilities/logger';
import { Endpoints } from '../../enums/endpoints';
import { IServiceLocator, SnapMDServiceLocator } from '../../dependenciesInjectionConfiguration';
import { ISnapMdHttpService, SnapMdHttpService } from '../../services/api/httpServices';
import HospitalDocumentDTO from '../../models/DTO/hospitalDocumentDTO';
import initialState from '../../reducers/initialState';


function getDocumentHttpAsync(httpService:ISnapMdHttpService, selectDocument:number, hospitalId:number){
    return httpService.getViaHttp(Endpoints.hospital.hospitalDocument.replace('%docId%', selectDocument.toString()), hospitalId);
}

function saveDocumentHttpAsync(httpService:ISnapMdHttpService, document:HospitalDocumentDTO){
    return httpService.postViaHttp(Endpoints.hospital.hospitalDocuments, document);
}

function deleteDocumentHttpAsync(httpService:ISnapMdHttpService, document:HospitalDocumentDTO){
    return httpService.deleteViaHttp(Endpoints.hospital.hospitalDocuments, -1, document);
}

export function loadHospitalDocument(locator:IServiceLocator, selectDocument:number, hospitalId:number){
    Logger.getLogger().debug(`Launched HospitalDocumentActions, function loadHospitalDocument, hospital id ${hospitalId}`);
    return function(dispatch){
        dispatch(ajaxStatusActions.beginAjaxCall());
        var httpService = locator.getObject<ISnapMdHttpService>('ISnapMdHttpService');
        return getDocumentHttpAsync(httpService, selectDocument, hospitalId)
            .then((document)=>{
                dispatch(loadDocument(document, selectDocument));
                dispatch(ajaxStatusActions.endAjaxCall());
                Logger.getLogger().debug('loadHospitalDocument function performed work and received data');
            })
            .catch((err:any )=>{
                dispatch(ajaxStatusActions.ajaxCallError(err.message, 'Network Error: loading Hospital Documents in HospitalDocumentActions'));
                dispatch(loadDocument('', null));
                Logger.getLogger().error(err.message , err);
            });
    }
}

export function saveHospitalDocument(locator:IServiceLocator, document:HospitalDocumentDTO){
    Logger.getLogger().debug('Launched HospitalDocumentActions, function saveHospitalDocument');
    return function(dispatch){
        dispatch(ajaxStatusActions.beginAjaxCall());
        var httpService = locator.getObject<ISnapMdHttpService>('ISnapMdHttpService');
        return saveDocumentHttpAsync(httpService, document)
            .then(()=>{
                dispatch(ajaxStatusActions.endAjaxCall());
                Logger.getLogger().debug('saveHospitalDocument function performed work and received data');
            })
            .catch((err:any )=>{
                dispatch(ajaxStatusActions.ajaxCallError(err.message, 'Network Error: save Hospital Documents in HospitalDocumentActions'));
                Logger.getLogger().error(err.message , err);
            });
    }
}

export function deleteHospitalDocument(locator:IServiceLocator, document:HospitalDocumentDTO){
    Logger.getLogger().debug('Launched HospitalDocumentActions, function deleteHospitalDocument');
    return function(dispatch){
        dispatch(ajaxStatusActions.beginAjaxCall());
        var httpService = locator.getObject<ISnapMdHttpService>('ISnapMdHttpService');
        return deleteDocumentHttpAsync(httpService, document)
            .then(()=>{
                dispatch(ajaxStatusActions.endAjaxCall());
                Logger.getLogger().debug('deleteHospitalDocument function performed work and received data');
            })
            .catch((err:any )=>{
                dispatch(ajaxStatusActions.ajaxCallError(err.message, 'Network Error: delete Hospital Documents in HospitalDocumentActions'));
                Logger.getLogger().error(err.message , err);
            });
    }
}

function loadDocument(document:any, selectDocument:number){
    return {type: types.SNAPMDADMIN_LOAD_HOSPITAL_DOCUMENT, document, selectDocument};
}