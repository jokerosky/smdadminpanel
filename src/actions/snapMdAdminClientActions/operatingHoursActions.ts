import * as ajaxStatusActions from '.././ajaxStatusActions';
import * as types from '../../enums/actionTypes';
import { Logger } from '../../utilities/logger';
import { Endpoints } from '../../enums/endpoints';
import { IServiceLocator, SnapMDServiceLocator } from '../../dependenciesInjectionConfiguration';
import { ISnapMdHttpService, SnapMdHttpService } from '../../services/api/httpServices';
import {EditorActions} from '../../enums/siteEnums';

function getOperatingHoursHttpAsync(httpService:ISnapMdHttpService, hospitalId:number):Promise<any>{
    return httpService.getViaHttp(Endpoints.snapmdAdmin.operatinghHours, hospitalId);
}

function postOperatingHoursHttpAsync(httpService:ISnapMdHttpService, operatingHoursdata:any, hospitalId:number):Promise<any>{
    return httpService.postViaHttp(Endpoints.snapmdAdmin.operatinghHours, operatingHoursdata, hospitalId);
}

function putOperatingHoursHttpAsync(httpService:ISnapMdHttpService, operatingHoursdata:any, hospitalId:number):Promise<any>{
    return httpService.putViaHttp(Endpoints.snapmdAdmin.operatinghHours, operatingHoursdata, hospitalId);
}

function delOperatingHoursHttpAsync(httpService:ISnapMdHttpService, dayId:number):Promise<any>{
    return httpService.deleteViaHttp(Endpoints.snapmdAdmin.operatinghHoursDelete, dayId);
}

function putEnableOperatingHoursHttpAsync(httpServise:ISnapMdHttpService, hospitalId:number, stateToggle:any):Promise<any>{
    return httpServise.putViaHttp(Endpoints.hospital.hospitalSettings, stateToggle, hospitalId);
}

export function loadOperatingHours(locator:IServiceLocator, hospitalId:number){
    Logger.getLogger().debug(`Launched OperatingHoursActions, function loadOperatingHours, hospital id ${hospitalId}`);
    return function(dispatch){
        dispatch(ajaxStatusActions.beginAjaxCall());
        var httpService = locator.getObject<ISnapMdHttpService>('ISnapMdHttpService');
        return getOperatingHoursHttpAsync(httpService, hospitalId)
            .then((operatingHours)=>{
                dispatch(getOperatingHoursData(operatingHours));
                dispatch(ajaxStatusActions.endAjaxCall());
                Logger.getLogger().debug('loadOperatingHours function performed work and received data');
            })
            .catch((err:any )=>{
                dispatch(ajaxStatusActions.ajaxCallError(err.message, 'Network Error: load Operating Hours in OperatingHoursActions'));
                dispatch(getOperatingHoursData([]));
                Logger.getLogger().error(err.message , err);
            });
    }
}

export function updateOperatingHours(locator:IServiceLocator, hospitalId:number, operatingHoursdata:any, editorAction:string){
    Logger.getLogger().debug(`Launched OperatingHoursActions, function updateOperatingHours, hospital id ${hospitalId}`);
    return function(dispatch){
        dispatch(ajaxStatusActions.beginAjaxCall());
        var httpService = locator.getObject<ISnapMdHttpService>('ISnapMdHttpService');
        let promise = editorAction === EditorActions.add  ? postOperatingHoursHttpAsync(httpService, operatingHoursdata, hospitalId)
                                                            : putOperatingHoursHttpAsync(httpService, operatingHoursdata, hospitalId);
        
        
        promise                 
            .then((operatingHours)=>{
                dispatch(loadOperatingHours(locator, hospitalId));;
                dispatch(ajaxStatusActions.endAjaxCall());
                Logger.getLogger().debug('updateOperatingHours function performed work and received data');
            })
            .catch((err:any )=>{
                let method = '';

                if(editorAction === EditorActions.add){
                    method = EditorActions.add;
                } else if(editorAction === EditorActions.edit){
                    method = EditorActions.edit;
                }

                dispatch(ajaxStatusActions.ajaxCallError(err.message, `Network Error: ${method} Operating Hours in OperatingHoursActions`));
                Logger.getLogger().error(err.message , err);
            });

        return promise;
    }
}

export function deleteOperatingHours(locator:IServiceLocator, dayId:number, hospitalId: number){
    Logger.getLogger().debug(`Launched OperatingHoursActions, function deleteOperatingHours, hospital id ${hospitalId}`);
    return function(dispatch){
        dispatch(ajaxStatusActions.beginAjaxCall());
        var httpService = locator.getObject<ISnapMdHttpService>('ISnapMdHttpService');
        return delOperatingHoursHttpAsync(httpService, dayId)
            .then((operatingHours)=>{
                dispatch(loadOperatingHours(locator, hospitalId));
                dispatch(ajaxStatusActions.endAjaxCall());
                Logger.getLogger().debug('deleteOperatingHours function performed work and received data');
            })
            .catch((err:any )=>{
                dispatch(ajaxStatusActions.ajaxCallError(err.message, 'Network Error: delete Operating Hours in OperatingHoursActions'));
                Logger.getLogger().error(err.message , err);
            });
    }
}

export function toggleOperatingHours(locator:IServiceLocator, stateToggle: any, hospitalId: number, fieldUpdate:string){
    Logger.getLogger().debug(`Launched OperatingHoursActions, function toggleOperatingHours, hospital id ${hospitalId}`);
    return function(dispatch){
        dispatch(ajaxStatusActions.beginAjaxCall());
        var httpServise = locator.getObject<ISnapMdHttpService>('ISnapMdHttpService');
        return putEnableOperatingHoursHttpAsync(httpServise, hospitalId, stateToggle)
            .then((toggle)=>{
                dispatch(getEnableOperatingHours(toggle,fieldUpdate));
                dispatch(ajaxStatusActions.endAjaxCall());
                Logger.getLogger().debug('updateToggle function performed work and received data');
            })
            .catch((err:any )=>{
                dispatch(ajaxStatusActions.ajaxCallError(err.message, 'Network Error: toggle Operating Hours in OperatingHoursActions'));
                dispatch(getEnableOperatingHours(stateToggle,fieldUpdate));
                Logger.getLogger().error(err.message , err);
            });
    }
}

export function getOperatingHoursData(hours: any){
    return {type: types.SNAPMDADMIN_OPERATING_HOURS_LOADED, hours};
};

export function getEnableOperatingHours(stateToggle: any, field:string){
    return {type: types.SNAPMDADMIN_UPDATE_TOGGLE, stateToggle, field};
};