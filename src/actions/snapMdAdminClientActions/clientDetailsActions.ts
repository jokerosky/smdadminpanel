import * as ajaxStatusActions from '.././ajaxStatusActions';
import * as types from '../../enums/actionTypes';
import { Logger, LogLevel } from '../../utilities/logger';
import { Endpoints } from '../../enums/endpoints';
import { IServiceLocator, SnapMDServiceLocator } from '../../dependenciesInjectionConfiguration';
import { ISnapMdHttpService, SnapMdHttpService } from '../../services/api/httpServices';
import {EditorActions} from '../../enums/siteEnums';

function getSeatsHttpAsync(httpService:ISnapMdHttpService, hospitalId:number):Promise<any>{
    return httpService.getViaHttp(Endpoints.snapmdAdmin.seats, hospitalId);
}

function getSeatsAssignedHttpAsync(httpService:ISnapMdHttpService, hospitalId:number):Promise<any>{
    return httpService.getViaHttp(Endpoints.snapmdAdmin.seatsAssigned, hospitalId);
}

function archiveClientHttpAsync(httpService:ISnapMdHttpService, hospitalId:number):Promise<any>{
    return httpService.deleteViaHttp(Endpoints.snapmdAdmin.clients, hospitalId);
}
function activateClientHttpAsync(httpService:ISnapMdHttpService, hospitalId:number, clientData:any):Promise<any>{
    return httpService.putViaHttp(Endpoints.snapmdAdmin.clients, clientData, hospitalId);
}

export function loadSeatsData(locator:IServiceLocator, hospitalId:number){
    Logger.getLogger().debug(`Launched ClientDetailsActions, function loadSeatsData, hospital id ${hospitalId}`);
    return function (dispatch){
        dispatch(ajaxStatusActions.beginAjaxCall());
        var httpService = locator.getObject<ISnapMdHttpService>('ISnapMdHttpService');
        return getSeatsHttpAsync(httpService, hospitalId)
            .then((seats)=>{
                dispatch(getSeatsData(seats));
                return getSeatsAssignedHttpAsync(httpService, hospitalId);
            })
            .then((seatsAssigned)=>{
                dispatch(getSeatsAssignedData(seatsAssigned));
                dispatch(ajaxStatusActions.endAjaxCall());
                Logger.getLogger().debug('loadSeatsData function performed work and received data');
            })
            .catch((err:any )=>{
                dispatch(ajaxStatusActions.ajaxCallError(err.message, 'Network Error: loading seats in clientDetailsActions'));
                dispatch(getSeatsData({}));
                Logger.getLogger().error(err.message , err);
            });
    }
}

export function cahngeClientActiveStatus(locator:IServiceLocator, hospitalId: number, isActive: string, methodType:string, clientData?:any){
    Logger.getLogger().debug(`Launched ClientDetailsActions, function archivClient, hospital id ${hospitalId}`);
    return function(dispatch){
        dispatch(ajaxStatusActions.beginAjaxCall());
        var httpService = locator.getObject<ISnapMdHttpService>('ISnapMdHttpService');
        let promise = methodType === EditorActions.activate ? archiveClientHttpAsync(httpService, hospitalId)
                                                            : activateClientHttpAsync(httpService, hospitalId, clientData);
        promise
        .then(()=>{
            dispatch(setClientActiveStatus(isActive));
            dispatch(ajaxStatusActions.endAjaxCall());
            Logger.getLogger().debug('archivClient function performed work and received data');
        })
        .catch((err:any )=>{
            dispatch(ajaxStatusActions.ajaxCallError(err.message, 'Network Error: arhivate or activate client in clientDetailsActions'));
            dispatch(setClientActiveStatus(''));
            Logger.getLogger().error(err.message , err);
        });                                    

        return promise;
    }
}

function getSeatsData(seats: any){
    return {type: types.SNAPMDADMIN_CLIENTS_SEATS_LOADED, seats};
};

function getSeatsAssignedData(seatsAssigned: any){
    return {type: types.SNAPMDADMIN_CLIENTS_SEATS_ASSIGNED_LOADED, seatsAssigned};
};

function setClientActiveStatus(active:string){
    return {type: types.SNAPMDADMIN_CLIENT_ACTIVE, active};
}