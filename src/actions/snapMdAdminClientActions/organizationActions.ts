import * as ajaxStatusActions from '.././ajaxStatusActions';
import * as types from '../../enums/actionTypes';
import { Logger } from '../../utilities/logger';
import { Endpoints } from '../../enums/endpoints';
import { IServiceLocator, SnapMDServiceLocator } from '../../dependenciesInjectionConfiguration';
import { ISnapMdHttpService, SnapMdHttpService } from '../../services/api/httpServices';
import {EditorActions} from '../../enums/siteEnums';
import {OrganizationDTO} from '../../models/DTO/organizationDTO';

function getOrganizationHttpAsync(httpService:ISnapMdHttpService, typeOrganization:number, hospitalId:number):Promise<any>{
    return httpService.getViaHttp(Endpoints.hospital.organizations+'?type='+typeOrganization, hospitalId);
}

function addOrganizationHttpAsync(httpService:ISnapMdHttpService, organization:OrganizationDTO):Promise<any>{
    return httpService.postViaHttp(Endpoints.hospital.addAndUpdateOrganization, organization);
}

function updOrganizationHttpAsync(httpService:ISnapMdHttpService, organization:OrganizationDTO):Promise<any>{
    return httpService.putViaHttp(Endpoints.hospital.addAndUpdateOrganization, organization);
}

export function loadOrganizations(locator:IServiceLocator, organizationType:number, hospitalId:number){
    Logger.getLogger().debug(`Launched OrganizationActions, function loadOrganizations, hospital id ${hospitalId}`);
    return function(dispatch){
        dispatch(ajaxStatusActions.beginAjaxCall());
        var httpService = locator.getObject<ISnapMdHttpService>('ISnapMdHttpService');
        return getOrganizationHttpAsync(httpService, organizationType, hospitalId)
            .then((organization)=>{
                dispatch(loadOrganization(organization));
                dispatch(ajaxStatusActions.endAjaxCall());
                Logger.getLogger().debug('loadOrganizations function performed work and received data');
            })
            .catch((err:any )=>{
                dispatch(ajaxStatusActions.ajaxCallError(err.message, 'Network Error: load Organizations in OrganizationActions'));
                dispatch(loadOrganization([]));
                Logger.getLogger().error(err.message , err);
            });
    }
}

export function organizationAddOrUpdate(locator:IServiceLocator, organization:OrganizationDTO, editorAction:string){
    Logger.getLogger().debug('Launched OrganizationActions, function organizationAddOrUpdate');
    return function(dispatch){
        dispatch(ajaxStatusActions.beginAjaxCall());
        var httpService = locator.getObject<ISnapMdHttpService>('ISnapMdHttpService');
        let promise = editorAction === EditorActions.add   ? addOrganizationHttpAsync(httpService, organization)
                                                                : updOrganizationHttpAsync(httpService, organization)
        promise
            .then((organization)=>{
                if (editorAction === EditorActions.add){
                    dispatch(addNewOrganization(organization));
                } else if (editorAction === EditorActions.edit) {
                    dispatch(updateOrganization(organization));
                }
                dispatch(ajaxStatusActions.endAjaxCall());
                Logger.getLogger().debug('organizationAddOrUpdate function performed work and received data');
            })
            .catch((err:any )=>{
                let method = '';

                if(editorAction === EditorActions.add){
                    method = EditorActions.add;
                } else if(editorAction === EditorActions.edit){
                    method = EditorActions.edit;
                }

                dispatch(ajaxStatusActions.ajaxCallError(err.message, `Network Error: ${method} Organizations in OrganizationActions`));
                Logger.getLogger().error(err.message , err);
            });
        return promise;
    }
}

export function loadOrganization(organization:any){
    return {type: types.SNAPMDADMIN_LOAD_ORGANIZATION, organization};
};

export function addNewOrganization(newOrganization:any){
    return {type: types.SNAPMDADMIN_ADD_ORGANIZATION, newOrganization};
};

export function updateOrganization(organization:any){
    return {type: types.SNAPMDADMIN_UPDATE_ORGANIZATION, organization};
};