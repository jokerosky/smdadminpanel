import * as ajaxStatusActions from '.././ajaxStatusActions';
import * as types from '../../enums/actionTypes';
import { Logger } from '../../utilities/logger';
import { Endpoints } from '../../enums/endpoints';
import { IServiceLocator, SnapMDServiceLocator } from '../../dependenciesInjectionConfiguration';
import { ISnapMdHttpService, SnapMdHttpService } from '../../services/api/httpServices';

function getClientDetailsHttpAsync(httpService:ISnapMdHttpService, hospitalId:number):Promise<any>{
    return httpService.getViaHttp(Endpoints.snapmdAdmin.clients, hospitalId);
}

export function loadClientsData(locator:IServiceLocator, hospitalId:number){
    Logger.getLogger().debug(`Launched ClientDataActions, function loadClientsData, hospital id ${hospitalId}`);
    return function (dispatch){
        dispatch(ajaxStatusActions.beginAjaxCall());
        var httpService = locator.getObject<ISnapMdHttpService>('ISnapMdHttpService');
        return getClientDetailsHttpAsync(httpService, hospitalId)
            .then((clientDetails)=>{
                dispatch(getClientsData(clientDetails));
                dispatch(ajaxStatusActions.endAjaxCall());
                Logger.getLogger().debug('loadClientsData function performed work and received data');
            })
            .catch((err:any )=>{
                dispatch(ajaxStatusActions.ajaxCallError(err.message, 'Network Error: loading client data in clientDataActions'));
                dispatch(getClientsData({}));
                Logger.getLogger().error(err.message , err);
            });
    }
}

export function getClientsData(data: any){
    return {type: types.SNAPMDADMIN_CLIENTS_DATA_LOADED, data};
};