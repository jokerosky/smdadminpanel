import * as ajaxStatusActions from '.././ajaxStatusActions';
import * as types from '../../enums/actionTypes';
import { Logger } from '../../utilities/logger';
import { Endpoints } from '../../enums/endpoints';
import { IServiceLocator, SnapMDServiceLocator } from '../../dependenciesInjectionConfiguration';
import { ISnapMdHttpService, SnapMdHttpService } from '../../services/api/httpServices';
import {EditorActions} from '../../enums/siteEnums';

function getOrganizationsHttpAsync(httpService:ISnapMdHttpService, hospitalId:number):Promise<any>{
    return httpService.getViaHttp(Endpoints.hospital.organizations, hospitalId);
}

function addLocationHttpAsync(httpService:ISnapMdHttpService, data:any):Promise<any>{
    return httpService.postViaHttp(Endpoints.hospital.locations.replace('/%id%', ''), data);
}

function updateLocationHttpAsync(httpService:ISnapMdHttpService, locationId:number, data:any):Promise<any>{
    return httpService.putViaHttp(Endpoints.hospital.locations, data, locationId);
}

function deleteLocationHttpAsync(httpService:ISnapMdHttpService, locationId:number):Promise<any>{
    return httpService.deleteViaHttp(Endpoints.hospital.locations, locationId);
}

export function loadOrganizationsWithLocations(locator:IServiceLocator, hospitalId:number){
    Logger.getLogger().debug(`Launched LocationActions, function saveHospitalDocument, hospital id ${hospitalId}`);
    return function(dispatch){
        dispatch(ajaxStatusActions.beginAjaxCall());
        var httpService = locator.getObject<ISnapMdHttpService>('ISnapMdHttpService');
        return getOrganizationsHttpAsync(httpService, hospitalId)
            .then((locations)=>{
                dispatch(loadOrganizations(locations));
                dispatch(ajaxStatusActions.endAjaxCall());
                Logger.getLogger().debug('saveHospitalDocument function performed work and received data');
            })
            .catch((err:any )=>{
                dispatch(ajaxStatusActions.ajaxCallError(err.message, 'Network Error: load Location in LocationsActions'));
                dispatch(loadOrganizations([]));
                Logger.getLogger().error(err.message , err);
            });
    }
}

export function updateOrgnizationsWithLocations(locator:IServiceLocator, organizationId:number, locationId:number, data:any, methodAddOrUpdate:string){
    Logger.getLogger().debug('Launched LocationActions, function updateOrgnizationsWithLocations');
    return function(dispatch){
        dispatch(ajaxStatusActions.beginAjaxCall());
        var httpService = locator.getObject<ISnapMdHttpService>('ISnapMdHttpService');
        var promise = methodAddOrUpdate === EditorActions.add   ? addLocationHttpAsync(httpService, data)
                                                                : updateLocationHttpAsync(httpService, locationId, data);
        promise
            .then((locations)=>{
                if(methodAddOrUpdate === EditorActions.add){
                    dispatch(addlocations(locations, organizationId));
                } else if(methodAddOrUpdate === EditorActions.edit){
                    dispatch(editLocation(locations, organizationId));
                }
                dispatch(ajaxStatusActions.endAjaxCall());
                Logger.getLogger().debug('updateOrgnizationsWithLocations function performed work and received data');
            })
            .catch((err:any)=>{
                let method = '';

                if(methodAddOrUpdate === EditorActions.add){
                    method = EditorActions.add;
                } else if(methodAddOrUpdate === EditorActions.edit){
                    method = EditorActions.edit;
                }

                dispatch(ajaxStatusActions.ajaxCallError(err.message, `Network Error: ${method} Location in LocationsActions`));
                Logger.getLogger().error(err.message , err);
            });

        return promise;
    }
}

export function deleteOrganizationsWithLocations(locator:IServiceLocator, organizationId:number, locationId:number){
    Logger.getLogger().debug('Launched LocationActions, function deleteOrganizationsWithLocations');
    return function(dispatch){
        dispatch(ajaxStatusActions.beginAjaxCall());
        var httpService = locator.getObject<ISnapMdHttpService>('ISnapMdHttpService');
        return deleteLocationHttpAsync(httpService, locationId)
            .then(()=>{
                dispatch(deleteLocation(locationId, organizationId));
                dispatch(ajaxStatusActions.endAjaxCall());
                Logger.getLogger().debug('deleteOrganizationsWithLocations function performed work and received data');
            })
            .catch((err:any )=>{
                dispatch(ajaxStatusActions.ajaxCallError(err.message, 'Network Error: delete Location in LocationsActions'));
                Logger.getLogger().error(err.message , err);
            });
    }
}

function loadOrganizations(locations:any){
    return {type: types.SNAPMDADMIN_LOAD_ORGANIZATIONS_WITH_LOCATIONS, locations};
};

function addlocations(location:any, organizationId:number){
    return {type: types.SNAPMDADMIN_ADD_LOCATIONS, location, organizationId};
};

function editLocation(location:any, organizationId:number){
    return {type: types.SNAPMDADMIN_UPDATE_LOCATIONS, location, organizationId};
};

function deleteLocation(locationId:number, organizationId:number){
    return {type: types.SNAPMDADMIN_DELETE_LOCATIONS, locationId, organizationId};
};