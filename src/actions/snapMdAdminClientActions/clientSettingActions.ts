import * as ajaxStatusActions from '../ajaxStatusActions';
import * as types from '../../enums/actionTypes';

import { Logger } from '../../utilities/logger';
import { IServiceLocator, SnapMDServiceLocator } from '../../dependenciesInjectionConfiguration';
import { ISnapMdHttpService } from '../../services/api/httpServices';
import { AddressObject } from '../../models/DTO/addressDTO';
import { Endpoints } from '../../enums/endpoints';

// function saveClientAddressHttpAsync(httpService: ISnapMdHttpService, hospitalId:number, address:AddressObject ){
//     return httpService.getViaHttp(Endpoints.snapmdAdmin.);
// }

// export function saveClientAddress(locator:IServiceLocator, hospitalId:number, address:AddressObject){
//     Logger.getLogger().debug(`Launched ClientDetailsActions, function loadSeatsData, hospital id ${hospitalId}`);
//     return function (dispatch){
//         dispatch(ajaxStatusActions.beginAjaxCall());
//         var httpService = locator.getObject<ISnapMdHttpService>('ISnapMdHttpService');
//         return saveClientAddressHttpAsync(httpService, hospitalId, address)
//             .then(()=>{
            
//                 Logger.getLogger().debug('loadSeatsData function performed work and received data');
//             })
//             .catch((err:any )=>{
//                 dispatch(ajaxStatusActions.ajaxCallError(err.message, 'Network Error: loading seats in clientDetailsActions'));
               
//                 Logger.getLogger().error(err.message , err);
//             });
//     }
// }