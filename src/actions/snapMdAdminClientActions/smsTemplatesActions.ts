import * as ajaxStatusActions from '.././ajaxStatusActions';
import * as types from '../../enums/actionTypes';
import { Logger } from '../../utilities/logger';
import { Endpoints } from '../../enums/endpoints';
import SiteNotification from '../../models/site/siteNotification';
import { EventType } from '../../enums/eventType';
import { IServiceLocator, SnapMDServiceLocator } from '../../dependenciesInjectionConfiguration';
import { ISnapMdHttpService, SnapMdHttpService } from '../../services/api/httpServices';
import SmsMessageDTO from '../../models/DTO/smsMessageDTO';
import {EditorActions} from '../../enums/siteEnums';

function getLoadSMSTemplatesHospitalHttpAsync(httpService:ISnapMdHttpService, hospitalId:number):Promise<any>{
    return httpService.getViaHttp(Endpoints.hospital.smstemplatesLoad, hospitalId);
}

function addSMSTemplatesHospitalHttpAsync(httpService:ISnapMdHttpService, message:SmsMessageDTO):Promise<any>{
    return httpService.postViaHttp(Endpoints.hospital.smsTemplates.replace('/%id%',''), message);
}

function updateSMSTemplatesHospitalHttpAsync(httpService:ISnapMdHttpService, messageId:number, message:SmsMessageDTO):Promise<any>{
    return httpService.putViaHttp(Endpoints.hospital.smsTemplates, message, messageId);
}

function deleteSMSTemplatesHospitalHttpAsync(httpService:ISnapMdHttpService, messageId:number):Promise<any>{
    return httpService.deleteViaHttp(Endpoints.hospital.smsTemplates, messageId);
}

export function loadSMSTemplates(locator:IServiceLocator, hospitalId:number){
    Logger.getLogger().debug(`Launched SMSTemplatesActions, function loadSMSTemplates, hospital id ${hospitalId}`);
    return function(dispatch){
        dispatch(ajaxStatusActions.beginAjaxCall());
        var httpService = locator.getObject<ISnapMdHttpService>('ISnapMdHttpService');
        return  getLoadSMSTemplatesHospitalHttpAsync(httpService, hospitalId)
            .then((smsTemplates)=>{
                dispatch(getSMSTemplates(smsTemplates));
                dispatch(ajaxStatusActions.endAjaxCall());
                Logger.getLogger().debug('loadSMSTemplates function performed work and received data');
            })
            .catch((err:any )=>{
                dispatch(getSMSTemplates([]));
                dispatch(ajaxStatusActions.ajaxCallError(err.message, 'Network Error: load sms Template in SMSTemplatesActions'));
                dispatch({ type: types.GLOBAL_NOTIFICATION_ADD, notification: { message: err.message, type: EventType.error } as SiteNotification });
                Logger.getLogger().error(err.message , err);
            });
    }
}

export function updateSMSTemplates(locator:IServiceLocator, messageId:any, message:SmsMessageDTO){
    Logger.getLogger().debug('Launched SMSTemplatesActions, function updateSMSTemplates');
    return function(dispatch){
        dispatch(ajaxStatusActions.beginAjaxCall());
        var httpService = locator.getObject<ISnapMdHttpService>('ISnapMdHttpService');
        let promise = messageId === ''   ? addSMSTemplatesHospitalHttpAsync(httpService, message)
                                        : updateSMSTemplatesHospitalHttpAsync(httpService, messageId, message);
        promise
            .then((smsTemplates)=>{
                messageId === '' ? dispatch(addSMSTemplates(smsTemplates))
                                : dispatch(updateSMSTemplate(smsTemplates));
                dispatch(ajaxStatusActions.endAjaxCall());
                Logger.getLogger().debug('updateSMSTemplates function performed work and received data');
            })
            .catch((err:any )=>{
                let method = '';

                if(messageId === ''){
                    method = EditorActions.add;
                } else 
                    method = EditorActions.edit;
                

                dispatch(ajaxStatusActions.ajaxCallError(err.message, `Network Error: ${method} sms Templates in SMSTemplatesActions`));
                Logger.getLogger().error(err.message , err);
            });
        return promise;
    }
}

export function deleteSMSTemplates(locator:IServiceLocator, messageId:number){
    Logger.getLogger().debug('Launched SMSTemplatesActions, function deleteSMSTemplates');
    return function(dispatch){
        dispatch(ajaxStatusActions.beginAjaxCall());
        var httpService = locator.getObject<ISnapMdHttpService>('ISnapMdHttpService');
        return deleteSMSTemplatesHospitalHttpAsync(httpService, messageId)
            .then(()=>{
                dispatch(deleteSMSTemplate(messageId));
                dispatch(ajaxStatusActions.endAjaxCall());
                Logger.getLogger().debug('deleteSMSTemplates function performed work and received data');
            })
            .catch((err:any )=>{
                dispatch(ajaxStatusActions.ajaxCallError(err.message, 'Network Error: delete sms Template in SMSTemplatesActions'));
                Logger.getLogger().error(err.message , err);
            });
    }
}

function getSMSTemplates(smsTemplates:any){
    return {type: types.SNAPMDADMIN_LOAD_SMS_TEMPLATES, smsTemplates};
};

function addSMSTemplates(smsTemplates:any){
    return {type: types.SNAPMDADMIN_ADD_SMS_TEMPLATES, smsTemplates};
};

function updateSMSTemplate(smsTemplates:any){
    return {type: types.SNAPMDADMIN_UPDATE_SMS_TEMPLATES, smsTemplates};
};

function deleteSMSTemplate(messageId:number){
    return {type: types.SNAPMDADMIN_DELETE_SMS_TEMPLATES, messageId};
};