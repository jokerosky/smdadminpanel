import * as ajaxStatusActions from '.././ajaxStatusActions';
import * as types from '../../enums/actionTypes';
import { Logger } from '../../utilities/logger';
import { Endpoints } from '../../enums/endpoints';
import { IServiceLocator } from '../../dependenciesInjectionConfiguration';
import { ISnapMdHttpService } from '../../services/api/httpServices';
import { ObjectHelper } from '../../utilities/objectHelper';

function queryConsultationMeetingsHttpAsync(httpService:ISnapMdHttpService, fieldQuery:any[]){
    return httpService.getViaHttp(Endpoints.snapmdAdmin.consultationMeetingsPath + '?' + ObjectHelper.generateRequestPath(fieldQuery));
}

export function queryConsultationMeetings(locator:IServiceLocator, fieldQuery:any[]){
    Logger.getLogger().debug('Launched viewConsultationMeetingsActions, function queryConsultationMeetings');
    return function (dispatch){
        dispatch(ajaxStatusActions.beginAjaxCall());
        var httpServise = locator.getObject<ISnapMdHttpService>('ISnapMdHttpService');
        return queryConsultationMeetingsHttpAsync(httpServise, fieldQuery)
            .then((data)=>{
                dispatch(consultationMeetingsLoaded(data));
                dispatch(ajaxStatusActions.endAjaxCall());
                Logger.getLogger().debug('queryConsultationMeetings function performed work and received data');
            })
            .catch((err:any )=>{
                dispatch(ajaxStatusActions.ajaxCallError(err.message, 'Network Error: request queryConsultationMeetings in viewConsultationMeetingsActions'));
                Logger.getLogger().error(err.message , err);
            });
    }
}

function consultationMeetingsLoaded(data){
    return {type: types.CONSULTATION_MEETINGS_DATA_LOADED, data}
}