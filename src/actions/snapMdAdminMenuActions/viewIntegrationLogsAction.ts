import * as ajaxStatusActions from '.././ajaxStatusActions';
import * as types from '../../enums/actionTypes';
import { Logger } from '../../utilities/logger';
import { Endpoints } from '../../enums/endpoints';
import { IServiceLocator } from '../../dependenciesInjectionConfiguration';
import { ISnapMdHttpService } from '../../services/api/httpServices';
import { ObjectHelper } from '../../utilities/objectHelper';

function getIntegrationLogsHttpAsync(httpService:ISnapMdHttpService, fieldQuery:any[]){
    return httpService.getViaHttp(Endpoints.snapmdAdmin.integrationLogsPath + '?' + ObjectHelper.generateRequestPathLoggedMessages(fieldQuery));
}

export function getIntegrationLogs(locator:IServiceLocator, fieldQuery:any[]) {
    Logger.getLogger().debug('Launched viewIntegrationLogsActions, function getIntegrationLogs');
    return function (dispatch) {
        dispatch(ajaxStatusActions.beginAjaxCall());
        var httpService = locator.getObject<ISnapMdHttpService>('ISnapMdHttpService');
        return getIntegrationLogsHttpAsync(httpService, fieldQuery)
            .then((response)=>{
                dispatch(loadIntegrationLogs(response));
                dispatch(ajaxStatusActions.endAjaxCall());
                Logger.getLogger().debug('getIntegrationLogs function performed work and received data');
            })
            .catch((err:any )=>{
                dispatch(ajaxStatusActions.ajaxCallError(err.message, 'Network Error: request getIntegrationLogs in viewIntegrationLogsActions'));
                Logger.getLogger().error(err.message , err);
            });
    }
}

function loadIntegrationLogs(logs){
    return {type: types.INTEGRATION_LOGS_DATA_LOADED, logs};
}