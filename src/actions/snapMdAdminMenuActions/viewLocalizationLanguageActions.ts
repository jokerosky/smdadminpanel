import * as types from '../../enums/actionTypes';
import { Logger } from '../../utilities/logger';
import { LocalizationService } from '../../localization/localizationService';

export function changeLanguage(language: string) {
    return { type: types.LANGUAGE_CHANGED, language };
}

export function loadDictionary() {
    Logger.getLogger().debug('Start loading dictionary');
    return function (dispatch) {
        if (LocalizationService.language.toUpperCase() in LocalizationService.dictionaries.vocabulary) {
            Logger.getLogger().debug('Get dictionary from cache');
            return dispatch(changeDictionary(LocalizationService.dictionaries.vocabulary[LocalizationService.language.toUpperCase()]));
        } else {
            return LocalizationService.getDictionary()
                .then((response) => {
                    LocalizationService.dictionaries.language = LocalizationService.language;
                    LocalizationService.dictionaries.vocabulary[response.data.LANGUAGE] = response.data;
                    dispatch(changeDictionary(response.data))
                })
                .catch((data) => {
                    Logger.getLogger().error('Dictionary loading error');
                });
        }
    }
}

function changeDictionary(dictionary) {
    return { type: types.DICTIONARY_CHANGED, dictionary };
}