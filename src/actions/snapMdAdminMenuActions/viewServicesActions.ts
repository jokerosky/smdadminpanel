import * as ajaxStatusActions from '.././ajaxStatusActions';
import * as types from '../../enums/actionTypes';
import { Logger } from '../../utilities/logger';
import { LocalizationService } from '../../localization/localizationService';
import { Endpoints } from '../../enums/endpoints';
import { IServiceLocator } from '../../dependenciesInjectionConfiguration';
import { ISnapMdHttpService } from '../../services/api/httpServices';

function getServicesHttpAsync(httpService: ISnapMdHttpService) {
    return httpService.getViaHttp(Endpoints.snapmdAdmin.servicesPath);
}

function getServiceLogRecordsHttpAsync(httpService: ISnapMdHttpService, serviceId: number, logparameters: any) {
    return httpService.getViaHttp(Endpoints.snapmdAdmin.servicesLogs + `?take=${logparameters.take}&ascending=${logparameters.ascending}&level=${logparameters.level}`,
        serviceId);
}

function updateStatusHttpAsync(httpService: ISnapMdHttpService, serviceStatusParameters: any){
    return httpService.postViaHttp(Endpoints.snapmdAdmin.updateStatus, serviceStatusParameters);
}

function updateLoggingLevel(httpService: ISnapMdHttpService, loggingLevelParameters: any){
    return httpService.postViaHttp(Endpoints.snapmdAdmin.updateLoggingLevel, loggingLevelParameters);
}

export function getServices(locator: IServiceLocator) {
    Logger.getLogger().debug('Launched viewServicesActions, function getServices');
    return function (dispatch) {
        dispatch(ajaxStatusActions.beginAjaxCall());
        var httpServise = locator.getObject<ISnapMdHttpService>('ISnapMdHttpService');
        return getServicesHttpAsync(httpServise)
            .then((response) => {
                dispatch(servicesLoaded(response));
                dispatch(ajaxStatusActions.endAjaxCall());
                Logger.getLogger().debug('getServices function performed work and received data');
            })
            .catch((err: any) => {
                dispatch(ajaxStatusActions.ajaxCallError(err.message, 'Network Error: request getServices in viewServicesActions'));
                Logger.getLogger().error(err.message, err);
            })
    }
}

export function getServiceLogRecords(locator: IServiceLocator, serviceId: number, logparameters: any) {
    Logger.getLogger().debug('Launched viewServicesActions, function getServiceLogRecords');
    return function (dispatch) {
        dispatch(ajaxStatusActions.beginAjaxCall());
        var httpServise = locator.getObject<ISnapMdHttpService>('ISnapMdHttpService');
        return getServiceLogRecordsHttpAsync(httpServise, serviceId, logparameters)
            .then((records) => {
                dispatch(serviceLogRecordsLoaded(records));
                dispatch(ajaxStatusActions.endAjaxCall());
                Logger.getLogger().debug('getServiceLogRecords function performed work and received data');
            })
            .catch((err: any) => {
                dispatch(ajaxStatusActions.ajaxCallError(err.message, 'Network Error: request getServiceLogRecords in viewServicesActions'));
                Logger.getLogger().error(err.message, err);
            });
    }
}

export function updateStateServices(locator: IServiceLocator, serviceStatusParameters: any, loggingLevelParameters: any){
    Logger.getLogger().debug('Launched viewServicesActions, function updateStateServices');
    return function (dispatch) {
        dispatch(ajaxStatusActions.beginAjaxCall());
        var httpServise = locator.getObject<ISnapMdHttpService>('ISnapMdHttpService');
        return updateStatusHttpAsync(httpServise, serviceStatusParameters)
            .then((response)=>{
                return updateLoggingLevel(httpServise, loggingLevelParameters);
            })
            .then((response)=>{
                dispatch(updateServicesStatus(serviceStatusParameters, loggingLevelParameters));
                dispatch(ajaxStatusActions.endAjaxCall());
                Logger.getLogger().debug('updateStateServices function performed work and received data');
            })
            .catch((err: any) => {
                dispatch(ajaxStatusActions.ajaxCallError(err.message, 'Network Error: request updateStateServices in viewServicesActions'));
                Logger.getLogger().error(err.message, err);
            });
    }
}

function servicesLoaded(services) {
    return { type: types.SERVICES_LOADED, services };
}

function serviceLogRecordsLoaded(records) {
    return { type: types.SERVICES_LOGS_RECORDS_LOADED, records };
}

function updateServicesStatus(serviceStatusParameters, loggingLevelParameters){
    return {type: types.SERVICES_STATUS_UPDATE, serviceStatusParameters, loggingLevelParameters};
}