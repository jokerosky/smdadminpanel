import * as ajaxStatusActions from '.././ajaxStatusActions';
import * as types from '../../enums/actionTypes';
import { Logger } from '../../utilities/logger';
import { Endpoints } from '../../enums/endpoints';
import { IServiceLocator } from '../../dependenciesInjectionConfiguration';
import { ISnapMdHttpService } from '../../services/api/httpServices';
import { ObjectHelper } from '../../utilities/objectHelper';

function queryLogMessagesHttpAsync(httpService: ISnapMdHttpService, fieldQuery: any[]) {
    return httpService.getViaHttp(Endpoints.snapmdAdmin.loggedMessagesPath + '?' + ObjectHelper.generateRequestPathLoggedMessages(fieldQuery));
}

export function loadLogMessages(locator: IServiceLocator, fieldQuery: any[]) {
    Logger.getLogger().debug('Launched viewLoggedMessagesActions, function loadLogMessages');
    return function (dispatch) {
        dispatch(ajaxStatusActions.beginAjaxCall());
        var httpServise = locator.getObject<ISnapMdHttpService>('ISnapMdHttpService');
        return queryLogMessagesHttpAsync(httpServise, fieldQuery)
            .then((data) => {
                dispatch(logMessagesLoaded(data));
                dispatch(ajaxStatusActions.endAjaxCall());
                Logger.getLogger().debug('loadLogMessages function performed work and received data');
            })
            .catch((err: any) => {
                dispatch(ajaxStatusActions.ajaxCallError(err.message, 'Network Error: request loadLogMessages in viewLoggedMessagesActions'));
                Logger.getLogger().error(err.message, err);
            });
    }
}

function logMessagesLoaded(data: any) {
    return { type: types.LOGGED_MESSAGES_DATA_LOADED, data }
}