import * as types from '../enums/actionTypes';
import { SiteConstants } from '../enums/siteConstants';
import * as ajaxStatusActions  from '../actions/ajaxStatusActions';
import * as siteActions  from '../actions/siteActions';
import ProviderService from '../services/api/providerService';
import { UserStaffProfile } from '../models/userStaffProfile';

function staffProfileLoaded(staffProfileData){
    return { type: types.STAFF_PROFILE_DATA_LOADED, data: staffProfileData };
}


export function loadStaffProfile() {
    return function(dispatch){
        dispatch(ajaxStatusActions.beginAjaxCall());
        return ProviderService.getStaffProfileAsync()
        .then((data)=>{
            dispatch(ajaxStatusActions.endAjaxCall());
            dispatch(staffProfileLoaded(Object.assign(new UserStaffProfile(), data)));
            dispatch(siteActions.saveSessionData(SiteConstants.session.staffprofile, data));
        })
        .catch((error)=>{
            dispatch(ajaxStatusActions.ajaxCallError());
        });

    };
}
