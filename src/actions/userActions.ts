import * as _ from 'lodash';
import { push } from 'react-router-redux';

import * as types from '../enums/actionTypes';
import * as userTypes from '../enums/userTypes';
import { EventType } from '../enums/eventType';
import { Endpoints } from '../enums/endpoints';
import { SiteConstants } from '../enums/siteConstants';
import hospitalDataService from '../services/api/hospitalDataService';
import userService from '../services/api/userService';
import * as ajaxStatusActions from './ajaxStatusActions';
import * as siteActions from './siteActions';
import LoginRequestDTO from '../models/DTO/loginRequestDTO';
import { NewPatientRequestDTO } from '../models/DTO/newPatientRequestDTO';
import { NewPatientResponseDTO } from '../models/DTO/newPatientResponseDTO';
import { IRegistrationAvailabilityResponse } from '../models/DTO/RegistrationAvailabilityResponse';
import SiteNotification from '../models/site/siteNotification';
import HttpUtility from '../utilities/httpUtility';
import { Logger } from '../utilities/logger';

export function selectLogin(userType, redirectUrl?:string) {
    return { type: types.LOGIN_TYPE_SELECTED, userType, redirectUrl };
}

export function setUserRegistrationStep(step: number) {
    return { type: types.SET_REGISTRATION_FORM_STEP, step };
}

export function setRegistrationFormData(data: any) {
    return { type: types.SET_REGISTRATION_FORM_DATA, data };
};

export function logIn(loginData: LoginRequestDTO) {
    return function (dispatch) {
        dispatch({ type: types.LOGIN_CLEAR });
        dispatch(ajaxStatusActions.beginAjaxCall());
        return userService.getTokenAsync(loginData).then((tokenData: any) => {
            Logger.getLogger().info(`User ${loginData.email} logged in.`);
            dispatch(ajaxStatusActions.endAjaxCall());
            dispatch({type: types.LOGIN_SUCCESS, tokenData });
            dispatch(siteActions.saveSessionData(SiteConstants.session.sessionData, tokenData));
            HttpUtility.configureAuthorization(tokenData.access_token);
            
            if (loginData.redirectUrl) {
                dispatch(push(loginData.redirectUrl));
                dispatch({type: types.SITE_CLEAR_TEMP_STATE});
            }
            else {
                let type = _.findKey(userTypes, x => { return x == loginData.userTypeId });
                dispatch(push(Endpoints.site.dashboard.replace(/%type%/, type)));
            }

        }).catch((error) => {
            Logger.getLogger().info(`Login error for user ${loginData.email}`);
            dispatch(ajaxStatusActions.ajaxCallError());
            dispatch({ type: types.LOGIN_FAILURE, error: error.serviceErrMessage }); // can handle this message in reducer also, but i think  better dispatch another action 
            dispatch({ type: types.GLOBAL_NOTIFICATION_ADD, notification: { message: error.errMessage, type: EventType.error } as SiteNotification });
        });
    };
}

export function logOut() {
    return function (dispatch) {
        dispatch(siteActions.deleteSessionData());
        dispatch(siteActions.logoutUserCleanup());
        dispatch({ type: types.LOGIN_CLEAR });
        
        dispatch(push(Endpoints.site.root));
    }
}

export function resetPassword(loginData: LoginRequestDTO) {
    return function (dispatch) {
        dispatch({ type: types.LOGIN_CLEAR });
        dispatch(ajaxStatusActions.beginAjaxCall());
        return userService.resetPasswordAsync(loginData).then((tokenData) => {
            dispatch({ type: types.RESET_PASSWORD_SUCCESS, tokenData });
            dispatch(ajaxStatusActions.endAjaxCall());
        }).catch((error) => {
            dispatch(ajaxStatusActions.ajaxCallError());
            dispatch({ type: types.RESET_PASSWORD_FAILURE, error: error.serviceErrMessage });
            dispatch({ type: types.GLOBAL_NOTIFICATION_ADD, notification: { message: error.errMessage, type: EventType.error } as SiteNotification });
        });
    };
}

export function validateRegistrationAddress(address: string, hospitalId: number) {
    return function (dispatch) {
        dispatch(ajaxStatusActions.beginAjaxCall());
        return userService.validateRegistrationAddressAsync(address, hospitalId).
            then((response: IRegistrationAvailabilityResponse) => {
                dispatch({ type: types.REGISTRATION_ADDRESS_VALIDATED, response: response });
                dispatch(ajaxStatusActions.endAjaxCall());
            })
            .catch((error) => {
                dispatch(ajaxStatusActions.ajaxCallError());
            })
    }
}

export function registerPatient(data: NewPatientRequestDTO) {
    return function (dispatch) {
        dispatch(ajaxStatusActions.beginAjaxCall());
        return userService.registerPatientAsync(data).
            then((response: NewPatientResponseDTO) => {
                dispatch({ type: types.REGISTRATION_NEW_PATIENT_REGISTERED, response: response });
                dispatch(ajaxStatusActions.endAjaxCall());
            })
            .catch((error) => {
                if (error.status == 400) {
                    dispatch({ type: types.REGISTRATION_NEW_PATIENT_ERROR, message: error.message });
                    dispatch(ajaxStatusActions.endAjaxCall());
                } else {
                    dispatch(ajaxStatusActions.ajaxCallError());
                }

            })
    }
}