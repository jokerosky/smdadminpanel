import { language } from '../enums/languages';
import * as Vocabulary from './vocabulary';
import axios from 'axios';
import { Endpoints } from '../enums/endpoints';

export class LocalizationService {
    //static dictionaries:string[][];
    static dictionaries = {
        language: '',
        vocabulary: {}
    };

    static countQuery = 0;

    static language: string;

    static getDictionary(): any {
        return  getLanguage();
    }
}

async function getLanguage() {
    let json = await axios(Endpoints.site.root + `assets/vocabularies/vocabulary.${LocalizationService.language.toUpperCase()}.json`);
    return json;
}