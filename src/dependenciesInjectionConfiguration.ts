import * as axios from 'axios';
import {AxiosInstance, AxiosStatic } from 'axios';
import { SignalRService } from './services/signalR/signalRService';
import { SecurityService } from './services/securityService';

import { LocalStorageService, SessionStorageService } from './services/storageService';

import { ISnapMdClientService, SnapMdClientService } from './services/api/snapMdClientService';
import { ISnapMdClientDataService, SnapMdClientDataService } from './services/api/snapMdClientDataService';
import { ISnapMdSeatsService, SnapMdSeatsService } from './services/api/snapMdSeatsService';
import { ISnapMdHttpService, SnapMdHttpService } from './services/api/httpServices';
import { TestService } from './services/testService';

export interface IServiceLocator {
    addObject(typeName:string, obj:object):void;
    getObject<T>(typeName:string):T;
    addType<T>(type:T):void
    getType<T>(type:T):T;
}

export class MyServiceLocator implements IServiceLocator{
    typesDictionary: object = {};
    objectsDictionary: object = {};
    
    getType<T>(type:T): T {
        return (this.typesDictionary[type.toString()] || null) as T;
    }
    getObject<T>(typeName: string): T {
        return (this.objectsDictionary[typeName] || null) as T;
    }
    addObject(typename: string, obj: object): void {
        this.objectsDictionary[typename] = obj;
    }
    addType<T>(type: T): void {
        this.typesDictionary[type.toString()] = type;
    }

}

export class SnapMDServiceLocator {
    static typesDictionary: object = {};
    static objectsDictionary: object = {};

    static getType<T>(typeName:T){
        return (this.typesDictionary[typeName.toString()] || null) as T;
    }

    static getObject<T>(typeName:string){
        return (this.objectsDictionary[typeName] || null) as T;
    }

    static addObject(typename: string, obj: object){
        this.objectsDictionary[typename] = obj;
    }

    static addType(constructor:any){
        this.typesDictionary[constructor.toString()] = constructor;
    }
}

export function configureSnapIoC(store, $){
    
    var securityService = new SecurityService(store);
    var signalRService = new SignalRService(store.dispatch, $);


    SnapMDServiceLocator.addObject('store', store);
    SnapMDServiceLocator.addObject('SecurityService', securityService);
    SnapMDServiceLocator.addObject('SignalRService', signalRService);

    SnapMDServiceLocator.addObject('LocalStorageService', new LocalStorageService());
    SnapMDServiceLocator.addObject('SessionStorageService', new SessionStorageService());
    //var instance  = (axios.default as AxiosStatic) .create() as AxiosInstance;
    //SnapMDServiceLocator.addObject('axios', instance);
    

    var snapMdClientService = new SnapMdClientService(axios.default as AxiosStatic);
    SnapMDServiceLocator.addObject('ISnapMdClientService', snapMdClientService);
    

    var snapMdHttpService = new SnapMdHttpService(axios.default as AxiosStatic);
    SnapMDServiceLocator.addObject('ISnapMdHttpService', snapMdHttpService);

    SnapMDServiceLocator.addObject('TestService',new TestService());
    SnapMDServiceLocator.addType(TestService);
}