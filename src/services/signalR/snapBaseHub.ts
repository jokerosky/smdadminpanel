
export class SnapBaseHub {
    constructor(proxy:any, dispatch:(any)=>void){
        this.hubProxy = proxy;
        this.dispatch = dispatch;
    }

    dispatch:(any)=>void;
    hubProxy:any;
}