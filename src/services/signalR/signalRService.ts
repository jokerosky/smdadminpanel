import * as _ from 'lodash';

import { Endpoints, baseApiUrl } from '../../enums/endpoints';
import { Logger } from '../../utilities/logger';

import { SnapWaitingRoomConsultationsHub } from './snapWaitingRoomConsultationsHub';
import { ObjectHelper } from '../../utilities/objectHelper';

let HubNames = {
    snapWaitingRoomConsultationsHub:'snapWaitingRoomConsultationsHub'
}

export { HubNames };

export enum SignalRConnectionState{
    Connecting = 0,
    Connected = 1,
    Reconnecting = 2,
    Disconnected = 4
}

export class SignalRService {
    constructor(dispatch: any, $:any) {
        this.dispatch = dispatch;
        this.$ = $;

        this.addHub = this.addHub.bind(this);
        this.initSignalR = this.initSignalR.bind(this);
    }

    dispatch: () => {};
    $:any; //jQuery with signalR plugin
    static connection: any;
    hubs: object = {};

    addHub(name: string, hub: any) {
        this.hubs[name] = hub;
    }

    initSignalR(token:any) {
        //var connection = $.connection.hub.start
        
        var connection = this.$.hubConnection( ObjectHelper.removeTrailingSlash(baseApiUrl) );
        
        connection.qs = {
            Bearer: token
        }

        this.addHub(HubNames.snapWaitingRoomConsultationsHub, 
        new SnapWaitingRoomConsultationsHub(connection.createHubProxy('snapWaitingRoomConsultationsHub'), this.dispatch));

        connection.received(connectionReceived);
        connection.error(connectionError);
        connection.stateChanged(connectionStateChanged);
        connection.reconnected(connectionReconnected);
        connection.starting(connectionStarting);

        connection.start({ transport: ['webSockets', 'serverSentEvents'] })
            .then(connectionStarted)
            .fail(connectionError);

        SignalRService.connection = connection;
    }
}

export function connectionReceived(data) {
    Logger.getLogger().info('SignalR connection recieved');
}

export function connectionError(data) {
    Logger.getLogger().error("SignalR error",data);
}

export function connectionStateChanged(data) {
    Logger.getLogger().debug(`SignalR state changed, oldState = ${_.findKey(SignalRConnectionState,(val)=>{return val == data.oldState})}, newState = ${_.findKey(SignalRConnectionState,(val)=>{return val == data.newState})}.`);
}

export function connectionReconnected(data) {
    Logger.getLogger().warn('SignalR reconnected');
}

export function connectionStarting(data) {
    Logger.getLogger().debug('SignalR connection starting.');
}

export function connectionStarted(data) {
    Logger.getLogger().info('SignalR started, connection ID=' + SignalRService.connection.id);
}


