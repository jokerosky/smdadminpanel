import { SnapBaseHub } from './snapBaseHub';
import * as actionTypes from '../../enums/actionTypes';
import { PatientQueueResponseDTO } from '../../models/DTO/PatientQueueResponseDTO';
import * as queueActions from '../../actions/patientQueueActions';
import { Logger } from '../../utilities/logger';
import { ObjectHelper } from '../../utilities/objectHelper';


export class SnapWaitingRoomConsultationsHub extends SnapBaseHub{
    constructor(proxy:any, dispatch:(any)=>void){
        super(proxy, dispatch);
        
        // bind event handlers 
        for(let item in queueActions.actionNames){
            proxy.on(queueActions.actionNames[item], ((data)=>{ 
                this.generalClientEvent(data, queueActions.actionNames[item]);
            }).bind(this));
        }

        this.refreshAsync = this.refreshAsync.bind(this);
    }

    // function to call event corresponding action 
    generalClientEvent(data: any, eventName: string){
        data = ObjectHelper.decapitalizeProperties(data);
        Logger.getLogger().debug(eventName);
        try {
            this.dispatch(queueActions[eventName](data));
        }
        catch(exp){
            Logger.getLogger().error(`Can't find or call to dispatch action ${eventName}`, exp);
        }
        
    }
    
    refreshAsync(){
        return this.hubProxy.invoke('refresh');
    }

}