import axios from 'axios';

import { Endpoints } from '../../enums/endpoints';
import MessagesFormatter from '../../utilities/messagesFormatter';
import HttpUtility from '../../utilities/httpUtility';

export default class ProviderService {

     static getStaffProfileAsync() {
        return new Promise((resolve, reject) => {
            return axios.get(Endpoints.provider.userStaffProfile,
                )
                .then(response => {
                    if (typeof (response) !== 'string') {
                        resolve(response.data.data[0]);
                    }
                    else {
                        reject(MessagesFormatter.formatHttpDataIsNotAnObject(response));
                    }
                })
                .catch(response => {
                    let result = {
                        errMessage: MessagesFormatter.formatUIResponseError(response),
                        serviceErrMessage: MessagesFormatter.formatHttpResponseError(response)
                    }

                    reject(result);
                });
        });
    }

}