import { AxiosInstance } from 'axios';

import { Endpoints } from '../../enums/endpoints';
import MessagesFormatter from '../../utilities/messagesFormatter';
import HttpUtility from '../../utilities/httpUtility';

export interface ISnapMdHttpService{
    postViaHttp(url: string, data:any, id?:number):Promise<any>;
    getViaHttp(url:string, id?:number, fields?:string[]):Promise<any>;
    putViaHttp(url: string, data?:any, id?:number, field?:string):Promise<any>;
    deleteViaHttp(url:string, id:number, data?:any):Promise<any>;
}

export class SnapMdHttpService implements ISnapMdHttpService{
    
    private _axios: AxiosInstance;
    headers = {'Content-Type':'application/json'};
    constructor(axios:AxiosInstance ){
        this._axios = axios;
    }
   
    postViaHttp(url: string, data:any, id?:number):Promise<any>{
        return new Promise((resolve, reject)=>{
            let _url;
            if(id > -1){
                _url = url.replace('%id%',id.toString()); 
            } else {
                _url = url;
            }
            return this._axios.post(_url, data)
                .then((response)=>{
                    resolve(HttpUtility.getResponseData(response));
                })
                .catch((err)=>{
                    reject(err);
                });
        });
    };

    getViaHttp(url:string, id?:number, fields?:string[]):Promise<any>{
        return new Promise((resolve, reject)=>{
            let _url;
            if(id > -1 && fields){
                _url = url.replace('%id%',id.toString()) + '?fields='+  fields;
            } else {
                if(id > -1){
                    _url = url.replace('%id%',id.toString());
                } else{
                    if(fields){
                        _url = (url + '?fields=' + fields);
                    } else {
                        _url = url;
                    }
                }
            }
            return this._axios.get( _url )
                .then((response)=>{
                    _url;
                    resolve(HttpUtility.getResponseData(response));
                })
                .catch((err)=>{
                    reject(err);
                });
        });
    
    };

    putViaHttp(url: string, data?:any, id?:number, field?:string):Promise<any>{
        return new Promise((resolve, reject)=>{
            let _url;
            let dataToSend;
            if(id > -1){
                _url = url.replace('%id%',id.toString());
            } else {
                _url = url;
            }

            if(data){
                dataToSend = data;                       
            } else if(field){
                        dataToSend = JSON.stringify(field);
            } else{
                dataToSend = '';
            }

            return this._axios.put(_url, dataToSend, {
                headers:{
                    'Content-Type': 'application/json',
                }
            })
                .then((response)=>{
                    resolve(HttpUtility.getResponseData(response));
                })
                .catch((err)=>{
                    reject(err);
                });
        });
    };

    deleteViaHttp(url:string, id:number, data?:any):Promise<any>{
        return new Promise((resolve, reject)=>{
            let _url;
            let _data;
            let request;
            if(id > -1){
                _url = url.replace('%id%',id.toString());
            } else{
                _url = url;
            }
            if(data){
                _data = data;
                request = this._axios.request(
                    {
                        method: 'delete',
                        url: _url,
                        data : _data
                    });
            } else {
                request = this._axios.delete(_url);
            }
            
            return request
                .then((response)=>{
                    resolve(HttpUtility.getResponseData(response));
                })
                .catch((err)=>{
                    reject(err);
                });
        });
    };
}