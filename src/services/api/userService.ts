import axios from 'axios';

import { Endpoints } from '../../enums/endpoints';
import LoginRequestDTO from '../../models/DTO/loginRequestDTO';
import { NewPatientRequestDTO } from '../../models/DTO/newPatientRequestDTO';
import { NewPatientResponseDTO } from '../../models/DTO/newPatientResponseDTO';
import MessagesFormatter from '../../utilities/messagesFormatter';
import HttpUtility from '../../utilities/httpUtility';
import { IRegistrationAvailabilityResponse } from '../../models/DTO/RegistrationAvailabilityResponse';

let store = {};

export default class UserService {
    static setStore(globalStore) {
        store = globalStore;
    }

    static checkLogin(nextState, replace, callback) {

    }

    static getTokenAsync(loginData: LoginRequestDTO) {
        return new Promise((resolve, reject) => {
            return axios.post(Endpoints.user.login,
                loginData)
                .then(response => {
                    if (typeof (response) !== 'string') {
                        resolve(response.data.data[0]);
                    }
                    else {
                        reject(MessagesFormatter.formatHttpDataIsNotAnObject(response));
                    }
                })
                .catch(response => {
                    let result = {
                        errMessage: MessagesFormatter.formatUIResponseError(response),
                        serviceErrMessage: MessagesFormatter.formatHttpResponseError(response)
                    }

                    reject(result);
                });
        });
    }

    static resetPasswordAsync(loginData: LoginRequestDTO) {
        return new Promise((resolve, reject) => {
            return axios.post(Endpoints.user.resetPassword,
                loginData)
                .then(response => {
                    if (typeof (response) !== 'string') {
                        resolve(HttpUtility.getResponseData(response));
                    }
                    else {
                        reject(MessagesFormatter.formatHttpDataIsNotAnObject(response));
                    }
                })
                .catch(response => {
                    let result = {
                        errMessage: MessagesFormatter.formatUIResponseError(response) || "Can't reset password or user with provided email not found" ,
                        serviceErrMessage: MessagesFormatter.formatHttpResponseError(response)
                    }

                    reject(result);
                });
        });
    }

    static validateRegistrationAddressAsync(addressText: string, hospitalId: number) {
        return new Promise((resolve, reject) => {
            let path = Endpoints.user.validateRegistrationAddress +
            "?addressText=" + addressText+
            "&hospitalId=" + hospitalId;
            return axios.get(path)
                .then(response => {
                    if (typeof (response) !== 'string') {
                        resolve(HttpUtility.getResponseData(response) as IRegistrationAvailabilityResponse);
                    }
                    else {
                        reject(MessagesFormatter.formatHttpDataIsNotAnObject(response));
                    }
                    
                })
                .catch(response => {
                    let result = {
                        
                    }
                    reject(result);
                });
        });
    }

    static registerPatientAsync(data:NewPatientRequestDTO) {
        return new Promise((resolve, reject) => {
            let path = Endpoints.user.registerNewPatient            
            return axios.post(path, data)
                .then(response => {
                    if (typeof (response) !== 'string') {
                        resolve(HttpUtility.getResponseData(response));
                    }
                    else {
                        reject(MessagesFormatter.formatHttpDataIsNotAnObject(response));
                    }
                    
                })
                .catch(config => {
                    let data = HttpUtility.getResponseData(config.response);
                    let result = {
                        status: config.response.status,
                        message: data ? data.message : config.message 
                    }

                    reject(result);
                });
        });
    }

}