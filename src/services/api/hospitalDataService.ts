import axios from 'axios';

import { Endpoints } from '../../enums/endpoints';
import MessagesFormatter from '../../utilities/messagesFormatter';

export default class HospitalDataService {

    static getIdAndApiKeys() {
        return new Promise((resolve, reject) => {
            let apiSessionData = {};
            return this.getApiSessionKey().then((data:any)=>{
                apiSessionData = data;
                return axios.get(Endpoints.hospital.getHospitalIdWithKeys)
            })
            .then( (response) => {
                if(typeof(response) === 'undefined'){
                    throw axios;
                }
                if(typeof(response) !== 'string'){
                    var keys =  Object.assign(apiSessionData, response.data.data[0]);
                    resolve(keys);
                }
                else{
                    reject(MessagesFormatter.formatHttpDataIsNotAnObject(response));    
                }
            })
            .catch( response => {
                reject(MessagesFormatter.formatHttpResponseError(response));
            });
        });
    }

    static getApiSessionKey(){
        return new Promise((resolve, reject) => {
            return axios.get(Endpoints.hospital.getApiSessionKey)
            .then( (response) => {
                if(typeof(response) === 'undefined'){
                    throw axios;
                }
                if(typeof(response) !== 'string'){
                    resolve(response.data);
                }
                else{
                    reject(MessagesFormatter.formatHttpDataIsNotAnObject(response));    
                }
            })
            .catch( response => {
                reject(MessagesFormatter.formatHttpResponseError(response));
            });
        });
    }

    static getData(hospitalId) {
        return new Promise((resolve, reject) => {
            return axios.get(Endpoints.hospital.getHospitalInfo.replace('%id%', hospitalId))
            .then( response => {
                if(typeof(response) !== 'string'){
                    resolve(response.data.data[0]);
                }
                else{
                    reject(MessagesFormatter.formatHttpDataIsNotAnObject(response));
                }
            })
            .catch( response => {
                reject(MessagesFormatter.formatHttpResponseError(response));
            });
        });
    }
}