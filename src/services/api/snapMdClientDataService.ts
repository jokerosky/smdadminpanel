import { AxiosInstance } from 'axios';

import { Endpoints } from '../../enums/endpoints';
import HttpUtility from '../../utilities/httpUtility';

export interface ISnapMdClientDataService{
    getClientDetails(id: number):Promise<any>;
}
export class SnapMdClientDataService implements ISnapMdClientDataService{
    private _axios: AxiosInstance;

    constructor(axios: AxiosInstance){
        this._axios = axios;
    }

    getClientDetails(id: number):Promise<any>{
        return new Promise((resolve, reject)=>{
            return this._axios.get(Endpoints.snapmdAdmin.clients+'/'+id)
                .then((response)=>{
                    resolve(HttpUtility.getResponseData(response));
                })
                .catch((err)=>{
                    reject(err);
                });
        });
    };

}