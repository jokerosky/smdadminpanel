import { AxiosInstance } from 'axios';

import { Endpoints } from '../../enums/endpoints';
import MessagesFormatter from '../../utilities/messagesFormatter';
import HttpUtility from '../../utilities/httpUtility';

export interface ISnapMdClientService{
    getClientsAsync():Promise<any>;
}

export class SnapMdClientService implements ISnapMdClientService{
    
    private _axios: AxiosInstance

    constructor(axios:AxiosInstance ){
        this._axios = axios;
    }
   

    getClientsAsync():Promise<any>{
        return new Promise((resolve, reject)=>{
            return this._axios.get(Endpoints.snapmdAdmin.clients)
            .then((response)=>{
                resolve(HttpUtility.getResponseData(response));
            })
            .catch((err)=>{
                reject(err);
            })
            ;
        });
        
        
    } ;
}