import { AxiosInstance } from 'axios';

import { Endpoints } from '../../enums/endpoints';
import MessagesFormatter from '../../utilities/messagesFormatter';
import HttpUtility from '../../utilities/httpUtility';

export interface ISnapMdSeatsService{
    getSeats(id:number):Promise<any>;
    getSeatsAssigned(id:number):Promise<any>;
}

export class SnapMdSeatsService implements ISnapMdSeatsService{
    private _axios: AxiosInstance
    
    constructor(axios:AxiosInstance ){
        this._axios = axios;
    }

    getHttp(url:string, id:number):Promise<any>{
        
        return new Promise((resolve, reject)=>{
            return this._axios.get(url.replace('%id%',id.toString()))
                .then((response)=>{
                    resolve(HttpUtility.getResponseData(response));
                })
                .catch((err)=>{
                    reject(err);
                });
        });
    };

    getSeats(id:number):Promise<any>{
        return this.getHttp(Endpoints.snapmdAdmin.seats, id);
    }

    getSeatsAssigned(id:number):Promise<any>{
        return this.getHttp(Endpoints.snapmdAdmin.seatsAssigned, id);
    }
}