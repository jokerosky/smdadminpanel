import * as  _ from 'lodash';

import * as userTypes from '../enums/userTypes';
import { Endpoints } from '../enums/endpoints';
import * as userActions from '../actions/userActions';

export interface ISecurityService {

}

export class SecurityService implements ISecurityService {
    private store: any;
    private isInitialized: boolean = false;

    constructor(store: any) {
        this.setStore(store);

        this.checkLogin = this.checkLogin.bind(this);
    }

    setStore(store) {
        if (store) {
            this.store = store;
            this.isInitialized = true;
        }
        else {
            this.store = null;
            this.isInitialized = false;
        }
    }

    static isPublicPage(location:string) 
    { 
        let area = location.split('/')
                    .filter((elm)=>{return elm})[0];
        return _.indexOf(_.keys(userTypes), area) < 0;
    }

    determineUserType(location: string) {
        let segments = location.split('/').filter((v) => (!!(v) === true));;
        let siteArea = segments[0];

        if (segments.length < 1) {
            return userTypes.guest;
        }

        for (var index in userTypes) {
            if (index.toLocaleLowerCase() == siteArea.toLocaleLowerCase())
                return userTypes[index];
        }

        return userTypes.guest; // for default result
    }

    //onEnter callback
    hasAccess(nextState, replace, callback) {

    }

    //onEnter callback
    checkLogin(nextState, replace, callback) {
        if (!this.isInitialized) {
            //todo: add here logging logic
            return false;
        }

        let state = this.store.getState();
        // ↓ routes will have segment /:userType  
        let isLogin = !!_.find(nextState.routes, (route: any) => { return route.path == Endpoints.site.loginPostfix }) || false;
        let userType = this.determineUserType(nextState.location.pathname);
        let redirectUrl = "";

        if (!(state.user.accessToken || isLogin || userType == userTypes.guest)) {
            replace('/' + Endpoints.site.login.replace(/%type%/, _.findKey(userTypes, (val) => { return val == userType })));
            redirectUrl = nextState.location.pathname;
        }

        // set user type in case it was not set before (direct link)
        var action = userActions.selectLogin(userType, redirectUrl);
        this.store.dispatch(action);

        callback();
    }
}