export interface IStorageService {
    get(key: string): any;
    del(key: string): void;
    add(key: string, value: any): void;
    clear(): void;
}

export class StorageService implements IStorageService {
    store:any;
    constructor(store?:any){
        this.store = store;
    }

    getStore(): any {
        if(this.store)
            return this.store;
        else
        return {
            getItem: (): any => { return null; },
            removeItem: (): any => { return null; },
            setItem: (): any => { return null; },
            clear: (): any => { return null; }
        };
    }

    get(key: string): any {
        let value = this.getStore().getItem(key);
        if (value) {
            value = JSON.parse(value);
        }
        return value;
    }

    add(key: string, value: any) {
        value = JSON.stringify(value);
        this.getStore().setItem(key, value);
    }

    del(key: string) {
        this.getStore().removeItem(key);
    }

    clear(){
        this.getStore().clear();
    }
}

export class LocalStorageService extends StorageService {
    getStore(): any {
        if(window.localStorage){
            return window.localStorage;
        }
        return super.getStore();
    }
}

export class SessionStorageService extends StorageService {
    getStore(): any {
        if(window.sessionStorage){
            return window.sessionStorage;
        }
        return super.getStore();
    }
}