import * as React from 'react';


export interface IAppointmentHistoryCmpntProps{
    historyIsOpenFlag: string;
}

export class AppointmentHistoryCmpnt extends React.Component<IAppointmentHistoryCmpntProps, any> {
    changeActiveTab(elm) {
    }

    render(){
        return (
            <div className={`col5-sm col12-md-6 col16-lg-5 patient-queue__history-tabs ${this.props.historyIsOpenFlag}`}  id="consultationsListingTabstrip">
                <ul onClick={this.changeActiveTab}>
                    <li data-index={1}>Scheduled</li>
                    <li data-index={2}>Past</li>
                    <li data-index={3}>Dropped</li>
                    <li data-index={4}>DNA</li>
                </ul>
            </div> 
        );
    }
}