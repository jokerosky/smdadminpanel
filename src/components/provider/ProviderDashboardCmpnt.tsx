import * as React from 'react';
import { Route, Link, IndexRoute } from 'react-router';
import { connect } from 'react-redux';

import { Endpoints } from '../../enums/endpoints';
import * as userActions from '../../actions/userActions';
import { SiteConstants } from '../../enums/siteConstants';


import { SubHeaderCmpnt } from '../common/SubHeaderCmpnt';
import { AppointmentHistoryCmpnt } from './AppointmentHistoryCmpnt';
import { PatientQueueHeaderCmpnt } from './PatientQueueHeaderCmpnt'
import { PatientQueueCmpnt } from './PatientQueueCmpnt';
import { PatientQueueDetailsCmpnt, PatientQueueDetailsDefaultCmpnt } from './PatientQueueDetailsCmpnt';

export interface IProviderDashboardCmpntProps {
    shceduledConsultations?: any;
    onDemandConsultations?: any;
}

export class ProviderDashboardCmpnt extends React.Component<IProviderDashboardCmpntProps, any> {
    constructor() {
        super();
        this.state = { 
            historyIsOpenFlag: '',
            isOnDemandActiveCssFlag: '',

            isOnDemandTabActiveCssFlag: SiteConstants.css.isActive,
            isScheduledTabActiveCssFlag: SiteConstants.css.isActive
         };
        this.subHeaderMenuItems = ProviderDashboardCmpnt.getSubHeaderMenuItems();

        this.toggleHistoryTab = this.toggleHistoryTab.bind(this);
        this.togglePanel = this.togglePanel.bind(this);
        this.toggleTabsMobile = this.toggleTabsMobile.bind(this);
    }

    toggleHistoryTab() {
        let historyIsOpenFlag = this.state.historyIsOpenFlag ? '' : SiteConstants.css.isActive;
        this.setState({ historyIsOpenFlag });
    }

    togglePanel(elm?){
        if(elm.target){
            let state = elm.target.attributes['data-state'].value;

            switch(state){
                case 'scheduled':{
                    let flag = this.state.isScheduledTabActiveCssFlag ? '' : SiteConstants.css.isActive;
                    this.setState({ isScheduledTabActiveCssFlag: flag });
                    break;
                }
                case 'onDemand':{
                    let flag = this.state.isOnDemandTabActiveCssFlag ? '' : SiteConstants.css.isActive;
                    this.setState({ isOnDemandTabActiveCssFlag: flag });
                    break;
                }
            }

            
        }
    }

    toggleTabsMobile(){
        let isOnDemandActiveCssFlag = this.state.isOnDemandActiveCssFlag ?  '': SiteConstants.css.isActive;
        this.setState({ isOnDemandActiveCssFlag });
    }

    historyIsOpenFlag: string = '';

    static getSubHeaderMenuItems() {
        return [];
    }

    subHeaderMenuItems: any;

    render() {
        return (
            <div className="PROVIDER Dashboard">
                <SubHeaderCmpnt
                    subHeaderTitle='Dashboard'
                    subHeaderModuleTitle='Patient Queue'
                    menuToggleItems={this.subHeaderMenuItems}

                />
                
                <div className={`patient-queue ${this.state.historyIsOpenFlag}`} >
                    <AppointmentHistoryCmpnt historyIsOpenFlag={this.historyIsOpenFlag} />

                    <div className="col5-sm col12-md col16-lg patient-queue__right-col">
                        <div className={`col5-sm col12-md-6 col16-lg-12 patient-queue__mainblock ${this.state.isOnDemandActiveCssFlag}`}>
                            <PatientQueueHeaderCmpnt 
                                toggleHistory = {this.toggleHistoryTab} 
                                toggleTabsMobile = {this.toggleTabsMobile}
                                isScheduledActiveCssFlag = { this.state.isScheduledTabOpen ? SiteConstants.css.isActive : '' }
                                onDemandActiveCssFlag = { this.state.isScheduledTabOpen ? '' : SiteConstants.css.isActive }

                                onDemandCount = {this.props.onDemandConsultations.length}
                                scheduledCount = {this.props.shceduledConsultations.length}
                                />
                                
                            <PatientQueueCmpnt 
                                isOnDemandConsultationsAvailableInHospital = {true} 
                                isPhysicianSlotAvailable = {true}
                                onDemandWaitingList = {this.props.onDemandConsultations}
                                scheduledWaitingList = {this.props.shceduledConsultations}

                                togglePanel = {this.togglePanel} 
                                isOnDemandTabActiveCssFlag = { this.state.isOnDemandTabActiveCssFlag }
                                isScheduledTabActiveCssFlag = { this.state.isScheduledTabActiveCssFlag }
                                />
                        </div>

                        <PatientQueueDetailsDefaultCmpnt />

                        <PatientQueueDetailsCmpnt />

                    </div>
                </div>

            </div>
        );
    }
}

export function mapStateToProps(state, ownProps) {
    return {
        waitingList: state.consultations.waitingList,
        shceduledConsultations: state.consultations.waitingList.filter((val)=>{return val.isScheduled}), 
        onDemandConsultations: state.consultations.waitingList.filter((val)=>{return !val.isScheduled})
    }
}

function mapDispatchToProps(dispatch) {
    return {
    };
}


export default connect(mapStateToProps, mapDispatchToProps)(ProviderDashboardCmpnt);

