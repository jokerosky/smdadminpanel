import * as  React from 'react';
import { connect } from 'react-redux';
import { push } from 'react-router-redux';

import { Endpoints } from '../../enums/endpoints';
import * as  providerActions from '../../actions/providerActions';
import * as userActions from '../../actions/userActions';
import * as siteActions from '../../actions/siteActions';
import * as patientQueueActions from '../../actions/patientQueueActions';
import ProviderService from '../../services/api/providerService';

import { Hospital } from '../../models/hospital';
import { UserStaffProfile } from '../../models/userStaffProfile';

import { MainNavCmpnt } from '../common/MainNavCmpnt';

import { SiteHeaderCmpnt } from '../common/SiteHeaderCmpnt';
// do not want to make the file too big so carry out menu generation to another file
import { getProfileMenuItems, getTopMenuItems, getMainMenuItems} from './providerAppMenuItems';

export interface IProviderAppProps {
    dependencies: any;

    hospital: Hospital;
    profile: UserStaffProfile;
    
    waitingListCount: number;

    loadProviderProfile: () => void;
    refreshPatientQueue: () => void;

    //* menu actions */
    logout: () => void;
    testConnectivity: () => void;
    goToHelpCenter: () => void;
    goToEPrescribing: () => void;
    goToFiles: () => void;
    goToEncounterHistory: () => void;
    documentEncounter: () => void;
    createNewAppointment: () => void;
    goToSchedulder: () => void;
    goToPatientRecords: () => void;
    goToMyAccount: () => void;
    goToPatientQueue: () => void;
}


export class ProviderApp extends React.Component<IProviderAppProps, any> {
    constructor() {
        super();

        this.toggleMainMenu = this.toggleMainMenu.bind(this);
    }

    static configureMenuActions(dispatch){
        let props = {
            goToMyAccount:()=>{ dispatch(push(Endpoints.site.accountPostfix));},
            goToSchedulder: ()=>{},
            goToEncounterHistory: ()=>{},
            goToEPrescribing: ()=>{},
            goToFiles: ()=>{},
            goToHelpCenter: ()=>{},
            goToPatientQueue: ()=>{},
            goToPatientRecords: ()=>{},
            documentEncounter: ()=>{},
            testConnectivity: ()=>{},
            createNewAppointment: ()=>{}
        } as IProviderAppProps;

        return props;
    }

    componentWillMount() {
        this.props.refreshPatientQueue();

        if(this.props.profile.profileId)
        {
            return ;
        }
        this.props.loadProviderProfile();
        
    }

    componentDidMount() {
        document.body.className = document.body.className + " cc-admin" // this is not feel right
    }

    toggleMainMenu() {
        siteActions.toggleMainMenu();
    }

    profileMenuItems: any;
    topMenuItems: any;
    mainMenuItems: any;

    render() {
        //Dependency Injection imitation, add dependencies to props
        let services = this.props.dependencies;
        let children = React.Children.map(this.props.children, (child: any) => {
            return React.cloneElement(child, { dependencies: services });
        })
        
        if (!this.profileMenuItems) {
            this.profileMenuItems = getProfileMenuItems(this.props);
            this.mainMenuItems = getMainMenuItems(this.props);
        }
        // because patient count in queue could be changed
        this.topMenuItems = getTopMenuItems(this.props, this.props.waitingListCount);

        return (
            <div className="PROVIDER_PAGE">
                <MainNavCmpnt
                    menuItems = {this.mainMenuItems} 
                    goToAccount = {this.props.goToMyAccount}
                    userProfile = {this.props.profile}
                />
                <div className='wrapper page'>
                    <SiteHeaderCmpnt
                        hospitalName={this.props.hospital.brandName}
                        profilePicUrl={this.props.profile.profileImagePath}
                        menuItems={this.topMenuItems}
                        profileMenuItems={this.profileMenuItems}
                        toggleMainMenu= { this.toggleMainMenu }
                    />
                    {children}
                </div>
            </div>
        );
    }
}

export function mapStateToProps(state, ownProps) {
    let profile = state.provider.profile.profileId ? state.provider.profile as UserStaffProfile : new UserStaffProfile();
    
    return {
        hospital: state.hospital as Hospital,
        profile: profile,
        waitingListCount: state.consultations.waitingList.length
    }
}

function mapDispatchToProps(dispatch) {
    let props = {
        loadProviderProfile: () => dispatch(providerActions.loadStaffProfile()),
        refreshPatientQueue: () => dispatch(patientQueueActions.refreshQueue()),
        logout: () => dispatch(userActions.logOut()),
    };
    
    return Object.assign(props, ProviderApp.configureMenuActions(dispatch));
}

export default connect(mapStateToProps, mapDispatchToProps)(ProviderApp);

