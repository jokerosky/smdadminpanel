import { MenuItemCmpnt } from '../common/menuItemCmpnt';
import { MenuItem, TopMenuItem } from '../../models/site/menuItems';

import { IProviderAppProps } from './ProviderApp';

export function getProfileMenuItems(options: IProviderAppProps) {
        let result = [];
        result.push(
            new MenuItemCmpnt({
                key: '1um',
                text: 'My Account',
                icoClass: 'icon_user_clinician',
                title: 'My Account',
                action: options.goToMyAccount
            } as TopMenuItem).render(),

            // a vaery f**n special styles ... has to have particular number of elements in particular order
            new MenuItemCmpnt({
                key: '1.5um',
                text: 'Provider Chat',
                icoClass: 'icon_chat',
                title: 'Provider Chat',
                additionalElements: '',
                flagText: '0'
            } as TopMenuItem).render(),

            new MenuItemCmpnt({
                key: '2um',
                text: 'Patient Queue',
                icoClass: 'icon_dashboard',
                title: 'Patient Queue'
            } as TopMenuItem).render(),

            new MenuItemCmpnt({
                key: '3um',
                text: 'Scheduler',
                icoClass: 'icon_calendar',
                title: 'Scheduler'
            } as TopMenuItem).render(),

            // same thing
            new MenuItemCmpnt({
                key: '3.5um',
                text: 'Notifications',
                icoClass: 'icon_bell',
                title: 'Notifications'
            } as TopMenuItem).render(),

            new MenuItemCmpnt({
                key: '4um',
                text: 'Logout',
                icoClass: 'icon_log-out',
                title: 'Logout',
                action: options.logout
            } as TopMenuItem).render()
        )

        return result;
    }

    export function getTopMenuItems(options: IProviderAppProps, patientQueueCount: number) {
        let result = [];
        result.push(
            new MenuItemCmpnt({
                key: '1m',
                text: 'Provider Chat',
                icoClass: 'icon_chat',
                title: 'Provider Chat'
            } as TopMenuItem).render(),

            new MenuItemCmpnt({
                key: '2m',
                text: 'Patient Queue',
                icoClass: 'icon_dashboard',
                title: 'Patient Queue',
                flagText: patientQueueCount.toString(),
                isFlagVisible: !!patientQueueCount
            } as TopMenuItem).render(),

            new MenuItemCmpnt({
                key: '3m',
                text: 'Scheduler',
                icoClass: 'icon_calendar',
                title: 'Scheduler'
            } as TopMenuItem).render(),

            new MenuItemCmpnt({
                key: '4m',
                text: 'Notifications',
                icoClass: 'icon_bell',
                isFlagVisible: false,
                flagText: '0',
                inVisible: true,
                title: 'Notifications'
            } as TopMenuItem).render()
        )

        return result;
    };

     export function getMainMenuItems(options: IProviderAppProps) {
        let result = [];
        result.push(
            new MenuItemCmpnt({
                key: '1mm',
                text: 'Patient Queue',
                icoClass: 'icon_dashboard',
                title: 'Patient Queue',
                } as TopMenuItem).render(),

             new MenuItemCmpnt({
                key: '2mm',
                text: 'My Account',
                icoClass: 'icon_user',
                title: 'My Account',
                action: options.goToMyAccount
            } as TopMenuItem).render(),

             new MenuItemCmpnt({
                key: '3mm',
                text: 'Patient Records',
                icoClass: 'icon_chat',
                title: 'Patient Records'
            } as TopMenuItem).render(),

            new MenuItemCmpnt({
                key: '4mm',
                text: 'Scheduler',
                title: 'Scheduler',
                icoClass: 'icon_calendar'
            } as TopMenuItem).render(),

            new MenuItemCmpnt({
                key: '5mm',
                text: 'New Appointment',
                title: 'New Appointment',
                icoClass: 'icon_new_calendar_item'
            } as TopMenuItem).render(),

            new MenuItemCmpnt({
                key: '6mm',
                text: 'Document Encounter',
                title: 'Document Encounter',
                icoClass: 'icon_text'
            } as TopMenuItem).render(),

            new MenuItemCmpnt({
                key: '7mm',
                text: 'Open Consultation',
                title: 'Open Consultation',
                icoClass: 'icon_video-camera'
            } as TopMenuItem).render(),

            new MenuItemCmpnt({
                key: '8mm',
                text: 'Encounter History',
                title: 'Encounter History',
                icoClass: 'icon_history'
            } as TopMenuItem).render(),

            new MenuItemCmpnt({
                key: '9mm',
                text: 'Files',
                title: 'Files',
                icoClass: 'icon_archive'
            } as TopMenuItem).render(),

            new MenuItemCmpnt({
                key: '10mm',
                text: 'E-Prescribing',
                title: 'E-Prescribing',
                icoClass: 'icon_rx'
            } as TopMenuItem).render(),

            new MenuItemCmpnt({
                key: '11mm',
                text: 'Help Center',
                title: 'Help Center',
                icoClass: 'icon_help-with-circle'
            } as TopMenuItem).render(),

            new MenuItemCmpnt({
                key: '12mm',
                text: 'Test Connectivit',
                title: 'Test Connectivit',
                icoClass: 'icon_gauge'
            } as TopMenuItem).render(),

            new MenuItemCmpnt({
                key: '13mm',
                text: 'Log Out',
                title: 'Log Out',
                icoClass: 'icon_log-out',
                action: options.logout
            } as TopMenuItem).render()         
        )

        return result;
    };