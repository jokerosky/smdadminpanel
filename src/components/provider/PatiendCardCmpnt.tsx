 import * as React from 'react';

 export interface IPatientCardCmpntProps{
    isLoadingCssFlag: string; //'is-active'
    
 }

 export class PatientCardCmpnt extends React.Component<any, any>{

     render(){
        return (
            <div className="drawer-card drawer-card--external drawer-card--arrow-right">
        {/*#if(data.vm_isNoAction) {# drawer-card--no-action #}#  
        #if(data.vm_isDisable){# drawer-card--opacity #}#
        #if(data.vm_isGray()){# drawer-card--gray #}#
        #if(data.vm_isBlue()){# drawer-card--blue #}#
        #if(data.vm_isGreen()){# drawer-card--green #}#
        #if(data.vm_isYellow()){# drawer-card--yellow #}#
        #if(data.vm_isRed()){# drawer-card--red #}#
        #if(!data.isScheduled){# drawer-card--on-demand #}#
        " 
        data-bind=""
            {/*css: {
                drawer-card--failed-time: vm_isFailedTime, 
                drawer-card--on-demand: vm_isOnDemand,  
                drawer-card--opacity: vm_isDisable,
                drawer-card--no-action: vm_isNoAction,
                drawer-card--locked: vm_isLocked,
                is-active: vm_isActive,
                is-unread: vm_isUnread,
                is-active-flags: vm_isActiveFlags,
                drawer-card--gray: vm_isGray,
                drawer-card--blue: vm_isBlue,
                drawer-card--green: vm_isGreen,
                drawer-card--yellow: vm_isYellow,
                drawer-card--red: vm_isRed,
                drawer-card--disconnected: vm_isDisconnected,
            }" >*/} 
        <div className={`loader loader--grey loader--big ${this.props.isLoadingCssFlag || ''}`} style={{zIndex: 100}} ></div>

        <div className="drawer-card__overlay" data-bind="fade: vm_showOptions">
            <button className="drawer-card__overlay-close icon_cross" data-bind="events: {click: vm_onHideOptionsClick}"></button>
            <div className="drawer-card__overlay-buttons">
                <button className="drawer-card__overlay-button drawer-card__overlay-button--download" data-bind="events: {click: vm_onSaveAndCloseClick}">
                    <span className="icon_download"></span>
                    <span className="drawer-card__overlay-button-tooltip">Save and close</span>
                </button>
                <button className="drawer-card__overlay-button drawer-card__overlay-button--forvard" data-bind="events: {click: vm_onForwardClick}">
                    <span className="icon_forward"></span>
                    <span className="drawer-card__overlay-button-tooltip">Send notification</span>
                </button>
                <button className="drawer-card__overlay-button drawer-card__overlay-button--close" data-bind="events: {click: vm_onDismissClick}">
                    <span className="icon_circle-with-cross"></span>
                    <span className="drawer-card__overlay-button-tooltip">Dismiss</span>
                </button>
            </div>
        </div>
        <div className="drawer-card__flags">
            <button className="drawer-card__flags-item drawer-card__flags-item--grey" data-flag="gray" data-bind="events: {click: vm_onFlagSelect}">
                <span className="icon_clipboard"></span>
            </button>
            <button className="drawer-card__flags-item drawer-card__flags-item--blue" data-flag="blue" data-bind="events: {click: vm_onFlagSelect}">
                <span className="icon_flag"></span>
            </button>
            <button className="drawer-card__flags-item drawer-card__flags-item--red" data-flag="red" data-bind="events: {click: vm_onFlagSelect}">
                <span className="icon_flag"></span>
            </button>
            <button className="drawer-card__flags-item drawer-card__flags-item--yellow" data-flag="yellow" data-bind="events: {click: vm_onFlagSelect}">
                <span className="icon_flag"></span>
            </button>
            <button className="drawer-card__flags-item drawer-card__flags-item--green" data-flag="green" data-bind="events: {click: vm_onFlagSelect}">
                <span className="icon_flag"></span>
            </button>

        </div>
        <div className="drawer-card__container">

            <div className="drawer-card__main" 
                data-bind="events: { click: vm_onPatientCardClick }">
                <div className="drawer-card__arrow"></div>
                <div className="drawer-card__card js-toggle-drawer-content">
                    <div className="drawer-card__userpic">
                        <span className="communication-method" data-bind="css: {communication-method--video: isVideoType, communication-method--visit: isVisitType, communication-method--chat: isChatType, communication-method--phone: isAudioType}"></span>

                        <div className="userpic userpic--drawer-card"
                            data-bind="events: {click: vm_onViewProfileClick}">
                            <img data-bind="attr: {src: profileImagePath}" data-onerror={"this.onerror=null;this.src= '#: data.defautProfileImage #'"} />
                        </div>

                        <div className="drawer-card__chevron"></div>
                    </div>

                    <button type="button" className="drawer-card__sub-action js-toggle-unread"
                        data-bind= "# if (!vm_isChatDisabled) { # events: { click: vm_showPatientChat } # } #" >
                        <div className="drawer-card__chat is-disabled" data-bind="css: {is-disabled: vm_isChatDisabled}">
                            <span data-bind="visible:vm_isUnread, text: meetingConversationCount"></span>
                            <img src="../../images/message-alert2.svg" alt=""/>
                            <div className="icon_message"></div>
                        </div>
                    </button>

                    <button type="button" className="drawer-card__show-overlay" data-bind="events: {click: vm_onShowOptionsClick}">
                        <span></span>
                    </button>

                    <div className="drawer-card__info">
                        <div className="drawer-card__name" data-bind="text: patientName">
                        </div>

                        <div className="drawer-card__secondary-info">
                            <span className="drawer-card__time" data-bind="text: consultationTime"></span>
                        </div>

                        <div className="drawer-card__tertiary-info">
                            <span data-bind="text: gender"></span> • <span data-bind="text: ageInfo"></span> • <span data-bind="text: state"></span>, <span data-bind="text: country"></span>
                        </div>

                        <div className="drawer-card__timer" data-bind="text: waitingTime"></div>
                    </div>
                </div>

                <div className="drawer-card__content js-drawer-content">
                    Drawer content opened
                </div>

                <footer className="drawer-card__footer">
                    <div className="drawer-card__footer-toggler js-toggle-drawer-footer" data-bind="text: vm_cardFoterHeaderText"></div>

                    <div className="drawer-card__footer-title js-toggle-drawer-footer">
                        <div className="drawer-card__footer-icon drawer-card__footer-icon--arrow" data-bind="visible: vm_isNotificationSent">
                            <div className="drawer-card__tooltip" data-bind="html: vm_notificationInfo">
                            </div>
                            <span className="icon_forward"></span>
                        </div>

                        <div className="drawer-card__footer-icon" data-bind="events: {click: vm_onFlagButtonClick}">
                            <span className="icon_clipboard" data-bind="css: {icon_clipboard: vm_isGray, icon_flag: vm_isNotGray}"></span>
                        </div>

                        <span data-bind="html: vm_cardFoterContentText, css:{drawer-card__footer-title-loading:vm_footerTitleLoading}"></span>
                    </div>

                    <div className="drawer-card__footer-content js-drawer-footer">
                        Opened concern description
                    </div>
                </footer>

            </div>
            <button type="button" className="drawer-card__action" 
                data-bind="events: {click: vm_onStartConsultationClick}">
                <span className="communication-method communication-method--action" data-bind="css: {communication-method--video: isVideoType, communication-method--visit: isVisitType, communication-method--chat: isChatType, communication-method--phone: isAudioType}"></span>
                <div>Start</div>
                <span className="icon_lock"></span>
            </button>
        </div>
    </div>
        )
     }
 }
 
 