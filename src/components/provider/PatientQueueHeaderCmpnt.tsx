import * as React from 'react';

export interface IPatientQueueHeaderCmpntProps{
    toggleHistory: ()=>void;
    toggleTabsMobile: ()=>void;

    isScheduledActiveCssFlag: string;
    onDemandActiveCssFlag: string;

    scheduledCount: number;
    onDemandCount: number;
}

export class PatientQueueHeaderCmpnt extends React.Component<IPatientQueueHeaderCmpntProps, any>{
    render()
    {
        return (
            <div className="patient-queue__header">
                 <a href="#" className="hide-mobile toggle-history"
                 onClick={this.props.toggleHistory}
                    data-bind="events: { click: vm_onHistoryButtonClick }, css: { is-active: vm_showLeftColumn }">
                    <span className="icon_history"  title="Toggle Consultations Histories" data-position="right" data-role="tooltip"></span>
                </a>

                <span className="hide-mobile title">Patient Waiting:</span>

                <ul data-bind="css:{is-ondemand:vm_isOnDemandActive}, click:vm_onMobileTabclick"
                    onClick = {this.props.toggleTabsMobile} >
                    <li 
                        className= { this.props.isScheduledActiveCssFlag || '' }
                        data-tabs="scheduled"
                        data-bind="events: { click: vm_onScheduledWaitingTabClick }, css: {is-active: vm_isScheduledTabActive}">
                        Scheduled
                        <span className="amount" data-bind="text: vm_ScheduledWaitingCount">{this.props.scheduledCount}</span></li>
                    <li 
                        className= { this.props.onDemandActiveCssFlag || '' }
                        data-tabs="on-demand" 
                        data-bind="events: { click: vm_onOnDemandWaitingTabClick }, css: {is-active: vm_isOnDemandTabActive}, visible: vm_isOnDemandConsultationsAvailableInHospital">
                            On-Demand
                        <span className="amount" data-bind="text: vm_OnDemandWaitingCount">{this.props.onDemandCount}</span>
                    </li>
                </ul>
                
                <div className="hide-mobile patient-queue__timerblock">
                    <span className="patient-queue__timertitle">Average Wait</span>
                    <span className="patient-queue__timer" data-bind="text: vm_patientAvgWaitTime">00:00:00</span>
                </div>
            </div>
        );
    }
}