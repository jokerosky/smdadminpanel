import * as React from 'react';

export interface IPatientQueueDetailsCmpntProps {

}

export class PatientQueueDetailsDefaultCmpnt extends React.Component<any, any>{

    render() {
        return (
            <div className="col5-sm col12-md-6 col16-lg-4 patient-queue__pdetails patient-queue__pdetails--default">
                <span className="icon_clipboard"></span>
                <h2>Select a Patient<br />to review<br />their details</h2>

                <p>OR</p>

                <button className="button button__transparent" data-bind="events: { click: vm_onHistoryButtonClick }"><span><span className="icon_list"></span> Review Previous Consultations</span></button>
            </div>
        );
    }
}

export class PatientQueueDetailsCmpnt extends React.Component<IPatientQueueDetailsCmpntProps, any>{

    render() {
        return (
            <div className="col5-sm col12-md-6 col16-lg-4 patient-queue__pdetails patient-queue__pdetails--profile">
                <div className="communication-method-header communication-method-header--video" data-bind="css: {communication-method-header--video: patientViewModel.isVideoType, communication-method-header--visit: patientViewModel.isVisitType, communication-method-header--chat: patientViewModel.isChatType, communication-method-header--phone: patientViewModel.isAudioType}">
                    <a href="#" className="communication-method-header__close js-right-col-close" data-bind="events: { click: vm_onProfileDetailsClose}">
                        close <span className="icon_circle-with-cross"></span>
                    </a>
                    <div className="communication-method-header__title">
                        <span className="communication-method-header__icon"></span>
                    </div>
                </div>
                <div className="patient-queue__userinfo">
                    <div className="userpic userpic--patient-queue userpic--white-border" data-bind="click: vm_redirectToPatientAccount">
                        <img data-bind="attr: {src: patientViewModel.profileImage, alt: patientViewModel.profileFirstName, title: patientViewModel.profileFirstName }" />
                    </div>
                    <div className="patient-queue__name">
                        <span className="patient-queue__firstname" data-bind="text: patientViewModel.profileFirstName"></span>
                        <span className="patient-queue__lastname" data-bind="text: patientViewModel.profileLastName"></span>
                    </div>
                    <div className="patient-queue__secondary-info">
                        <span className="patient-queue__gender" data-bind="text: patientViewModel.patientInformation.genderFullName">
                        </span>
                        <span className="patient-queue__age" data-bind="text: patientViewModel.patientInformation.age"></span>
                        • <span data-bind="text: patientViewModel.patientInformation.addressState"></span>, <span data-bind="text: patientViewModel.patientInformation.addressCountry"></span>
                    </div>

                    <div className="patient-queue__release-timer" data-bind="visible: vm_isTimerVisible">
                        Will be released in <span data-bind="text: vm_releasedInTime"> </span>
                    </div>
                </div>
            </div>
        );
    }
}