import * as React from 'react';
import { SiteConstants } from '../../enums/siteConstants';
import { PatientCardCmpnt } from './PatiendCardCmpnt';


export interface IPatientQueueCmpntProps {
    isOnDemandConsultationsAvailableInHospital: boolean
    isPhysicianSlotAvailable: boolean;

    isScheduledTabActiveCssFlag: string;
    isOnDemandTabActiveCssFlag: string;

    scheduledWaitingList? :any[];
    onDemandWaitingList? :any[]

    togglePanel: (elm?)=>void;
}

export class PatientQueueCmpnt extends React.Component<IPatientQueueCmpntProps, any> {
    constructor(){
        super()
    }

    onDemandCount: number;
    scheduledCount: number;

    onDemandTabMessage: any;
    onDemandTabMessage1 = <span>There are no On-Demand Consultations.</span>;
    onDemandTabMessage2 = <span>You are not currently scheduled to provide on-demand services.</span>;

    componentWillReceiveProps(nextProps){
        if(nextProps.scheduledWaitingList){
            this.scheduledCount = nextProps.scheduledWaitingList.length;
        } else {
            this.scheduledCount = 0;
        }


        if(nextProps.onDemandWaitingList){
            this.onDemandCount = nextProps.onDemandWaitingList.length;
        } else {
            this.onDemandCount = 0;
        }
        this.forceUpdate();
    }

    componentDidMount(){
        this.onDemandTabMessage = this.props.isPhysicianSlotAvailable ?  this.onDemandTabMessage1 : this.onDemandTabMessage2; 
        this.forceUpdate();
    }

    vm_scheduledTabMessage = <span>There are currently no scheduled patients waiting. "  
            {/*<a href = '' onClick='sessionStorage.setItem(\"snap_tabName_ref\", \"Scheduled\"); window.location.href=\" + snap.getBaseUrl() + provider/providerConsultations\"; return false;'} >*/}
            <a href = ''>
                View your upcoming appointments.
            </a>
        </span>


    render(){
        return (
            <div className="patient-queue__queue-wrap">
                <div 
                    className = {`hide-mobile patient-queue__tab ${this.props.isScheduledTabActiveCssFlag || ''}`} 
                    data-state = 'scheduled'
                    onClick = { this.props.togglePanel } >
                    <span className="patient-queue__tabtitle">Scheduled Appointments</span>
                    {this.scheduledCount && <span className="patient-queue__tabbage">{this.scheduledCount}</span>}

                    
                </div>

                {/*//todo: chang it to a toggle slide effect with css */}
                { !!this.props.isScheduledTabActiveCssFlag && <div className={`col5-sm patient-queue__queue patient-queue__queue--scheduled ${this.props.isScheduledTabActiveCssFlag || ''}`}
                    data-bind="css: {is-active: isScheduledTabActive}, slide: isScheduledTabActive">
                    <div 
                        data-bind="source: scheduledWaitingList"
                        data-template="patientCard">
                        

                        <PatientCardCmpnt />
                    </div>

                    {this.vm_scheduledTabMessage}
                </div> }

                { this.props.isOnDemandConsultationsAvailableInHospital && <div className={`hide-mobile patient-queue__tab ${this.props.isOnDemandTabActiveCssFlag || ''}`}
                    data-state = 'onDemand' 
                    onClick = { this.props.togglePanel}
                    data-bind="events: { click: vm_onOnDemandWaitingTabClick }, css: {is-active: isOnDemandTabActive}, visible: isOnDemandConsultationsAvailableInHospital">
                    <span className="patient-queue__tabtitle">On-Demand Appointments</span>
                    { this.onDemandCount && <span className="patient-queue__tabbage">{this.onDemandCount}</span>}
                </div> }

                { this.props.isOnDemandConsultationsAvailableInHospital && !!this.props.isOnDemandTabActiveCssFlag && <div 
                        className={`col5-sm patient-queue__queue patient-queue__queue--on-demand ${this.props.isOnDemandTabActiveCssFlag || ''}`} >
                    <div
                        data-bind="source: onDemandWaitingList"
                        data-template="patientCard">
                    </div>

                    {this.onDemandTabMessage}
                </div>}
            </div>
        );
    }
}