import * as React from 'react'
import { ConfigurationOfTheFilterSection } from './ConfigurationOfTheFilterSection';
import { getFormattedDate, secondsToMinutesString } from '../../../utilities/converters';
import * as Vocabulary from '../../../localization/vocabulary';

export interface ITableCreationComponent {
    titlePage: string;
    tabelHeaders: any[];
    classNameForButtonSelectAll: string;
    classNameForButtonDeselectAll: string;
    stateTable: any;
    tableBodyItemsFields: any;
    tableRecords: any;
    configurationOfTheFilterSection: any;
    language: any;
}

export class TableCreationComponent extends React.Component<ITableCreationComponent, any>{
    constructor(props) {
        super(props)

        this.state = props.stateTable;
        this.buttonShowTableColumn = this.buttonShowTableColumn.bind(this);
        this.showTableHead = this.showTableHead.bind(this);
        this.tableBodyItems = this.tableBodyItems.bind(this);
        this.tabelBody = this.tabelBody.bind(this);
        this.showTableColumns = this.showTableColumns.bind(this);
        this.renderItems = this.renderItems.bind(this);
    }

    showTableHead(title: string, column: string) {
        return this.state[column] ? <th key={column} className='smd-text-align-center'>{title}</th> : null;
    }

    showTableColumns(column: string) {
        this.setState({
            [column]: !this.state[column]
        });
    }

    buttonShowTableColumn(title: string, column: string) {
        return <th
            key={column}
            id={column}
            className={`col btn smd-cursor-pointer ${this.state[column] ? 'btn-primary' : ''}`}
            onClick={() => { this.showTableColumns(column); }}
        >{title}</th>;
    }

    tableHead(func) {
        let head: Array<any> = [];
        for (let index in this.props.tabelHeaders) {
            head.push(func(this.props.language[this.props.tabelHeaders[index].label], this.props.tabelHeaders[index].id));
        }
        return head;
    }

    inner = {
        __html: '<div></div>'
    }

    buttonSelectAndDeselectAll(isSelect: boolean) {
        let div = <div dangerouslySetInnerHTML={this.inner}></div>;

        for (let index in this.props.tabelHeaders) {
            this.setState({
                [this.props.tabelHeaders[index].id]: isSelect
            });
        }
    }

    tableBodyItems(titles: any, column: string) {
        let title: any = titles;
        for (let index in this.props.tableBodyItemsFields.field) {
            if (column === this.props.tableBodyItemsFields.field[index].title) {
                switch (this.props.tableBodyItemsFields.field[index].method) {
                    case 'formattedDate':
                        {
                               title = getFormattedDate(titles);
                            break;
                        }
                    case 'formattedTime':
                        {
                                title = secondsToMinutesString(title);
                            break;
                        }
                    case 'type':
                        {
                            title = this.props.tableBodyItemsFields.field[index].data[titles-1];
                            break;
                        }
                }
            }
        }

        return this.state[column] ? <td key={column} className='smd-text-align-center smd-text-overflow'>{title}</td> : null;
    }

    tabelBody(func, tableData: any) {
        let body: Array<any> = [];
        for (let index in this.props.tabelHeaders) {
            body.push(func(tableData[this.props.tabelHeaders[index].id], this.props.tabelHeaders[index].id));
        }
        return body;
    }

    renderItems(consultationMeetingsRecords) {
        let items: Array<any> = [];
        if (consultationMeetingsRecords) {
            for (let index in consultationMeetingsRecords) {
                let record = consultationMeetingsRecords[index];
                let li =
                    <tr key={index} className=''>
                        {this.tabelBody(this.tableBodyItems, record)}
                    </tr>
                items.push(li);
            }
        }
        return items;
    }

    render() {
        let items = this.renderItems(this.props.tableRecords);
        return (
            <div className="container-fluid">
                <dl className='row smd-v-padding'>
                    <dd className='col'>
                        <h5>{this.props.titlePage}</h5>
                    </dd>
                </dl>
                <ConfigurationOfTheFilterSection
                    stateFilterSection={this.props.configurationOfTheFilterSection.stateFilterSection}
                    mapObjects={this.props.configurationOfTheFilterSection.mapObjects}
                    transformationOfFields={this.props.tableBodyItemsFields}
                    records={this.props.tableRecords}
                    callData={this.state}
                    tableHeaders={this.props.tabelHeaders}
                    callBack={this.props.configurationOfTheFilterSection.callBack}
                />
                <br />
                <div className='row'>
                    <button
                        className={this.props.classNameForButtonSelectAll + ' smd-cursor-pointer'}
                        onClick={() => {
                            this.buttonSelectAndDeselectAll(true);
                        }}
                    >
                        {this.props.language[Vocabulary.BUTTON_SELECT_ALL]}
                    </button>
                    <button
                        className={this.props.classNameForButtonDeselectAll + ' smd-cursor-pointer'}
                        onClick={() => {
                            this.buttonSelectAndDeselectAll(false);
                        }}
                    >
                        {this.props.language[Vocabulary.BUTTON_DESELECT_ALL]}
                    </button>
                </div>
                <div className=''>
                    <table
                        id={'tableButtons'}
                        className='table table-hover'>
                        <thead>
                            <tr className='row'>
                                {this.tableHead(this.buttonShowTableColumn)}
                            </tr>
                        </thead>
                    </table>
                    <div className='smd-overflow-scroll'>
                        <table
                            id={'tableRecords'}
                            className='table table-hover'>
                            <thead>
                                <tr>
                                    {this.tableHead(this.showTableHead)}
                                </tr>
                            </thead>
                            <tbody>
                                {items || ''}
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        )
    }
}