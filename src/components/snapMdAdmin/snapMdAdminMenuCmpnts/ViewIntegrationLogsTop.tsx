import * as React from 'react';
import * as moment from 'moment';
import Select from 'react-select';

import * as Vocabulary from '../../../localization/vocabulary';
import { countRecord } from './ViewLoggedMessagesTop';
import * as Datetime from 'react-datetime';
import { ObjectHelper } from '../../../utilities/objectHelper';

import 'moment/min/locales.min';

export interface IViewIntegrationLogsTop {
    language: any;

    getIntegrationLogs: (fieldQuery: any[]) => void;
}

export class ViewIntegrationLogsTopCmpnt extends React.Component<IViewIntegrationLogsTop, any>{
    constructor(props) {
        super(props);

        this.state = {
            fromDate: moment(),
            messageTypesInternal: true,
            messageTypeAdmin: true,
            messageTypePublic: true,
            searchPhrase: '',
            sortAsceding: true,
            maxCount: null,
            maxCountSelected: 0
        };
    }

    messageTypes = [
        {
            key: 'messageTypesInternal',
            value: 'Internal'
        },
        {
            key: 'messageTypeAdmin',
            value: 'Admin'
        },
        {
            key: 'messageTypePublic',
            value: 'Public'
        }];

    changeRadioButton(isFlag: boolean) {
        this.setState({
            sortAsceding: isFlag
        });
    }

    getIntegrationLogs() {
        let fieldQuery: Array<any> = [];
        for (let index in this.messageTypes) {
            if (this.state[this.messageTypes[index].key]) {
                fieldQuery.push({
                    key: 'messageTypes[]',
                    value: this.messageTypes[index].value
                });
            }
        }
        fieldQuery.push(
            {
                key: 'searchPhrase',
                value: this.state.searchPhrase
            },
            {
                key: 'sortAscending',
                value: this.state.sortAsceding
            },
            {
                key: 'maxCount',
                value: this.state.maxCount
            },
            {
                key: 'fromDate',
                value: this.state.fromDate.toString()
            }
        );

        this.props.getIntegrationLogs(fieldQuery);
    }

    render() {
        return (
            <div>
                <div className='row'>
                    <div className="form-group col-md-5 col-sm-8 smd-vertical-align-bottom">
                        <label htmlFor="client">{this.props.language[Vocabulary.SEARCH_PHRASE]}</label>
                        <input
                            className='form-control'
                            name='SearchPhrase'
                            value={this.state.searchPhrase}
                            placeholder={this.props.language[Vocabulary.SEARCH]}
                            onChange={(e) => {
                                this.setState({
                                    searchPhrase: e.currentTarget.value
                                });
                            }}
                        />
                    </div>
                    <div className="form-group col-md-3 col-sm-5">
                        <label htmlFor="client">{this.props.language[Vocabulary.HEAD_SELECT_MAX_RECORDS_COUNT]}</label>
                        <Select
                            name={'SelectClient'}
                            options={countRecord}
                            value={countRecord[this.state.maxCountSelected]}
                            clearable={false}
                            onChange={(e) => {
                                this.setState({
                                    maxCountSelected: e.value,
                                    maxCount: e.label
                                });
                            }}
                        />
                    </div>
                    <div className="form-group col-md-3 col-sm-5 smd-vertical-align-bottom">
                        <label htmlFor="client">{this.props.language[Vocabulary.HEAD_SELECT_FROM_DATE]}</label>
                        <Datetime
                            dateFormat={'DD MMMM YYYY'}
                            timeFormat={false}
                            locale={ObjectHelper.getLocaleForMomentDateTime(this.props.language[Vocabulary.LANGUAGE] || 'us')}
                            inputProps={{
                                placeholder: "DD MMMM YYYY",
                            }}
                            value={this.state.fromDate}
                            onChange={(e) => {
                                this.setState({
                                    fromDate: e
                                });
                            }}
                        />
                    </div>
                </div>
                <div className='row'>
                    <div className='col-lg-3 col-md-3 col-sm-4 col-6'>
                        <h5><label className='col'>{this.props.language[Vocabulary.DATE_SORT]}</label></h5>
                        <div>
                            <label>
                                <input
                                    type="radio"
                                    name='messageTypes'
                                    value='asc'
                                    checked={this.state.sortAsceding}
                                    onChange={() => {
                                        this.changeRadioButton(true);
                                    }}
                                /> {this.props.language[Vocabulary.DATE_SORT_ASC]}
                            </label>
                        </div>
                        <div>
                            <label>
                                <input
                                    type="radio"
                                    name='messageTypes'
                                    value='desc'
                                    checked={!this.state.sortAsceding}
                                    onChange={() => {
                                        this.changeRadioButton(false);
                                    }}
                                /> {this.props.language[Vocabulary.DATE_SORT_DESC]}
                            </label>
                        </div>
                    </div>
                    <div className='col-lg-3 col-md-4 col-sm-5 col-6'>
                        <h5><label className='col'>{this.props.language[Vocabulary.MESSAGE_TYPES]}</label></h5>
                        <div>
                            <label>
                                <input
                                    type="checkbox"
                                    name='messageTypesInternal'
                                    checked={this.state.messageTypesInternal}
                                    onChange={() => {
                                        this.setState({
                                            messageTypesInternal: !this.state.messageTypesInternal
                                        });
                                    }}
                                /> {this.props.language[Vocabulary.MESSAGE_TYPES_INTERNAL]}
                            </label>
                        </div>
                        <div>
                            <label>
                                <input
                                    type="checkbox"
                                    name='messageTypeAdmin'
                                    checked={this.state.messageTypeAdmin}
                                    onChange={() => {
                                        this.setState({
                                            messageTypeAdmin: !this.state.messageTypeAdmin
                                        });
                                    }}
                                /> {this.props.language[Vocabulary.MESSAGE_TYPES_ADMIN]}
                            </label>
                        </div>
                        <div>
                            <label>
                                <input
                                    type="checkbox"
                                    name='messageTypePublic'
                                    checked={this.state.messageTypePublic}
                                    onChange={() => {
                                        this.setState({
                                            messageTypePublic: !this.state.messageTypePublic
                                        });
                                    }}
                                /> {this.props.language[Vocabulary.MESSAGE_TYPES_PUBLIC]}
                            </label>
                        </div>
                    </div>
                    <div className='col-sm-3 row d-sm-block'>
                        <button
                            className='btn btn-lg smd-10-margin'
                            name='query'
                            onClick={() => {
                                this.getIntegrationLogs();
                            }}
                        >
                            {this.props.language[Vocabulary.BUTTON_QUERY]}
                        </button>
                    </div>
                </div>
            </div>
        )
    }
}