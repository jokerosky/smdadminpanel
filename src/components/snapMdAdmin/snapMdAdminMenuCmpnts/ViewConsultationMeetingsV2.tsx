import * as React from 'react';
import { connect } from 'react-redux';
import * as moment from 'moment';
import * as _ from 'lodash';

import { TableCreationComponent } from './TableCreationComponent';
import { ConsultationMeetingsTableHeaders } from '../../../enums/tableHeaders';
import { SnapMDServiceLocator } from '../../../dependenciesInjectionConfiguration';
import * as consultationMeetingsActions from '../../../actions/snapMdAdminMenuActions/viewConsultationMeetingsActions';
import { EditorType } from '../../../enums/editorTypes';
import * as Vocabulary from '../../../localization/vocabulary';

export interface IViewConsultationMeetingsV2Props {
    hospitalNameForSelect: any[];
    consultationMeetingsRecords: any[];
    language: any;

    getConsultationMeetings: (requestArrayField: any[]) => void;
}

export class ViewConsultationMeetingsV2Cmpnt extends React.Component<IViewConsultationMeetingsV2Props, any>{
    constructor(props) {
        super(props);
    }

    callType: Array<string> = [
        'providerAudio',
        'providerVideo',
        'providerPatient',
        'openConsultation'
    ];

    callText: Array<string> = [
        'provide-provider audio',
        'provide-provider video',
        'provide-patient',
        'open consultation'
    ];

    countRecord: any = [
        {
            value: 0,
            label: 'Chose records count'
        },
        {
            value: 1,
            label: 100
        },
        {
            value: 2,
            label: 1000
        },
        {
            value: 3,
            label: 10000
        }
    ]

    fieldsTransformation = {
        field: [
            {
                title: 'startTime',
                method: 'formattedDate'
            },
            {
                title: 'callDuration',
                method: 'formattedTime'
            },
            {
                title: 'type',
                method: 'type',
                data: this.callText
            }
        ]
    }

    sendingObjectForRequest(state) {
        let queryProps = {
            fromDate: state.fromDate,
            toDate: state.toDate,
            recordSelectedValue: state.recordSelectedValue,
            countRecordSelected: state.countRecordSelected,
            selectedHospitalId: state.selectedHospitalId,
            providerAudio: state.providerAudio,
            providerVideo: state.providerVideo,
            providerPatient: state.providerPatient,
            openConsultation: state.openConsultation
        };

        return queryProps
    }

    stateTable() {
        let state: any = {};
        for (let index in ConsultationMeetingsTableHeaders) {
            state[ConsultationMeetingsTableHeaders[index].id] = true;
        }

        return state;
    }

    stateFilters = {
        fromDate: moment(),
        toDate: moment(),
        countRecordSelected: 0,
        recordSelectedValue: 0,
        selectedHospitalId: -1,
        providerAudio: true,
        providerVideo: true,
        providerPatient: true,
        openConsultation: true
    }

    layout() {
        let layout = [
            {
                classDiv: 'row',
                [EditorType.select]: {
                    [EditorType.select]: EditorType.select,
                    selected: [
                        {
                            name: 'SelectClient',
                            label: this.props.language[Vocabulary.HEAD_SELECT_CLIENT],
                            classDiv: 'form-group col-md-3 col-sm-6',
                            action: 'selectedHospitalId',
                            options: this.props.hospitalNameForSelect
                        },
                        {
                            name: 'SelectRecords',
                            label: this.props.language[Vocabulary.HEAD_SELECT_MAX_RECORDS_COUNT],
                            classDiv: 'form-group col-md-3 col-sm-6',
                            action: 'countRecordSelected',
                            options: this.countRecord
                        }
                    ]

                },
                [EditorType.dateTime]: {
                    [EditorType.dateTime]: EditorType.dateTime,
                    date: [
                        {
                            dateFormat: 'DD MMMM YYYY',
                            timeFormat: false,
                            placeholder: "DD MMMM YYYY",
                            label: this.props.language[Vocabulary.HEAD_SELECT_FROM_DATE],
                            classDiv: 'form-group col-md-3 col-sm-6',
                            action: 'fromDate'
                        },
                        {
                            dateFormat: 'DD MMMM YYYY',
                            timeFormat: false,
                            placeholder: "DD MMMM YYYY",
                            label: this.props.language[Vocabulary.HEAD_SELECT_TO_DATE],
                            classDiv: 'form-group col-md-3 col-sm-6',
                            action: 'toDate'
                        }
                    ]
                    
                }
            },
            {
                classDiv: 'row',
                [EditorType.check]: {
                    [EditorType.check]: EditorType.check,
                    classDiv: 'col-md-4 col-sm-6',
                    label: this.props.language[Vocabulary.CALL_TYPE],
                    checkBtn: [
                        {
                            name: 'check1',
                            label: this.props.language[Vocabulary.PROVIDER_PROVIDER_AUDIO],
                            action: this.callType[0]
                        },
                        {
                            name: 'check2',
                            label: this.props.language[Vocabulary.PROVIDER_PROVIDER_VIDEO],
                            action: this.callType[1]
                        },
                        {
                            name: 'check3',
                            label: this.props.language[Vocabulary.PROVIDER_PATIENT],
                            action: this.callType[2]
                        },
                        {
                            name: 'check4',
                            label: this.props.language[Vocabulary.OPEN_CONSULTATION],
                            action: this.callType[3]
                        }
                    ]
                },
                [EditorType.container]: {
                    [EditorType.container]: EditorType.container,
                    classDiv: 'col-sm-6 row d-sm-block',
                    [EditorType.excel]: {
                        [EditorType.excel]: EditorType.excel,
                        label: this.props.language[Vocabulary.BUTTON_TO_EXCEL],
                        classDiv: 'col-6 smd-10-margin'
                    },
                    [EditorType.button]: {
                        [EditorType.button]: EditorType.button,
                        name: 'ButtonQuery',
                        classBtn: 'btn btn-lg smd-10-margin',
                        label: this.props.language[Vocabulary.BUTTON_QUERY],
                        action: this.sendingObjectForRequest
                    }
                }
            }
        ];

        return layout;
    }

    configurationOfTheFilterSection() {
        let configurationOfTheFilterSection = {
            stateFilterSection: this.stateFilters,
            mapObjects: this.layout(),
            callBack: (state) => {this.getConsultationMeetings(state); }
        }

        return configurationOfTheFilterSection;
    }

    getConsultationMeetings(queryProps){
        let requestArray: any = [];
        let indexRowTypes: number = 1;
        let arrayIndex: number = 0;
        
        if (queryProps.selectedHospitalId > -1) {
            requestArray.push({
                key: 'hospitalId',
                value: queryProps.selectedHospitalId
            });
        };
        arrayIndex = requestArray.length;
        for (let i = requestArray.length; i < 4 + arrayIndex; i++) {
            if (queryProps[this.callType[i - arrayIndex]]) {
                requestArray.push({
                    key: 'rowTypes[]',
                    value: indexRowTypes
                });
            }
            indexRowTypes++;
        };

        requestArray.push(
            {
                key: 'fromDate',
                value: queryProps.fromDate.format('YYYY-MM-DDTHH:mm:ss.000') + 'Z'
            },
            {
                key: 'toDate',
                value: queryProps.toDate.format('YYYY-MM-DDTHH:mm:ss.000') + 'Z'
            },
            {
                key: 'maxCount',
                value: this.countRecord[queryProps.countRecordSelected].label
            },
            {
                key: 'skipCount',
                value: 0
            });

        this.props.getConsultationMeetings(requestArray);
    }

    render() {
        return (
            <div>
                <TableCreationComponent
                    titlePage={this.props.language[Vocabulary.CONSULTATION_MEETINGS_REPORT] + 'V2'}
                    tabelHeaders={ConsultationMeetingsTableHeaders}
                    classNameForButtonSelectAll='col-md-2 col-sm-3 btn btn-primary smd-10-margin'
                    classNameForButtonDeselectAll='col-md-2 col-sm-3 btn btn-primary smd-10-margin ml-sm-3'
                    stateTable={this.stateTable()}
                    tableBodyItemsFields={this.fieldsTransformation}
                    tableRecords={this.props.consultationMeetingsRecords}
                    configurationOfTheFilterSection={this.configurationOfTheFilterSection()}
                    language={this.props.language}
                />
            </div>
        )
    }


}

function getHospitalName(clients: any[]) {
    let client: Array<any> = [];
    for (let index in clients) {
        client.push({
            value: index,
            label: clients[index].hospitalName
        });
    }
    return client;
}

export function mapStateToProps(state, ownProps) {
    return {
        hospitalNameForSelect: getHospitalName(state.snapmdAdmin.lists.clients) as any[],
        consultationMeetingsRecords: state.snapmdAdmin.consultationMeetings as any[],
        language: state.site.misc.vocabulary as any
    }
}

export function mapDispatchToProps(dispatch) {
    return {
        getConsultationMeetings: (requestArrayField: any[]) => {
            dispatch(consultationMeetingsActions.queryConsultationMeetings(SnapMDServiceLocator, requestArrayField));
        }
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(ViewConsultationMeetingsV2Cmpnt);