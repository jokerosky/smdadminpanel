import * as React from 'react';
import { connect } from 'react-redux';
import * as moment from 'moment';
import * as _ from 'lodash';


import { ConsultationMeetingsTableHeaders } from '../../../enums/tableHeaders';

import * as consultationMeetingsActions from '../../../actions/snapMdAdminMenuActions/viewConsultationMeetingsActions';
import { SnapMDServiceLocator } from '../../../dependenciesInjectionConfiguration';
import * as Vocabulary from '../../../localization/vocabulary';

import { ViewConsultationMeetingsTopCmpnt, callType, callText} from './ViewConsultationMeetingsTop';
import { getFormattedDate, secondsToMinutesString } from '../../../utilities/converters';

export interface IViewConsultationMeetingsProps {
    hospitalNamesForSelect: any[];
    hospitals: any[];
    consultationMeetingsRecords: any[];
    language: any;

    getConsultationMeetings: (requestArrayField: any[]) => void;
}

export class ViewConsultationMeetingsCmpnt extends React.Component<IViewConsultationMeetingsProps, any>{
    constructor() {
        super();

        this.state = {
            hospitalId: true,
            hospitalName: true,
            calleeUserId: true,
            calleeName: true,
            patientUserId: true,
            callerUserId: true,
            callerName: true,
            type: true,
            status: true,
            wihoutCharge: true,
            startTime: true,
            callDuration: true,
            messageInReply: true,
            replyLength: true,
            chatDuration: true
        };

        this.buttonShowTableColumn = this.buttonShowTableColumn.bind(this);
        this.showTableHead = this.showTableHead.bind(this);
        this.tableBodyItems = this.tableBodyItems.bind(this);
        this.tabelBody = this.tabelBody.bind(this);
        this.showTableColumns = this.showTableColumns.bind(this);
        this.renderItems = this.renderItems.bind(this);
    }


    showTableColumns(column: string) {
        this.setState({
            [column]: !this.state[column]
        });
    }

    sendingObjectForRequest(queryProps: any) {
        let requestArray: any = [];
        let indexRowTypes: number = 1;
        let arrayIndex: number = 0;

        if (queryProps.selectedHospitalId > -1) {
            requestArray.push({
                key: 'hospitalId',
                value: queryProps.selectedHospitalId
            });
        };
        arrayIndex = requestArray.length;
        for (let i = requestArray.length; i < 4 + arrayIndex; i++) {
            if (queryProps[callType[i - arrayIndex]]) {
                requestArray.push({
                    key: 'rowTypes[]',
                    value: indexRowTypes
                });
            }
            indexRowTypes++;
        };

        requestArray.push(
            {
                key: 'fromDate',
                value: queryProps.fromDate.format('YYYY-MM-DDTHH:mm:ss.000') + 'Z'
            },
            {
                key: 'toDate',
                value: queryProps.toDate.format('YYYY-MM-DDTHH:mm:ss.000') + 'Z'
            },
            {
                key: 'maxCount',
                value: queryProps.recordSelectedValue
            },
            {
                key: 'skipCount',
                value: 0
            });
        this.props.getConsultationMeetings(requestArray);
    }

    showTableHead(title: string, column: string) {
        return this.state[column] ? <th key={column} className='smd-text-align-center th-smd-table'>{this.props.language[title]}</th> : null;
    }

    buttonShowTableColumn(title: string, column: string) {
        return <th
            key={column}
            id={column}
            className={`col btn ${this.state[column] ? 'btn-primary' : ''}`}
            onClick={() => { this.showTableColumns(column); }}
        >{this.props.language[title]}</th>;
    }

    tableHead(func) {
        let head: Array<any> = [];
        for (let index in ConsultationMeetingsTableHeaders) {
            head.push(func(ConsultationMeetingsTableHeaders[index].label, ConsultationMeetingsTableHeaders[index].id));
        }
        return head;
    }

    tableBodyItems(titles: any, column: string) {
        let title: any = titles;

        if (column === 'startTime') {
            title = getFormattedDate(titles, );
        }

        if (column === 'callDuration') {
            title = secondsToMinutesString(title);
        }

        if (column === 'type') {
            title = callText[titles - 1];
        }

        return this.state[column] ? <td key={column} className='smd-text-align-center smd-text-overflow td-smd-table'>{title}</td> : null;
    }

    tabelBody(func, tableData: any) {
        let body: Array<any> = [];
        for (let index in ConsultationMeetingsTableHeaders) {
            body.push(func(tableData[ConsultationMeetingsTableHeaders[index].id], ConsultationMeetingsTableHeaders[index].id));
        }
        return body;
    }

    buttonSelectAndDeselectAll(isSelect: boolean) {
        for (let index in ConsultationMeetingsTableHeaders) {
            this.setState({
                [ConsultationMeetingsTableHeaders[index].id]: isSelect
            });
        }
    }

    renderItems(consultationMeetingsRecords) {
        let items: Array<any> = [];
        if (consultationMeetingsRecords) {
            for (let index in consultationMeetingsRecords) {
                let record = consultationMeetingsRecords[index];
                let li =
                    <tr key={index} className=''>
                        {this.tabelBody(this.tableBodyItems, record)}
                    </tr>
                items.push(li);
            }
        }
        return items;
    }

    render() {
        let items = this.renderItems(this.props.consultationMeetingsRecords);
        return (
            <div className="container-fluid">
                <dl className='row smd-v-padding'>
                    <dd className='col'>
                        <h5>{this.props.language[Vocabulary.CONSULTATION_MEETINGS_REPORT]}</h5>
                    </dd>
                </dl>
                <ViewConsultationMeetingsTopCmpnt
                    hospitalNamesForSelect={this.props.hospitalNamesForSelect}
                    hospitals={this.props.hospitals}
                    consultationMeetingsRecords={this.props.consultationMeetingsRecords}
                    callData={this.state}
                    language={this.props.language}
                    sendingObjectForRequest={(queryProps: any) => {
                        this.sendingObjectForRequest(queryProps)
                    }}
                />
                <br />
                <div className='row'>
                    <button
                        className='col-lg-2 col-md-3 col-sm-4 btn btn-primary smd-10-margin'
                        onClick={() => {
                            this.buttonSelectAndDeselectAll(true);
                        }}
                    >
                        {this.props.language[Vocabulary.BUTTON_SELECT_ALL]}
                    </button>
                    <button
                        className='col-lg-2 col-md-3 col-sm-4 btn btn-primary smd-10-margin ml-sm-3'
                        onClick={() => {
                            this.buttonSelectAndDeselectAll(false);
                        }}
                    >
                        {this.props.language[Vocabulary.BUTTON_DESELECT_ALL]}
                    </button>
                </div>
                <div className=''>
                    <table
                        id={'tableButtons'}
                        className='table table-hover'>
                        <thead>
                            <tr className='row'>
                                {this.tableHead(this.buttonShowTableColumn)}
                            </tr>
                        </thead>
                    </table>
                    <div className='smd-overflow-scroll'>
                        <table
                            id={'tableRecords'}
                            className='table table-hover'>
                            <thead>
                                <tr>
                                    {this.tableHead(this.showTableHead)}
                                </tr>
                            </thead>
                            <tbody>
                                {items || ''}
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        );
    }
}

function getHospitalNamesForSelect(clients: any[]) {
    let clientsList: Array<any> = [];
    for (let index in clients) {
        clientsList.push({
            value: index,
            label: clients[index].hospitalName
        });
    }
    return clientsList;
}

export function mapStateToProps(state, ownProps) {
    return {
        hospitalNamesForSelect: getHospitalNamesForSelect(state.snapmdAdmin.lists.clients) as any[],
        hospitals: state.snapmdAdmin.lists.clients as any[],
        consultationMeetingsRecords: state.snapmdAdmin.consultationMeetings as any[],
        language: state.site.misc.vocabulary as any
    }
}

export function mapDispatchToProps(dispatch) {
    return {
        getConsultationMeetings: (requestArrayField: any[]) => {
            dispatch(consultationMeetingsActions.queryConsultationMeetings(SnapMDServiceLocator, requestArrayField));
        }
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(ViewConsultationMeetingsCmpnt);