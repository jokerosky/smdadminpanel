import * as React from 'react';
import { connect } from 'react-redux';
import * as moment from 'moment';

import { SnapMDServiceLocator } from '../../../dependenciesInjectionConfiguration';
import { LoggedMessagesTableHeaders } from '../../../enums/tableHeaders';

import { ViewLoggedMessagesTopCmpnt } from './ViewLoggedMessagesTop';
import * as loggedMessagesActions from '../../../actions/snapMdAdminMenuActions/viewLoggedMessagesActions';

import { getFormattedDate } from '../../../utilities/converters';
import * as Vocabulary from '../../../localization/vocabulary';

export interface IViewLoggedMessagesProps {
    logMessages: any[];
    language: any;
    locale: string;

    getLoggedMessages: (fieldQuery: any[]) => void;
}

export class ViewLoggedMessagesCmpnt extends React.Component<IViewLoggedMessagesProps, any>{
    constructor() {
        super();

        this.state = {
            id: true,
            dateStamp: true,
            messageOrigin: true,
            messageType: true,
            message: true,
            recommendation: true,
            logLevel: true,
            ipAddres: true
        };

        this.tableHead = this.tableHead.bind(this);
        this.buttonShowTableColumn = this.buttonShowTableColumn.bind(this);
        this.showTableColumns = this.showTableColumns.bind(this);
        this.showTableHead = this.showTableHead.bind(this);
        this.tabelBody = this.tabelBody.bind(this);
        this.tableBodyItems = this.tableBodyItems.bind(this);
    }

    showTableColumns(column: string) {
        this.setState({
            [column]: !this.state[column]
        });
    }

    showTableHead(title: string, column: string) {
        return this.state[column] ? <th key={column} className='smd-text-align-center th-smd-table'>{title}</th> : null;
    }

    buttonShowTableColumn(title: string, column: string) {
        return <th
            key={column}
            id={column}
            className={`col btn ${this.state[column] ? 'btn-primary' : ''}`}
            onClick={() => { this.showTableColumns(column); }}
        >{title}</th>;
    }

    tableHead(func) {
        let head: Array<any> = [];
        for (let index in LoggedMessagesTableHeaders) {
            head.push(func(this.props.language[LoggedMessagesTableHeaders[index].label], LoggedMessagesTableHeaders[index].id));
        }
        return head;
    }

    buttonSelectAndDeselectAll(isSelect: boolean) {
        for (let index in LoggedMessagesTableHeaders) {
            this.setState({
                [LoggedMessagesTableHeaders[index].id]: isSelect
            });
        }
    }

    sendingObjectForRequest(queryProps: any) {
        this.props.getLoggedMessages(queryProps);
    }

    tableBodyItems(titles: any, column: string) {
        let title: any = titles;

        if (column === 'dateStamp') {
            title = moment(titles).locale(this.props.locale).format('L') + ' ' + moment(titles).locale(this.props.locale).format('LT')//getFormattedDate(titles);
        }

        return this.state[column] ? <td key={column} className='smd-text-align-center smd-text-overflow td-smd-table'>{title}</td> : null;
    }

    tabelBody(func, tableData: any) {
        let body: Array<any> = [];
        for (let index in LoggedMessagesTableHeaders) {
            body.push(func(tableData[LoggedMessagesTableHeaders[index].id], LoggedMessagesTableHeaders[index].id));
        }
        return body;
    }


    renderItems(logMessages: any[]) {
        let items: Array<any> = [];
        if (logMessages.length > 0) {
            for (let index in logMessages) {
                let message = logMessages[index];
                let li =
                    <tr
                        key={index}
                    >
                        {this.tabelBody(this.tableBodyItems, message)}
                    </tr>

                items.push(li);
            }
        }
        return items;
    }

    render() {
        let items = this.renderItems(this.props.logMessages);
        return (
            <div className="container-fluid">
                <dl className='row smd-v-padding'>
                    <dd className='col'>
                        <h5>{this.props.language[Vocabulary.REVIEW_LOGGED_MESSAGES]}</h5>
                    </dd>
                </dl>
                <ViewLoggedMessagesTopCmpnt
                    language = {this.props.language}
                    sendingObjectForRequest={(queryProps: any) => {
                        this.sendingObjectForRequest(queryProps);
                    }}
                />
                <br />
                <div className='row'>
                    <button
                        className='col-md-2 col-sm-3 btn btn-primary smd-10-margin'
                        onClick={() => {
                            this.buttonSelectAndDeselectAll(true);
                        }}
                    >
                        {this.props.language[Vocabulary.BUTTON_SELECT_ALL]}
                    </button>
                    <button
                        className='col-md-2 col-sm-3 btn btn-primary smd-10-margin ml-sm-3'
                        onClick={() => {
                            this.buttonSelectAndDeselectAll(false);
                        }}
                    >
                        {this.props.language[Vocabulary.BUTTON_DESELECT_ALL]}
                    </button>
                </div>
                <div className=''>
                    <table
                        id={'tableButtons'}
                        className='table table-hover'>
                        <thead>
                            <tr className='row'>
                                {this.tableHead(this.buttonShowTableColumn)}
                            </tr>
                        </thead>
                    </table>
                    <div className='smd-overflow-scroll'>
                        <table
                            id={'tableRecords'}
                            className='table table-hover'>
                            <thead>
                                <tr>
                                    {this.tableHead(this.showTableHead)}
                                </tr>
                            </thead>
                            <tbody>
                                {items || ''}
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        )
    }
}

export function mapStateToProps(state, ownProps) {
    return {
        logMessages: state.snapmdAdmin.logMessages as any[],
        language: state.site.misc.vocabulary as any,
        locale: state.site.misc.language as string
    }
}

export function mapDispatchToProps(dispatch) {
    return {
        getLoggedMessages: (fieldQuery: any[]) => {
            dispatch(loggedMessagesActions.loadLogMessages(SnapMDServiceLocator, fieldQuery));
        }
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(ViewLoggedMessagesCmpnt);