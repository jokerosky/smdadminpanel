import * as React from 'react';
import Select from 'react-select';
import Workbook from 'react-excel-workbook';
import * as moment from 'moment';

import * as Datetime from 'react-datetime';
import '../../../../node_modules/react-datetime/css/react-datetime.css';

import { EditorType } from '../../../enums/editorTypes';
import { getFormattedDate, secondsToMinutesString } from '../../../utilities/converters';
import Img from 'react-image'

/*
    layout = [
        {'edit', 'select', 'dateTime'}, - container
        { container: 'container'[ edit, radio] }
    ]
*/

let editTemp = {
    state: '',
    text: ''
};

let dateTemp = [];

export interface IConfigurationOfTheFilterSection {
    stateFilterSection: any;
    mapObjects: any[];
    tableHeaders: any;
    records: any;
    callData: any;
    transformationOfFields: any;

    callBack: (state) => void;
}

export class ConfigurationOfTheFilterSection extends React.Component<IConfigurationOfTheFilterSection, any>{
    constructor(props, state) {
        super(props, state);

        this.state = props.stateFilterSection;
        this.creatingObjectsOnTheMapOfObjects = this.creatingObjectsOnTheMapOfObjects.bind(this);
        this.writeTheValueOfEditInState = this.writeTheValueOfEditInState.bind(this);
        this.writeTheValueOfSelectInState = this.writeTheValueOfSelectInState.bind(this);
        this.writeTheValueOfRadioInState = this.writeTheValueOfRadioInState.bind(this);
        this.writeTheValueOfDateInState = this.writeTheValueOfDateInState.bind(this);
    }

    writeTheValueOfEditInState(field: string, data: any) {
        this.setState({
            [field]: data.currentTarget.value
        });
    }

    writeTheValueOfSelectInState(field: string, data: any) {
        this.setState({
            [field]: data.value
        });
    }

    writeTheValueOfRadioInState(field: string, data: boolean) {
        this.setState({
            [field]: data
        });
    }

    writeTheValueOfCheckInState(field: string) {
        this.setState({
            [field]: !this.state[field]
        });
    }

    writeTheValueOfDateInState(field: string, date: any) {
        this.setState({
            [field]: date
        });
    }
    /*************** */
    workbookColumnShow(label: string, column: string) {
        return <Workbook.Column key={column} label={label} value={column} />
    }

    workbookColumn() {
        let col: Array<any> = [];
        for (let index in this.props.tableHeaders) {
            if (this.props.callData[this.props.tableHeaders[index].id]) {
                col.push(this.workbookColumnShow(this.props.tableHeaders[index].label, this.props.tableHeaders[index].id));
            }
        }
        return col;
    }

    comparisonOfFields(field: string, data: any) {
        let transfomation: any = null;

        for (let i in this.props.transformationOfFields.field) {
            if (field === this.props.transformationOfFields.field[i].title) {
                switch (this.props.transformationOfFields.field[i].method) {
                    case 'formattedDate':
                        {
                            transfomation = getFormattedDate(data);
                            break;
                        }
                    case 'formattedTime':
                        {
                            transfomation = secondsToMinutesString(data);
                            break;
                        }
                    case 'type':
                        {
                            transfomation = this.props.transformationOfFields.field[i].data[data - 1];
                            break;
                        }
                }

            }
        }
        return transfomation;
    }

    shapingExcelWorkBook(labelBtn: string) {
        let excelData: any = [];

        for (let record in this.props.records) {
            let rec: any = {};
            for (let index in this.props.tableHeaders) {
                if (this.props.callData[this.props.tableHeaders[index].id]) {

                    if (this.comparisonOfFields(this.props.tableHeaders[index].id, this.props.records[record][this.props.tableHeaders[index].id]) !== null) {
                        rec[this.props.tableHeaders[index].id] = this.comparisonOfFields(this.props.tableHeaders[index].id, this.props.records[record][this.props.tableHeaders[index].id]);
                    } else {
                        rec[this.props.tableHeaders[index].id] = this.props.records[record][this.props.tableHeaders[index].id];
                    }
                }
            }
            excelData.push(rec);
        }

        let workBook =
            <Workbook filename='report.xlsx'
                element={
                    <button
                        className='btn btn-lg smd-cursor-pointer'
                    > {labelBtn} </button>
                }>
                <Workbook.Sheet data={excelData} name='Sheet A'>
                    {this.workbookColumn()}
                </Workbook.Sheet>
            </Workbook>;

        return workBook;
    }
    /************************** */
    //The method creates components according to the map, writes them to an array, 
    // and returns the components wrapped by div.
    creatingObjectsOnTheMapOfObjects(htmlComponent: any) {
        let textComponent = { __html: '' };
        let arr: Array<any> = [];
        let rand = 1 + Math.random() * (30 + 1);
        rand = Math.floor(rand);

        for (let index in htmlComponent) {
            switch (htmlComponent[index][index]) {

                case EditorType.button:
                    {
                        let button =
                            <div
                                key={htmlComponent[index][index] + '_' + htmlComponent[index].name}
                                className='col'>
                                <button
                                    name={htmlComponent[index].name}
                                    className={htmlComponent[index].classBtn + ' smd-cursor-pointer'}
                                    onClick={() => {
                                        this.setState({
                                            [editTemp.state]: editTemp.text
                                        });
                                        for (let i in dateTemp) {
                                            this.setState({
                                                [dateTemp[i][this.state[dateTemp[i][i]]]]: dateTemp[i].value
                                            })
                                        }
                                        this.props.callBack(htmlComponent[index].action(this.state));

                                    }}
                                > {htmlComponent[index].label} </button>
                            </div>

                        arr.push(button);
                        break;
                    }

                case EditorType.check:
                    {
                        let check: Array<any> = [];
                        for (let i in htmlComponent[index].checkBtn) {
                            check.push(
                                <div
                                    key={htmlComponent[index][index] + '_' + htmlComponent[index].checkBtn[i].name + '_' + i}
                                >
                                    <label className='smd-cursor-pointer'><input
                                        className='smd-cursor-pointer'
                                        name={htmlComponent[index].checkBtn[i].name}
                                        type='checkbox'
                                        checked={this.state[htmlComponent[index].checkBtn[i].action]}
                                        onChange={() => {
                                            this.writeTheValueOfCheckInState(htmlComponent[index].checkBtn[i].action);
                                        }}
                                    />{htmlComponent[index].checkBtn[i].label}</label>
                                </div>
                            );
                        }

                        rand = 1 + Math.random() * (18 + (6 + Math.random()));
                        rand = Math.floor(rand);

                        let checkLabel =
                            <div
                                key={htmlComponent[index][index] + '_' + htmlComponent[index].name + '_' + rand}
                                className={htmlComponent[index].classDiv}>
                                <h5><label className='col'>{htmlComponent[index].label}</label></h5>
                                {check}
                            </div>

                        arr.push(checkLabel);
                        break;
                    }

                case EditorType.container:
                    {
                        let container = this.creatingObjectsOnTheMapOfObjects(htmlComponent[index]);

                        arr.push(container);
                        break;
                    }

                case EditorType.dateTime:
                    {
                        let dateTime: Array<any> = [];
                        for (let i in htmlComponent[index].date) {
                            dateTime.push(
                                <div
                                    key={htmlComponent[index][index] + '_' + htmlComponent[index].date[i].action + '_' + i}
                                    className={htmlComponent[index].date[i].classDiv}
                                >
                                    <label htmlFor="client">{htmlComponent[index].date[i].label}</label>
                                    <Datetime
                                        dateFormat={htmlComponent[index].date[i].dateFormat}
                                        timeFormat={htmlComponent[index].date[i].timeFormat}
                                        ref={`dateTime${i}`}
                                        inputProps={{
                                            placeholder: htmlComponent[index].date[i].placeholder,
                                        }}
                                        value={this.state[htmlComponent[index].date[i].action]}
                                        onChange={(e) => {
                                            dateTemp.push({
                                                [i]: htmlComponent[index].date[i].action,
                                                value: e
                                            });
                                            this.writeTheValueOfDateInState(htmlComponent[index].date[i].action, e);
                                        }}

                                    />
                                </div>
                            );
                        }

                        arr.push(dateTime);
                        break;
                    }

                case EditorType.edit:
                    {
                        let edit: Array<any> = [];
                        for (let i in htmlComponent[index].input) {
                            edit.push(
                                <div
                                    key={htmlComponent[index][index] + '_' + htmlComponent[index].input[i].name + '_' + i}
                                    className={htmlComponent[index].input[i].classDiv}
                                >
                                    <label htmlFor="client">{htmlComponent[index].input[i].label}</label>
                                    <input
                                        name={htmlComponent[index].input[i].name}
                                        type='text'
                                        className={htmlComponent[index].input[i].classComp}
                                        placeholder={htmlComponent[index].input[i].placeholderComp}
                                        defaultValue={this.state[htmlComponent[index].input[i].action]}
                                        onChange={(e) => {
                                            editTemp.state = htmlComponent[index].input[i].action;
                                            editTemp.text = e.currentTarget.value;
                                            /* this.setState({
                                                 [htmlComponent[index].input[i].action]: e.currentTarget.value
                                             })*/
                                            //this.writeTheValueOfEditInState(htmlComponent[index].action, e);
                                        }}
                                    />
                                </div>
                            );
                        }

                        arr.push(edit);
                        break;
                    }

                case EditorType.radio:
                    {
                        let radio: Array<any> = [];
                        for (let i in htmlComponent[index].radioBtn) {
                            radio.push(
                                <div
                                    key={htmlComponent[index][index] + '_' + htmlComponent[index].radioBtn[i].value + '_' + i}
                                >
                                    <label className='smd-cursor-pointer'>
                                        <input
                                            type="radio"
                                            className='smd-cursor-pointer'
                                            name={htmlComponent[index].radioBtn[i].name}
                                            value={htmlComponent[index].radioBtn[i].value}
                                            checked={htmlComponent[index].radioBtn[i].value === 'asc' ? this.state.dateSort : !this.state.dateSort}
                                            onChange={() => {
                                                this.writeTheValueOfRadioInState(htmlComponent[index].radioBtn[i].action, htmlComponent[index].radioBtn[i].value === 'asc' ? true : false);
                                            }}
                                        /> {htmlComponent[index].radioBtn[i].label}
                                    </label>
                                </div>
                            );
                        }

                        rand = 1 + Math.random() * (26 + (3 + Math.random()));
                        rand = Math.floor(rand);

                        let redioLabel =
                            <div
                                key={htmlComponent[index][index] + '_' + rand + 'radio'}
                                className={htmlComponent[index].classDiv}>
                                <h5><label className='col'>{htmlComponent[index].label}</label></h5>
                                {radio}
                            </div>

                        arr.push(redioLabel);
                        break;
                    }

                case EditorType.select:
                    {
                        let select: Array<any> = [];
                        for (let i in htmlComponent[index].selected) {
                            select.push(
                                <div
                                    key={htmlComponent[index][index] + '_' + htmlComponent[index].selected[i].name + '_' + i}
                                    className={htmlComponent[index].selected[i].classDiv}>
                                    <label htmlFor="client">{htmlComponent[index].selected[i].label}</label>
                                    <Select
                                        name={htmlComponent[index].selected[i].name}
                                        options={htmlComponent[index].selected[i].options}
                                        value={htmlComponent[index].selected[i].options[this.state[htmlComponent[index].selected[i].action]]}
                                        clearable={false}
                                        onChange={(e) => {
                                            this.writeTheValueOfSelectInState(htmlComponent[index].selected[i].action, e);
                                        }}
                                    />
                                </div>
                            );
                        }

                        arr.push(select);
                        break;
                    }

                case EditorType.excel:
                    {
                        let excel =
                            <div
                                key={htmlComponent[index][index] + '_' + htmlComponent[index].name}
                                className={htmlComponent[index].classDiv}>
                                {this.shapingExcelWorkBook(htmlComponent[index].label)}
                            </div>

                        arr.push(excel);
                        break;
                    }
            }
        }

        rand = Math.random() + (Math.random() * Math.random() + 0.5);
        // rand = Math.round(rand);

        let createdLayout: any =
            <div
                key={(htmlComponent.classDiv + '_' + rand + 'eer').replace(/ /ig, '_')}
                className={htmlComponent.classDiv}
            >
                {arr}
            </div>;
        return createdLayout;
    }

    //parses the map into blocks, then assembles the created blocks into an array and returns an array  
    createdLayout() {
        let layout: Array<any> = [];
        for (let index in this.props.mapObjects) {
            layout.push(this.creatingObjectsOnTheMapOfObjects(this.props.mapObjects[index]));
        }
        return layout;
    }

    collBack() {
        this.props.callBack(this.state);
    }

    render() {
        return (
            <div>
                {...this.createdLayout()}
            </div>
        )
    }

}