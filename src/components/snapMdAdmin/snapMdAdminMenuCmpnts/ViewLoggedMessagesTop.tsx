import * as React from 'react';
import Select from 'react-select';
import * as moment from 'moment';

import * as Datetime from 'react-datetime';
import '../../../../node_modules/react-datetime/css/react-datetime.css';
import * as Vocabulary from '../../../localization/vocabulary';
import { ObjectHelper } from '../../../utilities/objectHelper';

export const countRecord: any = [
    {
        value: 0,
        label: 'Chose logs count'
    },
    {
        value: 1,
        label: 1
    },
    {
        value: 2,
        label: 10
    },
    {
        value: 3,
        label: 100
    },
    {
        value: 4,
        label: 1000
    }
];

export interface IViewLoggedMessagesTopProps {
    language: any;

    sendingObjectForRequest: (queryProps) => void;
}

export class ViewLoggedMessagesTopCmpnt extends React.Component<IViewLoggedMessagesTopProps, any>{
    constructor() {
        super()

        this.state = {
            dateSort: true,
            date: moment(),
            searchText: '',
            selectedLogsCount: 0,
            info: true,
            error: true
        }
    }

    changeRadioButton(isFlag: boolean) {
        this.setState({
            dateSort: isFlag
        });
    }

    changeMessageTypes(name: string) {
        this.setState({
            [name]: !this.state[name]
        });
    }

    sendingObjectForRequest() {
        let queryProps = [];

        if (this.state.info) {
            queryProps.push({
                key: 'messageTypes[]',
                value: 'Info'
            });
        }

        if (this.state.error) {
            queryProps.push({
                key: 'messageTypes[]',
                value: 'Error'
            });
        }
        queryProps.push(
            {
                key: 'searchPrase',
                value: this.state.searchText
            },
            {
                key: 'sortAscending',
                value: this.state.dateSort
            },
            {
                key: 'maxCount',
                value: countRecord[this.state.selectedLogsCount].label
            },
            {
                key: 'fromDate',
                value: this.state.date.toString()//format('ddd MMM DD YYYY HH:MM:ss Z')
            }
        );

        this.props.sendingObjectForRequest(queryProps);
    }

    render() {
        return (
            <div>
                <div className='row'>
                    <div className="form-group col-md-5 col-sm-4 smd-vertical-align-bottom">
                        <label htmlFor="client">{this.props.language[Vocabulary.SEARCH_PHRASE]}</label>
                        <input
                            name={'SearchInput'}
                            type='text'
                            className='form-control'
                            placeholder={this.props.language[Vocabulary.SEARCH]}
                            value={this.state.searchText}
                            onChange={(e) => {
                                this.setState({
                                    searchText: e.currentTarget.value
                                });
                            }}
                        />
                    </div>
                    <div className="form-group col-md-3 col-sm-4">
                        <label htmlFor="client">{this.props.language[Vocabulary.LOGS_MAX_COUNT]}</label>
                        <Select
                            name={'maxCount'}
                            options={countRecord}
                            value={countRecord[this.state.selectedLogsCount]}
                            clearable={false}
                            onChange={(e) => {
                                this.setState({
                                    selectedLogsCount: e.value
                                });
                            }}
                        />
                    </div>
                    <div className="form-group col-md-3 col-sm-4 smd-vertical-align-bottom">
                        <label htmlFor="client">{this.props.language[Vocabulary.HEAD_SELECT_FROM_DATE]}</label>
                        <Datetime
                            dateFormat={'DD MMMM YYYY'}
                            timeFormat={false}
                            locale={ObjectHelper.getLocaleForMomentDateTime(this.props.language[Vocabulary.LANGUAGE] || 'us')}
                            inputProps={{
                                placeholder: "DD MMMM YYYY",
                            }}
                            value={this.state.date}
                            onChange={(e) => {
                                this.setState({
                                    date: e
                                });
                            }}
                        />
                    </div>
                </div>
                <div className='row'>
                    <div className='col-lg-3 col-md-3 col-sm-4 col-6'>
                        <h5><label className='col'>{this.props.language[Vocabulary.DATE_SORT]}</label></h5>
                        <div>
                            <label>
                                <input
                                    type="radio"
                                    name='messageTypes'
                                    value='asc'
                                    checked={this.state.dateSort}
                                    onChange={() => {
                                        this.changeRadioButton(true);
                                    }}
                                /> {this.props.language[Vocabulary.DATE_SORT_ASC]}
                            </label>
                        </div>
                        <div>
                            <label>
                                <input
                                    type="radio"
                                    name='messageTypes'
                                    value='desc'
                                    checked={!this.state.dateSort}
                                    onChange={() => {
                                        this.changeRadioButton(false);
                                    }}
                                /> {this.props.language[Vocabulary.DATE_SORT_DESC]}
                            </label>
                        </div>
                    </div>
                    <div className='col-lg-3 col-md-4 col-sm-5 col-6'>
                        <h5><label className='col'>{this.props.language[Vocabulary.MESSAGE_TYPES]}</label></h5>
                        <div>
                            <label>
                                <input
                                    type="checkbox"
                                    name='info'
                                    checked={this.state.info}
                                    onChange={() => {
                                        this.changeMessageTypes('info');
                                    }}
                                /> {this.props.language[Vocabulary.MESSAGE_TYPES_INFO]}
                            </label>
                        </div>
                        <div>
                            <label>
                                <input
                                    type="checkbox"
                                    name='error'
                                    checked={this.state.error}
                                    onChange={() => {
                                        this.changeMessageTypes('error');
                                    }}
                                /> {this.props.language[Vocabulary.MESSAGE_TYPES_ERROR]}
                            </label>
                        </div>
                    </div>
                    <div className='col-sm-3 row d-sm-block'>
                        <button
                            className='btn btn-lg smd-10-margin'
                            name='query'
                            onClick={() => {
                                this.sendingObjectForRequest();
                            }}
                        >
                            {this.props.language[Vocabulary.BUTTON_QUERY]}
                         </button>
                    </div>
                </div>
            </div>
        )
    }
}