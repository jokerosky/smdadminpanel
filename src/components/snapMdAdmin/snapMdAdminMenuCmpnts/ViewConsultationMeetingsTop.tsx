import * as React from 'react';
import Select from 'react-select';
import Workbook from 'react-excel-workbook';
import * as moment from 'moment';
import * as _ from 'lodash';

import * as Datetime from 'react-datetime';
import '../../../../node_modules/react-datetime/css/react-datetime.css';

import { ConsultationMeetingsTableHeaders } from '../../../enums/tableHeaders';
import * as Vocabulary from '../../../localization/vocabulary';
import { getFormattedDate, secondsToMinutesString } from '../../../utilities/converters';
import { ObjectHelper } from '../../../utilities/objectHelper';

export let countRecord: any = [
    {
        value: 0,
        label: 'Chose records count'
    },
    {
        value: 1,
        label: 100
    },
    {
        value: 2,
        label: 1000
    },
    {
        value: 3,
        label: 10000
    }
];

export let callType: Array<string> = [
    'providerAudio',
    'providerVideo',
    'providerPatient',
    'openConsultation'
];

export let callText: Array<string> = [
    'provide-provider audio',
    'provide-provider video',
    'provide-patient',
    'open consultation'
];

export interface IViewConsultationMeetingsTopProps {
    hospitalNamesForSelect: any[];
    hospitals: any[];
    consultationMeetingsRecords: any[];
    callData: any;
    language: any;

    sendingObjectForRequest: (queryProps: any) => void;
}

export class ViewConsultationMeetingsTopCmpnt extends React.Component<IViewConsultationMeetingsTopProps, any>{
    constructor() {
        super();

        this.state = {
            fromDate: moment(),
            toDate: moment(),
            countRecordSelected: 0,
            recordSelectedValue: 0,
            selectedHospitalId: -1,
            providerAudio: true,
            providerVideo: true,
            providerPatient: true,
            openConsultation: true
        };

        this.shapingExcelWorkBook = this.shapingExcelWorkBook.bind(this);
        this.workbookColumn = this.workbookColumn.bind(this);
        this.workbookColumnShow = this.workbookColumnShow.bind(this);
    }

    workBook = null;

    componentWillReceiveProps(nextProps) {
        if (nextProps.language !== this.props.language ||
            nextProps.consultationMeetingsRecords !== this.props.consultationMeetingsRecords) {
            this.workBook = this.shapingExcelWorkBook(nextProps);
        }
    }
    showTableColumns(column: string) {
        this.setState({
            [column]: !this.state[column]
        });
    }

    workbookColumnShow(label: string, column: string, language: any) {
        return <Workbook.Column key={column} label={language[label]} value={column} />
    }

    workbookColumn(props) {
        let col: Array<any> = [];
        for (let index in ConsultationMeetingsTableHeaders) {
            if (props.callData[ConsultationMeetingsTableHeaders[index].id]) {
                col.push(this.workbookColumnShow(ConsultationMeetingsTableHeaders[index].label, ConsultationMeetingsTableHeaders[index].id, props.language));
            }
        }
        return col;
    }

    shapingExcelWorkBook(props) {
        let excelData: any = [];
        let workBook = null;
        if (_.keys(props.language).length > 0) {
            for (let record in props.consultationMeetingsRecords) {
                let rec: any = {};
                for (let index in ConsultationMeetingsTableHeaders) {
                    if (props.callData[ConsultationMeetingsTableHeaders[index].id]) {
                        if (ConsultationMeetingsTableHeaders[index].id === 'callDuration') {
                            rec[ConsultationMeetingsTableHeaders[index].id] = secondsToMinutesString(props.consultationMeetingsRecords[record][ConsultationMeetingsTableHeaders[index].id]);
                        } else if (ConsultationMeetingsTableHeaders[index].id === 'startTime') {
                            rec[ConsultationMeetingsTableHeaders[index].id] = getFormattedDate(props.consultationMeetingsRecords[record][ConsultationMeetingsTableHeaders[index].id]);
                        } else {
                            rec[ConsultationMeetingsTableHeaders[index].id] = props.consultationMeetingsRecords[record][ConsultationMeetingsTableHeaders[index].id]
                        }
                    }
                }
                excelData.push(rec);
            }

            workBook =
                <Workbook filename='report.xlsx'
                    element={
                        <button
                            className='btn btn-lg'
                        >{props.language[Vocabulary.BUTTON_TO_EXCEL]}</button>
                    }>
                    <Workbook.Sheet data={excelData} name='Sheet A'>
                        {this.workbookColumn(props)}
                    </Workbook.Sheet>
                </Workbook>;
        }
        return workBook;
    }

    render() {
        return (
            <div>
                <div className='row'>
                    <div className="form-group col-md-3 col-sm-6 smd-vertical-align-bottom">
                        <label htmlFor="client">{this.props.language[Vocabulary.HEAD_SELECT_CLIENT]}</label>
                        <Select
                            name={'SelectClient'}
                            className={''}
                            options={this.props.hospitalNamesForSelect}
                            value={this.props.hospitalNamesForSelect[this.state.selectedHospitalId]}
                            clearable={false}
                            onChange={(e) => {
                                this.setState({
                                    selectedHospitalId: e.value
                                });
                            }}
                        />
                    </div>
                    <div className="form-group col-md-3 col-sm-6">
                        <label htmlFor="client">{this.props.language[Vocabulary.HEAD_SELECT_MAX_RECORDS_COUNT]}</label>
                        <Select
                            name={'SelectRecords'}
                            options={countRecord}
                            value={countRecord[this.state.countRecordSelected]}
                            clearable={false}
                            onChange={(e) => {
                                this.setState({
                                    countRecordSelected: e.value,
                                    recordSelectedValue: e.label
                                });
                            }}
                        />
                    </div>
                    <div className="form-group col-md-3 col-sm-6 smd-vertical-align-bottom"
                        onBlur={() => {
                            this.setState({
                                fromDate: moment(this.state.fromDate.toString())
                            })
                        }}
                    >
                        <label htmlFor="client">{this.props.language[Vocabulary.HEAD_SELECT_FROM_DATE]}</label>
                        <Datetime
                            dateFormat={'DD MMMM YYYY'}
                            timeFormat={false}
                            locale={ObjectHelper.getLocaleForMomentDateTime(this.props.language[Vocabulary.LANGUAGE] || 'us')}
                            inputProps={{
                                placeholder: "DD MMMM YYYY",
                            }}
                            value={this.state.fromDate}
                            onChange={(e) => {
                                this.setState({
                                    fromDate: e
                                })
                            }}
                        />
                    </div>
                    <div className="form-group col-md-3 col-sm-6 smd-vertical-align-bottom"
                        onBlur={() => {
                            this.setState({
                                toDate: moment(this.state.toDate.toString())
                            })
                        }}
                    >
                        <label htmlFor="client">{this.props.language[Vocabulary.HEAD_SELECT_TO_DATE]}</label>
                        <Datetime
                            dateFormat={'DD MMMM YYYY'}
                            timeFormat={false}
                            locale={ObjectHelper.getLocaleForMomentDateTime(this.props.language[Vocabulary.LANGUAGE] || 'us')}
                            inputProps={{
                                placeholder: "DD MMMM YYYY",
                            }}
                            value={this.state.toDate}
                            onChange={(e) => {
                                this.setState({
                                    toDate: e
                                })
                            }}
                        />
                    </div>
                </div>
                <div className='row'>
                    <div className='col-md-4 col-sm-6'>
                        <h6>{this.props.language[Vocabulary.CALL_TYPE]}</h6>
                        <div>
                            <div>
                                <label><input
                                    name={'check1'}
                                    type='checkbox'
                                    checked={this.state.providerAudio}
                                    onChange={() => {
                                        this.showTableColumns('providerAudio');
                                    }}
                                /> {this.props.language[Vocabulary.PROVIDER_PROVIDER_AUDIO]}</label>
                            </div>
                            <div>
                                <label><input
                                    name={'check2'}
                                    type='checkbox'
                                    checked={this.state.providerVideo}
                                    onChange={() => {
                                        this.showTableColumns('providerVideo');
                                    }}
                                /> {this.props.language[Vocabulary.PROVIDER_PROVIDER_VIDEO]}</label>
                            </div>
                            <div>
                                <label><input
                                    name={'check3'}
                                    type='checkbox'
                                    checked={this.state.providerPatient}
                                    onChange={() => {
                                        this.showTableColumns('providerPatient');
                                    }}
                                /> {this.props.language[Vocabulary.PROVIDER_PATIENT]}</label>
                            </div>
                            <div>
                                <label><input
                                    name={'check4'}
                                    type='checkbox'
                                    checked={this.state.openConsultation}
                                    onChange={() => {
                                        this.showTableColumns('openConsultation');
                                    }}
                                /> {this.props.language[Vocabulary.OPEN_CONSULTATION]}</label>
                            </div>
                        </div>
                    </div>
                    <div className=' col-sm-6 row d-sm-block '>
                        <div className='col-6 smd-10-margin'>
                            {this.shapingExcelWorkBook(this.props)}
                        </div>
                        <div className='col'>
                            <button
                                name={'ButtonQuery'}
                                className='btn btn-lg smd-10-margin'
                                onClick={() => {
                                    let queryProps = {
                                        fromDate: this.state.fromDate,
                                        toDate: this.state.toDate,
                                        recordSelectedValue: this.state.recordSelectedValue,
                                        selectedHospitalId: this.state.selectedHospitalId,
                                        providerAudio: this.state.providerAudio,
                                        providerVideo: this.state.providerVideo,
                                        providerPatient: this.state.providerPatient,
                                        openConsultation: this.state.openConsultation
                                    }
                                    this.props.sendingObjectForRequest(queryProps);
                                }}
                            > {this.props.language[Vocabulary.BUTTON_QUERY]} </button>
                        </div>
                    </div>
                </div>
            </div>
        );
    }
}