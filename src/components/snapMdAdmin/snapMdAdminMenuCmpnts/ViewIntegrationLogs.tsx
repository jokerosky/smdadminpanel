import * as React from 'react';
import { connect } from 'react-redux';

//import { LoggedMessagesTableHeaders } from '../../../enums/tableHeaders';
import { SnapMDServiceLocator } from '../../../dependenciesInjectionConfiguration';
import * as Vocabulary from '../../../localization/vocabulary';
import { ViewIntegrationLogsTopCmpnt } from './ViewIntegrationLogsTop';
import * as IntegrationActions from '../../../actions/snapMdAdminMenuActions/viewIntegrationLogsAction';
import { IntegrationLogsTableHeaders } from '../../../enums/tableHeaders';

export interface IViewIntegrationLogs {
    language: any;
    integrationLogs: any[];

    getIntegrationLogs: (fieldQuery: any[]) => void;
}

export class ViewIntegrationLogsCmpnt extends React.Component<IViewIntegrationLogs, any>{
    constructor() {
        super();

        this.state = {
            IntegrationLogId: true,
            LogType: true,
            DateCreated: true,
            PatientId: true,
            HospitalStaffProfileId: true,
            PartnerId: true,
            LogMessage: true,
            HospitalId: true,
            CommandName: true
        };

        this.buttonShowTableColumn = this.buttonShowTableColumn.bind(this);
        this.showTableHead = this.showTableHead.bind(this);
        this.showTableColumns = this.showTableColumns.bind(this);
        this.tableBodyItems = this.tableBodyItems.bind(this);
        this.tabelBody = this.tabelBody.bind(this);
        this.renderItems = this.renderItems.bind(this);
    }

    buttonSelectAndDeselectAll(isSelect: boolean) {
        for (let index in IntegrationLogsTableHeaders) {
            this.setState({
                [IntegrationLogsTableHeaders[index].id]: isSelect
            });
        }
    }

    showTableColumns(column: string) {
        this.setState({
            [column]: !this.state[column]
        });
    }

    buttonShowTableColumn(title: string, column: string) {
        return <th
            key={column}
            id={column}
            className={`col btn ${this.state[column] ? 'btn-primary' : ''}`}
            onClick={() => { this.showTableColumns(column); }}
        >{title}</th>;
    }

    showTableHead(title: string, column: string) {
        return this.state[column] ? <th key={column} className='smd-text-align-center th-smd-table'>{title}</th> : null;
    }

    tableHead(func) {
        let head: Array<any> = [];
        for (let index in IntegrationLogsTableHeaders) {
            head.push(func(this.props.language[IntegrationLogsTableHeaders[index].label], IntegrationLogsTableHeaders[index].id));
        }
        return head;
    }

    tableBodyItems(titles: any, column: string) {
        let title: any = titles;

        return this.state[column] ? <td key={column} className='smd-text-align-center smd-text-overflow td-smd-table'>{title}</td> : null;
    }

    tabelBody(func, tableData: any) {
        let body: Array<any> = [];
        for (let index in IntegrationLogsTableHeaders) {
            body.push(func(tableData[IntegrationLogsTableHeaders[index].id], IntegrationLogsTableHeaders[index].id));
        }
        return body;
    }

    renderItems(records: any[]) {
        let items: Array<any> = [];
        if (records.length > 0) {
            for (let index in records) {
                let li =
                    <tr key={index} className=''>
                        {this.tabelBody(this.tableBodyItems, records[index])}
                    </tr>
                items.push(li);
            }
        }
        return items;
    }

    render() {
        let items = this.renderItems(this.props.integrationLogs);
        return (
            <div className="container-fluid">
                <dl className='row smd-v-padding'>
                    <dd className='col'>
                        <h5>{this.props.language[Vocabulary.HEADER_INTEGRATION_LOGS]}</h5>
                    </dd>
                </dl>
                <ViewIntegrationLogsTopCmpnt
                    language={this.props.language}
                    getIntegrationLogs={(fieldQuery) => {
                        this.props.getIntegrationLogs(fieldQuery);
                    }}
                />
                <br />
                <div className='row'>
                    <button
                        className='col-lg-2 col-md-3 col-sm-4 btn btn-primary smd-10-margin'
                        onClick={() => {
                            this.buttonSelectAndDeselectAll(true);
                        }}
                    >
                        {this.props.language[Vocabulary.BUTTON_SELECT_ALL]}
                    </button>
                    <button
                        className='col-lg-2 col-md-3 col-sm-4 btn btn-primary smd-10-margin ml-sm-3'
                        onClick={() => {
                            this.buttonSelectAndDeselectAll(false);
                        }}
                    >
                        {this.props.language[Vocabulary.BUTTON_DESELECT_ALL]}
                    </button>
                </div>
                <div className=''>
                    <table
                        id={'tableButtons'}
                        className='table table-hover'>
                        <thead>
                            <tr className='row'>
                                {this.tableHead(this.buttonShowTableColumn)}
                            </tr>
                        </thead>
                    </table>
                    <div className='smd-overflow-scroll'>
                        <table
                            id={'tableRecords'}
                            className='table table-hover'>
                            <thead>
                                <tr>
                                    {this.tableHead(this.showTableHead)}
                                </tr>
                            </thead>
                            <tbody>
                                {items || ''}
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        );
    }
}

export function mapStateToProps(state, ownProps) {
    return {
        integrationLogs: state.snapmdAdmin.integrationLogs as any[],
        language: state.site.misc.vocabulary as any
    }
}

export function mapDispatchToProps(dispatch) {
    return {
        getIntegrationLogs: (fieldQuery: any[]) => {
            dispatch(IntegrationActions.getIntegrationLogs(SnapMDServiceLocator, fieldQuery));
        }
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(ViewIntegrationLogsCmpnt);