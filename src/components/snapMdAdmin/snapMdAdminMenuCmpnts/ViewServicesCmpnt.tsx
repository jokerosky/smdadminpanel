import * as React from 'react';
import { connect } from 'react-redux';
import * as moment from 'moment';

import * as Vocabulary from '../../../localization/vocabulary';
import * as servicesActions from '../../../actions/snapMdAdminMenuActions/viewServicesActions';
import { SnapMDServiceLocator } from '../../../dependenciesInjectionConfiguration';
import { getFormattedDate } from '../../../utilities/converters';
import { ModalWindow } from '../../common/ModalWindow';
import Select from 'react-select';
import { serverLogingLevel } from '../../../enums/logLevel';
import { ToggleButton } from '../../common/inputs/ToggleButtonCmpnt';
import { ServicesTableHeaders } from '../../../enums/tableHeaders';
import { Validator } from '../../../utilities/validators';
import { ServicesDTO } from '../../../models/DTO/snapmdAdminStateDTO';
import { ObjectHelper } from '../../../utilities/objectHelper';

export interface ISmdaServicesCmpntProps {
    services: Array<ServicesDTO>;
    language: any;

    getSystemServices: () => void;
    getServiceLogRecords: (serviceId: number, logparameters: any) => void;
    updateStateServices: (serviceStatus: any, loggingLevel: any) => void;
}

export class SmdaServicesCmpnt extends React.Component<ISmdaServicesCmpntProps, any>{
    constructor() {
        super();

        this.state = {
            modalVisibleDetails: false,
            modalVisibleLog: false,
            Description: '',
            LoggingLevel: 0,
            CycleMS: '',
            Status: false,
            LoggingLevelInLogModal: 0,
            selectedServices: -1
        }
        this.closeModalDetails = this.closeModalDetails.bind(this);
        this.openModalDetails = this.openModalDetails.bind(this);
        this.closeModalLog = this.closeModalLog.bind(this);
        this.openModalLog = this.openModalLog.bind(this);
    }

    componentDidMount() {
        this.props.getSystemServices();
    }
    changeInput = (val) => {
        if (Validator.isNumber(val.currentTarget.value)) {
            this.setState({ CycleMS: val.currentTarget.value });
        }
    };

    changeSelect = (val, field) => {
        this.setState({
            [field]: val.value,
        });
    };

    changeToggle = () => {
        this.setState({
            Status: !this.state.Status
        });
    };

    closeModalDetails() {
        this.setState({
            modalVisibleDetails: false
        });
    }

    openModalDetails(service, selectedServices) {
        let status = service.Status === 'On' ? true : false;
        this.setState({
            modalVisibleDetails: true,
            Description: service.Description,
            LoggingLevel: service.LoggingLevel,
            CycleMS: service.CycleMS,
            Status: status,
            selectedServices: selectedServices
        });
    }

    closeModalLog() {
        this.setState({
            modalVisibleLog: false
        });
    }

    openModalLog(selectedServices) {
        this.setState({
            modalVisibleLog: true,
            selectedServices: selectedServices
        });
        this.getLogRecords(selectedServices);
    }

    cycleSecFormatted(secs) {
        let sec: string = secs.toString();
        if (sec.length > 6) {
            sec = sec.slice(0, sec.length - 6) + ',' + sec.slice(sec.length - 6, sec.length);
        }
        if (sec.length > 3) {
            sec = sec.slice(0, sec.length - 3) + '.' + sec.slice(sec.length - 3, sec.length);
        }
        return sec;
    }

    getLogRecords(selectedServices) {
        let logparameters = {
            take: 100,
            ascending: false,
            level: this.state.LoggingLevelInLogModal
        };

        this.props.getServiceLogRecords(selectedServices, logparameters);
    }

    updateServicesStatus() {
        let serviceStatus = {
            CycleMS: this.state.CycleMS,
            ServiceId: this.state.selectedServices,
            Status: this.state.Status ? 'On' : 'Off'
        };

        let loggingLevel = {
            LoggingLevel: this.state.LoggingLevel,
            ServiceId: this.state.selectedServices
        };

        this.props.updateStateServices(serviceStatus, loggingLevel);
    }

    createTableHead(tableHead: Array<string>) {
        let itemsHead: Array<any> = [];
        if (tableHead.length > 0) {
            for (let index in tableHead) {
                let tableHeadItem = tableHead[index];
                let li =
                    <th
                        key={index + '_' + tableHeadItem}
                        className='smd-text-align-center th-smd-table'>
                        {this.props.language[tableHeadItem]}
                    </th>
                itemsHead.push(li);
            }
        }
        let tr =
            <tr>
                {itemsHead}
            </tr>
        return tr;
    }

    createItems(services: Array<ServicesDTO>) {
        let items: Array<ServicesDTO> = [];
        if (services.length > 0) {
            for (let index in services) {
                let li: any =
                    <tr
                        key={index}
                        className=''
                    >
                        <td className=' td-smd-table'>{services[index].Description}</td>
                        <td className='text-center td-smd-table'>{services[index].Status}</td>
                        <td className='text-center td-smd-table'>{services[index].LastRun !== null ?
                                moment(services[index].LastRun).locale(ObjectHelper.getLocaleForMomentDateTime(this.props.language[Vocabulary.LANGUAGE] || 'us')).format('L') + '  ' + 
                                moment(services[index].LastRun).locale(ObjectHelper.getLocaleForMomentDateTime(this.props.language[Vocabulary.LANGUAGE] || 'us')).format('LT') 
                            :   services[index].LastRun}</td>
                        <td className='text-center td-smd-table'>{services[index].VersionLastRun}</td>
                        <td className='text-center td-smd-table'>{services[index].ScheduleTypeid === 1 ? this.cycleSecFormatted(services[index].CycleMS) : 'once per day'}</td>
                        <td className='text-center td-smd-table'>{serverLogingLevel[services[index].LoggingLevel].label}</td>
                        <td className='text-center td-smd-table'>
                            <div className='row justify-content-between'>
                                <button
                                    className='btn smd-10-margin-bottom'
                                    name={'details' + index}
                                    onClick={() => {
                                        this.openModalDetails(services[index], services[index].ServiceId);
                                    }}
                                >{this.props.language[Vocabulary.BUTTON_DETAILS]}</button>
                                <button
                                    className='btn smd-10-margin-bottom'
                                    name={'log' + index}
                                    onClick={() => {
                                        this.openModalLog(services[index].ServiceId);
                                    }}
                                >{this.props.language[Vocabulary.BUTTON_LOG]}</button>
                            </div>
                        </td>
                    </tr>
                items.push(li);
            }
        }
        return items;
    }

    render() {
        let items = this.createItems(this.props.services);
        return (
            <div className="container-fluid">
                <dl className='row smd-v-padding'>
                    <dd className='col'>
                        <h5>{this.props.language[Vocabulary.HEADER_SERVICES]}</h5>
                    </dd>
                </dl>
                <div className='col'>
                    <div className='row justify-content-between smd-10-margin-bottom'>
                        <button
                            name='CommonLog'
                            className='btn'
                            onClick={() => {
                                this.openModalLog(-1);
                            }}
                        >
                            {this.props.language[Vocabulary.BUTTON_COMMON_LOG]}
                        </button>
                        <button
                            name='RefreshServices'
                            className='btn'
                            onClick={() => {
                                this.props.getSystemServices();
                            }}
                        >
                            {this.props.language[Vocabulary.BUTTON_REFRESH]}
                        </button>
                    </div>
                </div>
                <div className='smd-overflow-scroll'>
                    <table
                        id={'tableRecords'}
                        className='table table-hover'>
                        <thead>
                            {this.createTableHead(ServicesTableHeaders)}
                        </thead>
                        <tbody>
                            {items || ''}
                        </tbody>
                    </table>
                </div>
                <ModalWindow
                    visible={this.state.modalVisibleDetails}
                    title={this.props.language[Vocabulary.SERVICE_INFORMATION]}
                    cancel={this.closeModalDetails}
                    id='ModalDetails'
                    success={() => {
                        this.updateServicesStatus();
                        this.closeModalDetails();
                    }}>
                    <div className='row smd-10-margin-bottom'>
                        <label className='col-4'>{this.props.language[Vocabulary.TABLE_DESCRIPTION]}:</label>
                        <label id={'Description'} className='col-8 smd-no-padding'>{this.state.Description}</label>
                    </div>
                    <div className='row smd-10-margin-bottom'>
                        <label className='col-4 '>{this.props.language[Vocabulary.TABLE_LOGGING_LEVEL]}</label>
                        <Select
                            name={'Select'}
                            className={'col-4 smd-no-padding'}
                            options={serverLogingLevel}
                            value={serverLogingLevel[this.state.LoggingLevel]}
                            clearable={false}
                            onChange={(e) => {
                                this.changeSelect(e, 'LoggingLevel');
                            }}
                        />
                    </div>
                    <div className='row smd-10-margin-bottom'>
                        <label className='col-4'>{this.props.language[Vocabulary.TABLE_CYCLE_SEC]}</label>
                        <input
                            type='text'
                            className='col-4 form-control'
                            name={'Cycle'}
                            value={this.state.CycleMS}
                            onChange={(e) => {
                                this.changeInput(e);
                            }}
                        />
                    </div>
                    <div className='row smd-10-margin-bottom'>
                        <label className='col-4'>{this.props.language[Vocabulary.MODAL_STATUS]}</label>
                        <ToggleButton
                            name={'toggle'}
                            isChecked={this.state.Status}
                            onChange={this.changeToggle}
                        />
                    </div>
                </ModalWindow>
                <ModalWindow
                    visible={this.state.modalVisibleLog}
                    title={this.props.language[Vocabulary.SERVICE_LOG_RECORDS]}
                    id='ModalLogs'
                    cancel={this.closeModalLog}
                    dialogIsLarge={true}
                    btnSuccessShow={false}
                >
                    <div className='row smd-10-margin-bottom'>
                        <label className='col-lg-3 col-4 '>{this.props.language[Vocabulary.TABLE_LOGGING_LEVEL]}</label>
                        <span className='col-md-3 col-4'>
                            <Select
                                name={'Select'}
                                className={'col-12 smd-no-padding'}
                                options={serverLogingLevel}
                                value={serverLogingLevel[this.state.LoggingLevelInLogModal]}
                                clearable={false}
                                onChange={(e) => {
                                    this.changeSelect(e, 'LoggingLevelInLogModal');
                                }}
                            />
                        </span>
                        <button
                            name='Refresh'
                            className='btn'
                            onClick={() => {
                                this.getLogRecords(this.state.selectedServices);
                            }}
                        >
                            {this.props.language[Vocabulary.BUTTON_REFRESH]}
                        </button>
                    </div>
                </ModalWindow>
            </div>
        );
    }
}

export function mapStateToProps(state, ownProps) {
    return {
        services: state.snapmdAdmin.services as Array<ServicesDTO>,
        language: state.site.misc.vocabulary as any
    }
}

export function mapDispatchToProps(dispatch) {
    return {
        getSystemServices: () => {
            dispatch(servicesActions.getServices(SnapMDServiceLocator));
        },
        getServiceLogRecords: (serviceId: number, logparameters: any) => {
            dispatch(servicesActions.getServiceLogRecords(SnapMDServiceLocator, serviceId, logparameters));
        },
        updateStateServices: (serviceStatus: any, loggingLevel: any) => {
            dispatch(servicesActions.updateStateServices(SnapMDServiceLocator, serviceStatus, loggingLevel));
        }
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(SmdaServicesCmpnt);