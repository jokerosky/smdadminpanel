import { connect } from 'react-redux';
import { MenuItemCmpnt } from '../common/menuItemCmpnt';
import { MenuItem, TopMenuItem } from '../../models/site/menuItems';
import { toAbsolutePath, Endpoints } from '../../enums/endpoints';

import { ISnapMdAdminAppProps } from './SnapMdAdminApp';
import * as Vocabulary from '../../localization/vocabulary';

export function getMainMenuItems(options: ISnapMdAdminAppProps) {
    let result = [];
    let path = '';
    if (options.hospitalId !== null && options.clientView !== '') {
        path = `/${options.hospitalId}/${options.clientView}`;
    }

    let clientMenu = {
        key: 'dropdown1',
        name: options.language[Vocabulary.HEADER_CLIENTS],
        items: [
            {
                key: '1mm',
                link: toAbsolutePath(Endpoints.site.snapMdAdmin.dashboard + path),
                text: options.language[Vocabulary.HEADER_CLIENTS],
                icoClass: 'icon_dashboard',
                activeClass: 'active',
            },
            {
                key: '9mm',
                link: toAbsolutePath(Endpoints.site.snapMdAdmin.addnewclient),
                text: 'Add Client',
                icoClass: 'icon_dashboard',
                activeClass: 'active',
                action: () => { }
            }
        ]
    };

    let reportsMenu = {
        key: 'dropdown2',
        name: options.language[Vocabulary.HEADER_REPORTS],
        items: [
            {
                key: '2mm',
                linkClass: 'disabled',
                text: options.language[Vocabulary.HEADER_REPORTS],
                icoClass: 'fa file-text',
                activeClass: 'disabled',
                action: () => { }
            },
            {
                key: '6mm',
                linkClass: '',
                link: toAbsolutePath(Endpoints.site.snapMdAdmin.consultationmeetings),
                text: options.language[Vocabulary.HEADER_CONSULTATION_MEETINGS],
                icoClass: 'fa file-text',
                activeClass: 'active',
                action: () => { }
            },
            {
                key: '60mm',
                linkClass: '',
                link: toAbsolutePath(Endpoints.site.snapMdAdmin.consultationMeetingsV2),
                text: options.language[Vocabulary.HEADER_CONSULTATION_MEETINGS] + ' v2',
                icoClass: 'fa file-text',
                activeClass: 'active',
                action: () => { }
            }
        ]
    };

    let logsMenu = {
        key: 'dropdown3',
        name: options.language[Vocabulary.HEADER_LOGS],
        items: [
            {
                key: '4mm',
                linkClass: '',
                link: toAbsolutePath(Endpoints.site.snapMdAdmin.loggedmessages),
                text: options.language[Vocabulary.HEADER_VIEW_LOGS],
                icoClass: 'fa file-text',
                activeClass: 'active',
                action: () => { }
            },
            {
                key: '5mm',
                linkClass: '',
                link: toAbsolutePath(Endpoints.site.snapMdAdmin.integrationlogs),
                text: options.language[Vocabulary.HEADER_INTEGRATION_LOGS],
                icoClass: 'fa file-text',
                activeClass: 'active',
                action: () => { }
            }
        ]
    };

    result.push(
        /*  new MenuItemCmpnt({
              key: '1mm',
              link: toAbsolutePath(Endpoints.site.snapMdAdmin.dashboard + path),
              text: options.language[Vocabulary.HEADER_CLIENTS],
              icoClass: 'icon_dashboard',
              activeClass: 'active',
          } as TopMenuItem).renderBootstrapListElement(),
  */
        new MenuItemCmpnt({
            dropDown: clientMenu,
        } as TopMenuItem).renderDropDownMenu(),


        new MenuItemCmpnt({
            dropDown: reportsMenu,
        } as TopMenuItem).renderDropDownMenu(),


        new MenuItemCmpnt({
            key: '3mm',
            link: toAbsolutePath(Endpoints.site.snapMdAdmin.services),
            text: options.language[Vocabulary.HEADER_SERVICES],
            activeClass: 'active',
            action: () => { }
        } as TopMenuItem).renderBootstrapListElement(),

        
        new MenuItemCmpnt({
            dropDown: logsMenu,
        } as TopMenuItem).renderDropDownMenu(),


        new MenuItemCmpnt({
            key: '7mm',
            linkClass: '',
            link: toAbsolutePath(''),
            text: options.language[Vocabulary.HEADER_LOG_OUT],
            icoClass: 'fa file-text',
            activeClass: 'active',
            action: () => {
                options.logOut();
            }
        } as TopMenuItem).renderBootstrapListElement(),
    )

    return result;
};