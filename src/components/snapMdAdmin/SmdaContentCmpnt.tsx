import * as React from 'react';
import { } from 'react-router-redux';
import { Link } from 'react-router';
import * as _ from 'lodash';

import { MenuItem } from '../../models/site/menuItems';
import { toAbsolutePath, Endpoints } from '../../enums/endpoints';
import * as Vocabulary from '../../localization/vocabulary';
import SmdaAddNewClientCmpnt from './SmdaAddNewClient';

export interface ISmdaContentProps {
    dashboardChildren?: any;
    selectedView?: string;
    selectedId?: any;
    language: any;
    path: string;

    selectView?: (id: number, view: string) => void;
};

export class SmdaContentCmpnt extends React.Component<ISmdaContentProps, any>{
    constructor(props) {
        super(props);
        this.componentMenuCreate(props);
    }

    menuItems: MenuItem[] = [];

    componentWillReceiveProps(nextProps) {
        if (!_.isEqual(nextProps.language, this.props.language)) {
            this.createMenuItems(nextProps);
        }
    }

    componentMenuCreate(props) {
        if (_.keys(props.language).length > 0) {
            this.createMenuItems(this.props);
        }
    }


    createMenuItems(props) {
        this.menuItems = [
            {
                key: Endpoints.site.snapMdAdmin.detailsPostfix,
                text: props.language[Vocabulary.SNAPMDADMIN_DETAILS]
            } as MenuItem,
            /* { 
                 key: Endpoints.site.snapMdAdmin.settingsPostfix,
                 text: props.language[Vocabulary.SETTINGS] 
             } as MenuItem,*/
            {
                key: Endpoints.site.snapMdAdmin.settingsv2Postfix,
                text: props.language[Vocabulary.SETTINGS]
            } as MenuItem,
            {
                key: Endpoints.site.snapMdAdmin.opHoursPostfix,
                text: props.language[Vocabulary.SNAPMDADMIN_HOSPITAL_HOURS]
            } as MenuItem,
            {
                key: Endpoints.site.snapMdAdmin.smsTemplatePostfix,
                text: props.language[Vocabulary.SNAPMDADMIN_SMS_TEMPLATE]
            } as MenuItem,
            {
                key: Endpoints.site.snapMdAdmin.codeSetsPostfix,
                text: props.language[Vocabulary.SNAPMDADMIN_CODE_SETS]
            } as MenuItem,
            {
                key: Endpoints.site.snapMdAdmin.soapCodesPostfix,
                text: props.language[Vocabulary.SNAPMDADMIN_SOAP_CODES]
            } as MenuItem,
            {
                key: Endpoints.site.snapMdAdmin.hospitalDocPostfix,
                text: props.language[Vocabulary.SNAPMDADMIN_HOSPITAL_DOC]
            } as MenuItem,
            {
                key: Endpoints.site.snapMdAdmin.adminPostfix,
                text: 'Admin',
                activeClass: 'disabled smd-cursor-not-allowed'
            } as MenuItem,
            {
                key: Endpoints.site.snapMdAdmin.locationsPostfix,
                text: props.language[Vocabulary.SNAPMDADMIN_LOCATIONS]
            } as MenuItem,
            {
                key: Endpoints.site.snapMdAdmin.organizationsPostfix,
                text: props.language[Vocabulary.SNAPMDADMIN_ORGANIZATIONS]
            } as MenuItem,
            {
                key: Endpoints.site.snapMdAdmin.communicationsPostfix,
                text: 'Communications',
                activeClass: 'disabled smd-cursor-not-allowed'
            } as MenuItem
        ];
    }


    renderItems(): any {
        let result = [];
        for (let index in this.menuItems) {
            //client view == view → active

            let active = this.props.selectedView == this.menuItems[index].key ? 'active' : '';
            let regActive = /disabled/i;
            let item =
                <li className={`nav-item`} key={this.menuItems[index].key}
                    onClick={() => {
                        if (!regActive.test(this.menuItems[index].activeClass)) {
                            this.props.selectView(this.props.selectedId, this.menuItems[index].key);
                        }
                    }}
                >
                    <Link
                        to={regActive.test(this.menuItems[index].activeClass) ? null : this.menuItems[index].key}
                        activeClassName={`nav-link ${active}`}
                        className={`nav-link ${active} ${this.menuItems[index].activeClass || ''}`}
                    >
                        {this.menuItems[index].text}
                    </Link>
                </li>
            result.push(item);
        }
        return result;
    }

    showContent() {
        let content = null;
        let menuItems = this.renderItems();
        let path = this.props.path.split('/');
        if (path.indexOf(Endpoints.site.snapMdAdmin.addNewClientPostfix) != path.length -1) {
            content =
                <div id='contentView'>
                    <ul className='nav col-12 smd-border-bottom nav-pills nav-fill'>
                        {menuItems}
                    </ul>
                    {this.props.selectedId > -1 ? this.props.dashboardChildren : "No client is selected"}
                </div>
        } else {
            content = <SmdaAddNewClientCmpnt />
        }

        return content;
    }

    render() {

        return (
            <div className='col-12'>
                {this.showContent()}
            </div>
        );
    }
};
