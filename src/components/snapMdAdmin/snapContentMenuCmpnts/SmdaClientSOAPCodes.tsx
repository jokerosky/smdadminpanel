import * as React from 'react';
import { connect } from 'react-redux';

import * as snapmdAdminActions from '../../../actions/snapMdAdminActions';
import * as diagnosticCodingActions from '../../../actions/snapMdAdminClientActions/soapCodesActions';
import { SnapMDServiceLocator } from '../../../dependenciesInjectionConfiguration';
import { LocalizationService } from '../../../localization/localizationService';
import * as Vocabulary from '../../../localization/vocabulary';

export interface SmdaClientSOAPCodesProps{
    hospitalId:number;
    hospitalName:string;
    soapDiagnosticCodingSystem:string;
    language:any;
    
    saveDiagnosticCodingSettings:(hospitalId:number, diagnosticCodingSystem:string)=>void;
    deleteDiagnosticCodingSettings:(hospitalId:number)=>void;
    loadDiagnosticCodingSettings:(hospitalId:number)=>void;
}
export class SmdaClientSOAPCodesCmpnt extends React.Component<SmdaClientSOAPCodesProps, any>{
    constructor(props){
        super(props);


    let showRemoveDCSButton = props.soapDiagnosticCodingSystem ? true : false;

    this.state = {
        showRemoveDCSButton,
        selectedSoapDCS:  props.soapDiagnosticCodingSystem
    }

        this.renderRadioButton = this.renderRadioButton.bind(this);
    }


    componentWillReceiveProps(nextProps:SmdaClientSOAPCodesProps ) {
        let showRemoveDCSButton = nextProps.soapDiagnosticCodingSystem ? true : false;
        this.setState({
            showRemoveDCSButton,
            selectedSoapDCS: nextProps.soapDiagnosticCodingSystem
        });

        if(nextProps.hospitalId !== this.props.hospitalId){
            this.props.loadDiagnosticCodingSettings(nextProps.hospitalId);
        }

    }

    componentDidMount(){
        this.props.loadDiagnosticCodingSettings(this.props.hospitalId);
    }

    removeBtnClick(){
        this.setState({
            showRemoveDCSButton:false
        })
    }

    saveBtnClick(){
        this.setState({
            showRemoveDCSButton:true
        })
    }

    changeSelectedSoapDCS(name){

        this.setState({
            selectedSoapDCS:name
        })
    }

    renderRadioButton(name:string, idRadioButton:string){
        let checked = false;
        checked = name == this.state.selectedSoapDCS;
        let li =
        <div className='form-check'>
            <label className='form-check-label smd-cursor-pointer'>
                <input 
                    id={idRadioButton}
                    name='radios'
                    type='radio' 
                    className='form-check-input smd-cursor-pointer' 
                    value={name}
                    onChange={()=>{
                        this.changeSelectedSoapDCS(name);
                        }}    
                    checked = {checked}
                />
                {name}
            </label>
        </div>
        return li;
    }

    render(){
        return(
            <div className="container-fluid">
                <dl className='row smd-v-padding'>
                    <dd className='col'>
                        <h5 id='name'>{this.props.hospitalName} - {this.props.language[Vocabulary.SNAPMDADMIN_SOAP_CODES_SETTING]}</h5>
                    </dd>
                </dl>
                <div className='col'>
                    <div className='row'>
                        <h5>{this.props.language[Vocabulary.SNAPMDADMIN_CHOOSE_WHAT_MEDICAL_CODES]}:</h5>
                    </div>
                </div>
                <fieldset className="form-group">
                    {this.renderRadioButton('ICD-9-DX','optionsRadios1')}
                    {this.renderRadioButton('ICD-10-DX','optionsRadios2')}
                    {this.renderRadioButton('SNOMED-CT', 'optionsRadios3')}
                </fieldset>
                <div>
                    { this.state.showRemoveDCSButton && <button
                        id={'RemoveDiagnostic'}
                        className='btn smd-cursor-pointer'
                        onClick={()=>{
                            this.removeBtnClick();
                            this.props.deleteDiagnosticCodingSettings(this.props.hospitalId);
                        }}
                    >
                        {this.props.language[Vocabulary.SNAPMDADMIN_REMOVE_DIAGNOSTIC_CODING_SYSTEM]}
                    </button>}
                </div>
                <div className='row justify-content-end'>
                    <div className='col-2'>
                        <button
                            id={'Save'}
                            className='btn btn-success smd-cursor-pointer'
                            onClick={()=>{   
                                this.saveBtnClick();           
                                this.props.saveDiagnosticCodingSettings(this.props.hospitalId, this.state.selectedSoapDCS);
                            }}
                        >
                            {this.props.language[Vocabulary.SNAPMDADMIN_BUTTON_SAVE]}
                        </button>
                    </div>
                </div>
                <hr />

                
            </div>
        )
    }
}

export function mapStateToProps(state, ownProps){
    return{
        hospitalId: state.snapmdAdmin.misc.selectedId as number,
        hospitalName: state.snapmdAdmin.misc.hospitalName as string,
        soapDiagnosticCodingSystem: state.snapmdAdmin.client.soapCodes as string,
        language: state.site.misc.vocabulary as any
    }
}

export function mapDispatchToProps(dispath){
    return{
        saveDiagnosticCodingSettings:(hospitalId:number, diagnosticCodingSystem:string)=>{
            if(hospitalId && diagnosticCodingSystem){
                dispath(diagnosticCodingActions.updateDiagnosticCodingSettings(SnapMDServiceLocator, hospitalId, diagnosticCodingSystem));
            }
        },
        deleteDiagnosticCodingSettings:(hospitalId:number)=>{
            if(hospitalId){
                dispath(diagnosticCodingActions.deleteDiagnosticCodingSettings(SnapMDServiceLocator, hospitalId));
            }
        },
        loadDiagnosticCodingSettings:(hospitalId:number)=>{
            dispath(diagnosticCodingActions.loadDiagnosticCodingSettings(SnapMDServiceLocator, hospitalId));
        }
    }
}
export default connect(mapStateToProps, mapDispatchToProps)(SmdaClientSOAPCodesCmpnt);