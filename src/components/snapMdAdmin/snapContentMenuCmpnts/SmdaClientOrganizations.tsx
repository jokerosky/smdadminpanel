import * as React from 'react';
import { connect } from 'react-redux';
import Select from 'react-select';

import { ModalWindow } from '../../common/ModalWindow';
import * as snapmdAdminActions from '../../../actions/snapMdAdminActions';
import { SnapMDServiceLocator } from '../../../dependenciesInjectionConfiguration';
import * as organizationActions from '../../../actions/snapMdAdminClientActions/organizationActions';
import {EditorActions} from '../../../enums/siteEnums';
import { OrganizationDTO } from '../../../models/DTO/organizationDTO';
import { LocalizationService } from '../../../localization/localizationService';
import * as Vocabulary from '../../../localization/vocabulary';

export interface ISmdaOrganizationsCmpntProps{
    hospitalId:number;
    hospitalName:string;
    organizationTypes:any;
    organization:any;
    language:any;

    loadOrganizations:(typeOrganization:number, hospitalId:number)=>void;
    addAndUpdateOrganization:(organization:OrganizationDTO, methodAddOrUpdate:string)=>void;
}

export class SmdaOrganizationsCmpnt extends React.Component<ISmdaOrganizationsCmpntProps, any>{
    constructor(props){
        super(props);

        this.state = {
            modalVisible:false,
            organizations:[],
            selectedOption: 0,
            name:'',
            id: -1,
            methodAddOrUpdate:''
        }
        this.addOrganization = this.addOrganization.bind(this);
    }

    componentWillReceiveProps(nextProps){
        if(nextProps.hospitalId !== this.props.hospitalId){
            this.props.loadOrganizations(this.state.selectedOption, nextProps.hospitalId);
        }
    }

    componentDidMount(){
        this.props.loadOrganizations(this.state.selectedOption, this.props.hospitalId);
    }

    addOrganization(){
          let  organization: any = {
                hospitalId: this.props.hospitalId,
                name: this.state.name,
                organizationTypeId: this.state.selectedOption,
            };
          if (this.state.methodAddOrUpdate === EditorActions.edit) {
              organization['id'] = this.state.id;
          }

        this.props.addAndUpdateOrganization(organization, this.state.methodAddOrUpdate);
    }

    selectOrganization(organizationName:string, id?:number){
        this.setState({
                name: organizationName,
                id: id
        });
    }

    changeSelect = (val) => {
        this.setState({ selectedOption: val.value });
        this.props.loadOrganizations(val.value, this.props.hospitalId);
    }

    changeInput = (val) => {
        this.setState({ 
                name: val.currentTarget.value.trim()
        });
    }

    openModal(methodAddOrUpdate:string){
        this.setState({
            modalVisible: true,
            methodAddOrUpdate: methodAddOrUpdate
        });
    }

    closeModal = ()=>{
        this.setState({
            modalVisible: false,
            methodAddOrUpdate: ''
        });
    }

    renderItem(organization:any){
        let item:Array<any>=[];
        if(organization.length > 0){
            item=[];
            let arrayBool = false;
            if (organization instanceof Array) {
                arrayBool = true;
            }
            for(let index in organization){
                let types;
                types = arrayBool ? organization[index] : organization;
                let li =
                    <tr key={index} className='row'>
                        <td className='col-10 td-smd-table'>{types.name}</td>
                        <td className='col-2 td-smd-table'>
                            <button 
                                className='btn smd-cursor-pointer'
                                title={this.props.language[Vocabulary.SNAPMDADMIN_EDIT]}
                                id={index + 'Edit'}
                                onClick={() => {
                                        this.selectOrganization(types.name, types.id);
                                        this.openModal(EditorActions.edit);
                                    }
                                }
                                >
                                <i className="fa fa-pencil" aria-hidden="true" title={this.props.language[Vocabulary.SNAPMDADMIN_EDIT]}></i>
                            </button>
                        </td>
                    </tr>
                item.push(li);
                if (arrayBool === false)
                    break;
            }
        }
        return item;
    }

    render(){

        let items = this.props.organization ? this.renderItem(this.props.organization) : '';
        return(
            <div className="container-fluid">
                <dl className='row smd-v-padding'>
                    <dd className='col'>
                        <h5>{this.props.hospitalName} - {this.props.language[Vocabulary.SNAPMDADMIN_ORGANIZATIONS]}</h5>
                    </dd>
                </dl>
                <div className='row smd-v-padding'>
                    <span className='col-4'>{this.props.language[Vocabulary.SNAPMDADMIN_ORGANIZATION_TYPE]}</span>
                    <div className='col-6'>
                        <Select 
                            name={'Select'}
                            options={this.props.organizationTypes}
                            value={this.props.organizationTypes[this.state.selectedOption-1]}
                            clearable={false}
                            onChange={(e) => {
                                this.changeSelect(e);
                            }}
                        />
                    </div>
                </div>
                <div className='col smd-v-padding'>
                    <div className='row' >
                        <button className='btn align-self-center smd-cursor-pointer'
                            name={'Record'}
                            key={'AddNewRecord'}
                            onClick={() => {
                                this.selectOrganization('');
                                this.openModal(EditorActions.add);
                                }
                            }
                        >{this.props.language[Vocabulary.SNAPMDADMIN_ADD_NEW_RECORD]}</button>
                    </div>
                </div>
                <div className='col'>
                    <table className='table table-hover'>
                        <thead>
                            <tr className='row'>
                                <th className='col-10 th-smd-table'>{this.props.language[Vocabulary.SNAPMDADMIN_CLIENT_NAME]}</th>
                                <th className='col th-smd-table'></th>
                            </tr>
                        </thead>
                        <tbody>
                            {items || ''}
                        </tbody>
                    </table>
                </div>
                <ModalWindow
                    visible={this.state.modalVisible}
                    title={'Edit'}
                    cancel={this.closeModal}
                    success={() => { this.addOrganization(); this.closeModal();}}
                >
                    <div className='row justify-content-center'>
                        <span className='col-2'>{this.props.language[Vocabulary.SNAPMDADMIN_CLIENT_NAME]}</span>
                        <input 
                            name='Organization'
                            placeholder={this.props.language[Vocabulary.SNAPMDADMIN_ORGANIZATION]}
                            value={this.state.name}
                            onChange={(e)=>{
                                this.changeInput(e);
                            }}
                            />
                    </div>
                </ModalWindow>
            </div>
        );
    }
}

let organizations = [];

function createObjectCodeSetsTypeForSelect(props){ 
    if(organizations.length > 0 && organizations.length === props.length){
        return organizations;
    } else {
        for(let index in props){
            organizations.push({
                value:+index+1,
                label:props[index].description                
            });
        }
        return organizations;
    }
}

export function mapStateToProps(state, ownProps){
    return{
        hospitalId: state.snapmdAdmin.misc.selectedId as number,
        hospitalName: state.snapmdAdmin.misc.hospitalName as string,
        organizationTypes: createObjectCodeSetsTypeForSelect(state.snapmdAdmin.lists.organizationTypes) as any,
        organization: state.snapmdAdmin.client.organizations as any,
        language: state.site.misc.vocabulary as any
    }
}

export function mapDispatchToProps(dispatch){
    return{
        loadOrganizations:(typeOrganization:number, hospitalId:number)=>{
            dispatch(organizationActions.loadOrganizations(SnapMDServiceLocator, typeOrganization, hospitalId));
        },
        addAndUpdateOrganization:(organization:any, methodAddOrUpdate:string)=>{
            dispatch(organizationActions.organizationAddOrUpdate(SnapMDServiceLocator, organization, methodAddOrUpdate));
        }
    }
}
export default connect(mapStateToProps, mapDispatchToProps)(SmdaOrganizationsCmpnt);