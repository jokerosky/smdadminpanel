import * as React from 'react';
import Select from 'react-select';
import * as _   from 'lodash';

import * as Vocabulary from '../../../../localization/vocabulary';
import { language } from '../../../../enums/languages';
import { getHospitalPaymentPartnerKeys } from '../../../../models/DTO/hospitalSettingDTO';

export interface IPaymentPartnerProps {
    language: any;
    partners: any;
    hospitalId: number;

    setPropertyState: (field: string, value: any) => void;
    getPropertyState: (field: string) => any;
    saveComponentSettings: (settings: any) => any;
    integrationsHospitalsPaymentGateway: (data: any, hospitalId: number) => void;
}

export class PaymentPartner extends React.Component<IPaymentPartnerProps, any> {
    constructor() {
        super();

        this.state = {
            paymentPartnerSelect: 0,
            fieldTemplate: 'AuthorizeNet_'
        }
    }

    optionsPaymentPartnerSelect = [
        {
            value: 0,
            label: 'AuthorizeNet'
        },
        {
            value: 1,
            label: 'Chargify'
        }
    ];

    paymentPartnerAuthorizeNetForm = [
        {
            name: Vocabulary.SNAPMDADMIN_SETTINGSV2_AUTORIZE_NET_BASE_URL,
            key: getHospitalPaymentPartnerKeys().AuthorizeNet_BaseUrl
        },
        {
            name: Vocabulary.SNAPMDADMIN_SETTINGSV2_AUTORIZE_NET_POST_URL,
            key: getHospitalPaymentPartnerKeys().AuthorizeNet_PostURL
        },
        {
            name: Vocabulary.SNAPMDADMIN_SETTINGSV2_AUTORIZE_NET_TRANSACTION_KEY,
            key: getHospitalPaymentPartnerKeys().AuthorizeNet_TransactionKey
        },
        {
            name: Vocabulary.SNAPMDADMIN_SETTINGSV2_AUTORIZE_NET_LOGIN_ID,
            key: getHospitalPaymentPartnerKeys().AuthorizeNet_LoginID
        }
    ];

    paymentPartnerChargifyForm = [
        {
            name: Vocabulary.SNAPMDADMIN_SETTINGSV2_CHARGIFY_SUBDOMAIN,
            key: getHospitalPaymentPartnerKeys().Chargify_Subdomain
        },
        {
            name: Vocabulary.SNAPMDADMIN_SETTINGSV2_CHARGIFY_API_V1_KEY,
            key: getHospitalPaymentPartnerKeys().Chargify_V1Key
        },
        {
            name: Vocabulary.SNAPMDADMIN_SETTINGSV2_CHARGIFY_API_V2_KEY_ID,
            key: getHospitalPaymentPartnerKeys().Chargify_V2ApiId
        },
        {
            name: Vocabulary.SNAPMDADMIN_SETTINGSV2_CHARGIFY_API_V2_SHARED_SECRET,
            key: getHospitalPaymentPartnerKeys().Chargify_V2SharedSecret
        },
        {
            name: Vocabulary.SNAPMDADMIN_SETTINGSV2_CHARGIFY_API_V2_PASSWORD,
            key: getHospitalPaymentPartnerKeys().Chargify_V2Password
        }
    ];

    renderItem(label, key) {
        return (
            <div key={key} className="form-group">
                <label htmlFor={key}>{this.props.language[label]}</label>
                <input
                    type="text"
                    className="form-control"
                    id={key}
                    value={this.props.getPropertyState(key)}
                    onChange={(e: any) => {
                        this.props.setPropertyState(key, e.target.value);
                    }}
                />
            </div>
        );
    }

    renderElement() {
        let element: Array<any> = [];
        if (this.state.paymentPartnerSelect === 0) {
            for (let index in this.paymentPartnerAuthorizeNetForm) {
                element.push(this.renderItem(this.paymentPartnerAuthorizeNetForm[index].name, this.paymentPartnerAuthorizeNetForm[index].key));
            }
            element.push(
                <div key={1} className="form-group">
                    <button
                        id={'AuthorizeNetSave'}
                        className='btn'
                        onClick={() => { this.saveSettings(); }}
                    >{this.props.language[Vocabulary.SNAPMDADMIN_SAVE_SETTINGS]}</button>
                </div>,
                <div key={2} className="form-group">
                    <button
                        className='btn'
                        onClick={() => { this.savePartner();}}
                    >{this.props.language[Vocabulary.SNAPMDADMIN_SETTINGSV2_SAVE_SETTINGS_AND_SWITCH_TO_AUTORIZE_NET]}</button>
                </div>
            );
            return element;
        } else if (this.state.paymentPartnerSelect === 1) {
            for (let index in this.paymentPartnerAuthorizeNetForm) {
                element.push(this.renderItem(this.paymentPartnerChargifyForm[index].name, this.paymentPartnerChargifyForm[index].key));
            }
            element.push(
                <div key={1} className="form-group">
                    <button
                        className='btn'
                        onClick={() => { this.saveSettings(); }}
                    >{this.props.language[Vocabulary.SNAPMDADMIN_SAVE_SETTINGS]}</button>
                </div>,
                <div key={2} className="form-group">
                    <button
                        className='btn'
                        onClick={() => { this.savePartner();}}
                    >{this.props.language[Vocabulary.SNAPMDADMIN_SETTINGSV2_SAVE_SETTINGS_AND_SWITCH_TO_CHARGIFY]}</button>
                </div>
            );
            return element;
        }
    }

    saveSettings(){
        let regExp: RegExp = new RegExp(this.state.fieldTemplate+'[a-zA-z]{1,}', 'i');
        let settings: any = {};
        for(let index in getHospitalPaymentPartnerKeys()){
            if(index.search(regExp) === 0){
                settings[index] = this.props.getPropertyState(index);
            }
        }

        this.props.saveComponentSettings(settings);
    }

    savePartner(){
        this.saveSettings();

        let partnerId: any = {};
        let searchIndex = _.findIndex(this.props.partners, {name: this.optionsPaymentPartnerSelect[this.state.paymentPartnerSelect].label});
        partnerId = {
            partnerId: this.props.partners[searchIndex].partnerId
        };
        this.props.integrationsHospitalsPaymentGateway(partnerId, this.props.hospitalId);
    }

    render() {
        return (
            <div>
                <div className="form-group">
                    <label htmlFor="clientName">{this.props.language[Vocabulary.SNAPMDADMIN_SETTINGSV2_PAYMENT_PARTNER]}</label>
                    <Select
                        options={this.optionsPaymentPartnerSelect}
                        value={this.state.paymentPartnerSelect}
                        clearable={false}
                        onChange={(e) => {
                            this.setState({
                                paymentPartnerSelect: e.value,
                                fieldTemplate: e.label + '_'
                            });
                            this.props.setPropertyState(getHospitalPaymentPartnerKeys().paymentPartnerSelect, e.value);
                        }}
                    />
                </div>
                {this.renderElement()}
            </div>
        );
    }
}