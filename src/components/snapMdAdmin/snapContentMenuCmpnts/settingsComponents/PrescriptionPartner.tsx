import * as React from 'react';
import Select from 'react-select';
import * as _   from 'lodash';

import * as Vocabulary from '../../../../localization/vocabulary';
import { language } from '../../../../enums/languages';
import { getHospitalePrescriptionPartnerKeys } from '../../../../models/DTO/hospitalSettingDTO';

export interface IPrescriptionPartnerProps {
    language: any;
    partners: any;
    hospitalId: number;

    setPropertyState: (field: string, value: any) => void;
    getPropertyState: (field: string) => any;
    saveComponentSettings: (settings: any) => any;
    integrationsHospitalsPartnerTypes: (data: any, hospitalId: number, partnerTypes: number) => void;
}

export class PrescriptionPartner extends React.Component<IPrescriptionPartnerProps, any> {
    constructor() {
        super();

        this.state = {
            prescriptionPartnerSelect: 0,
            fieldTemplate: 'MDToolbox_'
        }
    }

    optionsPrescriptionPartnerSelect = [
        {
            value: 0,
            label: 'MDToolbox'
        },
        {
            value: 1,
            label: 'Rxnt'
        }
    ];

    prescriptionPartnerMDToolboxForm = [
        {
            name: Vocabulary.SNAPMDADMIN_SETTINGSV2_SERVICE_URL,
            key: getHospitalePrescriptionPartnerKeys().MDToolbox_ServiceUrl
        },
        {
            name: Vocabulary.SNAPMDADMIN_SETTINGSV2_SINGLE_SIGN_ON_URL,
            key: getHospitalePrescriptionPartnerKeys().MDToolbox_SsoUrl
        },
        {
            name: Vocabulary.SNAPMDADMIN_SETTINGSV2_ACCOUNT_ID,
            key: getHospitalePrescriptionPartnerKeys().MDToolbox_AccountId
        },
        {
            name: Vocabulary.SNAPMDADMIN_SETTINGSV2_PRACTICE_ID,
            key: getHospitalePrescriptionPartnerKeys().MDToolbox_PracticeId
        },
        {
            name: Vocabulary.SNAPMDADMIN_SETTINGSV2_AUTHENTICATION_KEY,
            key: getHospitalePrescriptionPartnerKeys().MDToolbox_AuthenticationKey
        }
    ];

    prescriptionPartnerRxntForm = [
        {
            name: Vocabulary.SNAPMDADMIN_SETTINGSV2_SINGLE_SIGN_ON_URL,
            key: getHospitalePrescriptionPartnerKeys().RxNTEPrescriptionSingleSignonUrl
        },
        {
            name: Vocabulary.SNAPMDADMIN_SETTINGSV2_API_URL,
            key: getHospitalePrescriptionPartnerKeys().PrescriptionWebServiceUrl
        }
    ];

    renderItem(label, key) {
        return (
            <div key={key} className="form-group">
                <label htmlFor={key}>{this.props.language[label]}</label>
                <input
                    type="text"
                    className="form-control"
                    id={key}
                    value={this.props.getPropertyState(key)}
                    onChange={(e: any) => {
                        this.props.setPropertyState(key, e.target.value);
                    }}
                />
            </div>
        );
    }

    renderElement() {
        let element: Array<any> = [];
        if (this.state.prescriptionPartnerSelect === 0) {
            for (let index in this.prescriptionPartnerMDToolboxForm) {
                element.push(this.renderItem(this.prescriptionPartnerMDToolboxForm[index].name, this.prescriptionPartnerMDToolboxForm[index].key));
            }
            element.push(
                <div key={1} className="form-group">
                    <button
                        className='btn'
                        onClick={() => { this.saveSettings(); }}
                    >{this.props.language[Vocabulary.SNAPMDADMIN_SAVE_SETTINGS]}</button>
                </div>,
                <div key={2} className="form-group">
                    <button
                        className='btn'
                        onClick={() => { this.saveSettingsMDToolbox(); }}
                    >{this.props.language[Vocabulary.SNAPMDADMIN_SETTINGSV2_SAVE_SETTINGS_AND_SWITCH_TO_MDTOOLBOX]}</button>
                </div>
            );
            return element;
        } else if (this.state.prescriptionPartnerSelect === 1) {
            for (let index in this.prescriptionPartnerRxntForm) {
                element.push(this.renderItem(this.prescriptionPartnerRxntForm[index].name, this.prescriptionPartnerRxntForm[index].key));
            }
            element.push(
                <div key={1} className="form-group">
                    <button
                        className='btn'
                        onClick={() => { this.saveSettings(); }}
                    >{this.props.language[Vocabulary.SNAPMDADMIN_SAVE_SETTINGS]}</button>
                </div>,
                <div key={2} className="form-group">
                    <button
                        className='btn'
                        onClick={() => { this.saveSettingsMDToolbox(); }}
                    >{this.props.language[Vocabulary.SNAPMDADMIN_SETTINGSV2_SAVE_SETTINGS_AND_SWITCH_TO_RXNT]}</button>
                </div>
            );
            return element;
        }
    }

    saveSettings(){
        let regExp1: RegExp = new RegExp('('+this.state.fieldTemplate+'[a-zA-z]{1,})', 'i');
        let regExp2: RegExp = new RegExp('(\\w{1,}Select)', 'i');
        let settings: any = {};
        for(let index in getHospitalePrescriptionPartnerKeys()){
            if(index.search(regExp1) === 0 && index.search(regExp2) === -1){
                settings[index] = this.props.getPropertyState(index);
            }
        }
        this.props.saveComponentSettings(settings);
    }

    saveSettingsMDToolbox(){
        this.saveSettings();
        let partnerId: any = {};
        let searchIndex = _.findIndex(this.props.partners, {name: this.optionsPrescriptionPartnerSelect[this.state.prescriptionPartnerSelect].label});
        partnerId = {
            partnerId: this.props.partners[searchIndex].partnerId
        };        
        this.props.integrationsHospitalsPartnerTypes(partnerId, this.props.hospitalId, 3);
    }

    render() {
        return (
            <div>
                <div className="form-group">
                    <label htmlFor="clientName">{this.props.language[Vocabulary.SNAPMDADMIN_SETTINGSV2_EPRESCRIPTION_PARTNER]}</label>
                    <Select 
                        options={this.optionsPrescriptionPartnerSelect}
                        value={this.state.prescriptionPartnerSelect}
                        clearable={false}
                        onChange={(e) => {
                            this.setState({
                                prescriptionPartnerSelect: e.value,
                                fieldTemplate: e.value === 0 ? 'MDToolbox_' : '[^(MDToolbox_)]'
                            });
                            this.props.setPropertyState(getHospitalePrescriptionPartnerKeys().prescriptionPartnerSelect, e.value);
                        }}
                    />
                </div>
                {this.renderElement()}
            </div>
        );
    }
}