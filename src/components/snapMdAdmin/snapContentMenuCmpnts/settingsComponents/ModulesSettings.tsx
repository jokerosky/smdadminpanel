import * as React from 'react';

import * as Vocabulary from '../../../../localization/vocabulary';
import { MODULE_KEYS } from '../../../../enums/clientSettingsKeys';
import { ToggleButton } from '../../../common/inputs/ToggleButtonCmpnt';
import {getHospitalModulesKeys} from '../../../../models/DTO/hospitalSettingDTO';

export interface IModuleSettingsProps {
    language: any;

    setPropertyState: (field: string, value: any) => void;
    getPropertyState: (field: string) => any;
}

export class ModuleSettings extends React.Component<IModuleSettingsProps, any> {
    constructor() {
        super();
    }

    togglesParameters: any = [
        {
            label: Vocabulary.SNAPMDADMIN_SETTINGSV2_E_COMMERCE,
            key: getHospitalModulesKeys().mECommerce,
            child: [
                {
                    label: Vocabulary.SNAPMDADMIN_SETTINGSV2_HIDE_PAYMENT_PAGE_BEFOR_WAITING_ROOM,
                    key: getHospitalModulesKeys().mHidePaymentBeforeWaiting
                }
            ]
        },
        {
            label: Vocabulary.SNAPMDADMIN_SETTINGSV2_INS_VERIFICATION,
            key: getHospitalModulesKeys().mInsVerification,
            child: [
                {
                    label: Vocabulary.SNAPMDADMIN_SETTINGSV2_INS_VERIFICATION_TESTING_COMMANDS,
                    key: getHospitalModulesKeys().mInsVerificationDummy
                },
                {
                    label: Vocabulary.SNAPMDADMIN_SETTINGSV2_INSURANCE_VERIFICATION_BEFORE_WAITING_ROOM,
                    key: getHospitalModulesKeys().mInsVerificationBeforeWaiting
                }
            ]
        },
        {
            label: Vocabulary.SNAPMDADMIN_SETTINGSV2_E_PRESCRIPTIONS,
            key: getHospitalModulesKeys().mEPerscriptions,
            child: [
                {
                    label: Vocabulary.SNAPMDADMIN_SETTINGSV2_SCHEDULE_1,
                    key: getHospitalModulesKeys().mEPSchedule1
                }
            ]
        },
        {
            label: Vocabulary.SNAPMDADMIN_SETTINGSV2_ON_DEMAND,
            key: getHospitalModulesKeys().mOnDemand
        },
        {
            label: Vocabulary.SNAPMDADMIN_SETTINGSV2_INTAKE_FROM,
            key: getHospitalModulesKeys().mIntakeForm,
            child: [
                {
                    label: Vocabulary.SNAPMDADMIN_SETTINGSV2_ON_DEMAND,
                    key: getHospitalModulesKeys().mIFOnDemand
                },
                {
                    label: Vocabulary.SNAPMDADMIN_SETTINGSV2_SCHEDULED,
                    key: getHospitalModulesKeys().mIFScheduled
                }
            ]
        },
        {
            label: Vocabulary.SNAPMDADMIN_SETTINGSV2_MEDICAL_CODES,
            key: getHospitalModulesKeys().mMedicalCodes
        },
        {
            label: Vocabulary.SNAPMDADMIN_SETTINGSV2_VIDEO_BETA,
            key: getHospitalModulesKeys().mVideoBeta
        },
        {
            label: Vocabulary.SNAPMDADMIN_SETTINGSV2_PROVIDER_SEARCH,
            key: getHospitalModulesKeys().mClinicianSearch
        },
        {
            label: Vocabulary.SNAPMDADMIN_SETTINGSV2_TEXT_MESSAGING,
            key: getHospitalModulesKeys().mTextMessaging
        },
        {
            label: Vocabulary.SNAPMDADMIN_SETTINGSV2_RXNT_EHR,
            key: getHospitalModulesKeys().mRxNTEHR
        },
        {
            label: Vocabulary.SNAPMDADMIN_SETTINGSV2_RXNT_PM,
            key: getHospitalModulesKeys().mRxNTPM
        },
        {
            label: Vocabulary.SNAPMDADMIN_SETTINGSV2_SHOW_CTT_BEFORE_SHEDULED,
            key: getHospitalModulesKeys().mShowCTTOnScheduled
        },
        {
            label: Vocabulary.SNAPMDADMIN_SETTINGSV2_FILE_SHARING,
            key: getHospitalModulesKeys().mFileSharing
        },
        {
            label: Vocabulary.SNAPMDADMIN_SETTINGSV2_ORGANIZATION_AND_LOCATION,
            key: getHospitalModulesKeys().mOrganizationLocation
        },
        {
            label: Vocabulary.SNAPMDADMIN_SETTINGSV2_ENABLE_ADDRESS_VALIDATION,
            key: getHospitalModulesKeys().mAddressValidation
        },
        {
            label: Vocabulary.SNAPMDADMIN_SETTINGSV2_HIDE_OPEN_CONSULTATION,
            key: getHospitalModulesKeys().mHideOpenConsultation
        },
        {
            label: Vocabulary.SNAPMDADMIN_SETTINGSV2_HIDE_DR_TO_DR_CHAT,
            key: getHospitalModulesKeys().mHideDrToDrChat
        },
        {
            label: Vocabulary.SNAPMDADMIN_SETTINGSV2_DISABLE_PHONE_CONSULTATION,
            key: getHospitalModulesKeys().mDisablePhoneConsultation
        },
        {
            label: Vocabulary.SNAPMDADMIN_SETTINGSV2_DR_TO_DR_CHAT_ON_ADMIN,
            key: getHospitalModulesKeys().mDrToDrChatInAdmin
        },
        {
            label: Vocabulary.SNAPMDADMIN_SETTINGSV2_ANNOTATION,
            key: getHospitalModulesKeys().mAnnotation
        },
        {
            label: Vocabulary.SNAPMDADMIN_SETTINGSV2_KUBI,
            key: getHospitalModulesKeys().mKubi
        },
        {
            label: Vocabulary.SNAPMDADMIN_SETTINGSV2_CONSULTATION_VIDEO_ARHIVE,
            key: getHospitalModulesKeys().mConsultationVideoArchive
        },
        {
            label: Vocabulary.SNAPMDADMIN_SETTINGSV2_ADMIN_MEETING_REPORT,
            key: getHospitalModulesKeys().mAdminMeetingReport
        },
        {
            label: Vocabulary.SNAPMDADMIN_SETTINGSV2_INCLUDE_DIRECTIONS,
            key: getHospitalModulesKeys().mIncludeDirections
        },
        {
            label: Vocabulary.SNAPMDADMIN_SETTINGSV2_HIDE_FORGOT_PASSWORD_LINK,
            key: getHospitalModulesKeys().mHideForgotPasswordLink
        },
        {
            label: Vocabulary.SNAPMDADMIN_SETTINGSV2_TAG_AN_ENCOUNTER_WITH_GEO_LOCATION,
            key: getHospitalModulesKeys().mEncounterGeoLocation
        },
        {
            label: Vocabulary.SNAPMDADMIN_SETTINGSV2_SHOW_APPOINTMENT_WIZARD,
            key: getHospitalModulesKeys().mShowAppointmentWizard
        },
        {
            label: Vocabulary.SNAPMDADMIN_SETTINGSV2_IS_NOW_SCHEDULER_BUTTON_ENABLED,
            key: getHospitalModulesKeys().mShowNowButton
        }
    ]

    toggleComponentChild(child) {
        let toggle: Array<any> = [];
        for (let index in child) {
            toggle.push(
                <div key={child[index].key} className='d-flex smd-margin-top-bottom-10px'>
                    <div className='smd-setting-label col-6'>{this.props.language[child[index].label]}</div>
                    <div className='col smd-10-margin-bottom'>
                        <ToggleButton
                            isChecked={this.props.getPropertyState(child[index].key)}
                            onChange={(e) => {
                                this.props.setPropertyState(child[index].key, e);
                            }}
                        />
                    </div>
                </div>
            );
        }
        return toggle;
    }

    toggleComponent() {
        let toggleModule: Array<any> = [];
        for (let index in this.togglesParameters) {
            toggleModule.push(
                <li key={index} className='list-group-item smd-transitions smd-no-h-padding'>
                    <div className='smd-full-width smd-no-h-padding col-12'>
                        <div className=' d-flex'>
                            <div className='smd-setting-label col-4'>{this.props.language[this.togglesParameters[index].label]}</div>
                            <div className='col smd-10-margin-bottom'>
                                <ToggleButton
                                    isChecked={this.props.getPropertyState(this.togglesParameters[index].key)}
                                    onChange={(e) => {
                                        this.props.setPropertyState(this.togglesParameters[index].key, e);
                                    }}
                                />
                            </div>
                        </div>
                        {('child' in this.togglesParameters[index]) ?
                            this.props.getPropertyState(this.togglesParameters[index].key) ?
                                this.toggleComponentChild(this.togglesParameters[index].child)
                            : null
                        : null}
                    </div>
                </li>
            );
        }
        return toggleModule;
    }

    render() {
        return (
            <div>
                <ul className='list-group smd-transitions smd-expanded'>
                    {this.toggleComponent()}                   
                </ul>
            </div>
        )
    }
}