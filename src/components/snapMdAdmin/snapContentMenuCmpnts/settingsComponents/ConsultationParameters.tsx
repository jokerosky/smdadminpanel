import * as React from 'react';
import Select from 'react-select';

import * as Vocabulary from '../../../../localization/vocabulary';
import { language } from '../../../../enums/languages';
import { getHospitalConsulatationParametersKeys } from '../../../../models/DTO/hospitalSettingDTO';

export interface IConsultationParametersProps {
    language: any;
    hospitalId: number;

    setPropertyState: (field: string, value: any) => void;
    getPropertyState: (field: string) => any;
    saveSettings: (settings: any, hospitalId: number) => void;
}

export class ConsultationParameters extends React.Component<IConsultationParametersProps, any> {
    constructor() {
        super();
    }

    saveSettings(){
        let settings: any = {};
        for(let index in getHospitalConsulatationParametersKeys()){
            settings[index] = this.props.getPropertyState(index);
        }        
        this.props.saveSettings(settings, this.props.hospitalId);
    }

    render() {
        return (
            <div>

                <div className="form-group row">
                    <div className='col-5'>
                        <label htmlFor={getHospitalConsulatationParametersKeys().DefaultAvailabilityBlockDuration}>{this.props.language[Vocabulary.SNAPMDADMIN_SETTINGSV2_DEFAULT_AVAILABILITY_BLOCK_DURATION]}</label>
                    </div>
                    <div className='col-5'>
                        <input
                            type="number"
                            className="form-control "
                            id={getHospitalConsulatationParametersKeys().DefaultAvailabilityBlockDuration}
                            value={this.props.getPropertyState(getHospitalConsulatationParametersKeys().DefaultAvailabilityBlockDuration)}
                            onChange={(e: any) => {
                                this.props.setPropertyState(getHospitalConsulatationParametersKeys().DefaultAvailabilityBlockDuration, e.target.value);
                            }}
                        />
                    </div> {this.props.language[Vocabulary.SNAPMDADMIN_SETTINGSV2_MINUTES]}
                </div>
                <div className="form-group row">
                    <div className='col-5'>
                        <label htmlFor={getHospitalConsulatationParametersKeys().CbMinClinicansGood}>{this.props.language[Vocabulary.SNAPMDADMIN_SETTINGSV2_GOOD_COVERAGE_MIN_PROVIDERS_COUNT]}</label>
                    </div>
                    <div className='col-5'>
                        <input
                            type="number"
                            className="form-control"
                            id={getHospitalConsulatationParametersKeys().CbMinClinicansGood}
                            value={this.props.getPropertyState(getHospitalConsulatationParametersKeys().CbMinClinicansGood)}
                            onChange={(e: any) => {
                                this.props.setPropertyState(getHospitalConsulatationParametersKeys().CbMinClinicansGood, e.target.value);
                            }}
                        />
                    </div>
                </div>
                <div className="form-group">
                    <button
                        className='btn'
                        onClick={() => { this.saveSettings();}}
                    >{this.props.language[Vocabulary.SNAPMDADMIN_SETTINGSV2_SAVE_CONSULTATION_PARAMS]}</button>
                </div>
            </div>
        );
    }
}