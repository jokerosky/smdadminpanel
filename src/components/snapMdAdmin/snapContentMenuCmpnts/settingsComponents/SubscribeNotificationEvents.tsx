import * as React from 'react';

import * as Vocabulary from '../../../../localization/vocabulary';
import { language } from '../../../../enums/languages';
import {getHospitalSubscribeNotificationEventsKeys} from '../../../../models/DTO/hospitalSettingDTO';

export interface ISubscribeNotificationEventsProps {
    language: any;

    setPropertyState: (field: string, value: any) => void;
    getPropertyState: (field: string) => any;
}

export class SubscribeNotificationEvents extends React.Component<ISubscribeNotificationEventsProps, any> {
    constructor() {
        super();
    }

    render() {
        return (
            <div>
                <div className="form-group">
                    <label htmlFor={getHospitalSubscribeNotificationEventsKeys().secret}>{this.props.language[Vocabulary.SNAPMDADMIN_SETTINGSV2_SECRET]}</label>
                    <input
                        type="text"
                        className="form-control "
                        id={getHospitalSubscribeNotificationEventsKeys().secret}
                        value={this.props.getPropertyState(getHospitalSubscribeNotificationEventsKeys().secret)}
                        onChange={(e: any) => {
                            this.props.setPropertyState(getHospitalSubscribeNotificationEventsKeys().secret, e.target.value);
                        }}
                    />
                </div>
                <div className="form-group">
                    <label htmlFor={getHospitalSubscribeNotificationEventsKeys().callback}>{this.props.language[Vocabulary.SNAPMDADMIN_SETTINGSV2_CALLBACK]}</label>
                    <input
                        type="text"
                        className="form-control"
                        id={getHospitalSubscribeNotificationEventsKeys().callback}
                        value={this.props.getPropertyState(getHospitalSubscribeNotificationEventsKeys().callback)}
                        onChange={(e: any) => {
                            this.props.setPropertyState(getHospitalSubscribeNotificationEventsKeys().callback, e.target.value);
                        }}
                    />
                </div>
            </div>
        );
    }
}