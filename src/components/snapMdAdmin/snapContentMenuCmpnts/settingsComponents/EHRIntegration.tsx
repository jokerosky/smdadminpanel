import * as React from 'react';
import Select from 'react-select';
import * as _ from 'lodash';

import { ToggleButton } from '../../../common/inputs/ToggleButtonCmpnt';
import * as Vocabulary from '../../../../localization/vocabulary';
import { language } from '../../../../enums/languages';
import { getHospitalEHRIntegrationKeys } from '../../../../models/DTO/hospitalSettingDTO';

export interface IEHRIntegrationProps {
    language: any;
    hospitalId: number;
    partners: any;

    setPropertyState: (field: string, value: any) => void;
    getPropertyState: (field: string) => any;
    saveSettings: (settings: any, hospitalId: number) => void;
    deleteHospitalPartnerType: (data: any, hospitalId: number, partnerTypes: number) => void;
    postHospitalPartnerType: (data: any, hospitalId: number) => void;
}
//Integrations.EHR.
//Integrations.
export class EHRIntegration extends React.Component<IEHRIntegrationProps, any> {
    constructor() {
        super();

        this.state = {
            eHRIntegrationSelect: 0
        }
    }

    optionsEHRIntegrationSelect = [
        {
            value: 0,
            label: 'none'
        },
        {
            value: 1,
            label: 'AllscriptsPM'
        },
        {
            value: 2,
            label: 'AthenaHealth'
        },
        {
            value: 3,
            label: 'PracticeMAX eCW'
        },
        {
            value: 4,
            label: 'Redox'
        }
    ];

    noneEHRPartnerForm = [
        {
            name: Vocabulary.SNAPMDADMIN_SETTINGSV2_EXTERNAL_HOSPITAL_ID,
            key: getHospitalEHRIntegrationKeys().externalHospitalId
        },
        {
            name: Vocabulary.SNAPMDADMIN_SETTINGSV2_FAILURE_NOTIFICATION_EMAILS,
            key: getHospitalEHRIntegrationKeys().ehrFailureNotificationEmails
        }
    ];

    practiceMAXeCWEHRPartnerForm = [
        {
            name: Vocabulary.SNAPMDADMIN_SETTINGSV2_EXTERNAL_HOSPITAL_ID,
            key: getHospitalEHRIntegrationKeys().externalHospitalId
        },
        {
            name: Vocabulary.SNAPMDADMIN_SETTINGSV2_FAILURE_NOTIFICATION_EMAILS,
            key: getHospitalEHRIntegrationKeys().FailureNotificationEmails
        },
        {
            name: Vocabulary.SNAPMDADMIN_SETTINGSV2_PRACTICE_MAX_SVC_URL,
            key: getHospitalEHRIntegrationKeys().PracticeMaxEcwSvcUrl
        },
        {
            name: Vocabulary.SNAPMDADMIN_SETTINGSV2_PRACTICE_MAX_SVC_LOGIN,
            key: getHospitalEHRIntegrationKeys().PracticeMaxEcwSvcLogin
        },
        {
            name: Vocabulary.SNAPMDADMIN_SETTINGSV2_PRACTICE_MAX_SVC_PASSWORD,
            key: getHospitalEHRIntegrationKeys().PracticeMaxEcwSvcPassword
        }
    ];

    athenaHealthEHRPartnerInput = [
        {
            name: Vocabulary.SNAPMDADMIN_SETTINGSV2_EXTERNAL_HOSPITAL_ID,
            key: getHospitalEHRIntegrationKeys().externalHospitalId
        },
        {
            name: Vocabulary.SNAPMDADMIN_SETTINGSV2_FAILURE_NOTIFICATION_EMAILS,
            key: getHospitalEHRIntegrationKeys().FailureNotificationEmails
        },
        {
            name: Vocabulary.SNAPMDADMIN_SETTINGSV2_TELEMED_APPOINTMENT_TYPE,
            key: getHospitalEHRIntegrationKeys().AthenaHealthAppointmentType
        },
        {
            name: Vocabulary.SNAPMDADMIN_SETTINGSV2_DEFAULT_DEPARTMENT_ID,
            key: getHospitalEHRIntegrationKeys().AthenaHealthDefaultDepartment
        },
        {
            name: Vocabulary.SNAPMDADMIN_SETTINGSV2_SEARCH_DEPARTMENTS,
            key: getHospitalEHRIntegrationKeys().AthenaHealthSearchDepartments
        },
        {
            name: Vocabulary.SNAPMDADMIN_SETTINGSV2_ATHENA_SVC_LOGIN,
            key: getHospitalEHRIntegrationKeys().AthenaHealthSvcLogin
        },
        {
            name: Vocabulary.SNAPMDADMIN_SETTINGSV2_ATHENA_SVC_PASSWORD,
            key: getHospitalEHRIntegrationKeys().AthenaHealthSvcPassword
        },
        {
            name: Vocabulary.SNAPMDADMIN_SETTINGSV2_ATHENA_SVC_URL,
            key: getHospitalEHRIntegrationKeys().AthenaHealthSvcUrl
        },
        {
            name: Vocabulary.SNAPMDADMIN_SETTINGSV2_ATHENA_SVC_AUTH_PREFIX,
            key: getHospitalEHRIntegrationKeys().AthenaHealthSvcAuthPrefix
        },
        {
            name: Vocabulary.SNAPMDADMIN_SETTINGSV2_ATHENA_SVC_API_PREFIX,
            key: getHospitalEHRIntegrationKeys().AthenaHealthSvcApiPrefix
        }
    ];

    athenaHealthEHRPartnerToggleFinancialGroups = [
        {
            name: Vocabulary.SNAPMDADMIN_SETTINGSV2_ATHENA_HAS_PROVIDER_OR_FINANCIAL_GROUPS,
            key: getHospitalEHRIntegrationKeys().AthenaHealthHasProviderGroups
        }
    ];

    athenaHealthEHRPartnerTogglePatientProfile = [
        {
            name: Vocabulary.SNAPMDADMIN_SETTINGSV2_HIDE_PATIENT_PROFILE,
            key: getHospitalEHRIntegrationKeys().AthenaHealthHidePatientProfile
        },
        {
            name: Vocabulary.SNAPMDADMIN_SETTINGSV2_SYNC_ALLERGIES,
            key: getHospitalEHRIntegrationKeys().AthenaHealthSyncPatientAllergies
        },
        {
            name: Vocabulary.SNAPMDADMIN_SETTINGSV2_SYNC_SURGERIES,
            key: getHospitalEHRIntegrationKeys().AthenaHealthSyncPatientSurgeries
        },
        {
            name: Vocabulary.SNAPMDADMIN_SETTINGSV2_SYNC_MEDICAL_HISTORY,
            key: getHospitalEHRIntegrationKeys().AthenaHealthSyncPatientMedicalHistory
        },
        {
            name: Vocabulary.SNAPMDADMIN_SETTINGSV2_SYNC_HEAD_CLINICIAN,
            key: getHospitalEHRIntegrationKeys().AthenaHealthSyncPatientHeadClinician
        },
        {
            name: Vocabulary.SNAPMDADMIN_SETTINGSV2_SYNC_PHARMACIES,
            key: getHospitalEHRIntegrationKeys().AthenaHealthSyncPatientPharmacies
        }
    ];

    renderInput(elements) {
        let component: Array<any> = [];
        for (let index in elements) {
            component.push(
                <div key={elements[index].key + '_' + index} className="form-group">
                    <label htmlFor={elements[index].key}>{this.props.language[elements[index].name]}</label>
                    <input
                        type="text"
                        className="form-control"
                        id={elements[index].key}
                        value={this.props.getPropertyState(elements[index].key)}
                        onChange={(e: any) => {
                            this.props.setPropertyState(elements[index].key, e.target.value);
                        }}
                    />
                </div>
            );
        }
        return component;
    }

    renderToggle(elements) {
        let component: Array<any> = [];
        for (let index in elements) {
            component.push(
                <li key={elements[index].key + '_' + index} className='list-group-item smd-transitions smd-no-h-padding'>
                    <div className='smd-full-width smd-no-h-padding col-12'>
                        <div className='d-flex'>
                            <div className='smd-setting-label col-6'>{this.props.language[elements[index].name]}</div>
                            <div className='col'>
                                <ToggleButton
                                    isChecked={this.props.getPropertyState(elements[index].key)}
                                    onChange={(e) => {
                                        this.props.setPropertyState(elements[index].key, e);
                                    }}
                                />
                            </div>
                        </div>
                    </div>
                </li>
            );
        }
        return component;
    }

    renderNone() {
        return (
            <div>
                {this.renderInput(this.noneEHRPartnerForm)}
                <label>{this.props.language[Vocabulary.SNAPMDADMIN_SETTINGSV2_NO_EHR_PARTNER]}</label>
                <div className="form-group">
                    <button
                        className='btn'
                        onClick={() => { this.saveNone(); }}
                    >{this.props.language[Vocabulary.SNAPMDADMIN_SETTINGSV2_SAVE_SETTINGS_AND_SWITCH_TO_NONE]}</button>
                </div>
            </div>
        );
    }

    renderAllscriptsPM() {
        return 'renderAllscriptsPM';
    }

    renderAthenaHealth() {
        return (
            <div>
                {this.renderInput(this.athenaHealthEHRPartnerInput)}
                <ul className='list-group smd-transitions smd-expanded smd-10-margin-bottom'>
                    <li> <label>{this.props.language[Vocabulary.SNAPMDADMIN_SETTINGSV2_FINANCIAL_GROUPS]}</label> </li>
                    {this.renderToggle(this.athenaHealthEHRPartnerToggleFinancialGroups)}
                    <li> <label>{this.props.language[Vocabulary.SNAPMDADMIN_SETTINGSV2_PATIENT_PROFILE]}</label> </li>
                    {this.renderToggle(this.athenaHealthEHRPartnerTogglePatientProfile)}
                </ul>
                <div className="form-group">
                    <button
                        className='btn'
                        onClick={() => { this.saveAthenaHealth(); }}
                    >{this.props.language[Vocabulary.SNAPMDADMIN_SAVE_SETTINGS]}</button>
                </div>
                <div className="form-group">
                    <button
                        className='btn'
                        onClick={() => { this.saveAthenaHealthAdditionally(); }}
                    >{this.props.language[Vocabulary.SNAPMDADMIN_SETTINGSV2_SAVE_SETTINGS_AND_SWITCH_TO_ATHENA]}</button>
                </div>
            </div>
        );
    }

    renderPracticeMAXeCW() {
        return (
            <div>
                {this.renderInput(this.practiceMAXeCWEHRPartnerForm)}
                <div className="form-group">
                    <button
                        className='btn'
                        onClick={() => { this.savePracticeMAXeCW(); }}
                    >{this.props.language[Vocabulary.SNAPMDADMIN_SAVE_SETTINGS]}</button>
                </div>
                <div className="form-group">
                    <button
                        className='btn'
                        onClick={() => { this.savePracticeMAXeCWAdditionally(); }}
                    >{this.props.language[Vocabulary.SNAPMDADMIN_SETTINGSV2_SAVE_SETTINGS_AND_SWITCH_TO_PRACTICE_MAX]}</button>
                </div>
            </div>);
    }

    renderRedox() {
        return 'renderRedox';
    }

    renderElement() {
        let functionsRender: Array<any> = [
            this.renderNone(),
            this.renderAllscriptsPM(),
            this.renderAthenaHealth(),
            this.renderPracticeMAXeCW(),
            this.renderRedox()
        ];

        return functionsRender[this.state.eHRIntegrationSelect];
    }

    saveAthenaHealth() {
        let fieldNameToSaves: string[] = [
            getHospitalEHRIntegrationKeys().AthenaHealthConsultationType,
            getHospitalEHRIntegrationKeys().AthenaHealthDefaultDepartment,
            getHospitalEHRIntegrationKeys().AthenaHealthHasProviderGroups,
            getHospitalEHRIntegrationKeys().AthenaHealthHidePatientProfile,
            getHospitalEHRIntegrationKeys().AthenaHealthSearchDepartments,
            getHospitalEHRIntegrationKeys().AthenaHealthSvcApiPrefix,
            getHospitalEHRIntegrationKeys().AthenaHealthSvcAuthPrefix,
            getHospitalEHRIntegrationKeys().AthenaHealthSvcLogin,
            getHospitalEHRIntegrationKeys().AthenaHealthSvcPassword,
            getHospitalEHRIntegrationKeys().AthenaHealthSvcUrl,
            getHospitalEHRIntegrationKeys().AthenaHealthSyncPatientAllergies,
            getHospitalEHRIntegrationKeys().AthenaHealthSyncPatientHeadClinician,
            getHospitalEHRIntegrationKeys().AthenaHealthSyncPatientMedicalHistory,
            getHospitalEHRIntegrationKeys().AthenaHealthSyncPatientPharmacies,
            getHospitalEHRIntegrationKeys().AthenaHealthSyncPatientSurgeries,
            getHospitalEHRIntegrationKeys().FailureNotificationEmails
        ];
        let settings: any = {};
        for (let index in fieldNameToSaves) {
            if (fieldNameToSaves[index] === getHospitalEHRIntegrationKeys().FailureNotificationEmails) {
                settings['Integrations.EHR.' + fieldNameToSaves[index]] = this.props.getPropertyState(fieldNameToSaves[index]).toString();
            } else {
                settings['Integrations.' + fieldNameToSaves[index]] = this.props.getPropertyState(fieldNameToSaves[index]).toString();
            }
        }
        this.props.saveSettings(settings, this.props.hospitalId);
    }

    saveAthenaHealthAdditionally(){
        this.saveAthenaHealth();

        let searchIndex = _.findIndex(this.props.partners, {name: this.optionsEHRIntegrationSelect[this.state.eHRIntegrationSelect].label});
        let settings: any = {
            [getHospitalEHRIntegrationKeys().externalHospitalId]: this.props.getPropertyState(getHospitalEHRIntegrationKeys().externalHospitalId),
            [getHospitalEHRIntegrationKeys().partnerId]: this.props.partners[searchIndex].partnerId
        };
        this.props.postHospitalPartnerType(settings, this.props.hospitalId);
    }

    savePracticeMAXeCW() {
        let fieldNameToSaves: string[] = [
            getHospitalEHRIntegrationKeys().PracticeMaxEcwSvcLogin,
            getHospitalEHRIntegrationKeys().PracticeMaxEcwSvcPassword,
            getHospitalEHRIntegrationKeys().PracticeMaxEcwSvcUrl,
            getHospitalEHRIntegrationKeys().FailureNotificationEmails
        ];
        let settings: any = {};
        for (let index in fieldNameToSaves) {
            if (fieldNameToSaves[index] === getHospitalEHRIntegrationKeys().FailureNotificationEmails) {
                settings['Integrations.EHR.' + fieldNameToSaves[index]] = this.props.getPropertyState(fieldNameToSaves[index]).toString();
            } else {
                settings['Integrations.' + fieldNameToSaves[index]] = this.props.getPropertyState(fieldNameToSaves[index]).toString();
            }
        }        
        this.props.saveSettings(settings, this.props.hospitalId);
    }

    savePracticeMAXeCWAdditionally(){
        this.savePracticeMAXeCW();

        let searchIndex = _.findIndex(this.props.partners, {name: this.optionsEHRIntegrationSelect[this.state.eHRIntegrationSelect].label});
        let settings: any = {
            [getHospitalEHRIntegrationKeys().externalHospitalId]: this.props.getPropertyState(getHospitalEHRIntegrationKeys().externalHospitalId),
            [getHospitalEHRIntegrationKeys().partnerId]: this.props.partners[searchIndex].partnerId
        };
        this.props.postHospitalPartnerType(settings, this.props.hospitalId);
    }

    saveNone() {
        let fieldNameToSaves: string[] = [
            getHospitalEHRIntegrationKeys().externalHospitalId,
            getHospitalEHRIntegrationKeys().partnerId
        ];
        let settings: any = {};
        for (let index in fieldNameToSaves) {
            settings[fieldNameToSaves[index]] = this.props.getPropertyState(fieldNameToSaves[index]).toString();
        }
        this.props.deleteHospitalPartnerType(settings, this.props.hospitalId, 1);
    }

    render() {
        return (
            <div>
                <div className="form-group">
                    <label htmlFor="ehrPartner">{this.props.language[Vocabulary.SNAPMDADMIN_SETTINGSV2_EHR_PARTNER]}</label>
                    <Select
                        options={this.optionsEHRIntegrationSelect}
                        value={this.state.eHRIntegrationSelect}
                        clearable={false}
                        onChange={(e) => {
                            this.setState({
                                eHRIntegrationSelect: e.value
                            })
                        }}
                    />
                </div>
                {this.renderElement()}

            </div>
        );
    }
}