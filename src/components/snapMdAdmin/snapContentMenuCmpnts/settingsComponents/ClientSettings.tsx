import * as React from 'react';
import Select from 'react-select';

import * as Vocabulary from '../../../../localization/vocabulary';
import {language} from '../../../../enums/languages';
import { getHospitalClientKeys, getHospitalClientInListsKey } from '../../../../models/DTO/hospitalSettingDTO';

export interface IClientSettingsProps {
    language: any;
    clientTypes: any;

    setPropertyState: (field: any, value: any)=>void;
    getPropertyState: (field: any)=>any;
}

export class ClientSettings extends React.Component<IClientSettingsProps, any> {
    constructor() {
        super();
    }

    optionsLanguageSelect = [
        {
            value: 'en',
            label: 'English'
        },
        {
            value: 'es',
            label: 'Español'
        },
        {
            value: 'ru',
            label: 'Русский'
        },
    ]

    countriesForSelect(){
        let countries: Array<any> = [];
        for(let index in this.props.clientTypes){
            countries.push(
                {
                    value: this.props.clientTypes[index].key,
                    label: this.props.clientTypes[index].value
                }
            );
        }
        return countries;
    }

    render() {
        return (
            <div>
                <div className="form-group">
                    <label htmlFor="clientType">{this.props.language[Vocabulary.SNAPMDADMIN_SETTINGSV2_CLIENT_TYPE]}</label>
                    <Select 
                        id={'clientType'}
                        options = {this.countriesForSelect()}
                        value = {this.props.getPropertyState(getHospitalClientKeys().hospitalType)}
                        clearable={false}
                        onChange={(e)=>{
                            this.props.setPropertyState(getHospitalClientKeys().hospitalType, e.value);getHospitalClientInListsKey
                            this.props.setPropertyState(getHospitalClientInListsKey().hospitalTypeName, e.label);
                        }}
                    />
                </div>
                <div className="form-group">
                    <label htmlFor={getHospitalClientKeys().hospitalName}>{this.props.language[Vocabulary.SNAPMDADMIN_SETTINGSV2_CLIENT_NAME]}</label>
                    <input
                        type="text"
                        className="form-control"
                        id={getHospitalClientKeys().hospitalName}
                        value={this.props.getPropertyState(getHospitalClientKeys().hospitalName)}
                        onChange={(e: any)=>{
                            this.props.setPropertyState(getHospitalClientKeys().hospitalName, e.target.value);
                        }}
                    />
                </div>
                <div className="form-group">
                    <label htmlFor="useAsProductName">{this.props.language[Vocabulary.SNAPMDADMIN_SETTINGSV2_USE_AS_PRODUCT_NAME]} </label>
                    <input 
                        type='checkBox'
                        id='useAsProductName'
                        onClick={(e: any)=>{
                            this.props.setPropertyState('useAsProductName', e.target.checked);
                        }}
                    />
                </div>
                <div className="form-group">
                    <label htmlFor={getHospitalClientKeys().consultationCharge}>{this.props.language[Vocabulary.SNAPMDADMIN_SETTINGSV2_CONSULTATION_FEE]}</label>
                    <input
                        type="text"
                        className="form-control"
                        id={getHospitalClientKeys().consultationCharge}
                        value={this.props.getPropertyState(getHospitalClientKeys().consultationCharge)}
                        onChange={(e: any)=>{
                            this.props.setPropertyState(getHospitalClientKeys().consultationCharge, e.target.value);
                        }}
                    />
                </div>
                <div className="form-group">
                    <label htmlFor={getHospitalClientKeys().contactNumber}>{this.props.language[Vocabulary.SNAPMDADMIN_SETTINGSV2_CONTACT_NUMBER]}</label>
                    <input
                        type="text"
                        className="form-control"
                        id={getHospitalClientKeys().contactNumber}
                        value={this.props.getPropertyState(getHospitalClientKeys().contactNumber)}
                        onChange={(e: any)=>{
                            this.props.setPropertyState(getHospitalClientKeys().contactNumber, e.target.value);
                        }}
                    />
                </div>
                <div className="form-group">
                    <label htmlFor={getHospitalClientKeys().itDeptContactNumber}>{this.props.language[Vocabulary.SNAPMDADMIN_SETTINGSV2_IT_DEPT_CONTACT_NUMBER]}</label>
                    <input
                        type="text"
                        className="form-control"
                        id={getHospitalClientKeys().itDeptContactNumber}
                        value={this.props.getPropertyState(getHospitalClientKeys().itDeptContactNumber)}
                        onChange={(e: any)=>{
                            this.props.setPropertyState(getHospitalClientKeys().itDeptContactNumber, e.target.value);
                        }}
                    />
                </div>
                <div className="form-group">
                    <label htmlFor={getHospitalClientKeys().appointmentsContactNumber}>{this.props.language[Vocabulary.SNAPMDADMIN_SETTINGSV2_APPOINTMENTS_CONTACT_NUMBER]}</label>
                    <input
                        type="text"
                        className="form-control"
                        id={getHospitalClientKeys().appointmentsContactNumber}
                        value={this.props.getPropertyState(getHospitalClientKeys().appointmentsContactNumber)}
                        onChange={(e: any)=>{
                            this.props.setPropertyState(getHospitalClientKeys().appointmentsContactNumber, e.target.value);
                        }}
                    />
                </div>
                <div className="form-group">
                    <label htmlFor={getHospitalClientKeys().npiNumber}>{this.props.language[Vocabulary.SNAPMDADMIN_SETTINGSV2_NPI_NUMBER]}</label>
                    <input
                        type="text"
                        className="form-control"
                        id={getHospitalClientKeys().npiNumber}
                        value={this.props.getPropertyState(getHospitalClientKeys().npiNumber)}
                        onChange={(e: any)=>{
                            this.props.setPropertyState(getHospitalClientKeys().npiNumber, e.target.value);
                        }}
                    />
                </div>
                <div className="form-group">
                    <label htmlFor={getHospitalClientKeys().language}>{this.props.language[Vocabulary.SNAPMDADMIN_SETTINGSV2_LANGUAGE]}</label>
                    <Select 
                        id={getHospitalClientKeys().language}
                        options={this.optionsLanguageSelect}
                        value={this.props.getPropertyState(getHospitalClientKeys().language) || 'en'}
                        clearable={false}
                        onChange={(e)=>{
                            this.props.setPropertyState(getHospitalClientKeys().language, e.value);
                        }}
                    />
                </div>
            </div>
        );
    }
}