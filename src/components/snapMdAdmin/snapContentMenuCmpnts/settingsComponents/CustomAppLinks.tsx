import * as React from 'react';

import * as Vocabulary from '../../../../localization/vocabulary';
import { language } from '../../../../enums/languages';
import {getHospitalCustomAppLinksKeys} from '../../../../models/DTO/hospitalSettingDTO';

export interface ICustomAppLinksProps {
    language: any;
    hospitalId: number;

    setPropertyState: (field: string, value: any) => void;
    getPropertyState: (field: string) => any;
    saveSettings: (settings: any, hospitalId: number) => void;
}

export class CustomAppLinks extends React.Component<ICustomAppLinksProps, any> {
    constructor() {
        super();
    }

    saveSettings(){
        let settings: any = {};
        for(let index in getHospitalCustomAppLinksKeys()){
            settings[index] = this.props.getPropertyState(index);
        }
        this.props.saveSettings(settings, this.props.hospitalId);
    }

    render() {
        return (
            <div>
                <div className="form-group">
                    <label htmlFor={getHospitalCustomAppLinksKeys().iOSSchemaUrl}>{this.props.language[Vocabulary.SNAPMDADMIN_SETTINGSV2_IOS_SCHEMA_URL]}</label>
                    <input
                        type="text"
                        className="form-control "
                        id={getHospitalCustomAppLinksKeys().iOSSchemaUrl}
                        value={this.props.getPropertyState(getHospitalCustomAppLinksKeys().iOSSchemaUrl)}
                        onChange={(e: any) => {
                            this.props.setPropertyState(getHospitalCustomAppLinksKeys().iOSSchemaUrl, e.target.value);
                        }}
                    />
                </div>
                <div className="form-group">
                    <label htmlFor={getHospitalCustomAppLinksKeys().androidSchemaUrl}>{this.props.language[Vocabulary.SNAPMDADMIN_SETTINGSV2_ANDROID_SCHEMA_URL]}</label>
                    <input
                        type="text"
                        className="form-control"
                        id={getHospitalCustomAppLinksKeys().androidSchemaUrl}
                        value={this.props.getPropertyState(getHospitalCustomAppLinksKeys().androidSchemaUrl)}
                        onChange={(e: any) => {
                            this.props.setPropertyState(getHospitalCustomAppLinksKeys().androidSchemaUrl, e.target.value);
                        }}
                    />
                </div>
                <div className="form-group">
                    <label htmlFor={getHospitalCustomAppLinksKeys().iOSAppStoreUrl}>{this.props.language[Vocabulary.SNAPMDADMIN_SETTINGSV2_IOS_APP_STORE_URL]}</label>
                    <input
                        type="text"
                        className="form-control"
                        id={getHospitalCustomAppLinksKeys().iOSAppStoreUrl}
                        value={this.props.getPropertyState(getHospitalCustomAppLinksKeys().iOSAppStoreUrl)}
                        onChange={(e: any) => {
                            this.props.setPropertyState(getHospitalCustomAppLinksKeys().iOSAppStoreUrl, e.target.value);
                        }}
                    />
                </div>
                <div className="form-group">
                    <label htmlFor={getHospitalCustomAppLinksKeys().androidPlayStoreUrl}>{this.props.language[Vocabulary.SNAPMDADMIN_SETTINGSV2_ANDROID_PLAY_STORE_URL]}</label>
                    <input
                        type="text"
                        className="form-control"
                        id={getHospitalCustomAppLinksKeys().androidPlayStoreUrl}
                        value={this.props.getPropertyState(getHospitalCustomAppLinksKeys().androidPlayStoreUrl)}
                        onChange={(e: any) => {
                            this.props.setPropertyState(getHospitalCustomAppLinksKeys().androidPlayStoreUrl, e.target.value);
                        }}
                    />
                </div>
                <div className="form-group">
                    <button
                        id='customAppLinksSave'
                        className='btn'
                        onClick={() => { this.saveSettings();}}
                    >{this.props.language[Vocabulary.SNAPMDADMIN_SETTINGSV2_SAVE_APP_LINKS]}</button>
                </div>
            </div>
        );
    }
}