import * as React from 'react';
import Select from 'react-select';

import * as Vocabulary from '../../../../localization/vocabulary';
import { language } from '../../../../enums/languages';
import { getHospitalSingleSignOnKeys, HospitalSingleSignOnKeysDTO } from '../../../../models/DTO/hospitalSettingDTO';

export interface ISingleSignOnProps {
    language: any;
    hospitalId: number;

    setPropertyState: (field: string, value: any) => void;
    getPropertyState: (field: string) => any;
    saveSettings: (settings: any, hospitalId: number) => void;
}

export class SingleSignOn extends React.Component<ISingleSignOnProps, any> {
    constructor() {
        super();
    }

    singleSignOnForm = [
        {
            name: Vocabulary.SNAPMDADMIN_SETTINGSV2_OAUTH_CLIENT_ID,
            key: getHospitalSingleSignOnKeys().oAuthClientId
        },
        {
            name: Vocabulary.SNAPMDADMIN_SETTINGSV2_OAUTH_CLIENT_SECRET,
            key: getHospitalSingleSignOnKeys().oAuthClientSecret
        },
        {
            name: Vocabulary.SNAPMDADMIN_SETTINGSV2_OAUTH_URL,
            key: getHospitalSingleSignOnKeys().oAuthUrl
        },
        {
            name: Vocabulary.SNAPMDADMIN_SETTINGSV2_OAUTH_CALLBACK,
            key: getHospitalSingleSignOnKeys().oAuthCallback
        },
        {
            name: Vocabulary.SNAPMDADMIN_SETTINGSV2_SSO_PROVIDER_LOGIN_URL,
            key: getHospitalSingleSignOnKeys().ssoClinicianExternalLoginUrl
        },
        {
            name: Vocabulary.SNAPMDADMIN_SETTINGSV2_PROVIDER_CONSULTATION_RETURN_URL,
            key: getHospitalSingleSignOnKeys().ssoClinicianReturnUrl
        },
        {
            name: Vocabulary.SNAPMDADMIN_SETTINGSV2_PROVIDER_SSO_OPTIONS,
            key: getHospitalSingleSignOnKeys().clinicianSSO
        },
        {
            name: Vocabulary.SNAPMDADMIN_SETTINGSV2_PROVIDER_SSO_BUTTON_TEXT,
            key: getHospitalSingleSignOnKeys().clinicianSSOButtonText
        },
        {
            name: Vocabulary.SNAPMDADMIN_SETTINGSV2_SSO_ADMIN_LOGIN_URL,
            key: getHospitalSingleSignOnKeys().ssoAdminExternalLoginUrl
        },
        {
            name: Vocabulary.SNAPMDADMIN_SETTINGSV2_ADMIN_CONSULTATION_RETURN_URL,
            key: getHospitalSingleSignOnKeys().adminConsultEndUrl
        },
        {
            name: Vocabulary.SNAPMDADMIN_SETTINGSV2_ADMIN_SSO_OPTIONS,
            key: getHospitalSingleSignOnKeys().adminSSO
        },
        {
            name: Vocabulary.SNAPMDADMIN_SETTINGSV2_ADMIN_SSO_BUTTON_TEXT,
            key: getHospitalSingleSignOnKeys().adminSSOButtonText
        },
        {
            name: Vocabulary.SNAPMDADMIN_SETTINGSV2_SSO_CUSTOMER_LOGIN_URL,
            key: getHospitalSingleSignOnKeys().ssoExternalLoginUrl
        },
        {
            name: Vocabulary.SNAPMDADMIN_SETTINGSV2_CUSTOMER_CONSULTATION_RETURN_URL,
            key: getHospitalSingleSignOnKeys().ssoReturnUrl
        },
        {
            name: Vocabulary.SNAPMDADMIN_SETTINGSV2_CUSTOMER_SSO_OPTIONS,
            key: getHospitalSingleSignOnKeys().customerSSO
        },
        {
            name: Vocabulary.SNAPMDADMIN_SETTINGSV2_CUSTOMER_SSO_BUTTON_TEXT,
            key: getHospitalSingleSignOnKeys().customerSSOButtonText
        },
        {
            name: Vocabulary.SNAPMDADMIN_SETTINGSV2_JWT_ISSUER,
            key: getHospitalSingleSignOnKeys().jwtIssuer
        },
        {
            name: Vocabulary.SNAPMDADMIN_SETTINGSV2_PATIENT_TOKEN_API,
            key: getHospitalSingleSignOnKeys().patientTokenApi
        },
        {
            name: Vocabulary.SNAPMDADMIN_SETTINGSV2_PATIENT_REGISTRATION_API,
            key: getHospitalSingleSignOnKeys().patientRegistrationApi
        },
        {
            name: Vocabulary.SNAPMDADMIN_SETTINGSV2_PATIENT_FORGOT_PASSWORD_API,
            key: getHospitalSingleSignOnKeys().patientForgotPasswordApi
        }
    ];

    renderElement() {
        let element: Array<any> = [];
        for (let index in this.singleSignOnForm) {
            element.push(
                <div key={index} className="form-group">
                    <label htmlFor={this.singleSignOnForm[index].key}>{this.props.language[this.singleSignOnForm[index].name]}</label>
                    <input
                        type="text"
                        className="form-control"
                        id={this.singleSignOnForm[index].key}
                        value={this.props.getPropertyState(this.singleSignOnForm[index].key)}
                        onChange={(e: any) => {
                            this.props.setPropertyState(this.singleSignOnForm[index].key, e.target.value);
                        }}
                    />
                </div>
            );
        }
        return element;
    }

    saveSettings() {
        let settings: any = {};
        for (let index in getHospitalSingleSignOnKeys()) {
            settings[index] = this.props.getPropertyState(index);
        }        
        this.props.saveSettings(settings, this.props.hospitalId);
    }

    render() {
        return (
            <div>
                {this.renderElement()}
                <div className="form-group">
                    <button
                        className='btn'
                        onClick={() => { this.saveSettings(); }}
                    >{this.props.language[Vocabulary.SNAPMDADMIN_SETTINGSV2_SAVE_SSO]}</button>
                </div>
            </div>
        );
    }
}