import * as React from 'react';

import * as Vocabulary from '../../../../localization/vocabulary';
import { MODULE_KEYS } from '../../../../enums/clientSettingsKeys';
import { ToggleButton } from '../../../common/inputs/ToggleButtonCmpnt';
import {getHospitalModulesKeys} from '../../../../models/DTO/hospitalSettingDTO';

export interface IMobileSettingsProps {
    language: any;

    setPropertyState: (field: string, value: any) => void;
    getPropertyState: (field: string) => any;
}

export class MobileSettings extends React.Component<IMobileSettingsProps, any> {
    constructor() {
        super();
    }

    render() {
        return (
            <div>
                <ul className='list-group smd-transitions smd-expanded'>
                    <li className='list-group-item smd-transitions smd-no-h-padding'>
                        <div className='smd-full-width smd-no-h-padding col-12'>
                            <div className='d-flex'>
                                <div className='smd-setting-label col-6'>{this.props.language[Vocabulary.SNAPMDADMIN_SETTINGSV2_INCLUDE_IN_GENERIC_APP]}</div>
                                <div className='col'>
                                    <ToggleButton
                                        isChecked={this.props.getPropertyState(getHospitalModulesKeys().GenericMobileAppEnabled)}
                                        onChange={(e) => {
                                            this.props.setPropertyState(getHospitalModulesKeys().GenericMobileAppEnabled, e);
                                        }}
                                    />
                                </div>
                            </div>
                        </div>
                    </li>
                </ul>
            </div>
        );
    }
}