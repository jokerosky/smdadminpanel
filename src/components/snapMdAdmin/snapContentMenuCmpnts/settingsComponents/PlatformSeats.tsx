import * as React from 'react';

import * as Vocabulary from '../../../../localization/vocabulary';
import { MODULE_KEYS } from '../../../../enums/clientSettingsKeys';
import { ToggleButton } from '../../../common/inputs/ToggleButtonCmpnt';
import {getHospitalPlatformSeatsKeys} from '../../../../models/DTO/hospitalSettingDTO';

export interface IPlatformSeatsProps {
    language: any;

    setPropertyState: (field: string, value: any) => void;
    getPropertyState: (field: string) => any;
}

export class PlatformSeats extends React.Component<IPlatformSeatsProps, any> {
    constructor() {
        super();
    }

    totslSeats(seats, adminSeats){
        let totalUserCount = (Number(seats) || 0) + (Number(adminSeats) || 0);
        this.props.setPropertyState(getHospitalPlatformSeatsKeys().snTotalSeats, totalUserCount);
    }

    render() {
        return (
            <div>
                <table className='table table-hover'>
                    <thead>
                        <tr className='row'>
                            <th className='td-smd-table col-4'>{this.props.language[Vocabulary.SNAPMDADMIN_SETTINGSV2_SEATS_TYPE]}</th>
                            <th className='td-smd-table col-4'>{this.props.language[Vocabulary.SNAPMDADMIN_SETTINGSV2_ASSIGNED]}</th>
                            <th className='td-smd-table col-4'>{this.props.language[Vocabulary.SNAPMDADMIN_SETTINGSV2_AVAILABLE]}</th>
                        </tr>
                    </thead>
                    <tbody>
                        <tr className='row'>
                            <td className='td-smd-table col-4'>{this.props.language[Vocabulary.SNAPMDADMIN_SETTINGSV2_ENTERPRISE]}</td>
                            <td className='td-smd-table col-4'>
                            {`${this.props.getPropertyState(getHospitalPlatformSeatsKeys().snEnterpriseAssigned)} (${this.props.language[Vocabulary.SNAPMDADMIN_SETTINGSV2_INCL] || ''}. ${this.props.getPropertyState(getHospitalPlatformSeatsKeys().snEnterprisePendingAssigned)} ${this.props.language[Vocabulary.SNAPMDADMIN_SETTINGSV2_PENDING] || ''})`}
                            </td>
                            <td className='td-smd-table col-4'>
                                <input 
                                    type="text"
                                    className="form-control"
                                    value={this.props.getPropertyState(getHospitalPlatformSeatsKeys().snEnterprise)}
                                    onChange={(e: any)=>{
                                        this.props.setPropertyState(getHospitalPlatformSeatsKeys().snEnterprise, Number(e.target.value));
                                        this.totslSeats(e.target.value, this.props.getPropertyState(getHospitalPlatformSeatsKeys().snAdmin));
                                    }}
                                /></td>
                        </tr>
                        <tr className='row'>
                            <td className='td-smd-table col-4'>{this.props.language[Vocabulary.SNAPMDADMIN_SETTINGSV2_ADMIN]}</td>
                            <td className='td-smd-table col-4'>
                            {`${this.props.getPropertyState(getHospitalPlatformSeatsKeys().snAdminAssigned)} (${this.props.language[Vocabulary.SNAPMDADMIN_SETTINGSV2_INCL] || ''}. ${this.props.getPropertyState(getHospitalPlatformSeatsKeys().snAdminPendingAssigned)} ${this.props.language[Vocabulary.SNAPMDADMIN_SETTINGSV2_PENDING] || ''})`}
                            </td>
                            <td className='td-smd-table col-4'>
                                <input 
                                    type="text"
                                    className="form-control"
                                    value={this.props.getPropertyState(getHospitalPlatformSeatsKeys().snAdmin)}
                                    onChange={(e: any)=>{
                                        this.props.setPropertyState(getHospitalPlatformSeatsKeys().snAdmin, Number(e.target.value));
                                        this.totslSeats(this.props.getPropertyState(getHospitalPlatformSeatsKeys().snEnterprise), e.target.value);
                                    }}
                                /></td>
                        </tr>
                        <tr className='row'>
                            <td className='td-smd-table col-4'>{this.props.language[Vocabulary.SNAPMDADMIN_SETTINGSV2_TOTAL_SEATS]}</td>
                            <td className='td-smd-table col-4'>
                            {this.props.getPropertyState(getHospitalPlatformSeatsKeys().snTotalSeatsAssigned)}
                            </td>
                            <td className='td-smd-table col-4'>
                                    {this.props.getPropertyState(getHospitalPlatformSeatsKeys().snTotalSeats)}
                            </td>
                        </tr>
                    </tbody>
                </table>
            </div>
        );
    }
}