import * as React from 'react';
import reactCSS from 'reactcss'
import { SketchPicker } from 'react-color'

import * as Vocabulary from '../../../../localization/vocabulary';
import { MODULE_KEYS } from '../../../../enums/clientSettingsKeys';
import { ToggleButton } from '../../../common/inputs/ToggleButtonCmpnt';
import { getHospitalBrandingKeys } from '../../../../models/DTO/hospitalSettingDTO';
import { LoadImageCmpnt } from '../../../common/LoadImageCmpnt';
import { Endpoints } from '../../../../enums/endpoints';

export interface IBrandingSettingsProps {
    language: any;
    hospitalId: number;

    setPropertyState: (field: string, value: any) => void;
    getPropertyState: (field: string) => any;
}

export class BrandingSettings extends React.Component<IBrandingSettingsProps, any> {
    constructor() {
        super();

        this.state = {
            displayColorPrimary: false,
            displayColorText: false,
            displayColorBackground: false
        }
    }    

    handleClick = (field) => {
        this.setState({ [field]: !this.state[field] });
        if (!this.state[field]) {
            this.handleCloseAllBut(field);
        }
    };

    handleClose = (field) => {
        this.setState({ [field]: false });
    };

    handleCloseAll() {
        this.setState({
            displayColorPrimary: false,
            displayColorText: false,
            displayColorBackground: false
        });
    };

    handleCloseAllBut(field) {
        let stateTemp = {
            displayColorPrimary: false,
            displayColorText: false,
            displayColorBackground: false
        }
        for (let index in stateTemp) {
            if (index === field) {
                stateTemp[index] = true;
            }
        }
        this.setState((prevState) => {
            return Object.assign(prevState, stateTemp)
        })
    };

    styleColor(field) {
        return reactCSS({
            'default': {
                color: {
                    width: '50px',
                    height: '30px',
                    border: '1px solid rgb(0, 0, 0)',
                    borderRadius: '2px',
                    background: this.props.getPropertyState(field),
                }
            }
        });
    };

    clearStyle(field) {
        this.props.setPropertyState(field, '#fff');
    };

    saveFileInState(file: any, pathImage: string, editField: string, endpoints: string) {
        if (pathImage) {
            let tempObjecyForSave = {
                fileImage: file,
                pathSaveImage: endpoints,
                fieldNameToSave: editField
            };
            this.props.setPropertyState(editField + '_Temp', tempObjecyForSave);
        }
        this.props.setPropertyState(editField, pathImage);
    }

    render() {
        return (
            <div>
                <div className="form-group">
                    <label htmlFor={getHospitalBrandingKeys().brandName}>{this.props.language[Vocabulary.SNAPMDADMIN_SETTINGSV2_PLATFORM_NAME]}</label>
                    <input
                        type="text"
                        className="form-control"
                        id={getHospitalBrandingKeys().brandName}
                        value={this.props.getPropertyState(getHospitalBrandingKeys().brandName)}
                        onChange={(e: any) => {
                            this.props.setPropertyState(getHospitalBrandingKeys().brandName, e.target.value);
                        }}
                    />
                </div>
                <div className="form-group">
                    <label htmlFor={getHospitalBrandingKeys().brandTitle}>{this.props.language[Vocabulary.SNAPMDADMIN_SETTINGSV2_SUB_TITLE]}</label>
                    <input
                        type="text"
                        className="form-control"
                        id={getHospitalBrandingKeys().brandTitle}
                        value={this.props.getPropertyState(getHospitalBrandingKeys().brandTitle)}
                        onChange={(e: any) => {
                            this.props.setPropertyState(getHospitalBrandingKeys().brandTitle, e.target.value);
                        }}
                    />
                </div>
                <div className="form-group">
                    <label htmlFor={getHospitalBrandingKeys().hospitalDomainName}>{this.props.language[Vocabulary.SNAPMDADMIN_SETTINGSV2_PLATFORM_URL]}</label>
                    <input
                        type="text"
                        className="form-control"
                        id={getHospitalBrandingKeys().hospitalDomainName}
                        value={this.props.getPropertyState(getHospitalBrandingKeys().hospitalDomainName)}
                        onChange={(e: any) => {
                            this.props.setPropertyState(getHospitalBrandingKeys().hospitalDomainName, e.target.value);
                        }}
                    />
                </div>
                <div className="form-group">
                    <label htmlFor="primaryBrandColor">{this.props.language[Vocabulary.SNAPMDADMIN_SETTINGSV2_PRIMARY_BRAND_COLOR]}</label>
                    <div className='row' id='primaryBrandColor'>
                        <div className='col-md-2 col-3'>
                            <div className='smd-color-picker-swatch smd-cursor-pointer'>
                                <div style={this.styleColor(getHospitalBrandingKeys().brandColor).color} onClick={() => { this.handleClick('displayColorPrimary'); }} />
                            </div>
                            {this.state.displayColorPrimary ? <div className='smd-color-picker-popover' >
                                <div
                                    className='smd-color-picker-cover'
                                    onClick={() => { this.handleClose('displayColorPrimary'); }}
                                />
                                <SketchPicker
                                    color={this.props.getPropertyState(getHospitalBrandingKeys().brandColor)}
                                    onChange={(color) => {
                                        this.props.setPropertyState(getHospitalBrandingKeys().brandColor, color.hex);
                                    }}
                                />
                                <button
                                    className='btn btn-info smd-button-width-220 smd-cursor-pointer'
                                    onClick={() => {
                                        this.handleClose('displayColorPrimary');
                                    }}
                                >{this.props.language[Vocabulary.SNAPMDADMIN_SETTINGSV2_OK]}</button>
                            </div> : null}
                        </div>
                        <button
                            className='btn smb-clear-btn-max-height smd-cursor-pointer'
                            onClick={() => {
                                this.clearStyle(getHospitalBrandingKeys().brandColor);
                            }}
                        > {this.props.language[Vocabulary.SNAPMDADMIN_SETTINGSV2_CLEAR]} </button>
                    </div>
                </div>
                <div className="form-group">
                    <label htmlFor="brandTextColor">{this.props.language[Vocabulary.SNAPMDADMIN_SETTINGSV2_BRAND_TEXT_COLOR]}</label>
                    <div className='row' id='brandTextColor'>
                        <div className='col-md-2 col-3'>
                            <div className='smd-color-picker-swatch smd-cursor-pointer' >
                                <div style={this.styleColor(getHospitalBrandingKeys().BrandTextColor).color} onClick={() => { this.handleClick('displayColorText'); }} />
                            </div>
                            {this.state.displayColorText ? <div className='smd-color-picker-popover'>
                                <div
                                    className='smd-color-picker-cover'
                                    onClick={() => { this.handleClose('displayColorText'); }}
                                />
                                <SketchPicker
                                    color={this.props.getPropertyState(getHospitalBrandingKeys().BrandTextColor)}
                                    onChange={(color) => {
                                        this.props.setPropertyState(getHospitalBrandingKeys().BrandTextColor, color.hex);
                                    }}
                                />
                                <button
                                    className='btn btn-info smd-button-width-220 smd-cursor-pointer'
                                    onClick={() => {
                                        this.handleClose('displayColorText');
                                    }}
                                >{this.props.language[Vocabulary.SNAPMDADMIN_SETTINGSV2_OK]}</button>
                            </div> : null}

                        </div>
                        <button
                            className='btn smb-clear-btn-max-height smd-cursor-pointer'
                            onClick={() => {
                                this.clearStyle(getHospitalBrandingKeys().BrandTextColor);
                            }}
                        > {this.props.language[Vocabulary.SNAPMDADMIN_SETTINGSV2_CLEAR]} </button>
                    </div>
                </div>
                <div className="form-group">
                    <label htmlFor="brandBackgroundColor">{this.props.language[Vocabulary.SNAPMDADMIN_SETTINGSV2_BRAND_BACKGROUND_COLOR]}</label>
                    <div className='row' id='brandBackgroundColor'>
                        <div className='col-md-2 col-3'>
                            <div className='smd-color-picker-swatch smd-cursor-pointer' >
                                <div style={this.styleColor(getHospitalBrandingKeys().BrandBackgroundColor).color} onClick={() => { this.handleClick('displayColorBackground'); }} />
                            </div>
                            {this.state.displayColorBackground ? <div className='smd-color-picker-popover'>
                                <div
                                    className='smd-color-picker-cover'
                                    onClick={() => { this.handleClose('displayColorBackground'); }}
                                />
                                <SketchPicker
                                    color={this.props.getPropertyState(getHospitalBrandingKeys().BrandBackgroundColor)}
                                    onChange={(color) => {
                                        this.props.setPropertyState(getHospitalBrandingKeys().BrandBackgroundColor, color.hex);
                                    }}
                                />
                                <button
                                    className='btn btn-info smd-button-width-220 smd-cursor-pointer'
                                    onClick={() => {
                                        this.handleClose('displayColorBackground');
                                    }}
                                >{this.props.language[Vocabulary.SNAPMDADMIN_SETTINGSV2_OK]}</button>
                            </div> : null}

                        </div>
                        <button
                            className='btn smb-clear-btn-max-height smd-cursor-pointer'
                            onClick={() => {
                                this.clearStyle(getHospitalBrandingKeys().BrandBackgroundColor);
                            }}
                        > {this.props.language[Vocabulary.SNAPMDADMIN_SETTINGSV2_CLEAR]} </button>
                    </div>
                </div>
                <LoadImageCmpnt
                    title={'Brand Background Image'}
                    imagePath={this.props.getPropertyState(getHospitalBrandingKeys().BrandBackgroundImage)}
                    language={this.props.language}
                    pathForFileCallback={(file: any, pathImage: string) => {
                        this.saveFileInState(file, pathImage, getHospitalBrandingKeys().BrandBackgroundImage, Endpoints.snapmdAdmin.brandBackgroundImage);
                    }}
                />
                <LoadImageCmpnt
                    title={'Brand Background Login Image'}
                    imagePath={this.props.getPropertyState(getHospitalBrandingKeys().BrandBackgroundLoginImage)}
                    language={this.props.language}
                    pathForFileCallback={(file: any, pathImage: string) => {
                        this.saveFileInState(file, pathImage, getHospitalBrandingKeys().BrandBackgroundLoginImage, Endpoints.snapmdAdmin.brandBackgroundLoginImage);
                    }}
                />
                <LoadImageCmpnt
                    title={'Contact Image'}
                    imagePath={this.props.getPropertyState(getHospitalBrandingKeys().ContactUsImage)}
                    language={this.props.language}
                    pathForFileCallback={(file: any, pathImage: string) => {
                        this.saveFileInState(file, pathImage, getHospitalBrandingKeys().ContactUsImage, Endpoints.snapmdAdmin.contactUsImage);
                    }}
                />
                <LoadImageCmpnt
                    title={'Logo'}
                    imagePath={this.props.getPropertyState(getHospitalBrandingKeys().hospitalImage)}
                    language={this.props.language}
                    pathForFileCallback={(file: any, pathImage: string) => {
                        this.saveFileInState(file, pathImage, getHospitalBrandingKeys().hospitalImage, Endpoints.snapmdAdmin.providerLogo);
                    }}
                />
            </div>
        );
    }
}