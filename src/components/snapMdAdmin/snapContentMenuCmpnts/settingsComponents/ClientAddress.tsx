import * as React from 'react';
import Select from 'react-select';

import * as Vocabulary from '../../../../localization/vocabulary';
import { getHospitalAddressKeys } from '../../../../models/DTO/hospitalSettingDTO';

export interface IClientAddressProps {
    language: any[];
    countries: any[];

    setPropertyState: (field: string, value: any)=>void;
    getPropertyState: (field: string)=>any;
}

export class ClientAddress extends React.Component<IClientAddressProps, any> {
    constructor() {
        super();
    }

    createState(){
        let state:Array<any> = [];
        for(let index in getHospitalAddressKeys()){
            state.push(getHospitalAddressKeys()[index]);
        }
        return state;
    }

    countriesForSelect(){
        let countries: Array<any> = [];
        for(let index in this.props.countries){
            countries.push(
                {
                    value: this.props.countries[index].alpha2,
                    label: this.props.countries[index].name
                }
            );
        }
        return countries;
    }

    render(){
        return(
            <div>
                <div className="form-group">
                    <label htmlFor={getHospitalAddressKeys().line1}>{this.props.language[Vocabulary.SNAPMDADMIN_SETTINGSV2_ADDRESS_LINE_1]}</label>
                    <input
                        type="text"
                        className="form-control"
                        id={getHospitalAddressKeys().line1}
                        value={this.props.getPropertyState(getHospitalAddressKeys().line1)}
                        onChange={(e: any)=>{
                            this.props.setPropertyState(getHospitalAddressKeys().line1, e.target.value);
                        }}
                    />
                </div>
                <div className="form-group">
                    <label htmlFor={getHospitalAddressKeys().line2}>{this.props.language[Vocabulary.SNAPMDADMIN_SETTINGSV2_ADDRESS_LINE_2]}</label>
                    <input
                        type="text"
                        className="form-control"
                        id={getHospitalAddressKeys().line2}
                        value={this.props.getPropertyState(getHospitalAddressKeys().line2)}
                        onChange={(e: any)=>{
                            this.props.setPropertyState(getHospitalAddressKeys().line2, e.target.value);
                        }}
                    />
                </div>
                <div className="form-group">
                    <label htmlFor={getHospitalAddressKeys().city}>{this.props.language[Vocabulary.SNAPMDADMIN_SETTINGSV2_CITY]}</label>
                    <input
                        type="text"
                        className="form-control"
                        id={getHospitalAddressKeys().city}
                        value={this.props.getPropertyState(getHospitalAddressKeys().city)}
                        onChange={(e: any)=>{
                            this.props.setPropertyState(getHospitalAddressKeys().city, e.target.value);
                        }}
                    />
                </div>
                <div className="form-group">
                    <label htmlFor={getHospitalAddressKeys().state}>{this.props.language[Vocabulary.SNAPMDADMIN_SETTINGSV2_STATE_PROVINCE]}</label>
                    <input
                        type="text"
                        className="form-control"
                        id={getHospitalAddressKeys().state}
                        value={this.props.getPropertyState(getHospitalAddressKeys().state)}
                        onChange={(e: any)=>{
                            this.props.setPropertyState(getHospitalAddressKeys().state, e.target.value);
                        }}
                    />
                </div>
                <div className="form-group">
                    <label htmlFor={getHospitalAddressKeys().postalCode}>{this.props.language[Vocabulary.SNAPMDADMIN_SETTINGSV2_ZIP_POSTAL_CODE]}</label>
                    <input
                        type="text"
                        className="form-control"
                        id={getHospitalAddressKeys().postalCode}
                        value={this.props.getPropertyState(getHospitalAddressKeys().postalCode)}
                        onChange={(e: any)=>{
                            this.props.setPropertyState(getHospitalAddressKeys().postalCode, e.target.value);
                        }}
                    />
                </div>
                <div className="form-group">
                    <label htmlFor={getHospitalAddressKeys().countryCode}>{this.props.language[Vocabulary.SNAPMDADMIN_SETTINGSV2_COUNTRY]}</label>
                    <Select 
                        id={getHospitalAddressKeys().countryCode}
                        options={this.countriesForSelect()}
                        value={this.props.getPropertyState(getHospitalAddressKeys().countryCode)}
                        clearable={false}
                        onChange={(e)=>{
                            this.props.setPropertyState(getHospitalAddressKeys().countryCode, e.value)
                        }}
                    />
                </div>
            </div>
        );
    }
}