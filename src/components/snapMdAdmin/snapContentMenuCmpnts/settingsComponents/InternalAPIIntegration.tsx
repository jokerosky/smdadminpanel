import * as React from 'react';
import Select from 'react-select';
import * as _ from 'lodash';

import * as Vocabulary from '../../../../localization/vocabulary';
import { language } from '../../../../enums/languages';
import { getHospitalInternalAPIIntegrationKeys } from '../../../../models/DTO/hospitalSettingDTO';

export interface IInternalAPIIntegrationProps {
    language: any;
    partners: any;
    hospitalId: any;

    setPropertyState: (field: string, value: any) => void;
    getPropertyState: (field: string) => any;
    saveSettings: (settings: any, hospitalId: number) => void;
    postHospitalPartnerType: (data: any, hospitalId: number) => void;
}

export class InternalAPIIntegration extends React.Component<IInternalAPIIntegrationProps, any> {
    constructor() {
        super();

        this.state = {
            internalAPIIntegrationSelect: 0
        }
    }

    optionsInternalAPIIntegrationSelect = [
        {
            value: 0,
            label: 'None'
        },
        {
            value: 1,
            label: 'NEW NAME'
        },
        {
            value: 2,
            label: 'SnapMD Api'
        }
    ];

    internalAPIIntegrationForm = [
        {
            name: Vocabulary.SNAPMDADMIN_SETTINGSV2_API_USER_NAME,
            key: getHospitalInternalAPIIntegrationKeys().AdminUserName
        },
        {
            name: Vocabulary.SNAPMDADMIN_SETTINGSV2_API_USER_PASSWORD,
            key: getHospitalInternalAPIIntegrationKeys().AdminPassword
        }
    ];

    renderInput(elements) {
        let component: Array<any> = [];
        for (let index in elements) {
            component.push(
                <div key={elements[index].key + '_' + index} className="form-group">
                    <label htmlFor={elements[index].key}>{this.props.language[elements[index].name]}</label>
                    <input
                        type="text"
                        className="form-control"
                        id={elements[index].key}
                        value={this.props.getPropertyState(elements[index].key)}
                        onChange={(e: any) => {
                            this.props.setPropertyState(elements[index].key, e.target.value);
                        }}
                    />
                </div>
            );
        }
        return component;
    }

    saveSnapMDAPI() {
        let fieldNameToSaves: string[] = [
            getHospitalInternalAPIIntegrationKeys().AdminUserName,
            getHospitalInternalAPIIntegrationKeys().AdminPassword
        ];
        let settings: any = {};

        for (let index in fieldNameToSaves) {
            settings['Integrations.SnapMD.' + fieldNameToSaves[index]] = this.props.getPropertyState(fieldNameToSaves[index]);
        }
        this.props.saveSettings(settings, this.props.hospitalId);
    }

    saveSettingsAndEnableTheInternalAPI() {
        this.saveSnapMDAPI();

        let searchIndex = _.findIndex(this.props.partners, { name: this.optionsInternalAPIIntegrationSelect[this.state.internalAPIIntegrationSelect].label });
        let settings: any = {
            [getHospitalInternalAPIIntegrationKeys().externalHospitalId]: this.props.getPropertyState(getHospitalInternalAPIIntegrationKeys().externalHospitalId),
            [getHospitalInternalAPIIntegrationKeys().partnerId]: this.props.partners[searchIndex].partnerId
        };
        this.props.postHospitalPartnerType(settings, this.props.hospitalId);
    }

    renderNone() {
        return (
            <div>
                <label>{this.props.language[Vocabulary.SNAPMDADMIN_SETTINGSV2_NO_INTERNAL_API_INTEGRATION]}</label>
                <div className="form-group">
                    <button
                        className='btn'
                        onClick={() => { }}
                    >{this.props.language[Vocabulary.SNAPMDADMIN_SETTINGSV2_DISABLE_THE_INTERNAL_API]}</button>
                </div>
            </div>
        );
    }

    renderNewName() {
        return null;
    }

    renderSnapMDAPI() {
        return (
            <div>
                {this.renderInput(this.internalAPIIntegrationForm)}
                <div className="form-group">
                    <button
                        className='btn'
                        onClick={() => { this.saveSnapMDAPI(); }}
                    >{this.props.language[Vocabulary.SNAPMDADMIN_SAVE_SETTINGS]}</button>
                </div>
                <div className="form-group">
                    <button
                        className='btn'
                        onClick={() => { this.saveSettingsAndEnableTheInternalAPI(); }}
                    >{this.props.language[Vocabulary.SNAPMDADMIN_SETTINGSV2_SAVE_SETTINGS_AND_ENABLE_THE_INTERNAL_API]}</button>
                </div>
            </div>
        );
    }

    renderElement() {
        let element: Array<any> = [
            this.renderNone(),
            this.renderNewName(),
            this.renderSnapMDAPI()
        ];

        return element[this.state.internalAPIIntegrationSelect];
    }

    render() {
        return (
            <div>
                <div className="form-group">
                    <label htmlFor="ehrPartner"></label>
                    <Select
                        options={this.optionsInternalAPIIntegrationSelect}
                        value={this.state.internalAPIIntegrationSelect}
                        clearable={false}
                        onChange={(e) => {
                            this.setState({
                                internalAPIIntegrationSelect: e.value
                            });
                        }}
                    />
                </div>
                {this.renderElement()}
            </div>
        );
    }
}