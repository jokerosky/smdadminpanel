import * as React from 'react';

import * as Vocabulary from '../../../../localization/vocabulary';
import { MODULE_KEYS } from '../../../../enums/clientSettingsKeys';
import { ToggleButton } from '../../../common/inputs/ToggleButtonCmpnt';
import { getHospitalRequiredCustomerFieldsKeys } from '../../../../models/DTO/hospitalSettingDTO';

export interface IRequiredCustomerFieldsProps {
    language: any;
    hospitalId: number;

    setPropertyState: (field: string, value: any) => void;
    getPropertyState: (field: string) => any;
    saveSettings: (settings: any, hospitalId: number) => void;
}

export class RequiredCustomerFields extends React.Component<IRequiredCustomerFieldsProps, any> {
    constructor() {
        super();
    }

    requiredCustomerFields = [
        {
            name: Vocabulary.SNAPMDADMIN_SETTINGSV2_BLOOD_TYPE,
            key: getHospitalRequiredCustomerFieldsKeys().IsBloodTypeRequired
        },
        {
            name: Vocabulary.SNAPMDADMIN_SETTINGSV2_HAIR_COLOR,
            key: getHospitalRequiredCustomerFieldsKeys().IsHairColorRequired
        },
        {
            name: Vocabulary.SNAPMDADMIN_SETTINGSV2_ETHNICITY,
            key: getHospitalRequiredCustomerFieldsKeys().IsEthnicityRequired
        },
        {
            name: Vocabulary.SNAPMDADMIN_SETTINGSV2_EYE_COLOR,
            key: getHospitalRequiredCustomerFieldsKeys().IsEyeColorRequired
        },
        {
            name: Vocabulary.SNAPMDADMIN_SETTINGSV2_HEIGHT,
            key: getHospitalRequiredCustomerFieldsKeys().IsHeightRequired
        },
        {
            name: Vocabulary.SNAPMDADMIN_SETTINGSV2_WEIGHT,
            key: getHospitalRequiredCustomerFieldsKeys().IsWeightRequired
        },
        {
            name: Vocabulary.SNAPMDADMIN_SETTINGSV2_PREFERRED_PHARMACY,
            key: getHospitalRequiredCustomerFieldsKeys().IsPharmacyRequired
        },
        {
            name: Vocabulary.SNAPMDADMIN_SETTINGSV2_PREFERRED_PROVIDER,
            key: getHospitalRequiredCustomerFieldsKeys().IsPreferredProviderRequired
        }
    ];

    renderElement() {
        let element: Array<any> = [];
        for (let index in this.requiredCustomerFields) {
            element.push(
                <li key={this.requiredCustomerFields[index].key + '_' +index} className='list-group-item smd-transitions smd-no-h-padding'>
                    <div className='smd-full-width smd-no-h-padding col-12'>
                        <div className='d-flex'>
                            <div className='smd-setting-label col-6'>{this.props.language[this.requiredCustomerFields[index].name]}</div>
                            <div className='col'>
                                <ToggleButton
                                    isChecked={this.props.getPropertyState(this.requiredCustomerFields[index].key)}
                                    onChange={(e) => {
                                        this.props.setPropertyState(this.requiredCustomerFields[index].key, e);
                                    }}
                                />
                            </div>
                        </div>
                    </div>
                </li>
            );
        }
        return element;
    }

    saveSettings(){
        let settings: any = {};
        for(let index in getHospitalRequiredCustomerFieldsKeys()){
            settings['PatientProfile.' + index] = this.props.getPropertyState(index).toString();
        }
        this.props.saveSettings(settings, this.props.hospitalId);
    }

    render() {
        return (
            <div>
                <ul className='list-group smd-transitions smd-expanded smd-10-margin-bottom'>
                    {this.renderElement()}
                </ul>
                <div className="form-group">
                    <button
                        className='btn'
                        onClick={() => { this.saveSettings();}}
                    >{this.props.language[Vocabulary.SNAPMDADMIN_SETTINGSV2_SAVE_CUSTOMER_FIELDS]}</button>
                </div>
            </div>
        );
    }
}