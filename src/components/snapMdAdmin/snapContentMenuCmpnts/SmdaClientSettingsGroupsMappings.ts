import { IMappedSetting, SettingsMapping, ToggleMappedSetting } from '../../../models/site/settingsMapping';
import { EditorType } from '../../../enums/editorTypes';
import { ObjectHelper } from '../../../utilities/objectHelper';

let standardToggleAction = (value:any, item:IMappedSetting, section:IMappedSetting[])=>{
    item.value = ObjectHelper.dataBaseStringFromBool(value);
};

let turnOffAllChildrenOnFalseAction = (value:any, item:IMappedSetting, section:IMappedSetting[])=>{
    item.value = ObjectHelper.dataBaseStringFromBool(value);
    if(!value)
    {
        item.children.forEach( x => {x.value = 'False'; x.isVisible = false; });
    }
    else{
        item.children.forEach( x => { x.isVisible = true; });
    }
};

let clientSection = [

] as IMappedSetting[];

let addressSection = [
    {
        label: 'Address Line 1',
        key: 'line1',
        editorType: EditorType.edit,
    },
    {
        label: 'Address Line 2',
        key: 'line2',
        editorType: EditorType.edit,
    },
    {
        label: 'City',
        key: 'city',
        editorType: EditorType.edit,
    },
    {
        label: 'State / Province',
        key: 'state',
        editorType: EditorType.edit,
    },
    {
        label: 'Zip / Postal Code',
        key: 'postalCode',
        editorType: EditorType.edit,
    },
    {
        label: 'Country',
        key: 'country',
    },
] as IMappedSetting[];

let modulesSection = [
    new ToggleMappedSetting(
       'mECommerce',
        'E-commerce',
        [
            new ToggleMappedSetting(
                'mHidePaymentBeforeWaiting',
                'Hide Payment Page before Waiting Room'
            )
        ],
        turnOffAllChildrenOnFalseAction
    ),
    new ToggleMappedSetting(
        'mInsVerification',
        'Ins. Verification',
        [
            new ToggleMappedSetting(
                'mInsVerificationDummy',
                'Ins. Verification Testing Commands'
            ),
            new ToggleMappedSetting(
                'mInsVerificationBeforeWaiting',
                'Insurance Verification Before Waiting Room'
            ),
        ],
        turnOffAllChildrenOnFalseAction
    ),
    new ToggleMappedSetting(
        'mOnDemand',
        'On-Demand'
    ),
    new ToggleMappedSetting(
        'mIntakeForm',
        'Intake Form',
        [
            new ToggleMappedSetting(
                'mIFOnDemand',
                'On-Demand'
            ),
            new ToggleMappedSetting(
                'mIFScheduled',
                'Scheduled'
            )
        ],
        turnOffAllChildrenOnFalseAction
    ),
    new ToggleMappedSetting(
        'mMedicalCodes',
        'Medical Codes'
    ),
    new ToggleMappedSetting(
        'mVideoBeta',
        'Video Beta'
    ),
    new ToggleMappedSetting(
        'mClinicianSearch',
        'Provider Search'
    ),
    new ToggleMappedSetting(
        'mTextMessaging',
        'Text Messaging'
    ),
    new ToggleMappedSetting(
        'mRxNTEHR',
        'RxNT EHR'
    ),
    new ToggleMappedSetting(
        'mRxNTPM',
        'RxNT PM'
    ),
    new ToggleMappedSetting(
        'mShowCTTOnScheduled',
        'Show CTT before scheduled'
    ),
    new ToggleMappedSetting(
        'mFileSharing',
        'File Sharing'
    ),
    new ToggleMappedSetting(
        'mOrganizationLocation',
        'Organization and Location'
    ),
    new ToggleMappedSetting(
        'mAddressValidation',
        'Enable Address Validation'
    ),
    new ToggleMappedSetting(
        'mHideOpenConsultation',
        'Hide Open Consultation (P2P open still allowed)'
    ),
    new ToggleMappedSetting(
        'mHideDrToDrChat',
        'Hide Dr to Dr Chat'
    ),
    new ToggleMappedSetting(
        'mDisablePhoneConsultation',
        'Disable Phone Consultation'
    ),
    new ToggleMappedSetting(
        'mDrToDrChatInAdmin',
        'Dr to Dr Chat on Admin'
    ),
    new ToggleMappedSetting(
        'mAnnotation',
        'Annotation'
    ),
    new ToggleMappedSetting(
        'mKubi',
        'Kubi'
    ),
    new ToggleMappedSetting(
        'mConsultationVideoArchive',
        'Consultation Video Archive'
    ),
    
 ] as IMappedSetting[];

let mobileSection = [
    
] as IMappedSetting[];

let platformSeatsSection = [
    
] as IMappedSetting[];

let brandingSection = [
    
] as IMappedSetting[];

let paymentPartnerSection = [
    
] as IMappedSetting[];

let ePrescriptionPartnerSection = [
    
] as IMappedSetting[];

let singleSignOnSection = [
    
] as IMappedSetting[];

let consultationParametersSection = [
    
] as IMappedSetting[];

let requiredCustomerFieldsSection = [
    
] as IMappedSetting[];

let customAppLinksSection = [
    
] as IMappedSetting[];

let ehrIntegrationSection = [
    
] as IMappedSetting[];

let InternalAPIIntegrationSection = [
    
] as IMappedSetting[];
        

export {
    clientSection,
    addressSection,
    modulesSection,
    mobileSection,
    platformSeatsSection,
    brandingSection,
    paymentPartnerSection,
    ePrescriptionPartnerSection,
    singleSignOnSection,
    consultationParametersSection,
    requiredCustomerFieldsSection,
    customAppLinksSection,
    ehrIntegrationSection,
    InternalAPIIntegrationSection
};