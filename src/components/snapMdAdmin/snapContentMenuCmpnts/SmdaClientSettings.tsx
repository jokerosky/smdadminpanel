import * as React from 'react';
import { connect } from 'react-redux';
import * as _ from 'lodash';

import * as snapMdAdminActions from '../../../actions/snapMdAdminActions';
import { SnapMDServiceLocator } from '../../../dependenciesInjectionConfiguration'
import { MODULE_KEYS, SETTINGS_KEYS } from '../../../enums/clientSettingsKeys';

import * as types from '../../../enums/actionTypes';
import { SettingsGroup } from '../../common/SettingsGroupCmpnt';

import * as vocabulary from '../../../localization/vocabulary';
import { ILocalizable } from '../../../localization/ILocalizable';
import { EditorType } from '../../../enums/editorTypes'; 
import { HospitalSettingDTO } from '../../../models/DTO/hospitalSettingDTO';
import { IMappedSetting, SettingsMapping } from '../../../models/site/settingsMapping';

import { Details } from '../../../models/state/snapMDAdminDetails';

// definition of sections for settings and their interraction and hierarchy
import { clientSection, addressSection ,modulesSection, mobileSection } from './SmdaClientSettingsGroupsMappings';

export interface ISmdaClientSettingsCmpntProps extends ILocalizable {
    hospitalId:number;
    hospitalName?:string;
    loadedSettings?:Object;
    clientDetails?:Details;

    settingChanged:(setting:IMappedSetting)=>void;
    loadAllSettingsData:(hospitalId:number)=>void;
}

export class SmdaClientSettingsCmpnt extends React.Component<ISmdaClientSettingsCmpntProps, any> {
    constructor(props){
        super(props);
     
        let collapsedMap = {
            client: true,
            modules: true,
            mobile: true,
            platformSeats: true,
            branding: true,
            paymentPartner: true,
            ePrescriptionPartner: true,
            sso: true,
            consultationParameters: true,
            requiredFields: true,
            customAppLinks: true,
            ehrIntegration: true,
            internalApiIntegration: true
        };

        this.state = {
            modulesSettings:{ mappedSettings:[] },
            addressSettings:{ mappedSettings:[] },
            collapsedMap
        };

        this.save = this.save.bind(this);
        this.change = this.change.bind(this);
        this.toggleAll = this.toggleAll.bind(this);
    }

    componentDidMount(){
        this.props.loadAllSettingsData(this.props.hospitalId);
        this.remapSettings(this.props);
    };

    componentWillReceiveProps(nextProps){
        if(nextProps.hospitalId !== this.props.hospitalId){
            this.props.loadAllSettingsData(nextProps.hospitalId);
        }

        this.remapSettings(nextProps);
    }

    remapSettings(props: ISmdaClientSettingsCmpntProps){
        let modulesSettings = new SettingsMapping(modulesSection, props.loadedSettings );
        let clientSettings = new SettingsMapping(clientSection, props.loadedSettings );
        let addressSettings = new SettingsMapping(addressSection, props.clientDetails.address.addressObject );

        this.setState({
            modulesSettings,
            addressSettings
        });
    }

        

    change(setting :IMappedSetting) {
        this.props.settingChanged(setting);
    }

    collectSettingsToSave(){
        let result = [];
        let modulesSettings = this.state.modulesSettings.getSettingsArrayToSave();
        result = result.concat(modulesSettings);

        return result;
    };

    save() {
        //
        let data = this.collectSettingsToSave();
        // request for client settings https://snap.local/api/v2/admin/clients/{id}
        // request for modules and additional settings https://snap.local/api/v2/admin/hospitals/settings/{id}
        

        console.log(this.state.settings);
    }

    toggleAll(isCollapse){
        let collapsedMap = this.state.collapsedMap;
        for(let key in collapsedMap){
            collapsedMap[key] = isCollapse;
        }
        this.setState({collapsedMap});
    }

    render(){

        return (
            <div className="container-fluid">
                <dl className='row smd-v-padding'>
                    <dd className='col'>
                        <h5>{this.props.hospitalName} - {this.props.language[vocabulary.SETTINGS]}</h5>
                    </dd>
                </dl>
                <div className='pull-left'>
                    <button 
                        className='btn btn-sm btn-outline-secondary'
                        onClick={()=>{this.toggleAll(false)}} >
                        {this.props.language[vocabulary.SNAPMDADMIN_EXPAND_ALL]}
                    </button>&nbsp;
                    <button 
                        className='btn btn-outline-dark btn-sm'
                        onClick={()=>{this.toggleAll(true)}} >
                        {this.props.language[vocabulary.SNAPMDADMIN_COLLAPSE_ALL]}
                    </button>
                </div>
                <button className='btn btn-success btn-lg pull-right'
                    onClick={this.save} 
                    > {this.props.language[vocabulary.SNAPMDADMIN_BUTTON_SAVE]} 
                </button>
                <div className='smd-v-padding clearfix'></div>
                
                <SettingsGroup 
                    title = {this.props.language[vocabulary.SETTINGS_CLIENT]}
                    isCollapsed = {this.state.collapsedMap.client}
                    onToggle = {(isCollapsed)=>{ this.state.collapsedMap.client = isCollapsed; }}
                    onSettingChanged = {this.change}
                />
                
                <SettingsGroup 
                    settings = {this.state.addressSettings.mappedSettings} 
                    title = {'Address'}
                    isCollapsed = {false}
                    onToggle = {(isCollapsed)=>{  }}
                    onSettingChanged = {(set)=>{  debugger;}}
                />

                <SettingsGroup 
                    settings = { this.state.modulesSettings.mappedSettings } 
                    title = {this.props.language[vocabulary.SETTINGS_MODULES]}
                    isCollapsed = {this.state.collapsedMap.modules}
                    onToggle = {(isCollapsed)=>{ this.state.collapsedMap.modules = isCollapsed; }}
                    onSettingChanged = {this.change}
                />
                <SettingsGroup 
                    title = {this.props.language[vocabulary.SETTINGS_MOBILE]}
                    isCollapsed = {this.state.collapsedMap.mobile}
                    onToggle = {(isCollapsed)=>{ this.state.collapsedMap.mobile = isCollapsed; }}
                    onSettingChanged = {this.change}
                />
                <SettingsGroup 
                    title = {this.props.language[vocabulary.SETTINGS_PLATFORM_SEATS]}
                    isCollapsed = {this.state.collapsedMap.platformSeats}
                    onToggle = {(isCollapsed)=>{ this.state.collapsedMap.platformSeats = isCollapsed; }}
                    onSettingChanged = {this.change}
                />
                <SettingsGroup 
                    title = {this.props.language[vocabulary.SETTINGS_BRANDING]}
                    isCollapsed = {this.state.collapsedMap.branding}
                    onToggle = {(isCollapsed)=>{ this.state.collapsedMap.branding = isCollapsed; }}
                    onSettingChanged = {this.change}
                />
                <SettingsGroup 
                    title = {this.props.language[vocabulary.SETTINGS_PAYMENT_PARTNER]}
                    isCollapsed = {this.state.collapsedMap.paymentPartner}
                    onToggle = {(isCollapsed)=>{ this.state.collapsedMap.paymentPartner = isCollapsed; }}
                    onSettingChanged = {this.change}
                />
                <SettingsGroup 
                    title = {this.props.language[vocabulary.SETTINGS_EPRESCRIPTION_PARTNER]}
                    isCollapsed = {this.state.collapsedMap.ePrescriptionPartner}
                    onToggle = {(isCollapsed)=>{ this.state.collapsedMap.ePrescriptionPartner = isCollapsed; }}
                    onSettingChanged = {this.change}
                />
                <SettingsGroup 
                    title = {this.props.language[vocabulary.SETTINGS_SINGLE_SIGN_ON]}
                    isCollapsed = {this.state.collapsedMap.sso}
                    onToggle = {(isCollapsed)=>{ this.state.collapsedMap.sso = isCollapsed; }}
                    onSettingChanged = {this.change}
                />
                <SettingsGroup 
                    title = {this.props.language[vocabulary.SETTINGS_CONSULTATION_PARAMETERS]}
                    isCollapsed = {this.state.collapsedMap.consultationParameters}
                    onToggle = {(isCollapsed)=>{ this.state.collapsedMap.consultationParameters = isCollapsed; }}
                    onSettingChanged = {this.change}
                />
                <SettingsGroup 
                    title = {this.props.language[vocabulary.SETTINGS_REQUIRED_CUSTOMER_FIELDS]}
                    isCollapsed = {this.state.collapsedMap.requiredFields}
                    onToggle = {(isCollapsed)=>{ this.state.collapsedMap.requiredFields = isCollapsed; }}
                    onSettingChanged = {this.change}
                />
                <SettingsGroup 
                    title = {this.props.language[vocabulary.SETTINGS_CUSTOM_APP_LINKS]}
                    isCollapsed = {this.state.collapsedMap.customAppLinks}
                    onToggle = {(isCollapsed)=>{ this.state.collapsedMap.customAppLinks = isCollapsed; }}
                    onSettingChanged = {this.change}
                />
                <SettingsGroup 
                    title = {this.props.language[vocabulary.SETTINGS_EHR_INTEGRATION]}
                    isCollapsed = {this.state.collapsedMap.ehrIntegration}
                    onToggle = {(isCollapsed)=>{ this.state.collapsedMap.ehrIntegration = isCollapsed; }}
                    onSettingChanged = {this.change}
                />
                <SettingsGroup 
                    title = {this.props.language[vocabulary.SETTINGS_INTERNAL_API_INTEGRATION]}
                    isCollapsed = {this.state.collapsedMap.internalApiIntegration}
                    onToggle = {(isCollapsed)=>{ this.state.collapsedMap.internalApiIntegration = isCollapsed; }}
                    onSettingChanged = {this.change}
                />

                <div className='smd-v-padding'></div>
            </div>
        );
    }
}

export function mapStateToProps(state, ownProps){
    let client = _.find(state.snapmdAdmin.clients,
         (x:any)=>{return x.hospitalId == state.snapmdAdmin.selectedId}) ;
         
    let loadedSettings = Object.assign({}, state.snapmdAdmin.client.settings);
    let clientDetails = Object.assign({}, state.snapmdAdmin.client.details);

    return {
        hospitalId: state.snapmdAdmin.misc.selectedId as number,
        hospitalName: state.snapmdAdmin.misc.hospitalName as string,
        loadedSettings,
        clientDetails,
        language: state.site.misc.vocabulary
    };
}

export function mapDispatchToProps(dispatch){
    return {
        settingChanged: (setting: IMappedSetting)=>{ dispatch({type: types.SNAPMDADMIN_CLIENT_SETTING_CHANGED, setting})},
        
        loadAllSettingsData:(hospitalId:number)=>{ 
            dispatch(snapMdAdminActions.clearState());
            dispatch(snapMdAdminActions.loadHospitalSettings(SnapMDServiceLocator, [...MODULE_KEYS, ...SETTINGS_KEYS], hospitalId));
            dispatch(snapMdAdminActions.loadHospitalAddress(SnapMDServiceLocator, hospitalId));
        }  
    };
}

export default connect(mapStateToProps, mapDispatchToProps)(SmdaClientSettingsCmpnt);