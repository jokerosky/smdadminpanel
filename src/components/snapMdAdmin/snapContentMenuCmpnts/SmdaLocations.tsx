import * as React from 'react';
import { connect } from 'react-redux';
import Select from 'react-select';

import { ModalWindow } from '../../common/ModalWindow';
import * as snapmdAdminActions from '../../../actions/snapMdAdminActions';
import { SnapMDServiceLocator } from '../../../dependenciesInjectionConfiguration';
import {EditorActions} from '../../../enums/siteEnums';
import * as locationsAction from '../../../actions/snapMdAdminClientActions/locationActions';
import { LocalizationService } from '../../../localization/localizationService';
import * as Vocabulary from '../../../localization/vocabulary';

export interface ISmdaLocationsCmpntProps{
    hospitalId:number;
    hospitalName:string;
    organizationsWithLocations:any;
    locationsForSelect: any;
    language:any;    

    loadLocations:(hospitalId:number)=>void;
    updateLocations:(locationId:number, organizationId:number, locationData:any, editorAction:string)=>void;
    deleteLocation:(locationId:number, organizationId:number)=>void;
}

export class SmdaLocationsCmpnt extends React.Component<ISmdaLocationsCmpntProps, any>{
    constructor(props){
        super(props);

        this.state = {
            modalVisible:false,
            organizationsWithLocations:[],
            selectedOrganization: 0,
            methodType: EditorActions.empty,
            locations:[],
            locationName:'',
            locationId:-1,
        }
    }

    componentWillReceiveProps(nextProps){
        if(nextProps.hospitalId !== this.props.hospitalId){
            this.props.loadLocations(nextProps.hospitalId);
            this.setState({
                selectedOrganization: 0
            });
        }
    }

    componentDidMount(){
        this.props.loadLocations(this.props.hospitalId);
    }

    changeSelect = (val) => {
        this.setState({ selectedOrganization: val.value });
    }

    changeInput = (val) => {
        this.setState({ 
            locationName: val.currentTarget.value
        });
    }

    openModal(methodType:string, locationId:number){
        this.setState({
            modalVisible:true,
            methodType: methodType,
            locationId: locationId,
        });
    }

    closeModal = ()=>{
        this.setState({
            modalVisible:false,
            methodType: EditorActions.empty
        });
    }

    addOrEditLocations(){
        let locationDispatch: any = {
            hospitalId: this.props.hospitalId,
            name: this.state.locationName,
            organizationId: this.props.organizationsWithLocations[this.state.selectedOrganization].id
        };
        if(this.state.methodType === EditorActions.edit){
            locationDispatch['id'] = this.state.locationId;
        }

        this.props.updateLocations(this.state.locationId, this.state.selectedOrganization, locationDispatch, this.state.methodType);
    }

    delLocations(locationId:number){
        this.props.deleteLocation(locationId, this.state.selectedOrganization);
    }

    renderItem(organization:any){
        let items: Array<any> = null;
        if(organization){
            items = [];
            let arrayBool = false;
            if(organization instanceof Array){
                arrayBool = true;
            }
            for(let index in organization){
                let locale = arrayBool ? organization[index] : organization;
                if(locale.statusCode === 1) {
                    let li =
                        <tr key={index} className='row'>
                            <td className='col td-smd-table'>{locale.name}</td>
                            <td className='col-2 td-smd-table'>
                                <div className='row  justify-content-around'>
                                    <button
                                        className='btn smd-cursor-pointer'
                                        name={index + 'Edit'}
                                        title={this.props.language[Vocabulary.SNAPMDADMIN_EDIT]}
                                        onClick={() => {
                                        this.setState({
                                            locationName: locale.name
                                        });
                                        this.openModal(EditorActions.edit, locale.id);
                                    }}><i className="fa fa-pencil" aria-hidden="true" title={this.props.language[Vocabulary.SNAPMDADMIN_EDIT]}></i></button>
                                    <button
                                        className='btn smd-cursor-pointer'
                                        name={index + 'Delete'}
                                        title={this.props.language[Vocabulary.SNAPMDADMIN_DELETE]}
                                        onClick={() => {
                                        this.delLocations(locale.id);
                                    }}><i className="fa fa-trash-o" aria-hidden="true" title={this.props.language[Vocabulary.SNAPMDADMIN_DELETE]}></i></button>
                                </div>
                            </td>
                        </tr>;
                items.push(li);
                }
                if (arrayBool === false){
                    break;
                }
            }
        }
        return items;
    }

    render(){
        let items;
        if(this.props.organizationsWithLocations.length > 0){
            items = this.renderItem(this.props.organizationsWithLocations[this.state.selectedOrganization].locations);
        }
        return(
            <div className="container-fluid">
                <dl className='row smd-v-padding'>
                    <dd className='col'>
                        <h5>{this.props.hospitalName} - {this.props.language[Vocabulary.SNAPMDADMIN_DOCUMENT_LOCATIONS]}</h5>
                    </dd>
                </dl>
                <div className='row smd-v-padding'>
                    <span className='col-3'>{this.props.language[Vocabulary.SNAPMDADMIN_ORGANIZATION]}</span>
                    <div className='col-6'>
                        <Select
                            name={'Select'}
                            options={this.props.locationsForSelect}
                            value={this.props.locationsForSelect[this.state.selectedOrganization]}
                            clearable={false}
                            onChange={(e) => {
                                this.changeSelect(e);
                            }}/>
                    </div>
                </div>
                <div className='col smd-v-padding'>
                    <div className='row'>
                        <button className='btn align-self-center smd-cursor-pointer'
                                name={'Record'}
                                onClick={() => {
                                    this.setState({
                                        locationName: ''
                                    })
                                    this.openModal(EditorActions.add, 0);
                                }}
                        >{this.props.language[Vocabulary.SNAPMDADMIN_ADD_NEW_RECORD]}</button>
                    </div>
                </div>
                <div className='col'>
                    <table className='table table-hover'>
                        <thead>
                        <tr className='row'>
                            <th className='col-10 th-smd-table'>{this.props.language[Vocabulary.SNAPMDADMIN_CLIENT_NAME]}</th>
                            <th className='col th-smd-table'></th>
                        </tr>
                        </thead>
                        <tbody>
                        {items || <tr><td>{''}</td></tr>}
                        </tbody>
                    </table>
                </div>
                <ModalWindow
                    visible={this.state.modalVisible}
                    title={this.props.language[Vocabulary.SNAPMDADMIN_EDIT]}
                    cancel={this.closeModal}
                    success={() => {
                        this.addOrEditLocations();
                        this.closeModal();
                    }}>
                    <div className='row justify-content-center'>
                        <span className='col-2'>{this.props.language[Vocabulary.SNAPMDADMIN_CLIENT_NAME]}</span>
                        <input
                            name='Locations'
                            placeholder={this.props.language[Vocabulary.SNAPMDADMIN_DOCUMENT_LOCATIONS]}
                            value={this.state.locationName}
                            onChange={(e) => {
                                this.changeInput(e);
                            }}/>
                    </div>
                </ModalWindow>
            </div>
        );
    }
}

function createObjectCodeSetsTypeForSelect(props){ 
    let locations = [];

    if(props.length === 0){
        return locations;
    }
    for(let index in props){
        locations.push({
            value:+index,
            label:props[index].name
        });
    }
    return locations;
}

export function mapStateToProps(state, ownProps){
    return{
        hospitalId: state.snapmdAdmin.misc.selectedId as number,
        hospitalName: state.snapmdAdmin.misc.hospitalName as string,
        locationsForSelect: createObjectCodeSetsTypeForSelect(state.snapmdAdmin.client.locations) as any, 
        organizationsWithLocations: state.snapmdAdmin.client.locations as any,
        language: state.site.misc.vocabulary as any
    }
}

export function mapDispatchToProps(dispatch){
    return{
        loadLocations:(hospitalId:number)=>{
            dispatch(locationsAction.loadOrganizationsWithLocations(SnapMDServiceLocator, hospitalId));
        },
        updateLocations:(locationId:number, organizationId:number, locationData:any, editorAction:string)=>{
            dispatch(locationsAction.updateOrgnizationsWithLocations(SnapMDServiceLocator, organizationId, locationId, locationData, editorAction));
        },
        deleteLocation:(locationId:number, organizationId:number)=>{
            dispatch(locationsAction.deleteOrganizationsWithLocations(SnapMDServiceLocator, organizationId, locationId));
        }
    }
}
export default connect(mapStateToProps, mapDispatchToProps)(SmdaLocationsCmpnt);