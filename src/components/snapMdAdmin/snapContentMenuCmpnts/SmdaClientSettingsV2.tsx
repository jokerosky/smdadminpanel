import * as React from 'react';
import { connect } from 'react-redux';
import * as _ from 'lodash';

import { ClientSettings } from './settingsComponents/ClientSettings';
import { ClientAddress } from './settingsComponents/ClientAddress';
import { ModuleSettings } from './settingsComponents/ModulesSettings';
import { MobileSettings } from './settingsComponents/MobileSettings';
import { PlatformSeats } from './settingsComponents/PlatformSeats';
import { BrandingSettings } from './settingsComponents/BrandingSettings';
import { PaymentPartner } from './settingsComponents/PaymentPartner';
import { PrescriptionPartner } from './settingsComponents/PrescriptionPartner';
import { SingleSignOn } from './settingsComponents/SingleSignOn';
import { ConsultationParameters } from './settingsComponents/ConsultationParameters';
import { RequiredCustomerFields } from './settingsComponents/RequiredCustomerFields';
import { CustomAppLinks } from './settingsComponents/CustomAppLinks';
import { EHRIntegration } from './settingsComponents/EHRIntegration';
import { InternalAPIIntegration } from './settingsComponents/InternalAPIIntegration';
import { SubscribeNotificationEvents } from './settingsComponents/SubscribeNotificationEvents';

import * as SettingsActions from '../../../actions/snapMdAdminClientActions/settingsActions';
import * as clientDetailsActions from '../../../actions/snapMdAdminClientActions/clientDetailsActions';
import * as snapMdAdminAction from '../../../actions/snapMdAdminActions';
import { SnapMDServiceLocator } from '../../../dependenciesInjectionConfiguration';
import {
    getDefaultState, getSaveHospitalClientKeys, getSaveHospitalModulesKeys, getDefaultAdditionalSettings,
    getSaveHospitalAddressKey, SaveHospitalClientDTO, HospitalModulesKeysDTO, AdditionalSettingsKeysDTO,
    getHospitalSeatsAssignedKeys, getHospitalSeatsKeys, getHospitalPlatformSeatsKeys, getHospitalClientInListsKey,
    HospitalClientInListsKeyDTO, getHospitalClientKeys, getHospitalBrandingKeys, HospitalImagesDTO
} from '../../../models/DTO/hospitalSettingDTO';
import { MODULE_KEYS, SETTINGS_KEYS, SETTINGS_KEYS2 } from '../../../enums/clientSettingsKeys';
import { ObjectHelper } from '../../../utilities/objectHelper';
import * as Vocabulary from '../../../localization/vocabulary';

export interface ISmdaClientSettingsV2Props {
    language: any;
    countries: any[];
    clientTypes: any[];
    hospitalId: number;
    settings: any;
    seats: any;
    partners: any;
    hospitalAddress: any;

    getCountries: () => void;
    getSettings: (hospitalId: number, fields: string[]) => void;
    loadSeats: (hospitalId: number) => void;
    saveSettings: (settings: any, hospitalId: number) => void;
    integrationsHospitalsPaymentGateway: (data: any, hospitalId: number) => void;
    integrationsHospitalsPartnerTypes: (data: any, hospitalId: number, partnerTypes: number) => void;
    deleteHospitalPartnerType: (data: any, hospitalId: number, partnerTypes: number) => void;
    postHospitalPartnerType: (data: any, hospitalId: number) => void;
    saveClientData: (imageObjects: HospitalImagesDTO[], settings: any, hospitalId: number) => void;
    loadHospitalAddress: (hospitalId: number) => void;
}

export class SmdaClientSettingsV2 extends React.Component<ISmdaClientSettingsV2Props, any> {
    constructor(props) {
        super(props);

        let state = {}

        this.state = Object.assign(state, getDefaultState(), props.settings, props.hospitalAddress);
        this.saveClientSettings = this.saveClientSettings.bind(this);
        this.assembleObject = this.assembleObject.bind(this);
        this.getSettingsComponents = this.getSettingsComponents.bind(this);
        this.saveImages = this.saveImages.bind(this);
    }

    statePrefix = [
        'PatientProfile.',
        'clientDetails.',
        'additionalClientDetails_'
    ]

    componentWillReceiveProps(nextProps) {
        this.setState((prevState) => {
            let newState = {};
            for (let index in prevState) {
                if (/\w{1,}_Temp/i.test(index)) {
                    delete prevState[index];
                }
            }
            return Object.assign(prevState, nextProps.settings, nextProps.seats, nextProps.hospitalAddress);
        });

        if (nextProps.hospitalId !== this.props.hospitalId) {
            nextProps.loadSeats(nextProps.hospitalId);
            nextProps.loadHospitalAddress(nextProps.hospitalId);
        }
    }

    componentWillMount() {
        this.props.getCountries();
        this.props.getSettings(this.props.hospitalId, MODULE_KEYS);
        this.props.getSettings(this.props.hospitalId, SETTINGS_KEYS);
        this.props.getSettings(this.props.hospitalId, SETTINGS_KEYS2);
        this.props.loadSeats(this.props.hospitalId);
    }

    // returns an object of components, which is then assembled into an accordion
    getSettingsComponents() {
        return {
            ClientSettings: {
                name: this.props.language[Vocabulary.SNAPMDADMIN_SETTINGSV2_CLIENT],
                component: <ClientSettings
                    language={this.props.language}
                    clientTypes={this.props.clientTypes}
                    setPropertyState={(field, value) => { this.setPropertyState(field, value); }}
                    getPropertyState={(field) => { return this.getPropertyState(field); }}
                />
            },
            ClientAddress: {
                name: this.props.language[Vocabulary.SNAPMDADMIN_SETTINGSV2_CLIENT_ADDRESS],
                component: <ClientAddress
                    language={this.props.language}
                    countries={this.props.countries}
                    setPropertyState={(field, value) => { this.setPropertyState(field, value); }}
                    getPropertyState={(field) => { return this.getPropertyState(field); }}
                />
            },
            ModuleSettings: {
                name: this.props.language[Vocabulary.SNAPMDADMIN_SETTINGSV2_MODULES],
                component: <ModuleSettings language={this.props.language} setPropertyState={(field, value) => { this.setPropertyState(field, value); }} getPropertyState={(field) => { return this.getPropertyState(field); }} />
            },
            MobileSettings: {
                name: this.props.language[Vocabulary.SNAPMDADMIN_SETTINGSV2_MOBILE],
                component: <MobileSettings language={this.props.language} setPropertyState={(field, value) => { this.setPropertyState(field, value); }} getPropertyState={(field) => { return this.getPropertyState(field); }} />
            },
            PlatformSeats: {
                name: this.props.language[Vocabulary.SNAPMDADMIN_SETTINGSV2_PLATFORM_SEATS],
                component: <PlatformSeats language={this.props.language} setPropertyState={(field, value) => { this.setPropertyState(field, value); }} getPropertyState={(field) => { return this.getPropertyState(field); }} />
            },
            BrandingSettings: {
                name: this.props.language[Vocabulary.SNAPMDADMIN_SETTINGSV2_BRANDING],
                component: <BrandingSettings
                    language={this.props.language}
                    hospitalId={this.props.hospitalId}
                    setPropertyState={(field, value) => { this.setPropertyState(field, value); }}
                    getPropertyState={(field) => { return this.getPropertyState(field); }}
                />
            },
            PaymentPartner: {
                name: this.props.language[Vocabulary.SNAPMDADMIN_SETTINGSV2_PAYMENT_PARTNER],
                component: <PaymentPartner
                    language={this.props.language}
                    hospitalId={this.props.hospitalId}
                    partners={this.props.partners}
                    setPropertyState={(field, value) => { this.setPropertyState(field, value); }}
                    getPropertyState={(field) => { return this.getPropertyState(field); }}
                    saveComponentSettings={(fields) => { this.saveComponentSettings(fields); }}
                    integrationsHospitalsPaymentGateway={this.props.integrationsHospitalsPaymentGateway}
                />
            },
            PrescriptionPartner: {
                name: this.props.language[Vocabulary.SNAPMDADMIN_SETTINGSV2_EPRESCRIPTION_PARTNER],
                component: <PrescriptionPartner
                    language={this.props.language}
                    partners={this.props.partners}
                    hospitalId={this.props.hospitalId}
                    setPropertyState={(field, value) => { this.setPropertyState(field, value); }}
                    getPropertyState={(field) => { return this.getPropertyState(field); }}
                    saveComponentSettings={(fields) => { this.saveComponentSettings(fields); }}
                    integrationsHospitalsPartnerTypes={this.props.integrationsHospitalsPartnerTypes}
                />
            },
            SingleSignOn: {
                name: this.props.language[Vocabulary.SNAPMDADMIN_SETTINGSV2_SINGLE_SIGN_ON],
                component: <SingleSignOn
                    language={this.props.language}
                    hospitalId={this.props.hospitalId}
                    setPropertyState={(field, value) => { this.setPropertyState(field, value); }}
                    getPropertyState={(field) => { return this.getPropertyState(field); }}
                    saveSettings={(settings: any, hospitalId: number) => { this.props.saveSettings(settings, hospitalId); }}
                />
            },
            ConsultationParameters: {
                name: this.props.language[Vocabulary.SNAPMDADMIN_SETTINGSV2_CONSULTATION_PARAMETERS],
                component: <ConsultationParameters
                    language={this.props.language}
                    hospitalId={this.props.hospitalId}
                    setPropertyState={(field, value) => { this.setPropertyState(field, value); }}
                    getPropertyState={(field) => { return this.getPropertyState(field); }}
                    saveSettings={(settings: any, hospitalId: number) => { this.props.saveSettings(settings, hospitalId); }}
                />
            },
            RequiredCustomerFields: {
                name: this.props.language[Vocabulary.SNAPMDADMIN_SETTINGSV2_REQUIRED_CUSTOMER_FIELDS],
                component: <RequiredCustomerFields
                    language={this.props.language}
                    hospitalId={this.props.hospitalId}
                    setPropertyState={(field, value) => { this.setPropertyState(field, value); }}
                    getPropertyState={(field) => { return this.getPropertyState(field); }}
                    saveSettings={(settings: any, hospitalId: number) => { this.props.saveSettings(settings, hospitalId); }}
                />
            },
            CustomAppLinks: {
                name: this.props.language[Vocabulary.SNAPMDADMIN_SETTINGSV2_CUSTOM_APP_LINKS],
                component: <CustomAppLinks
                    language={this.props.language}
                    hospitalId={this.props.hospitalId}
                    setPropertyState={(field, value) => { this.setPropertyState(field, value); }}
                    getPropertyState={(field) => { return this.getPropertyState(field); }}
                    saveSettings={(settings: any, hospitalId: number) => { this.props.saveSettings(settings, hospitalId); }}
                />
            },
            EHRIntegration: {
                name: this.props.language[Vocabulary.SNAPMDADMIN_SETTINGSV2_EHR_INTEGRATION],
                component: <EHRIntegration
                    language={this.props.language}
                    partners={this.props.partners}
                    hospitalId={this.props.hospitalId}
                    setPropertyState={(field, value) => { this.setPropertyState(field, value); }}
                    getPropertyState={(field) => { return this.getPropertyState(field); }}
                    saveSettings={(settings: any, hospitalId: number) => { this.props.saveSettings(settings, hospitalId); }}
                    deleteHospitalPartnerType={(data: any, hospitalId: number, partnerTypes: number) => { this.props.deleteHospitalPartnerType(data, hospitalId, partnerTypes); }}
                    postHospitalPartnerType={(data: any, hospitalId: number) => { this.props.postHospitalPartnerType(data, hospitalId); }}
                />
            },
            InternalAPIIntegration: {
                name: this.props.language[Vocabulary.SNAPMDADMIN_SETTINGSV2_INTERNAL_API_INTEGRATION],
                component: <InternalAPIIntegration
                    language={this.props.language}
                    partners={this.props.partners}
                    hospitalId={this.props.hospitalId}
                    setPropertyState={(field, value) => { this.setPropertyState(field, value); }}
                    getPropertyState={(field) => { return this.getPropertyState(field); }}
                    saveSettings={(settings: any, hospitalId: number) => { this.props.saveSettings(settings, hospitalId); }}
                    postHospitalPartnerType={(data: any, hospitalId: number) => { this.props.postHospitalPartnerType(data, hospitalId); }}
                />
            },
            SubscribeNotificationEvents: {
                name: this.props.language[Vocabulary.SNAPMDADMIN_SETTINGSV2_SUBSCRIBE_NOTIFICATION_EVENTS],
                component: <SubscribeNotificationEvents language={this.props.language} setPropertyState={(field, value) => { this.setPropertyState(field, value); }} getPropertyState={(field) => { return this.getPropertyState(field); }} />
            }
        }
    }

    // sets the necessary data to the required field
    setPropertyState(field: any, value: any) {
        this.setState({
            [field]: value
        });
    }

    // получает из нужного поля состояния данные
    getPropertyState(field: any) {
        return this.state[field];
    }

    // checks whether the field is a flag and converts it to a string
    assembleObject(fieldObject: any, isflag: boolean) {
        let assembledObject: any = {};
        for (let index in fieldObject) {
            if (isflag) {
                assembledObject[index] = ObjectHelper.dataBaseStringFromBool(this.state[index]);
            } else {
                if (index === getSaveHospitalClientKeys().cliniciansCount) {
                    assembledObject[index] = this.state[getHospitalPlatformSeatsKeys().snEnterprise];
                } else if (index === getSaveHospitalClientKeys().totalUsersCount) {
                    assembledObject[index] = this.state[getHospitalPlatformSeatsKeys().snTotalSeats];
                } else {
                    assembledObject[index] = this.state[index];
                }
            }
        }
        return assembledObject;
    }

    // saves general client data
    saveClientSettings() {
        let client: any = {};
        let settings: any = {};
        let additionalSettings: any = {};
        let hospitalAddress: any = {};
        let clientInLists: any = {};

        client = this.assembleObject(getSaveHospitalClientKeys(), false);
        settings = this.assembleObject(getSaveHospitalModulesKeys(), true);

        additionalSettings = this.assembleObject(getDefaultAdditionalSettings(), false);
        hospitalAddress = {
            addressObject: this.assembleObject(getSaveHospitalAddressKey(), false),
            hospitalId: this.state.hospitalId
        };
        clientInLists = this.assembleObject(getHospitalClientInListsKey(), false);
        let foundIndex = _.findIndex(this.props.clientTypes, { key: this.state[getHospitalClientKeys().hospitalType] });
        clientInLists.hospitalTypeName = foundIndex > -1 ? this.props.clientTypes[foundIndex].value : null;
        clientInLists.seats = this.state[getHospitalPlatformSeatsKeys().snTotalSeats];

        let resultSettings = {
            client,
            settings,
            additionalSettings,
            hospitalAddress,
            clientInLists
        }
        return resultSettings;
    }

    saveImages() {
        let imagesGields = {
            [getHospitalBrandingKeys().BrandBackgroundImage + '_Temp']: '',
            [getHospitalBrandingKeys().BrandBackgroundLoginImage + '_Temp']: '',
            [getHospitalBrandingKeys().ContactUsImage + '_Temp']: '',
            [getHospitalBrandingKeys().hospitalImage + '_Temp']: ''
        };
        let imageObjects: Array<HospitalImagesDTO> = [];

        for (let index in imagesGields) {
            if (index in this.state) {
                imageObjects.push({
                    pathSaveImage: this.state[index].pathSaveImage,
                    fileImage: this.state[index].fileImage,
                    fieldNameToSave: this.state[index].fieldNameToSave
                })
            }
        }

        this.props.saveClientData(imageObjects, this.saveClientSettings(), this.props.hospitalId);
    }

    // saves client settings
    saveComponentSettings(settings: any) {
        this.props.saveSettings(settings, this.props.hospitalId);
    }

    renderAccordion() {
        if (typeof this.props.language !== "undefined") {
            let accordionComponents: Array<any> = [];
            for (let index in this.getSettingsComponents()) {
                accordionComponents.push(
                    <div key={index} className='card'>
                        <div className='card-header' id={`heading${index}`}>
                            <button
                                id={'showComponent' + index}
                                className={`btn col-12 smd-cursor-pointer ${this.state[index] ? 'btn-primary' : ''}`}
                                data-toggle="collapse"
                                data-target={`#collapse${index}`}
                                aria-expanded="false"
                                aria-controls={`collapse${index}`}
                                onClick={() => {
                                    this.setState({
                                        [index]: this.state[index] ? false : true
                                    });
                                }}
                            >{this.getSettingsComponents()[index].name}</button>
                        </div>
                        <div id={`collapse${index}`} className="collapse" aria-labelledby={`heading${index}`} >
                            <div className='card-body'>
                                {this.getSettingsComponents()[index].component}
                            </div>
                        </div>
                    </div>
                );
            }
            return accordionComponents;
        }
    }

    render() {
        return (
            <div>
                <div className='row justify-content-end'>
                    <div className='col-lg-3 col-4 smd-margin-top-bottom-10px'>
                        <button
                            id='btnSave'
                            className='btn btn-lg btn-success smd-cursor-pointer col-md-12'
                            onClick={() => {
                                this.saveImages();
                            }}
                        >{this.props.language[Vocabulary.SNAPMDADMIN_BUTTON_SAVE]}</button>
                    </div>
                </div>
                {this.renderAccordion()}
                <div className='row justify-content-end'>
                    <div className='col-lg-3 col-4 smd-margin-top-bottom-10px'>
                        <button
                            className='btn btn-lg btn-success smd-cursor-pointer col-md-12'
                            onClick={() => {
                                this.saveImages();
                            }}
                        >{this.props.language[Vocabulary.SNAPMDADMIN_BUTTON_SAVE]}</button>
                    </div>
                </div>
            </div>
        );
    }
}

function objectFormationSeats(seats, seatsAssigned) {
    let tempSeats: any = {};
    for (let index in getHospitalSeatsKeys()) {
        tempSeats[index] = seats[index];
    }
    for (let index in getHospitalSeatsAssignedKeys()) {
        tempSeats[index] = seatsAssigned[index.replace('Assigned', '')];
    }
    return tempSeats;
}

export function mapStateToProps(state, ownProps) {
    let settings: any = {};
    let seats: any = {};
    let seatsAssigned: any = {};

    if (state.snapmdAdmin.client.details !== undefined) {
        settings = Object.assign(state.snapmdAdmin.client.settings, state.snapmdAdmin.client.details.clientData);
        seats = objectFormationSeats(state.snapmdAdmin.client.details.seats, state.snapmdAdmin.client.details.seatsAssigned);
    }
    return {
        language: state.site.misc.vocabulary as any,
        countries: state.snapmdAdmin.lists.countries as any[],
        clientTypes: state.snapmdAdmin.lists.clientsTypes as any[],
        hospitalId: state.snapmdAdmin.misc.selectedId as number,
        settings: settings as any,
        seats: seats as any,
        partners: state.snapmdAdmin.lists.partner as any[],
        hospitalAddress: state.snapmdAdmin.client.details.address.addressObject as any
    }
}

export function mapDispatchToProps(dispatch) {
    return {
        getCountries: () => {
            dispatch(SettingsActions.getCountries(SnapMDServiceLocator));
        },
        getSettings: (hospitalId: number, fields: string[]) => {
            dispatch(SettingsActions.getClientSettings(SnapMDServiceLocator, hospitalId, fields));
        },
        saveSettings: (settings: any, hospitalId: number) => {
            dispatch(SettingsActions.saveSettings(SnapMDServiceLocator, settings, hospitalId));
        },
        loadSeats: (hospitalId: number) => {
            dispatch(clientDetailsActions.loadSeatsData(SnapMDServiceLocator, hospitalId));
        },
        integrationsHospitalsPaymentGateway: (data: any, hospitalId: number) => {
            dispatch(SettingsActions.savePartnerIdPaymentGateway(SnapMDServiceLocator, data, hospitalId));
        },
        integrationsHospitalsPartnerTypes: (data: any, hospitalId: number, partnerTypes: number) => {
            dispatch(SettingsActions.saveIntegrationsHospitalsPartnerTypes(SnapMDServiceLocator, data, hospitalId, partnerTypes));
        },
        deleteHospitalPartnerType: (data: any, hospitalId: number, partnerTypes: number) => {
            dispatch(SettingsActions.deleteHospitalPartnerType(SnapMDServiceLocator, data, hospitalId, partnerTypes));
        },
        postHospitalPartnerType: (data: any, hospitalId: number) => {
            dispatch(SettingsActions.postHospitalPartnerType(SnapMDServiceLocator, data, hospitalId));
        },
        saveClientData: (imageObjects: HospitalImagesDTO[], settings: any, hospitalId: number) => {
            dispatch(SettingsActions.saveImagesAndClientData(SnapMDServiceLocator, imageObjects, settings, hospitalId));
        },
        loadHospitalAddress: (hospitalId: number) => {
            dispatch(snapMdAdminAction.loadHospitalAddress(SnapMDServiceLocator, hospitalId));
        }
    };
}

export default connect(mapStateToProps, mapDispatchToProps)(SmdaClientSettingsV2);