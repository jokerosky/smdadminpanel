import * as React from 'react';
import { connect } from 'react-redux';
import Select from 'react-select';

import RichEditCmpnt from '../../common/RichEditCmpnt';
import 'react-quill/dist/quill.snow.css';

import { SnapMDServiceLocator } from '../../../dependenciesInjectionConfiguration'
import * as hospitalDocumentActions from '../../../actions/snapMdAdminClientActions/hospitalDocumentActions';
import HospitalDocumentDTO from '../../../models/DTO/hospitalDocumentDTO';
import { LocalizationService } from '../../../localization/localizationService';
import * as Vocabulary from '../../../localization/vocabulary';

export interface ISmdaHospitalDocumentProps{
    hospitalId:number;
    hospitalName:string;
    documentType:any;
    hospitalDocument:string;
    selectedHospitalDocumentId:number;
    language:any;

    getDocument:(selectedDocumentType:number, hospitalId:number)=>void;
    saveDocument:(document:HospitalDocumentDTO)=>void;
    deleteDocument:(document:HospitalDocumentDTO)=>void;
}

export class SmdaHospitalDocumentCmpnt extends React.Component<ISmdaHospitalDocumentProps, any>{
    constructor(props){
        super(props);

        this.state={
            documentType: [],
            selectedDocumentType: this.props.selectedHospitalDocumentId || 0,
            textEdit: '',
        }
        this.createRichEdit = this.createRichEdit.bind(this); 
        this.saveDocument = this.saveDocument.bind(this);
        this.deleteDocument = this.deleteDocument.bind(this);
        this.createRichEdit('');
    }

    ritchEdit:any;

    componentWillReceiveProps(nextProps){
        if(nextProps.hospitalId !== this.props.hospitalId && this.props.selectedHospitalDocumentId > 0){
            this.props.getDocument(this.props.selectedHospitalDocumentId, nextProps.hospitalId);
        }

        if(nextProps.hospitalDocument){
            this.setState({
                textEdit: nextProps.hospitalDocument
            });

            this.createRichEdit(nextProps.hospitalDocument);
        }
    }

    componentDidMount(){
        if(this.props.selectedHospitalDocumentId > 0){
            this.props.getDocument(this.props.selectedHospitalDocumentId, this.props.hospitalId);
        }
    }

    changeSelect = (val) => {
        this.setState({ selectedDocumentType: val.value });
    }

    textEditChange(text:string){
        this.setState({
            textEdit:text
        });
    }

    getDocument(selectedDocumentType:any){
        if(selectedDocumentType.value > 0){
            this.props.getDocument(selectedDocumentType.value, this.props.hospitalId);
        }else {
            this.createRichEdit('');
        }
    }

    saveDocument(){
        let document:HospitalDocumentDTO={
            documentText: this.state.textEdit,
            documentType: this.state.selectedDocumentType,
            hospitalId: Number(this.props.hospitalId)
        };

        this.props.saveDocument(document);
    }

    deleteDocument(){
        let document:HospitalDocumentDTO={
            documentText: 'default-text',
            documentType: this.state.selectedDocumentType,
            hospitalId:  Number(this.props.hospitalId)
        };

        this.setState({
            selectedDocumentType: 0,
            textEdit: ''
        });

        this.createRichEdit('');
        this.props.deleteDocument(document);
    }

// insert comment abot why we need separate function to create rich edit
    createRichEdit(text:string){
        this.ritchEdit = React.createElement(RichEditCmpnt, 
            {   
                text: text,
                textEditChange:(e)=>{this.textEditChange(e);}
            } as any);
    }

    render(){
        return(
            <div className="container-fluid">
                <dl className='row smd-v-padding'>
                    <dd className='col'>
                        <h5>{this.props.hospitalName} - {this.props.language[Vocabulary.SNAPMDADMIN_HOSPITAL_DOCUMENT]}</h5>
                    </dd>
                </dl>
                <div className='row smd-v-padding'>
                    <span className='col-3'>{this.props.language[Vocabulary.SNAPMDADMIN_DOCUMENT_TYPE]}</span>
                    <div className='col-6'>
                        <Select 
                            name={'Select'}
                            className='smd-z-index-2'
                            options={this.props.documentType}
                            value={this.props.documentType[this.state.selectedDocumentType-1]}
                            clearable={false}
                            onChange={(e) => {
                                this.changeSelect(e);
                                this.getDocument(e);
                            }}
                        />
                    </div>
                </div>
                <br />
                <div >
                    <h5>{this.props.language[Vocabulary.SNAPMDADMIN_DOCUMENT_TEXT]}</h5>
                    <div  className='smd-height-400 smd-display-flex'>
                        <div
                            className='smd-display-flex'
                        >
                            {this.ritchEdit}
                        </div>
                        <div className='row smd-margin-top-10'>
                            <div className='col-6'>
                                <button
                                    name='Save'
                                    className='btn smd-cursor-pointer'
                                    onClick={()=>{
                                        this.saveDocument();
                                    }}
                                >{this.props.language[Vocabulary.SNAPMDADMIN_BUTTON_SAVE]}</button>
                                </div>
                            <div className='col-6'>
                                <button
                                    name='Delete'
                                    className='btn smd-cursor-pointer'
                                    onClick={()=>{
                                        this.deleteDocument();
                                    }}
                                >{this.props.language[Vocabulary.SNAPMDADMIN_DELETE]}</button>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        )
    }
}

let documentType = [];

function createObjectCodeSetsTypeForSelect(props){ 
    if(documentType.length > 0 && documentType.length === props.length){
        return documentType;
    } else {
        for(let index in props){
            documentType.push({
                value:+index+1,
                label:props[index].defaultDocumentText
            });
        }
        return documentType;
    }
}

export function mapStateToProps(state, ownProps){
    return{
        hospitalId: state.snapmdAdmin.misc.selectedId as number,
        hospitalName: state.snapmdAdmin.misc.hospitalName as string,
        documentType: createObjectCodeSetsTypeForSelect(state.snapmdAdmin.lists.documentTypes) as any,
        hospitalDocument: state.snapmdAdmin.client.hospitalDocument.textDocument as string,
        selectedHospitalDocumentId: state.snapmdAdmin.client.hospitalDocument.selectedDocument as number,
        language: state.site.misc.vocabulary as any
    }
}

export function mapDispatchToProps(dispatch){
    return{
        getDocument:(selectedDocumentType:number, hospitalId:number)=>{
            dispatch(hospitalDocumentActions.loadHospitalDocument(SnapMDServiceLocator, selectedDocumentType, hospitalId));
        },
        saveDocument:(document:HospitalDocumentDTO)=>{
            dispatch(hospitalDocumentActions.saveHospitalDocument(SnapMDServiceLocator, document));
        },
        deleteDocument:(document:HospitalDocumentDTO)=>{
            dispatch(hospitalDocumentActions.deleteHospitalDocument(SnapMDServiceLocator, document));
        }
    }
}
export default connect(mapStateToProps, mapDispatchToProps)(SmdaHospitalDocumentCmpnt);