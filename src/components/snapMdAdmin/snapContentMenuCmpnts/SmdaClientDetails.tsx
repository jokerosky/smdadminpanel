import * as React from 'react';
import { connect } from 'react-redux';
import { push } from 'react-router-redux';

import * as snapmdAdminActions from '../../../actions/snapMdAdminActions';
import * as clientDetailsActions from '../../../actions/snapMdAdminClientActions/clientDetailsActions';
import { SnapMDServiceLocator } from '../../../dependenciesInjectionConfiguration';
import {EditorActions} from '../../../enums/siteEnums';
import { LocalizationService } from '../../../localization/localizationService';
import * as Vocabulary from '../../../localization/vocabulary';

export interface ISmdaClientDetailsProps{
    hospitalId:number;
    hospitalName: string;
    seats: any;
    seatsAssigned: any;
    isActiv: any;
    clientData: any;
    language:any;

    chandeClientActiveStatus:(hospitalId:number, isActive:string, methodType:string, clientData?:any)=>void;
    loadSeats:(hospitalId:number)=>void;
}

export class SmdaClientDetailsCmpnt extends React.Component<ISmdaClientDetailsProps, any> {
    constructor(){
        super();

        this.state={styleBtn: 'btn-warning'};
    }

    progressBarValueProvaider={min: 0, max: 100, value: 50, style:{ width: '', height: 25, paddingTop:'5px' }};
    progressBarValueAdmin={min: 0, max: 100, value: 50, style:{ width: '', height: 25, paddingTop:'5px' }};
    NAMES={
        provider: Vocabulary.SNAPMDADMIN_PROVIDER_SEATS, 
        admin: Vocabulary.SNAPMDADMIN_ADMIN_SEATS, 
        total: Vocabulary.SNAPMDADMIN_TOTAL_SEATS
    };
    NAMES_SEATS={ provider: 'snEnterprise', admin: 'snAdmin', total: 'snTotalSeats'};

    componentWillReceiveProps(nextProps){
        if(nextProps.hospitalId !== this.props.hospitalId){
            this.props.loadSeats(nextProps.hospitalId);
        }
    }

    componentDidMount(){
        if(this.props.hospitalId > -1){
            this.props.loadSeats(this.props.hospitalId);
        }
    }

    calculationOfInterest(max:number, value:number){
        let num = max/100;
        return (value/num).toFixed(2)+'%';
    }

    zeroingOut(){
        this.progressBarValueProvaider.value = 0;
        this.progressBarValueProvaider.max = 0;
        this.progressBarValueProvaider.style.width = '';

        this.progressBarValueAdmin.value = 0;
        this.progressBarValueAdmin.max = 0;
        this.progressBarValueAdmin.style.width='';
    }

    assign(obj:any, seats:number, seatsAssigned:number){
        obj.max=seats;
        obj.value=seatsAssigned;
    }

    renderItems(seates?:any, seatsAssigned?:any){
        let items:Array<any> = null;
        if(seates && seatsAssigned){
            items=[];
             for(let item in this.NAMES_SEATS){
                let seat = seates[this.NAMES_SEATS[item]];
                let names = this.props.language[this.NAMES[item]];
                let seatAssig = seatsAssigned[this.NAMES_SEATS[item]];

                let li =
                <tr key={item} className='row'>
                    <td className='col td-smd-table'>{names}</td>
                    <td className='col d-flex justify-content-center td-smd-table'>{seat}</td>
                    <td className='col d-flex justify-content-center td-smd-table'>{seatAssig}</td>
                </tr>
                items.push(li);
            }

            this.zeroingOut();
            if(seates[this.NAMES_SEATS.provider] && seatsAssigned[this.NAMES_SEATS.provider]){
                this.assign(this.progressBarValueProvaider, seates[this.NAMES_SEATS.provider], seatsAssigned[this.NAMES_SEATS.provider]);
                this.progressBarValueProvaider.style.width = this.calculationOfInterest(this.progressBarValueProvaider.max,this.progressBarValueProvaider.value);
            }

            if(seates[this.NAMES_SEATS.admin] && seatsAssigned[this.NAMES_SEATS.admin]){
                this.assign(this.progressBarValueAdmin, seates[this.NAMES_SEATS.admin], seatsAssigned[this.NAMES_SEATS.admin]);
                this.progressBarValueAdmin.style.width = this.calculationOfInterest(this.progressBarValueAdmin.max, this.progressBarValueAdmin.value);
            }
        }
        return items;
    }
    
    render(){
        let items = this.renderItems(this.props.seats, this.props.seatsAssigned);
        return (
            <div className="container-fluid">
                <dl className='row smd-v-padding'>
                    <dd className='col'>
                        <h5 id='name'>{this.props.hospitalName} - {this.props.language[Vocabulary.SNAPMDADMIN_CLIENT_DETAILS]}</h5>
                    </dd>
                </dl>
            <div  className=' row align-items-center '>
            <div className='col'>
                <div >
                <div className='progress'>
                <div className='progress-bar progress-bar-striped '
                    role="progressbar" 
                    aria-valuenow={this.progressBarValueProvaider.value}
                    aria-valuemin={this.progressBarValueProvaider.min}
                    aria-valuemax={this.progressBarValueProvaider.max}
                    style={{...this.progressBarValueProvaider.style}}>
                    {this.progressBarValueProvaider.style.width}
                </div>
                </div>
                </div> 
                <br />
                <div >
                <div className='progress'>
                <div className='progress-bar progress-bar-striped '
                    role="progressbar" 
                    aria-valuenow={this.progressBarValueAdmin.value}
                    aria-valuemin={this.progressBarValueAdmin.min}
                    aria-valuemax={this.progressBarValueAdmin.max}
                    style={{...this.progressBarValueAdmin.style}}>
                    {this.progressBarValueAdmin.style.width}
                </div>
                </div>
                </div>                
            </div>
            </div>
            <br />
            <div className='col'>
            <table className='table'>
                <thead>
                    <tr className='row'>
                        <th className='col th-smd-table'>{this.props.language[Vocabulary.SNAPMDADMIN_CLIENT_NAME]}</th>
                        <th className='col d-flex justify-content-center th-smd-table' >{this.props.language[Vocabulary.SNAPMDADMIN_CLIENT_SEATS]}</th>
                        <th className='col d-flex justify-content-center th-smd-table'>{this.props.language[Vocabulary.SNAPMDADMIN_CLIENT_SEATS_ASSIGNED]}</th>
                    </tr>
                </thead>
                <tbody className='col'>
                    {items || <tr><td>{this.props.language[Vocabulary.SNAPMDADMIN_NO_DATA_TO_DISPLAY]}</td></tr>}
                </tbody>
            </table>
            </div>
            <div className='col' >
                <button 
                    className={`btn  btn-lg ${this.props.isActiv != null && this.props.isActiv==='A'? 'btn-warning':'btn-success'}`} 
                    onClick={()=>{
                        let isActive = '';
                        let methodType = '';
                        let client = this.props.clientData;
                        if(this.props.isActiv==='N'){
                            isActive = 'A';
                            methodType = EditorActions.archive;
                            client.isActive = 'A';
                        } else if(this.props.isActiv==='A'){
                            isActive = 'N';
                            methodType = EditorActions.activate;
                        }
                
                        this.props.chandeClientActiveStatus(this.props.hospitalId, isActive, methodType, client);
                        }
                    }
                    >
                    {this.props.isActiv != null && this.props.isActiv==='A'? 
                                this.props.language[Vocabulary.SNAPMDADMIN_BUTTON_ARCHIVE_CLIENT]
                            :   this.props.language[Vocabulary.SNAPMDADMIN_BUTTON_ACTIVATE_CLIENT]}
                </button>
            </div>
            <hr />
            <div className='d-flex justify-content-center'>
                <h4>{this.props.language[Vocabulary.SNAPMDADMIN_CLIENT_CONTACTS_AREA_IN_DEVELOPMENT]}</h4>
            </div>
            <hr />
            <div className='d-flex justify-content-center'>
                <h4>{this.props.language[Vocabulary.SNAPMDADMIN_CLIENT_ANALITICS_AREA_IN_DEVELOPMENT]}</h4>
            </div>
          </div>
        );
    }
} 


export function mapStateToProps(state, ownProps) {
    let isActiv = null;
    if(state.snapmdAdmin.client.details.clientData!= undefined && state.snapmdAdmin.client.details.clientData.isActive != undefined){
        isActiv = state.snapmdAdmin.client.details.clientData.isActive;
    } 

    return {
        hospitalId: state.snapmdAdmin.misc.selectedId as number,
        hospitalName: state.snapmdAdmin.misc.hospitalName as string,
        seats: state.snapmdAdmin.client.details.seats as any,
        seatsAssigned: state.snapmdAdmin.client.details.seatsAssigned as any,
        isActiv: state.snapmdAdmin.client.details.clientData.isActive as any,
        clientData: state.snapmdAdmin.client.details.clientData as any,
        language: state.site.misc.vocabulary as any
    }
}

export function mapDispatchToProps(dispatch) {
    return {
        chandeClientActiveStatus:(hospitalId:number, isActive:string, methodType:string, clientData?:any)=>{
            dispatch(clientDetailsActions.cahngeClientActiveStatus(SnapMDServiceLocator, hospitalId, isActive, methodType, clientData));
        },
        loadSeats:(hospitalId:number)=>{
            dispatch(clientDetailsActions.loadSeatsData(SnapMDServiceLocator, hospitalId));
        }
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(SmdaClientDetailsCmpnt);
