import * as React from 'react';
import { connect } from 'react-redux';
import { push } from 'react-router-redux';
import Select from 'react-select';

import 'react-select/dist/react-select.css';

import { SnapMDServiceLocator } from '../../../dependenciesInjectionConfiguration'
import * as snapmdAdminActions from '../../../actions/snapMdAdminActions';
import * as SettingsActions from '../../../actions/snapMdAdminClientActions/settingsActions';
import * as operatingHoursActions from '../../../actions/snapMdAdminClientActions/operatingHoursActions';
import { SiteConstants } from '../../../enums/siteConstants';
import { ModalWindow } from '../../common/ModalWindow';
import { ObjectHelper } from '../../../utilities/objectHelper';
import { LocalizationService } from '../../../localization/localizationService';
import * as Vocabulary from '../../../localization/vocabulary';
import { MODULE_KEYS, SETTINGS_KEYS, SETTINGS_KEYS2 } from '../../../enums/clientSettingsKeys';

import { ToggleButton } from '../../common/inputs/ToggleButtonCmpnt';
import {EditorActions} from '../../../enums/siteEnums';

export interface IOpHoursProps {
    hospitalId: number,
    hospitalName: string,
    operatingHours?: any[];
    toggleState?: any;
    language:any;

    updateOperatingHours?: (hospitalId: number, operatingHoursdata: any, dispatchMethod: string) => void;
    deleteOperatingHours?: (dayId: number, hospitalId: number) => void;
    toggleOperatingHours?: (stateToggle: any, hospitalId: number, fieldUpdate: string) => void;
    loadOperatingHours?:(hospitalId: number)=>void;
    getSettings?: (hospitalId: number, fields: string[]) => void;
}

const Week = SiteConstants.daysOfWeek;

export class SmdaClientOpHoursCmpnt extends React.Component<IOpHoursProps, any>{
    constructor() {
        super()
        this.state = {
            modalVisible: false,
            startDay: 1,
            endDay: 1,
            startTime: '',
            endTime: '',
            operatingHoursId: 0,
            method: '',
            toggle: ''
        };

        this.updateOperatingHours=this.updateOperatingHours.bind(this);
    }

    searchForm: HTMLFormElement;
    toggle;

    componentWillMount() {
        this.props.getSettings(this.props.hospitalId, MODULE_KEYS);
        this.props.getSettings(this.props.hospitalId, SETTINGS_KEYS);
        this.props.getSettings(this.props.hospitalId, SETTINGS_KEYS2);
    }
    componentWillReceiveProps(nextProps){
        if(nextProps.hospitalId !== this.props.hospitalId){
            this.props.loadOperatingHours(nextProps.hospitalId);
        }
    }

    componentDidMount(){
       this.props.loadOperatingHours(this.props.hospitalId);
    }

    selectDay(dayUpdate) {
        this.setState({
            startDay: dayUpdate.startDay,
            endDay: dayUpdate.endDay,
            startTime: dayUpdate.startTime,
            endTime: dayUpdate.endTime,
            operatingHoursId: dayUpdate.operatingHoursId
        });
    };
    openModal = (dispatchMethod) => {
        this.setState({
            modalVisible: true,
            method: dispatchMethod
        });
    };
    closeModal = () => {
        this.setState({
            modalVisible: false,
            startDay: 1,
            endDay: 1,
            startTime: '',
            endTime: '',
            method: ''
        });
    };
    changeToggle = (isOn) => {
        let bool = ObjectHelper.dataBaseStringFromBool(isOn);
        let data = {
            ['mOnDemandHourly']: bool
        };
        this.props.toggleOperatingHours(data, this.props.hospitalId, 'mOnDemandHourly');
    };
    changeSelect = (val, top) => {
        this.setState({ [top]: val.value });
    };
    changeInput = (val, top) => {
        this.setState({ [top]: val.currentTarget.value.trim() });
    };
    updateOperatingHours() {
        let data = {
            endDayOfWeek: this.state.endDay,
            endTime: this.state.endTime,
            hospitalId: this.props.hospitalId,
            operatingHoursId: this.state.operatingHoursId,
            startDayOfWeek: this.state.startDay,
            startTime: this.state.startTime
        }
        this.props.updateOperatingHours(this.props.hospitalId, data, this.state.method);
        this.closeModal();
    };

    renderItems(operatingHours: any[]) {
        let items: Array<any> = [];
        if (operatingHours.length > 0) {
            for (let index in operatingHours) {
                let hours = operatingHours[index];
                let li =
                    <tr key={index} className='row'>
                        <td className='col td-smd-table'>{Week[hours.startDayOfWeek].label}</td>
                        <td className='col td-smd-table'>{Week[hours.endDayOfWeek].label}</td>
                        <td className='col td-smd-table'>{hours.startTime}</td>
                        <td className='col td-smd-table'>{hours.endTime}</td>
                        <td className='col td-smd-table'>
                            <div className='row  justify-content-around'>
                                <button className='btn smd-cursor-pointer'
                                    key={index + 'btnEdit'}
                                    name={index + 'Edit'}
                                    title={'Edit'}
                                    onClick={() => {
                                        let dayUpdate = {
                                            startDay: hours.startDayOfWeek,
                                            endDay: hours.endDayOfWeek,
                                            startTime: hours.startTime,
                                            endTime: hours.endTime,
                                            operatingHoursId: hours.operatingHoursId
                                        }
                                        this.selectDay(dayUpdate);
                                        this.openModal(EditorActions.edit);
                                    }
                                    } ><i className="fa fa-pencil" aria-hidden="true" title={'Edit'}></i></button>
                                <button className='btn smd-cursor-pointer'
                                    key={index + 'btnDelete'}
                                    name = {index + 'Delete'}
                                    title={'Delete'}
                                    onClick={() => {
                                        this.props.deleteOperatingHours(hours.operatingHoursId, this.props.hospitalId);
                                    }}><i className="fa fa-trash-o" aria-hidden="true" title={'Delete'}></i></button>
                            </div>
                        </td>
                    </tr>
                items.push(li);
            }
        }
        return items;
    };

    render() {
        let items = this.renderItems(this.props.operatingHours);
        return (
            <div className="container-fluid">
                <dl className='row smd-v-padding'>
                    <dd className='col-6'>
                        <h5>{this.props.hospitalName} - {this.props.language[Vocabulary.SNAPMDADMIN_ENABLE_HOSPITAL_HOURS_ON_CALENDAR]}</h5>
                    </dd>
                    <dt className='col-6'>                    
                        <ToggleButton
                            name={'toggle'}
                            isChecked={this.props.toggleState != undefined ? this.props.toggleState : false}
                            onChange={this.changeToggle}
                        />
                    </dt>
                </dl>

                <div className='col smd-v-padding'>
                    <div className='row' >
                        <button className='btn align-self-center smd-cursor-pointer'
                            name={'Record'}
                            key={'AddNewRecord'}
                            onClick={() => {
                                this.openModal(EditorActions.add);
                                }
                            }
                        >{this.props.language[Vocabulary.SNAPMDADMIN_ADD_NEW_RECORD]}</button>
                    </div>
                </div>
                
                <div className='col'>
                    <table className='table table-hover'>
                        <thead>
                            <tr className='row'>
                                <th className='col th-smd-table'>{this.props.language[Vocabulary.SNAPMDADMIN_START_DAY]}</th>
                                <th className='col th-smd-table'>{this.props.language[Vocabulary.SNAPMDADMIN_END_DAY]}</th>
                                <th className='col th-smd-table'>{this.props.language[Vocabulary.SNAPMDADMIN_START_TIME]}</th>
                                <th className='col th-smd-table'>{this.props.language[Vocabulary.SNAPMDADMIN_END_TIME]}</th>
                                <th className='col th-smd-table'></th>
                            </tr>
                        </thead>
                        <tbody>
                            {items || ''}
                        </tbody>
                    </table>
                </div>
                <ModalWindow
                    visible={this.state.modalVisible}
                    title={'Edit'}
                    cancel={this.closeModal}
                    success={() => { this.updateOperatingHours(); }}
                >

                    <div className='row justify-content-center smd-v-padding'>
                        <span className='col-4'>{this.props.language[Vocabulary.SNAPMDADMIN_START_DAY]}</span>
                        <Select
                            name={'firstSelect'}
                            className='col-5'
                            value={SiteConstants.daysOfWeek[this.state.startDay]}
                            options={SiteConstants.daysOfWeek}
                            clearable={false}
                            onChange={(e) => {
                                this.changeSelect(e, 'startDay')
                            }}
                        />
                    </div>

                    <div className='row justify-content-center smd-v-padding'>
                        <span className='col-4'>{this.props.language[Vocabulary.SNAPMDADMIN_END_DAY]}</span>
                        <Select
                            name={'secondSelect'}
                            className='col-5'
                            value={SiteConstants.daysOfWeek[this.state.endDay]}
                            options={SiteConstants.daysOfWeek}
                            clearable={false}
                            onChange={(e) => {
                                this.changeSelect(e, 'endDay')
                            }}
                        />
                    </div>
                    <div className='row justify-content-center smd-v-padding'>
                        <span className='col-4'>{this.props.language[Vocabulary.SNAPMDADMIN_START_TIME]}</span>
                        <input
                            name='startTime'
                            type='text'
                            className='form-control col-4'
                            placeholder={'HH:mm'}
                            value={this.state.startTime}
                            onChange={(e) => {
                                this.changeInput(e, 'startTime')
                            }}
                        />
                    </div>
                    <div className='row justify-content-center smd-v-padding'>
                        <span className='col-4'>{this.props.language[Vocabulary.SNAPMDADMIN_END_TIME]}</span>
                        <input
                            name='endTime'
                            type='text'
                            className='form-control col-4'
                            placeholder={'HH:mm'}
                            value={this.state.endTime}
                            onChange={(e) => {
                                this.changeInput(e, 'endTime');
                            }}
                        />
                    </div>
                </ModalWindow>
            </div>
        );
    }
}

export function mapStateToProps(state, ownProps) {
    let toggleStateSettingsState;

            toggleStateSettingsState = state.snapmdAdmin.client.settings['mOnDemandHourly'];

    return {
        hospitalId: state.snapmdAdmin.misc.selectedId as number,
        hospitalName: state.snapmdAdmin.misc.hospitalName as string,
        operatingHours: state.snapmdAdmin.client.operatingHours as any[],
        toggleState: toggleStateSettingsState as any,
        language: state.site.misc.vocabulary as any
    }
}

export function mapDispatchToProps(dispatch) {
    return {
        updateOperatingHours: (hospitalId: number, operatingHoursdata: any, dispatchMethod: string) => {
            hospitalId && operatingHoursdata
                ? dispatch(operatingHoursActions.updateOperatingHours(SnapMDServiceLocator, hospitalId, operatingHoursdata, dispatchMethod))
                : '';
        },
        deleteOperatingHours: (dayId: number, hospitalId: number) => {
            dayId && hospitalId
                ? dispatch(operatingHoursActions.deleteOperatingHours(SnapMDServiceLocator, dayId, hospitalId))
                : '';
        },
        toggleOperatingHours: (stateToggle: any, hospitalId: number, fieldUpdate: string) => {
            stateToggle && hospitalId
                ? dispatch(operatingHoursActions.toggleOperatingHours(SnapMDServiceLocator, stateToggle, hospitalId, fieldUpdate))
                : '';
        },
        loadOperatingHours:(hospitalId:number)=>{
            dispatch(operatingHoursActions.loadOperatingHours(SnapMDServiceLocator, hospitalId));
        },
        getSettings: (hospitalId: number, fields: string[]) => {
            dispatch(SettingsActions.getClientSettings(SnapMDServiceLocator, hospitalId, fields));
        }
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(SmdaClientOpHoursCmpnt);