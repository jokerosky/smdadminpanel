import * as React from 'react';
import { connect } from 'react-redux';
import Select from 'react-select';
import * as _ from 'lodash';

import { SnapMDServiceLocator } from '../../../dependenciesInjectionConfiguration'
import * as codeSetsActions from '../../../actions/snapMdAdminClientActions/codeSetsActions';
import { ModalWindow } from '../../common/ModalWindow';
import {EditorActions} from '../../../enums/siteEnums';
import {CodeSetsDTO} from '../../../models/DTO/codeSetsDTO';
import {Endpoints} from '../../../enums/endpoints';
import { LocalizationService } from '../../../localization/localizationService';
import * as Vocabulary from '../../../localization/vocabulary';

export interface ISmdaCodeSetsProps{
    hospitalId:number;
    hospitalName:string;
    codeSetsTypeForSelect:any[];
    codeSetsType:any[];
    codeSetsTypeId:number;
    codeSets:CodeSetsDTO[];
    language:any;

    loadCodes:(hospitalId:number, codeSetsTypeId:number)=>void;
    codesAddOrUpdate:(codeSet:CodeSetsDTO, methodType:string)=>void;
    saveSelectedCodeSetsTypeId:(codeSetsTypeId:number)=>void;
}

export class SmdaCodeSetsCmpnt extends React.Component<ISmdaCodeSetsProps, any>{
    constructor(props){
        super(props);

        this.state={
            modalVisible:false,
            methodType: EditorActions.empty,
            CodeId: null,
            CodeSetId: props.codeSetsTypeId || -1,
            Description: '',
            DisplayOrder: 0,
            HospitalId: 0,
            IsActive: false,
            PartnerCode: '',
            hospitalId:-1,
        }

        this.updateCodeSets=this.updateCodeSets.bind(this);
        this.openModal=this.openModal.bind(this);
        this.selectChecked=this.selectChecked.bind(this);
    }

    componentWillReceiveProps(nextProps){
        if(nextProps.hospitalId !== this.props.hospitalId && this.props.codeSetsTypeId> 0){
            this.props.loadCodes(nextProps.hospitalId, this.selectedCodeSets(this.props.codeSetsTypeId));
        }
    }

    componentDidMount(){
        if(this.state.CodeSetId > -1){
            this.props.loadCodes(this.props.hospitalId, this.selectedCodeSets(this.props.codeSetsTypeId));
        }        
    }

    changeSelect = (val) => {
        let value = Number(val.value);
        this.setState({ 
            CodeSetId: value,
        });

        this.props.loadCodes(this.props.hospitalId, this.selectedCodeSets(value));
        this.props.saveSelectedCodeSetsTypeId(value);
    };

    selectedCodeSets(props){
        let index = this.props.codeSetsType[props-1];
        return index.CodeSetId;
    }

    closeModal = ()=>{
        this.setState({
            modalVisible:false,
            Description: '',
            PartnerCode: '',
            IsActive: false
        })
    }

    openModal(method:string){
        this.setState({
            modalVisible:true,
            methodType:method
        })
    }

    selectCodes(select:any){
        this.setState({
                Description: select.Description,
                PartnerCode: select.PartnerCode,
                IsActive: select.IsActive,
        })
    }

    selectInput(val, top){
        this.setState({ [top]: val.currentTarget.value});
    }

    selectChecked(){
        this.setState({
            IsActive: !this.state.IsActive
        })
    }

    updateCodeSets(){
        let codes: any;
            codes = {
                CodeId: this.state.CodeId,
                CodeSetId: this.selectedCodeSets(this.props.codeSetsTypeId),
                Description: this.state.Description,
                DisplayOrder: this.state.DisplayOrder,
                HospitalId: this.state.HospitalId,
                IsActive: this.state.IsActive,
                PartnerCode: this.state.PartnerCode,
                codeSetId: this.selectedCodeSets(this.props.codeSetsTypeId),
                displayOrder: this.state.DisplayOrder,
                hospitalId: this.props.hospitalId,
            }
            if(this.state.methodType === EditorActions.add){
                codes.DisplayOrder = this.props.codeSets.length + 1;
                codes.displayOrder = this.props.codeSets.length + 1;
                codes.codeSetId = this.selectedCodeSets(this.props.codeSetsTypeId),
                codes.CodeSetId = 0;
            }
            this.props.codesAddOrUpdate(codes, this.state.methodType);
    }

    renderItem(codeSets:any){
        let items: Array<any>=[];
        if(codeSets && codeSets.length > 0){
            for(let index in codeSets){
                let codes = codeSets[index];
                let li = 
                    <tr key={index} className='row'>
                        <td className='col-4 td-smd-table'>{codes.Description}</td>
                        <td className='col-4 td-smd-table'>{codes.PartnerCode}</td>
                        <td className='col-2 td-smd-table'>
                            <input 
                                type='checkbox' 
                                className='smd-cursor-not-allowed'
                                disabled checked={codes.IsActive}
                            /></td>
                        <td className='col'>
                            <button className='btn smd-cursor-pointer'
                                    key={index + 'btnEdit'}
                                    name={index + 'Edit'}
                                    title={this.props.language[Vocabulary.SNAPMDADMIN_EDIT]}
                                    onClick={() => {
                                        let selectCodes={
                                            Description: codes.Description,
                                            PartnerCode: codes.PartnerCode,
                                            IsActive: codes.IsActive
                                        }
                                        this.setState({
                                            CodeId: codes.CodeId,
                                            CodeSetId: codes.CodeSetId,
                                            Description: codes.Description,
                                            DisplayOrder: codes.DisplayOrder,
                                            HospitalId: codes.HospitalId,
                                            IsActive: codes.IsActive,
                                            PartnerCode: codes.PartnerCode,                                            
                                        });
                                        this.selectCodes(selectCodes);
                                        this.openModal(EditorActions.edit);
                                    }
                                    } ><i className="fa fa-pencil" aria-hidden="true" title={this.props.language[Vocabulary.SNAPMDADMIN_EDIT]}></i></button>
                        </td>
                    </tr>

                items.push(li);
            }
        }
        return items;
    }

    render(){
        let items = []
        if(this.props.codeSets.length>0){
            items = this.renderItem(this.props.codeSets);
        }
        return(
            <div className="container-fluid">
                <dl className='row smd-v-padding'>
                    <dd className='col'>
                        <h5>{this.props.hospitalName} - {this.props.language[Vocabulary.SNAPMDADMIN_CODE_SET_SETTINGS]}</h5>
                    </dd>
                </dl>
                <div className='row smd-v-padding'>
                    <span className='col-3'>{this.props.language[Vocabulary.SNAPMDADMIN_CODE_SET]}</span>
                    <div className='col-6'>
                        <Select 
                            name={'Select'}
                            options={this.props.codeSetsTypeForSelect}
                            value={this.props.codeSetsTypeForSelect[this.props.codeSetsTypeId-1]}
                            clearable={false}
                            onChange={(e) => {
                                this.changeSelect(e);
                            }}
                        />
                    </div>
                </div>
                <br />
                <div className='col smd-v-padding'>
                    <div className='row' >
                        <button className='btn align-self-center smd-cursor-pointer'
                            name={'Record'}
                            key={'AddNewRecord'}
                            onClick={() => {
                                this.openModal(EditorActions.add);
                                }
                            }
                        >{this.props.language[Vocabulary.SNAPMDADMIN_ADD_NEW_RECORD]}</button>
                    </div>
                </div>

                <div className='col'>
                    <table className='table table-hover'>
                        <thead>
                            <tr className='row'>
                                <th className='col-4 th-smd-table'>{this.props.language[Vocabulary.SNAPMDADMIN_DESCRIPTION]}</th>
                                <th className='col-4 th-smd-table'>{this.props.language[Vocabulary.SNAPMDADMIN_PARTNER_CODE]}</th>
                                <th className='col-2 th-smd-table'>{this.props.language[Vocabulary.SNAPMDADMIN_IS_ACTIVE]}</th>
                                <th className='col th-smd-table'></th>
                            </tr>
                        </thead>
                        <tbody>
                            {items || ''}
                        </tbody>
                    </table>
                </div>
                <ModalWindow
                    visible={this.state.modalVisible}
                    title={'Edit'}
                    cancel={this.closeModal}
                    success={() => { this.updateCodeSets(); this.closeModal()}}
                >
                    <div className='row justify-content-center smd-v-padding'>
                        <span className='col-3'>{this.props.language[Vocabulary.SNAPMDADMIN_DESCRIPTION]}</span>
                        <input 
                            className='col-5' 
                            name='Description'
                            defaultValue={this.state.Description}
                            onChange={(e)=>{
                                this.selectInput(e, 'Description')
                            }}
                        />
                    </div>

                    <div className='row justify-content-center smd-v-padding'>
                        <span className='col-3'>{this.props.language[Vocabulary.SNAPMDADMIN_PARTNER_CODE]}</span>
                        <input 
                            className='col-5 disabled smd-cursor-not-allowed'
                            disabled 
                            defaultValue={this.state.PartnerCode}
                            onChange={(e)=>{
                                this.selectInput(e, 'PartnerCode')
                            }}
                        />
                    </div>

                    <div className='row justify-content-center smd-v-padding'>
                        <div className='col-8'>
                            <label className='col-4 smd-cursor-pointer' htmlFor={'check'}>
                                {this.props.language[Vocabulary.SNAPMDADMIN_IS_ACTIVE]}
                            </label> 
                            <input 
                                id='check'
                                className='smd-cursor-pointer' 
                                type='checkbox' 
                                checked={this.state.IsActive}
                                onChange={()=>{
                                    this.selectChecked();
                                }}
                            />
                        </div>                        
                    </div>
                </ModalWindow>
            </div>
        )
    }
}

let codeSets = [];

function createObjectCodeSetsTypeForSelect(props){ 
    if(codeSets.length > 0 && codeSets.length === props.length){
        return codeSets;
    } else {
        for(let index in props){
            codeSets.push({
                value:+index+1,
                label:props[index].Description
            });
        }
        return codeSets;
    }
}

export function mapStateToProps(state, ownProps){
    return{
        hospitalId: state.snapmdAdmin.misc.selectedId as number,
        hospitalName: state.snapmdAdmin.misc.hospitalName as string,
        codeSetsTypeForSelect: createObjectCodeSetsTypeForSelect(state.snapmdAdmin.lists.codeSets) as any[],
        codeSetsType: state.snapmdAdmin.lists.codeSets as any [],
        codeSets: state.snapmdAdmin.client.codeSets.codeSetsRecords as CodeSetsDTO[],
        codeSetsTypeId: state.snapmdAdmin.client.codeSets.codeSetsTypeId as number,
        language: state.site.misc.vocabulary as any
    }
}

export function mapDispatchToProps(dispatch){
    return{
        loadCodes:(hospitalId:number, codeSetsTypeId:number)=>{
            dispatch(codeSetsActions.loadCodeSets(SnapMDServiceLocator, hospitalId, codeSetsTypeId));
        },
        codesAddOrUpdate:(codeSet:CodeSetsDTO, methodType:string)=>{
            dispatch(codeSetsActions.codeSetsAddOrUpdate(SnapMDServiceLocator, codeSet, methodType));
        },
        saveSelectedCodeSetsTypeId:(codeSetsTypeId:number)=>{
            dispatch(codeSetsActions.saveSelectedCodeSetsTypeId(codeSetsTypeId));
        }
    }
}
export default connect(mapStateToProps, mapDispatchToProps)(SmdaCodeSetsCmpnt);