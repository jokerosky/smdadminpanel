import * as React from 'react';
import { connect } from 'react-redux';

import { ModalWindow } from '../../common/ModalWindow';
import * as snapmdAdminActions from '../../../actions/snapMdAdminActions';
import { SnapMDServiceLocator } from '../../../dependenciesInjectionConfiguration';
import * as smsTemplatesActions from '../../../actions/snapMdAdminClientActions/smsTemplatesActions';
import MessageDTO from '../../../models/DTO/smsMessageDTO';
import * as Vocabulary from '../../../localization/vocabulary';

export interface SmdaClientSmsTemplateCmpntProps{
    hospitalId:number;
    hospitalName:string;
    smsTemplates:any;
    language:any;

    updateSMSText:(messageId:number, message:any)=>void;
    deleteSMSText:(messageId:number)=>void;
    loadSMSTemplate:(hospitalId:number)=>void;
}

export class SmdaClientSmsTemplateCmpnt extends React.Component<SmdaClientSmsTemplateCmpntProps, any>{
    constructor(props){
        super(props);

        this.state={
            modalVisible: false, 
            messageType:'', 
            smsText: '',
            id: '',
            isActive: ''
        };
        this.updateMessage = this.updateMessage.bind(this);
        this.deleteMessage = this.deleteMessage.bind(this);
        this.changeInput = this.changeInput.bind(this);
    }

    componentWillReceiveProps(nextProps){
        if(nextProps.hospitalId !== this.props.hospitalId){
           this.props.loadSMSTemplate(nextProps.hospitalId);
        }  
    }

    componentDidMount(){
        this.props.loadSMSTemplate(this.props.hospitalId);
    }

    closeModal = () => {
        this.setState({
            modalVisible: false
        });
    };

    openModal = () => {
        this.setState({
            modalVisible: true
        });
    };

    changeInput = (val, top) => {
        this.setState({ [top]: val.currentTarget.value });
    };

    selectSMS(val){
        this.setState({ 
            messageType: val.type,
            smsText: val.smsText,
            id: val.id,
            isActive: val.isActive
        });
    };

    clearStateSMS(){
        this.setState({
            messageType:'', 
            smsText: '',
            id: '',
            isActive: ''
        });
    };

    updateMessage(){
        let smsMessage = {
            hospitalId: this.props.hospitalId,
            id: this.state.id,
            isActive: this.state.isActive,
            type: this.state.messageType,
            smsText: this.state.smsText
        };
        this.props.updateSMSText(this.state.id, smsMessage);
    };

    deleteMessage(messageId:number){
        this.props.deleteSMSText(messageId);
    };

    renderItems(smsTemplates:any){
        let items: Array<any> = [];
        if(smsTemplates){
          
            for(let index in smsTemplates) {
                let sms = smsTemplates[index];
                let li =
                    <tr key={index} className='row'>
                        <td className='col-3 smd-text-hidden td-smd-table'><abbr className='smd-no-decoration' title={sms.type}>{sms.type}</abbr></td>
                        <td className='col-7 td-smd-table'>{sms.smsText}</td>
                        <td className='col td-smd-table'>
                            <div className='row  justify-content-around'>
                                <button 
                                    id={'Edit'+index}
                                    title={'Edit'}
                                    className='btn smd-cursor-pointer'
                                    onClick={() => {
                                            this.selectSMS(sms);
                                            this.openModal();
                                        }
                                    }
                                ><i className="fa fa-pencil" aria-hidden="true" title={'Edit'}></i></button>
                                <button
                                    id={'Delete'+index}
                                    title={'Delete'}
                                    className='btn smd-cursor-pointer'
                                    onClick={() => {
                                            this.deleteMessage(sms.id);
                                        }
                                    }
                                >
                                    <i className="fa fa-trash-o" aria-hidden="true" title={'Delete'}></i>
                                </button>
                            </div>
                        </td>
                    </tr>
                
                items.push(li);
             
            }
        }
        return items;
    }
    render(){
        let items = this.renderItems(this.props.smsTemplates);
        return(
            <div className="container-fluid">
                <dl className='row smd-v-padding'>
                    <dd className='col'>
                        <h5>{this.props.hospitalName} - {this.props.language[Vocabulary.SNAPMDADMIN_CLIENT_SMS_TEMPLATE]}</h5>
                    </dd>
                </dl>
                <div className='col smd-v-padding'>
                    <div className='row'>
                        <button className='btn align-self-center smd-cursor-pointer'
                                name={'Record'}
                                key={'AddNewRecord'}
                                onClick={() => {
                                this.clearStateSMS();
                                this.openModal();
                            }
                            }>{this.props.language[Vocabulary.SNAPMDADMIN_ADD_NEW_RECORD]}</button>
                    </div>
                </div>
                <div className='col'>
                    <table className='table table-hover'>
                        <thead>
                        <tr className='row'>
                            <th className='col-3 th-smd-table'>{this.props.language[Vocabulary.SNAPMDADMIN_TYPE]}</th>
                            <th className='col-4 th-smd-table'>{this.props.language[Vocabulary.SNAPMDADMIN_SMS_TEXT]}</th>
                            <th className='col th-smd-table'></th>
                        </tr>
                        </thead>
                        <tbody>
                        {items || ''}
                        </tbody>
                    </table>
                </div>
                <ModalWindow
                    visible={this.state.modalVisible}
                    title={this.props.language[Vocabulary.SNAPMDADMIN_EDIT]}
                    cancel={this.closeModal}
                    success={() => {
                        this.updateMessage();
                        this.closeModal();
                    }}>
                    <div className='row justify-content-center'>
                        <div className='col-3 '>
                            <div className='col smd-10-margin'>{this.props.language[Vocabulary.SNAPMDADMIN_TYPE]}</div>
                            <div className='col '>{this.props.language[Vocabulary.SNAPMDADMIN_SMS_TEXT]}</div>
                        </div>
                        <div className='col-7 justify-content-center'>

                            <input
                                className='smd-10-margin'
                                type='text'
                                placeholder={this.props.language[Vocabulary.SNAPMDADMIN_SMS_TYPE]}
                                size={27}
                                value={this.state.messageType}
                                onChange={(e) => {
                                    this.changeInput(e, 'messageType');
                                }}
                            />
                            <textarea 
                                className='' 
                                placeholder={this.props.language[Vocabulary.SNAPMDADMIN_SMS_BODY]}
                                cols={30} 
                                rows={10}
                                value={this.state.smsText}
                                onChange={(e) => {
                                    this.changeInput(e, 'smsText');
                                }}>
                            </textarea>
                        </div>
                    </div>
                </ModalWindow>
            </div>
        );
    }
}

export function mapStateToProps(state, ownProps){
    return{
        hospitalId: state.snapmdAdmin.misc.selectedId as number,
        hospitalName: state.snapmdAdmin.misc.hospitalName as string,
        smsTemplates: state.snapmdAdmin.client.smsTemplates as MessageDTO[],
        language: state.site.misc.vocabulary as any
    }
}

export function mapDispatchToProps(dispath){
    return{
        updateSMSText:(messageId:number, message:any)=>{
            dispath(smsTemplatesActions.updateSMSTemplates(SnapMDServiceLocator, messageId, message));
        },
        deleteSMSText:(messageId:number)=>{
            dispath(smsTemplatesActions.deleteSMSTemplates(SnapMDServiceLocator,messageId));
        },
        loadSMSTemplate:(hospitalId:number)=>{
            dispath(smsTemplatesActions.loadSMSTemplates(SnapMDServiceLocator, hospitalId));
        }
    }
}
export default connect(mapStateToProps, mapDispatchToProps)(SmdaClientSmsTemplateCmpnt);