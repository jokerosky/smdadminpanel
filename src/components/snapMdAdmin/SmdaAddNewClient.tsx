import * as React from 'react';
import { connect } from 'react-redux';
import Select from 'react-select';

import { Validator } from '../../utilities/validators';
import * as snapMdAdminActions from '../../actions/snapMdAdminActions';
import { SnapMDServiceLocator } from '../../dependenciesInjectionConfiguration';
import * as Vocabulary from '../../localization/vocabulary';
import { getDefaultAdditionalSettings, getDefaultHospitalSettings } from '../../models/DTO/hospitalSettingDTO';

export interface ISmdaAddNewClientProps {
    language: any;
    hospitalType: any[];
    hospitalTypeForSelect: any[];
    hospitals: any[];

    addNewClient: (newClient: any, hospitalSettings: any, additionalSettings: any) => void;
}

export class SmdaAddNewClientCmpnt extends React.Component<ISmdaAddNewClientProps, any>{
    constructor(props) {
        super(props);

        this.state = {
            hospitalTypeId: 0,
            emailInput: '',
            phoneInput: '',
            nameInput: '',
            emailInputFlag: false,
            nameInputFlag: false
        }
        this.onCrop = this.onCrop.bind(this);
        this.onClose = this.onClose.bind(this);
        this.changeInputNumber = this.changeInputNumber.bind(this);
        this.changeInput = this.changeInput.bind(this);
        this.addNewClient = this.addNewClient.bind(this);
        this.refEmail;
        this.refName;
    }

    refEmail: any;
    refName: any;

    changeInputNumber = (val, field) => {
        if (Validator.isNumber(val.currentTarget.value)) {
            this.setState({ [field]: val.currentTarget.value });
        }
    };

    changeInput = (val, field) => {
        this.setState({
            [field]: val.currentTarget.value,
            [field + 'Flag']: false,
        });
    };

    onClose() {
        this.setState({ preview: null })
    }

    onCrop(preview) {
        this.setState({ preview })
    }

    changeSelect = (val) => {
        this.setState({
            hospitalTypeId: val.value,
        });
    };

    isValid(value, field, listValidate, isBtn?) {
        let isValid = validation(value, listValidate, isBtn);
            this.setState({
                [field + 'Flag']: isValid
            });
            return isValid;
    }

    addNewClient() {
        if (!this.isValid(this.state.nameInput, 'nameInput', [listOfPossibleValidations.whiteSpase], true) && !this.state.emailInputFlag) {
            let newClient = {
                appointmentsContactNumber: "",
                brandColor: "#d14a43",
                brandName: "",
                brandTitle: "",
                children: 0,
                cliniciansCount: 0,
                consultationCharge: 0,
                contactNumber: this.state.phoneInput,
                ePrescriptionGateWay: "",
                email: this.state.emailInput,
                hospitalCode: "",
                hospitalDomainName: "",
                hospitalId: null,
                hospitalImage: "",
                hospitalName: this.state.nameInput,
                hospitalType: this.props.hospitalType[this.state.hospitalTypeId].key,
                iTDeptContactNumber: "",
                insuranceValidationGateWay: "",
                isActive: null,
                nPINumber: 0,
                paymentGateWay: "",
                smsGateway: "",
                totalUsersCount: 0
            };

            this.props.addNewClient(newClient, getDefaultHospitalSettings(), getDefaultAdditionalSettings());
        }
    }

    render() {
        return (
            <div className=''>
                <form className='mx-auto col-lg-8 col-md-7 col-sm-8 col-11 smd-10-margin' onSubmit={(e) => { e.preventDefault(); }}>

                    <div className="form-group">
                        <label htmlFor="nameInput" className={`${this.state.nameInputFlag ? 'smd-validate-color-text' : ''}`}>{this.props.language[Vocabulary.ADD_CLIENT_HOSPITAL_NAME]}</label>
                        <div className={`${this.state.nameInputFlag ? 'smd-validate-border' : ''} `}>
                            <input
                                ref={(name) => {this.refName = name; }}
                                type="text"
                                className="form-control"
                                id="nameInput"
                                placeholder={this.props.language[Vocabulary.ADD_CLIENT_HOSPITAL_NAME]}
                                value={this.state.nameInput}
                                onBlur={(element) => {
                                    this.isValid(this.state.nameInput, 'nameInput', [listOfPossibleValidations.whiteSpase]);
                                }}
                                onChange={(e) => {
                                    this.changeInput(e, 'nameInput')
                                }}
                            />
                        </div>
                    </div>
                    <div className="form-group ">
                        <label htmlFor="emailInput" className={`${this.state.emailInputFlag ? 'smd-validate-color-text' : ''}`}>{this.state.emailInputFlag ? this.props.language[Vocabulary.VALIDATION_INVALID_EMIAL] : this.props.language[Vocabulary.ADD_CLIENT_EMAIL]}</label>
                        <div className={`${this.state.emailInputFlag ? 'smd-validate-border' : ''} `}>
                            <input
                                ref={(email) => { this.refEmail = email; }}
                                type="text"
                                className="form-control"
                                id="emailInput"
                                placeholder={this.props.language[Vocabulary.ADD_CLIENT_EMAIL]}
                                value={this.state.emailInput}
                                onBlur={(element) => {
                                    this.isValid(this.state.emailInput, 'emailInput', [listOfPossibleValidations.email]);
                                }}
                                onChange={(e) => {
                                    this.changeInput(e, 'emailInput')
                                }}
                            />
                        </div>
                    </div>
                    <div className="form-group">
                        <label htmlFor="phoneInput">{this.props.language[Vocabulary.ADD_CLIENT_PHONE]}</label>
                        <input
                            type="text"
                            className="form-control"
                            id="phoneInput"
                            placeholder={this.props.language[Vocabulary.ADD_CLIENT_PHONE]}
                            value={this.state.phoneInput}
                            onChange={(e) => {
                                this.changeInput(e, 'phoneInput')
                            }}
                        />
                    </div>
                    <div className="form-group">
                        <label htmlFor="hospitalType">{this.props.language[Vocabulary.ADD_CLIENT_HOSPITAL_TYPE]}</label>
                        <Select
                            id='hospitalType'
                            name={'Select'}
                            options={this.props.hospitalTypeForSelect}
                            value={this.props.hospitalTypeForSelect[this.state.hospitalTypeId]}
                            clearable={false}
                            onChange={(e) => {
                                this.changeSelect(e);
                            }}
                        />
                    </div>
                    <div className='row justify-content-around smd-10-margin-bottom'>
                        <button
                            id='RegisterUser'
                            className='btn smd-cursor-pointer'
                            onClick={() => {
                                this.addNewClient();
                            }}
                        >{this.props.language[Vocabulary.ADD_CLIENT]}</button>
                        <button
                            id='ClearUser'
                            className='btn smd-cursor-pointer'
                            onClick={() => {
                                this.setState({
                                    hospitalTypeId: 0,
                                    emailInput: '',
                                    phoneInput: '',
                                    nameInput: ''
                                });
                            }}
                        >{this.props.language[Vocabulary.ADD_CLIENT_CLEAR]}</button>
                    </div>
                </form>
            </div>
        );
    }
}

let clientType = [];

export function createObjectHospitalTypesForSelect(props) {
    if (clientType.length > 0 && clientType.length === props.length) {
        return clientType;
    } else {
        for (let index in props) {
            clientType.push({
                value: +index, //convert string to number
                label: props[index].value
            });
        }
        return clientType;
    }
}

export function mapStateToProps(state, ownProps) {
    let hospitalTypeForSelect = [];
    if (state.snapmdAdmin.lists.clientsTypes) {
        hospitalTypeForSelect = createObjectHospitalTypesForSelect(state.snapmdAdmin.lists.clientsTypes);
    }
    return {
        language: state.site.misc.vocabulary as any,
        hospitalType: state.snapmdAdmin.lists.clientsTypes as any[],
        hospitalTypeForSelect: hospitalTypeForSelect as any[],
        hospitals: state.snapmdAdmin.lists.clients as any[]
    }
}

export function mapDispatchToProps(dispatch) {
    return {
        addNewClient: (newClient: any, hospitalSettings: any, additionalSettings: any) => {
            dispatch(snapMdAdminActions.addNewClient(SnapMDServiceLocator, newClient, hospitalSettings, additionalSettings));
        }
    };
}

export default connect(mapStateToProps, mapDispatchToProps)(SmdaAddNewClientCmpnt);


/*********************************************** */
function validation(value, listValidate, isBtn) {
    if (value) {
        let validate = false;
        for (let index in listValidate) {
            switch (listValidate[index]) {

                case listOfPossibleValidations.email:
                    if (!Validator.isEmail(value)) {
                        validate = true;
                    } else {
                        return false;
                    }
                    break;
                case listOfPossibleValidations.whiteSpase:
                    if (!Validator.isNotEmptyOrWhitespace(value)) {
                        validate = true;
                    } else {
                        return false;
                    }
                    break;

            }
        }
        return validate;
    } else {
        if (isBtn) {
            return true;
        } else {
            return false;
        }
    }
}

let listOfPossibleValidations = {
    email: 'email',
    whiteSpase: 'whiteSpase'
}