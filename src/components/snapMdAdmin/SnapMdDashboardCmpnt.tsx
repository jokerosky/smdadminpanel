import * as React from 'react';
import { connect } from 'react-redux';
import { push } from 'react-router-redux';


import { toAbsolutePath, Endpoints } from '../../enums/endpoints';
import { SmdaLeftMenuCmpnt } from './SmdaLeftMenuCmpnt';
import { SmdaContentCmpnt } from './SmdaContentCmpnt';
import { SnapMDServiceLocator } from '../../dependenciesInjectionConfiguration';

import { ClientSummaryDTO } from '../../models/DTO/clientSummaryDTO';

import * as snapmdAdminActions from '../../actions/snapMdAdminActions';
import * as clientsDataActions from '../../actions/snapMdAdminClientActions/clientDataActions';
import * as SettingsActions from '../../actions/snapMdAdminClientActions/settingsActions';
import { MODULE_KEYS, SETTINGS_KEYS, SETTINGS_KEYS2 } from '../../enums/clientSettingsKeys';
import * as Vocabulary from '../../localization/vocabulary';
import { getDefaultState } from '../../models/DTO/hospitalSettingDTO';

export interface ISnapMdDashboardCmpntProps {
    clientList: ClientSummaryDTO[];
    selectedId: number;
    clientView: string;
    filters: any;
    seats: any;
    seatsAssigned: any;
    language: any;
    path: string;

    selectItem: (id: number, clientView: string, hospitals: any[]) => void;
    filtersChanged: (filters: any) => any;
    selectView: (id: number, clientView: string) => void;
    getClientData: (hospitalId: number) => void;
}

export class SnapMdDashboardCmpnt extends React.Component<ISnapMdDashboardCmpntProps, any> {
    constructor() {
        super();

        this.state = {
            isLeftMenuShown: false
        };
    }

    componentWillReceiveProps(nextProps) {
        if (this.props.clientList.length > 0 && nextProps.clientList.length > this.props.clientList.length) {
            this.props.selectItem(nextProps.clientList[nextProps.clientList.length - 1].hospitalId, Endpoints.site.snapMdAdmin.detailsPostfix, nextProps.clientList);
            this.props.selectView(nextProps.clientList[nextProps.clientList.length - 1].hospitalId, Endpoints.site.snapMdAdmin.detailsPostfix);
            this.props.getClientData(nextProps.clientList[nextProps.clientList.length - 1].hospitalId);
        }
    }

    render() {
        return (
            <div className='row'>
                <div className={`${this.state.isLeftMenuShown ? '' : 'd-none'} col-xs-12 col-lg-5 d-lg-flex smd-no-padding`}>
                    {/* Menu */}
                    <SmdaLeftMenuCmpnt
                        hospitalItems={this.props.clientList}
                        selectedId={this.props.selectedId}
                        selectItem={(id: number) => { this.props.selectItem(id, this.props.clientView, this.props.clientList) }}
                        filtersChanged={this.props.filtersChanged}
                        filters={this.props.filters}
                        getClientData={this.props.getClientData}
                        seats={this.props.seats}
                        seatsAssigned={this.props.seatsAssigned}
                        language={this.props.language}
                    />
                </div>
                <div className={`${this.state.isLeftMenuShown ? 'd-none' : ''} d-lg-flex col-lg-7 border rounded-top align-items-start`}>
                    {/* content */}
                    <SmdaContentCmpnt
                        dashboardChildren={this.props.children}
                        selectedView={this.props.clientView}
                        selectedId={this.props.selectedId}
                        selectView={this.props.selectView}
                        language={this.props.language}
                        path={this.props.path}
                    />

                </div>
                <div className='col-12 border d-lg-none d-xs-flex rounded-bottom'>
                    <div className='row justify-content-between smd-height-50'>
                        <button
                            className={`col-6 btn ${this.state.isLeftMenuShown ? 'btn-primary' : ''}`}
                            onClick={() => {
                                this.setState({
                                    isLeftMenuShown: true
                                });
                            }}
                        >
                            {this.props.language[Vocabulary.DASHBOARD_CLIENT]}
                        </button>
                        <button
                            className={`col-6 btn ${this.state.isLeftMenuShown ? '' : 'btn-primary'}`}
                            onClick={() => {
                                this.setState({
                                    isLeftMenuShown: false
                                });
                            }}
                        >
                            {this.props.language[Vocabulary.DASHBOARD]}
                        </button>
                    </div>
                </div>
            </div>
        );
    }
}

export function mapStateToProps(state, ownProps) {
    let selectedDocumentId;
    if (state.snapmdAdmin.client.hospitalDocument && state.snapmdAdmin.client.hospitalDocument.selectedDocument > 0) {
        selectedDocumentId = state.snapmdAdmin.client.hospitalDocument.selectedDocument;
    }
    return {
        clientList: state.snapmdAdmin.lists.clients as ClientSummaryDTO[],
        selectedId: state.snapmdAdmin.misc.selectedId as number,
        clientView: state.snapmdAdmin.misc.clientView as string,
        filters: state.snapmdAdmin.misc.filters as any,
        seats: state.snapmdAdmin.client.details.seats as any,
        seatsAssigned: state.snapmdAdmin.client.details.seatsAssigned as any,
        language: state.site.misc.vocabulary as any,
        path: state.routing.locationBeforeTransitions.pathname as string,
    }
}

export function mapDispatchToProps(dispatch) {
    return {
        selectItem: (id: number, clientView: string, hospitals: any[]) => {
            if (id < 0 || id === null) {
                dispatch(snapmdAdminActions.selectView(null));
                dispatch(snapmdAdminActions.selectClient(null));
                dispatch(snapmdAdminActions.findClientName([]));
                dispatch(push(toAbsolutePath(Endpoints.site.snapMdAdmin.dashboard)));
            } else {
                dispatch(snapmdAdminActions.selectClient(id));
                dispatch(snapmdAdminActions.findClientName(hospitals));
                let path = '/' + (id > -1 ? `${id}/` + (clientView || Endpoints.site.snapMdAdmin.detailsPostfix) : '');
                dispatch(push(toAbsolutePath(Endpoints.site.snapMdAdmin.dashboard + path)));
                if (clientView === null || clientView === '') {
                    dispatch(snapmdAdminActions.selectView(Endpoints.site.snapMdAdmin.detailsPostfix));
                }
            }
        },
        filtersChanged: (filters: any) => dispatch(snapmdAdminActions.filtersChanged(filters)),
        selectView: (id: number, clientView: string) => {
            if (id < 0 || id === null) {
                dispatch(snapmdAdminActions.selectView(null));
                dispatch(push(toAbsolutePath(Endpoints.site.snapMdAdmin.dashboard)));
            } else {
                dispatch(snapmdAdminActions.selectView(clientView));
                let path = '/' + (id > -1 ? `${id}/` + (clientView || '') : '');
                dispatch(push(toAbsolutePath(Endpoints.site.snapMdAdmin.dashboard + path)));
                if (clientView === Endpoints.site.snapMdAdmin.hospitalDocPostfix) {
                }
            }
        },
        getClientData: (hospitalId: number) => {
            if (hospitalId != null && hospitalId > -1) {
                dispatch(clientsDataActions.loadClientsData(SnapMDServiceLocator, hospitalId));
                dispatch(SettingsActions.clearSettings(getDefaultState()));
                dispatch(SettingsActions.getClientSettings(SnapMDServiceLocator, hospitalId, MODULE_KEYS));
                dispatch(SettingsActions.getClientSettings(SnapMDServiceLocator, hospitalId, SETTINGS_KEYS));
                dispatch(SettingsActions.getClientSettings(SnapMDServiceLocator, hospitalId, SETTINGS_KEYS2));
            }
        },
    };
}

export default connect(mapStateToProps, mapDispatchToProps)(SnapMdDashboardCmpnt);