import * as React from 'react';
import * as Helmet from 'react-helmet';
import { connect } from 'react-redux';
import { push } from 'react-router-redux';

import * as siteActions from '../../actions/siteActions';
import * as snapMdAdminActions from '../../actions/snapMdAdminActions';
import * as userActions from '../../actions/userActions';

import { toAbsolutePath, Endpoints } from '../../enums/endpoints';

import { SnapMDServiceLocator } from '../../dependenciesInjectionConfiguration'

import { MenuItem } from '../../models/site/menuItems';
import { SmdaLeftMenuCmpnt } from './SmdaLeftMenuCmpnt';
import { SmdaHeaderCmpnt } from './SmdaHeaderCmpnt';
import { SmdaContentCmpnt } from './SmdaContentCmpnt';
import { getMainMenuItems } from './snapMdAdminMenuItems';

import { ClientSummaryDTO } from '../../models/DTO/clientSummaryDTO';

import * as clientsDataActions from '../../actions/snapMdAdminClientActions/clientDataActions';
import * as LanguageActions from '../../actions/snapMdAdminMenuActions/viewLocalizationLanguageActions';
import { LocalizationService } from '../../localization/localizationService';
import { Validator } from '../../utilities/validators';

import { TestService } from '../../services/testService';

export interface ISnapMdAdminAppProps {

    toggleHospitalStyles: (isEnabled: boolean) => void;
    getClients: () => void;
    getLists: () => void;
    selectClient: (hospitalId: number) => void;
    selectView: (view: string) => void;

    clients?: ClientSummaryDTO[];
    userState?: any;
    hospitalId?: number;
    clientView?: string;
    language?: any;
    symbolOfLanguage?: string;

    goTo: (path: string) => void;
    logOut: () => void;
    goToClientsPage: (e?: any) => void;
    changeLanguage:(symbolOfLanguage: string) => void;
}


export class SnapMdAdminApp extends React.Component<ISnapMdAdminAppProps, any> {
    constructor() {
        super();
    }

    componentWillMount() {
        this.props.toggleHospitalStyles(false);
        this.props.getClients();
        this.props.getLists();
        this.props.changeLanguage(this.props.symbolOfLanguage);
        //if we have id in browser location try to select it
        let hospitalId = (this.props as any).params.id;
        if (Number.parseInt(hospitalId) > -1) {
            this.props.selectClient(hospitalId);
            if ((this.props as any).routes[3].path != 'details') {
                let postfix = '';
                let path = '';
                for (let index in Endpoints.site.snapMdAdmin) {
                    if (Endpoints.site.snapMdAdmin[index + 'Postfix'] === (this.props as any).routes[3].path) {
                        postfix = (Endpoints.site.snapMdAdmin[index + 'Postfix']).toLowerCase();
                        path = (Endpoints.site.snapMdAdmin[index]).toLowerCase();
                        break;
                    }
                }
                this.props.selectView(postfix);
                this.props.goTo(toAbsolutePath(path.replace('%id%', hospitalId)));
            } else {
                this.props.selectView(Endpoints.site.snapMdAdmin.detailsPostfix);
                this.props.goTo(toAbsolutePath(Endpoints.site.snapMdAdmin.details.replace('%id%', hospitalId)));
            }
        }
        else {
            if ((this.props as any).routes.length < 3 ||
                (this.props as any).routes[2].path === Endpoints.site.dashboardPostfix + '(/:id)') {
                if (!Validator.isNumber((this.props as any).params.id)) {
                    let path = '';
                    if ((this.props as any).params.id === Endpoints.site.snapMdAdmin.addNewClientPostfix) {
                        this.props.goTo(toAbsolutePath(Endpoints.site.snapMdAdmin.dashboard + '/' + (this.props as any).params.id));
                    } else {
                        this.props.goTo(toAbsolutePath(Endpoints.site.snapMdAdmin.dashboard));
                    }
                } else {
                    this.props.goTo(toAbsolutePath(Endpoints.site.snapMdAdmin.dashboard));
                }
            } else {
                this.props.goTo(toAbsolutePath(Endpoints.site.snapMdAdmin[(this.props as any).routes[2].path]));
            }
        }

    }

    componentDidMount() {

    }

    componentWillUnmount() {
        this.props.toggleHospitalStyles(true);
    }

    componentWillReceiveProps(nextProps) {
        if (nextProps.hospitalId !== this.props.hospitalId || nextProps.clientView !== this.props.clientView ||
            nextProps.language !== this.props.language ) {
            this.mainMenuItems = getMainMenuItems(nextProps);
        }
    }

    mainMenuItems: any;

    static configureMenuActions() {
        let props = {
            logout: () => { },
            createNewAppointment: () => { }
        };

        return props;
    }

    stylesHref = 'https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-beta/css/bootstrap.min.css';
    fontAwesomeHref = "https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css";

    renderTitle(): string {
        var title = (SnapMDServiceLocator.getObject("TestService") as TestService).getString();

        return title;
    }

    render() {
        if (!this.mainMenuItems) {
            this.mainMenuItems = getMainMenuItems(this.props);
        }
        return (
            <div className="SNAPMD_ADMIN_PAGE container-fluid">
                <Helmet
                    title={this.renderTitle()}
                    link={[
                        { rel: "stylesheet", type: 'text/css', href: this.stylesHref },
                        { rel: "stylesheet", type: 'text/css', href: this.fontAwesomeHref },
                    ]}
                    script={[
                        { src: "https://code.jquery.com/jquery-3.2.1.slim.min.js" },
                        { src: "https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.11.0/umd/popper.min.js" },
                        { src: "https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-beta/js/bootstrap.min.js" }
                    ]}
                />
                {/* Header */}
                <SmdaHeaderCmpnt
                    brand=""
                    searchAction={(query) => { }}
                    menuItems={this.mainMenuItems}
                />

                {this.props.children}

            </div>
        );
    }
}

export function mapStateToProps(state, ownProps) {
    return {
        userState: state.user as any,
        hospitalId: state.snapmdAdmin.misc.selectedId as number,
        clientView: state.snapmdAdmin.misc.clientView as string,
        language: state.site.misc.vocabulary as any,
        symbolOfLanguage: state.site.misc.language as string,
    }
}

export function mapDispatchToProps(dispatch, ownProps) {
    return {
        toggleHospitalStyles: isEnabled => dispatch(siteActions.toggleHospitalStyles(isEnabled)),
        getClients: () => dispatch(snapMdAdminActions.loadClients(SnapMDServiceLocator)),
        getLists: () => dispatch(snapMdAdminActions.loadLists(SnapMDServiceLocator)),
        selectClient: (hospitalId: number) => {
            if (hospitalId) {
                dispatch(snapMdAdminActions.selectClient(hospitalId));
                dispatch(clientsDataActions.loadClientsData(SnapMDServiceLocator, hospitalId));
                dispatch(snapMdAdminActions.loadHospitalAddress(SnapMDServiceLocator, hospitalId));
            }
        },
        selectView: (view: string) => {
            dispatch(snapMdAdminActions.selectView(view));
        },
        goTo: (path: string) => dispatch(push(path)),
        logOut: () => dispatch(userActions.logOut()),
        changeLanguage: (language: string) => {
            LocalizationService.language = language;
            dispatch(LanguageActions.changeLanguage(language));
            dispatch(LanguageActions.loadDictionary());
        }
    };
}

export default connect(mapStateToProps, mapDispatchToProps)(SnapMdAdminApp);

