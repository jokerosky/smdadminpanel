import * as React from 'react';
import { MenuItem } from '../../models/site/menuItems';
import { Link } from 'react-router';
import { Endpoints, toAbsolutePath } from '../../enums/endpoints';
import LanguageCmpnt from '../common/LocalizationLanguageCmpnt';

export interface ISmdaHeaderProps {
    brand: string;
    searchAction: (query: string) => void;

    //* menu actions */
    menuItems?: MenuItem[];

}


export class SmdaHeaderCmpnt extends React.Component<ISmdaHeaderProps, any> {
    constructor() {
        super()
    }

    render() {
        return (
            <nav className="row navbar navbar-expand-lg navbar-light alert alert-secondary ">
                <button className="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
                    <span className="navbar-toggler-icon"></span>
                </button>
                <Link
                    to={toAbsolutePath(Endpoints.site.snapMdAdmin.dashboard)}
                    className={`navbar-brand`}
                >
                    SnapMD Admin
                </Link>
                <form className="justify-content-md-end justify-content-lg-start nav-item form-inline col-lg-4 col-md-5 my-2 my-lg-0  d-none d-md-flex" onSubmit={(e) => { e.preventDefault(); return false; }}>
                    <input disabled className="form-control mr-sm-2 smd-cursor-not-allowed" type="text" placeholder="Search" aria-label="Search" />
                    <button disabled className="btn btn-outline-disabled my-2 my-sm-0 smd-cursor-not-allowed" type="submit"><i className="fa fa-search"></i></button>
                </form>

                <div className="collapse navbar-collapse col-lg-5 col" id="navbarSupportedContent">
                    <ul className="navbar-nav mr-auto">
                        {this.props.menuItems}
                    </ul>

                </div>
                <div className='smd-width-100  ml-auto'>
                    <LanguageCmpnt />
                </div>
            </nav>
        );
    }
}