﻿import * as React from 'react';
import * as _ from 'lodash';

import { ClientSummaryDTO } from '../../models/DTO/clientSummaryDTO';

import { LocalizationService } from '../../localization/localizationService';
import * as Vocabulary from '../../localization/vocabulary';
import { SortOrder, getNext } from '../../enums/siteEnums';

export interface ISmdaLeftMenuProps {
    hospitalItems?: ClientSummaryDTO[];
    selectedId?: number;
    filters?: any;
    seats: any;
    seatsAssigned: any;
    language: any;

    selectItem: (id: number) => void;
    filtersChanged: (filters: any) => void;
    getClientData: (hospitalId: number) => void;
}

export class SmdaLeftMenuCmpnt extends React.Component<ISmdaLeftMenuProps, any> {

    constructor(props, ctx) {
        super(props, ctx);
        this.state = {
            filteredItems: this.props.hospitalItems
        }
        this.filterItems = this.filterItems.bind(this);
        this.filterChanged = this.filterChanged.bind(this);
    }

    static searchDebounceTime = 400;

    searchForm: HTMLFormElement;
    timeoutHandler = null;

    blankCssSortFlags = {
        hospitalId: '',
        hospitalName: '',
        seats: ''
    }

    cssSortFlags = Object.assign({}, this.blankCssSortFlags);

    componentWillReceiveProps(nextProps: ISmdaLeftMenuProps) {
        this.filterItems(nextProps.filters, nextProps.hospitalItems)
    }

    filterChanged(event: any, sortKey: string = '') {
        let form = this.searchForm;
        let sortOrder = this.props.filters.sortOrder || '';
        if (sortKey == this.props.filters.sortKey) {
            sortOrder = getNext(SortOrder, this.props.filters.sortOrder);
        }
        else {
            sortOrder = SortOrder.asc;
        }

        if (this.timeoutHandler) window.clearTimeout(this.timeoutHandler);
        this.timeoutHandler = setTimeout(() => {
            let filters = {
                id: form['hid'].value.trim().toLowerCase(),
                name: form['hname'].value.trim().toLowerCase(),
                sortKey: sortKey || '',
                sortOrder: sortOrder || ''
            };
            this.timeoutHandler = null;
            this.props.filtersChanged(filters);
        }, SmdaLeftMenuCmpnt.searchDebounceTime);
    }

    filterItems(filters: any, hospitalItems: ClientSummaryDTO[]) {
        let items = [];
        this.cssSortFlags = Object.assign({}, this.blankCssSortFlags);
        if (filters.sortKey && filters.sortOrder) {
            if (filters.sortOrder == SortOrder.no) {
                this.cssSortFlags[filters.sortKey] = "";
            } else {
                hospitalItems = _.orderBy(hospitalItems, [(hospital) => { return hospital[filters.sortKey] }], [filters.sortOrder]);
                this.cssSortFlags[filters.sortKey] = "fa-sort-" + filters.sortOrder;
            }
        }

        if (!(filters.id || filters.name)) {
            this.setState({
                filteredItems: hospitalItems
            });
            return;
        }

        items = _.filter(hospitalItems,
            (item: ClientSummaryDTO) => {
                let idCondition = filters.id ?
                    item.hospitalId.toString().toLowerCase().indexOf(filters.id) > -1
                    : true;
                let nameCondition = filters.name ?
                    item.hospitalName.toString().toLowerCase().indexOf(filters.name) > -1
                    : true;

                return idCondition && nameCondition;
            });
        this.setState({
            filteredItems: items
        });
    };

    renderItems(hospitalItems?: any[], selectedId?: number) {
        let items: Array<any> = null;
        if (hospitalItems) {
            items = [];
            for (let item in hospitalItems) {
                let hospital = hospitalItems[item] as ClientSummaryDTO;
                let isActive = hospital.hospitalId == this.props.selectedId ? 'table-success' : '';
                let classActive = hospital.isActive === 'N' ? 'bg-light' : '';
                let li =
                    <tr key={item}
                        className={`row smd-cursor-pointer ${isActive + ' ' + classActive}`}
                        onClick={(e) => {
                            let hospitalId = hospital.hospitalId;

                            hospitalId = hospitalId == this.props.selectedId ? null : hospitalId;
                            this.props.selectItem(hospitalId);
                            this.props.getClientData(hospitalId);

                        }}
                    >
                        <td className='col'>{hospital.hospitalId}</td>
                        <td className='col-8'>{hospital.hospitalName}</td>
                        <td className='col'>{hospital.seats}</td>
                    </tr>;
                items.push(li);
            }
        }

        return items;
    }

    render() {
        let items = this.renderItems(this.state.filteredItems);
        return (
            //todo: extract search box into separate component
            <div className='container-fluid'>
                <form className='form-inline smd-v-padding'
                    ref={(form) => { this.searchForm = form; }}
                    onSubmit={
                        (e) => {
                            e.preventDefault();
                            return false;
                        }
                    }>

                    <input className="form-control mr-sm-2 col-2"
                        name='hid'
                        type="text"
                        placeholder="Id"
                        aria-label="Id"
                        defaultValue={this.props.filters.id || ''}
                        onChange={this.filterChanged} />

                    <input className="form-control mr-sm-2 col"
                        name='hname'
                        type="text"
                        placeholder="Name"
                        aria-label="Name"
                        defaultValue={this.props.filters.name || ''}
                        onChange={this.filterChanged} />
                </form>
                <h6 className='text-muted'>{this.props.language[Vocabulary.SNAPMDADMIN_TOTAL_ITEMS]}: {items ? items.length : 0}</h6>
                <table className="table table-hover " id='tableListClient'>
                    <thead>
                        <tr className='row'>
                            <th className='col smd-cursor-pointer smd-underlined'
                                onClick={() => {
                                    this.filterChanged(null, 'hospitalId');
                                }
                                }
                            >{this.props.language[Vocabulary.SNAPMDADMIN_CLIENT_ID]} <i className={`fa ${this.cssSortFlags['hospitalId']}`} aria-hidden="true"></i></th>
                            <th className='col-7 smd-cursor-pointer smd-underlined'
                                onClick={() => {
                                    this.filterChanged(null, 'hospitalName');
                                }
                                }
                            >{this.props.language[Vocabulary.SNAPMDADMIN_CLIENT_NAME]} <i className={`fa ${this.cssSortFlags['hospitalName']}`} aria-hidden="true"></i></th>
                            <th className='col smd-cursor-pointer smd-underlined'
                                onClick={() => {
                                    this.filterChanged(null, 'seats');
                                }
                                }
                            >{this.props.language[Vocabulary.SNAPMDADMIN_CLIENT_SEATS]} <i className={`fa ${this.cssSortFlags['seats']}`} aria-hidden="true"></i></th>
                        </tr>
                    </thead>
                    <tbody>
                        {items || <tr><td>No clients to display</td></tr>}
                    </tbody>
                </table>
            </div>

        );
    }

}    