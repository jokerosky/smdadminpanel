import React from 'react';
import { Link } from 'react-router';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import Helmet from 'react-helmet';

import * as siteActions from '../../actions/siteActions';
import { Endpoints } from '../../enums/endpoints';


const lessURL = Endpoints.site.rootSnapLocal + "less/v3/styles.v3.less.dynamic?";

//another dirty hack for styles 
if (window) {
    window.addStylesHandler = function () {
        if (window.dispatch) {
            window.dispatch(siteActions.stylesLoaded());
        }
    }


    window.errorStylesHandler = function (err) {
        if (window.dispatch) {
            window.dispatch(siteActions.loadApiKeysFailure(err));
        }
    }
}


export class LessStylesCmpnt extends React.Component {
    constructor(props, context) {
        super(props, context);
        this.branding = this.props.branding;

        this.stylesShown = false;
    }

    composeServerSideLessString() {
        let result = lessURL;
        result += `brandColor=${encodeURIComponent(this.branding.brandColor)}&`;
        result += `brandTextColor=${encodeURIComponent(this.branding.brandTextColor)}&`;
        result += `brandBackgroundColor=${encodeURIComponent(this.branding.brandBackgroundColor)}&`;
        result += `brandBackgroundImage=${encodeURIComponent(this.branding.brandBackgroundImage)}&`;
        result += `brandBackgroundLoginImage=${encodeURIComponent(this.branding.brandBackgroundLoginImage)}`;
        return result;
    }

    render() {
        return (
            <Helmet

                link={[
                    // front-end compilation with less.js library (need to add this in scripts section above)
                    /*{
                        rel: "stylesheet/less", href: "/less/v3/styles.v3.less",
                        dataGlobalVars: {
                            main_brand_color: this.branding.brandColor,
                            main_brand_background_image: this.branding.brandBackgroundImage,
                            main_brand_background_login_image: this.branding.brandBackgroundLoginImage,
                            main_brand_background_color: this.branding.brandBackgroundColor,
                            main_brand_text_color: this.branding.brandTextColor
                        }
                    },*/
                    { rel: "stylesheet", type: 'text/css', href: this.composeServerSideLessString(), onload: 'addStylesHandler()', onerror:'errorStylesHandler()'} 
                ]}
                /* script = {
                    [{
                        src:"//cdnjs.cloudflare.com/ajax/libs/less.js/2.7.2/less.min.js", onload: 'addStylesHandler()'
                    }]
                } */
            />
        );
    }
}

function mapHospitalDataToBranding(hospitalData) {
    return {
        brandColor: hospitalData.brandColor || '#00008B',
        brandBackgroundColor: hospitalData.settings.brandBackgroundColor || 'transparent',
        brandBackgroundImage: hospitalData.settings.brandBackgroundImage || '',
        brandBackgroundLoginImage: hospitalData.settings.brandBackgroundLoginImage || '',
        brandTextColor: hospitalData.settings.brandTextColor || '#FFFFFF'
    }
}

function mapStateToProps(state, ownProps) {
    return {
        branding: mapHospitalDataToBranding(state.hospital),
        stylesLoaded: state.site.ajax.stelesLoaded
    };
}

function mapDispatchToProps(dispatch) {
    return {
        actions: bindActionCreators(siteActions, dispatch)
    };
}

export default connect(mapStateToProps, mapDispatchToProps)(LessStylesCmpnt);

