import * as React from 'react';
import { connect } from 'react-redux';
import { SnapMDServiceLocator } from '../../dependenciesInjectionConfiguration';
import * as LanguageActions from '../../actions/snapMdAdminMenuActions/viewLocalizationLanguageActions';
import { LocalizationService } from '../../localization/localizationService';

import Select from 'react-select';
import Img from 'react-image';
import { flags, language } from '../../enums/languages';
import { Endpoints } from '../../enums/endpoints';

export interface ILanguageCmpntProps {
    changeLanguage: (language: string) => void;
}

export class LanguageCmpnt extends React.Component<ILanguageCmpntProps, any>{
    constructor(props) {
        super(props);

        this.state = { 
            languageSelected: 'us'
        };
        
        this.setLanguage = this.setLanguage.bind(this);
    }

    setLanguage(value) {
        this.setState({
            languageSelected: value.value
        });
        this.props.changeLanguage(value.value);
    }

    render() {
        return (
            <div className=''>
                <Select
                    name = {'Language'}
                    valueComponent={ImagesValue}
                    optionComponent={ImagesOption}
                    options={language}
                    value={this.state.languageSelected}
                    onChange={this.setLanguage}
                    clearable={false}
                />
            </div>
        );
    }
}

export function mapStateToProps(state, ownProps) {
    return {
    }
}

export function mapDispatchToProps(dispatch) {
    return {
        changeLanguage: (language: string) => {
            LocalizationService.language = language;
            dispatch(LanguageActions.changeLanguage(language));
            dispatch(LanguageActions.loadDictionary());
        }
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(LanguageCmpnt);

/******************************************************************* */

class ImagesOption extends React.Component<any, any>{
    constructor() {
        super();

        this.handleMouseEnter = this.handleMouseEnter.bind(this);
        this.handleMouseMove = this.handleMouseMove.bind(this);
        this.handleMouseDown = this.handleMouseDown.bind(this);
    }

    handleMouseDown(event) {
        event.preventDefault();
        event.stopPropagation();
        this.props.onSelect(this.props.option, event);
    }

    handleMouseEnter(event) {
        this.props.onFocus(this.props.option, event);
    }

    handleMouseMove(event) {
        if (this.props.isFocused) return;
        this.props.onFocus(this.props.option, event);
    }

    render() {
        return (
            <div className={this.props.className}
                onMouseDown={this.handleMouseDown}
                onMouseEnter={this.handleMouseEnter}
                onMouseMove={this.handleMouseMove}
            >
                <Img src={Endpoints.site.root + flags[this.props.option.value]} />
                {' ' + this.props.children}
            </div>
        )
    }
}

class ImagesValue extends React.Component<any, any>{
    constructor() {
        super();
    }

    render() {
        return (
            <div className="Select-value">
                <span className="Select-value-label">
                    <Img src={Endpoints.site.root + flags[this.props.value.value]} />
                    {' ' + this.props.children}
                </span>
            </div>
        )
    }
}