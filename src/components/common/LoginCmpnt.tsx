import * as React from 'react';
import { connect } from 'react-redux';
import { Link } from 'react-router';
import { push } from 'react-router-redux';
import * as _ from 'lodash';

import * as userActions from '../../actions/userActions';
import * as siteActions from '../../actions/siteActions';
import * as userTypes from '../../enums/userTypes';
import { Endpoints, toAbsolutePath }  from '../../enums/endpoints';
import { Hospital } from '../../models/hospital';
import { IStorageService, LocalStorageService } from '../../services/storageService';
import { LocalizationService } from '../../localization/localizationService';
import * as Vocabulary from '../../localization/vocabulary';
import {ValidationSet, Validator, getCssFlags } from '../../utilities/validators';
import LoginRequestDTO from '../../models/DTO/loginRequestDTO';
import {BaseSnapComponent} from '../BaseSnapComponent';


import { validateSet, validateAll } from '../../utilities/validators';

//because we use it outside and can`t use static values becaues of overriding base class
let rememberLogin = 'rememberLogin';
let loginEmail = 'loginEmail';

export class LoginCmpnt extends BaseSnapComponent {
    constructor(props, context) {
        super(props, context);
        //DI imitation
        this.localStorage = this.props.dependencies.localStorageService as IStorageService;

        this.state = {
            rememberLogin: this.localStorage.get(rememberLogin),
            cssFlags:props.cssFlags
        };

        if(this.state.rememberLogin){
            this.rememberedLogin =  this.localStorage.get(loginEmail);
        }

        this.prepareValidationRules();
        //functions binding
        this.rememberMyEmail = this.rememberMyEmail.bind(this);
        this.cleanError = this.cleanError.bind(this);
        this.validate = this.validate.bind(this);
        this.validateInput = this.validateInput.bind(this);
        this.logIn = this.logIn.bind(this);
        this.submitClicked = this.submitClicked.bind(this);
    }

    localStorage: IStorageService;
    emailId: string = 'txtloginemail';
    passwordId: string = 'txtPassword';
    rememberedLogin: string = '';
    allowedLoginTypes = [userTypes.patient, userTypes.provider, userTypes.admin, userTypes.snapMdAdmin];


    componentWillMount(){
        var userType = this.props.params.userType;
        var index = _.keys(userTypes).findIndex( (x) => {return x == userType});
        if(index < 0)
            this.props.goToRoute(push(Endpoints.site.root));
    }

    prepareValidationRules() {
        let emailValidation = {
            elementId: this.emailId,
            element: null,
            rules: [
                // if passed value is wrong we return error message, other case we return null or empty string
                (val) => {
                    if (!Validator.isEmail(val)) {
                        return this.props.language[Vocabulary.VALIDATION_INVALID_EMIAL];
                    }
                },
            ]
        } as ValidationSet;


        //add empty rule because we store link to element inside validation set
        let passwordlValidation = {
            elementId: this.passwordId,
            element: null,
            rules: [
                (val) => {
                    if (!Validator.isNotEmptyOrWhitespace(val)) {
                        return this.props.language[Vocabulary.VALIDATION_INVALID_PASSWORD];
                    }
                },
            ]
        } as ValidationSet;


        this.validationSets[this.emailId] = emailValidation;
        this.validationSets[this.passwordId] = passwordlValidation;
    }

    renderTitle() {
        let title = '';
        switch (this.props.userType) {
            case userTypes.admin: {
                return title = this.props.language[Vocabulary.COMPONENTS_LOGIN_ADMIN];
            }
            case userTypes.provider: {
                return title = this.props.language[Vocabulary.COMPONENTS_LOGIN_PROVIDER];
            }
            case userTypes.patient: {
                return title = this.props.language[Vocabulary.COMPONENTS_LOGIN_PATIENT];
            }
        }

        return title;
    }

    rememberMyEmail(e) {
        if (this.state.rememberLogin) {
            this.localStorage.del(rememberLogin);
            this.setState({ rememberLogin: false });
        }
        else {
            this.localStorage.add(rememberLogin, true);
            this.setState({ rememberLogin: true });
        }
    }


    logIn(e) {
        e.preventDefault();

        if(this.validate(true)){
            let hospitalId = this.props.hospital.hospitalId;
            let userTypeId = this.props.userType;
            let redirectUrl = this.props.redirectUrl;

            if(this.props.userType == userTypes.snapMdAdmin){
                userTypeId = userTypes.admin;
                hospitalId = 0;
                redirectUrl = toAbsolutePath(Endpoints.site.snapMdAdmin.dashboard);
            }

            let credentials = {
                hospitalId: hospitalId,
                email:this.validationSets[this.emailId].element.value,
                password:this.validationSets[this.passwordId].element.value,
                userTypeId: userTypeId,
                redirectUrl: redirectUrl
            } as LoginRequestDTO;

            if(this.state.rememberLogin){
                this.localStorage.add(loginEmail,credentials.email);
            }
            else{
                this.localStorage.del(loginEmail);
            }

            this.props.sendCredentials(credentials);
        }
        return false;
    }

    static childContextTypes = {
    };

    render() {
        return (
            <div className='cc-public'>
                <div id="divMainWrap" className="mainWrap">
                    <div id="page-wrap">
                        {/*<!--Header-->*/}
                        <header className="header cc-public__header" id="divHeader"><div className="centerWrap">
                            <h1 className="title" title={this.props.hospital.brandName}> {this.props.hospital.brandName} </h1>
                        </div></header>
                        {/*<!-- // End Header-->*/}
                        {/*<!--Content-->*/}
                        <section className="col5-sm col12-md col16-lg content cc-public__content cc-public__content--login active" id="divLogin">

                            <div className={`col12-sm col12-md-8 col16-lg-6 box ${this.props.cssFlags[BaseSnapComponent.shakeClass] || ''} `} >
                                <form action="#" id="divLoginContent" onSubmit={this.logIn}>
                                    <div className="box__header box__header--login">
                                        <h2>{this.renderTitle()}</h2>
                                    </div>
                                    <div className="box__content">
                                        <div className="inputs inputs__icon inputs__email">
                                            <span className="icon_user"></span>
                                            <input ref={(email) => { this.validationSets[this.emailId].element = email; }}
                                                placeholder="Email"
                                                id={this.emailId}
                                                tabIndex={1}
                                                autoFocus={true}
                                                className={`k-input preventSpaces ${this.cssFlags[this.emailId] || ''}`}
                                                type="text"
                                                onChange={this.cleanError}
                                                onBlur={this.validateInput}
                                                defaultValue = {this.rememberedLogin}
                                                required />
                                        </div>
                                        <div className="inputs inputs__icon inputs__password">
                                            <span className="icon_lock"></span>
                                            <input ref={(password) => { this.validationSets[this.passwordId].element = password;}}
                                                id={this.passwordId}
                                                className={`k-input preventSpaces ${this.cssFlags[this.passwordId] || ''}`}
                                                tabIndex={2}
                                                placeholder="Password"
                                                onChange={this.cleanError}
                                                onBlur={this.validateInput}
                                                type="password"
                                                required />
                                        </div>
                                        <div className="check-box">
                                            <label>
                                                <div className="checkbox">
                                                    <input ref={elem => elem && (elem.indeterminate = false)}
                                                        tabIndex={3}
                                                        id="myCheck"
                                                        checked={this.state.rememberLogin}
                                                        onChange={this.rememberMyEmail}
                                                        type="checkbox" />
                                                    <span className="checkbox__control"></span>
                                                    <span>
                                                        Remember my email
                                                        <p className="small">(Do not check if using a public computer)</p>
                                                    </span>
                                                </div>
                                            </label>
                                        </div>
                                    </div>
                                    <div className="box__footer">
                                        <button className={`button  button__brand--loginbtn ${this.props.cssFlags[BaseSnapComponent.hasErrors] || 'button__brand'}`}
                                         tabIndex={4} 
                                         type="submit" 
                                         onClick={this.submitClicked}>
                                         <span>Login</span></button>
                                        <p>Forgot <Link to={Endpoints.site.root + Endpoints.site.forgotPassword.replace(/%type%/, this.props.userTypeName)}>Password</Link>?</p>
                                        {(this.props.userType == userTypes.patient) && <p>Need an Account? <Link to={Endpoints.site.patientSignup}>Register Here</Link></p>}
                                    </div>
                                </form>
                            </div>
                            <section className="cc-public__footer" id="secLoginFooter"><a href="/public/#/userTerms" data-target="#carousel-registration" data-slide-to="0" target="_blank">Terms &amp; Conditions</a></section>
                        </section>
                        {/*<!-- // End Content-->*/}
                    </div>
                    {/*<!-- //End Page Wrap -->*/}
                </div>
            </div>
        );
    }
}

export function mapStateToProps(state, ownProps) {
    let flags = getCssFlags(LoginCmpnt.name, BaseSnapComponent.errClass, state.site.validation.validations);
    let willShakeIt = state.user.loginError || Object.keys(flags).length > 0;
    flags[BaseSnapComponent.shakeClass] = willShakeIt  ? BaseSnapComponent.shakeClass : '';
    flags[BaseSnapComponent.hasErrors] = willShakeIt  ? 'button__red error' : '';

    let userTypeName = ownProps.params.userType || state.user.userTypeName;
    let userType = userTypes[userTypeName] || state.user.userType;
    
    return {
        hospital: state.hospital as Hospital,
        userType: userType,
        userTypeName: userTypeName,
        cssFlags: flags,
        cmpntClassName: LoginCmpnt.name,
        redirectUrl: state.user.redirectUrl,
        language: state.site.misc.vocabulary as any
    }
}

function mapDispatchToProps(dispatch) {
     return {
       sendCredentials : credentials => dispatch(userActions.logIn(credentials)),
       showValidationErrors : (errors, cmpntClassName) => dispatch(siteActions.showValidationErrors(errors, cmpntClassName)),
       goToRoute: action => dispatch(action)
    };
}

export default connect(mapStateToProps, mapDispatchToProps)(LoginCmpnt);
