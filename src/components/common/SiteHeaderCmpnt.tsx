import * as  React from 'react';

import { MenuItem } from '../../models/site/menuItems';

import { BurgerMenuCmpnt } from '../common/BurgerMenuCmpnt';
import { SiteHeaderMenuCmpnt } from '../common/SiteHeaderMenuCmpnt';
import { MenuItemCmpnt } from '../common/MenuItemCmpnt'; 
import { ProfilePictureCmpnt } from '../common/ProfilePictureCmpnt';
import { ProfilePictureMenuCmpnt } from '../common/ProfilePictureMenuCmpnt';

interface ISiteHeaderCmpntProps {
    hospitalName: string;
    profilePicUrl: string;
    toggleMainMenu(): void;
    menuItems?: MenuItemCmpnt[];
    profileMenuItems?: MenuItemCmpnt[];
}

export class SiteHeaderCmpnt extends React.Component<ISiteHeaderCmpntProps, any> {
    constructor(){
        super()

        let isProfileMenuActive: boolean = false;
        this.state = { isProfileMenuActive };

        this.toggleProfileMenu = this.toggleProfileMenu.bind(this);
    }
    

    toggleProfileMenu(){
        this.setState({isProfileMenuActive: !this.state.isProfileMenuActive});
    }

    render() {
        return (
            <header className="header">
                <div className="row">
                    <div className="header__menu">
                        <BurgerMenuCmpnt
                            onNavClick={this.props.toggleMainMenu}
                        />
                    </div>
                    <div className="header__brand">
                        <h1 title={this.props.hospitalName}>{this.props.hospitalName}</h1>
                    </div>

                    <div className="header__nav">
                        <ProfilePictureCmpnt
                             profileImg = {this.props.profilePicUrl}
                             toggleHeaderDD ={this.toggleProfileMenu}
                        />

                        <SiteHeaderMenuCmpnt  
                            menuItems = {this.props.menuItems}
                        />

                        <ProfilePictureMenuCmpnt 
                            menuItems = {this.props.profileMenuItems}
                            isActive = {this.state.isProfileMenuActive}
                        />

                    </div>
                </div>
            </header>
        );
    }
}


// export default SiteHeaderCmpnt;