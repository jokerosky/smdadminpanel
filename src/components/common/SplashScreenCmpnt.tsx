import * as  React from 'react';
import { Link } from 'react-router';
import { Endpoints } from '../../enums/endpoints';


export class SplashScreenCmpnt extends React.Component<any, any> {
    render() {
        return (
            <div style={{display: 'flex', position:'absolute', zIndex:10, background: 'linear-gradient(to left, #ff7f7f , #bfe9ff)',  repeatingConicalGradient:"black, black 5%, #f06 5%, #f06 10%", width: '100%', height: '100%',alignItems:'center', justifyContent:'center'  }}>
                <div>
                     <object style={{ width: '30vh', height: '30vh'}} type="image/svg+xml" data={Endpoints.site.root + "assets/images/rings.svg"} className="loading-img"></object>
                </div>
            </div>
        );
    }
}


