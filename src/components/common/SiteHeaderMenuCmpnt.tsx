import * as  React from 'react';
import * as _ from 'lodash';

import { MenuItemCmpnt } from './MenuItemCmpnt';
import { MenuItem, TopMenuItem } from '../../models/site/menuItems';


interface ISiteHeaderMenuCmpntPprops {
    menuItems:MenuItemCmpnt[]
}

export class SiteHeaderMenuCmpnt extends React.Component<ISiteHeaderMenuCmpntPprops, any> {
    // here we prepare menu items for header

    render() {
        return (
            <nav className="hide-mobile nav nav--header nav--header--admin">
                <ul>
                    {this.props.menuItems}
                </ul>
            </nav>
        );
    }
}