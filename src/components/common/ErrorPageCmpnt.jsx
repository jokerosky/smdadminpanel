import React from 'react';
import {Route, Link, IndexRoute} from 'react-router';
import { connect } from 'react-redux';
import Helmet from 'react-helmet';


class ErrorPageCmpnt extends React.Component {
    reloadApp(){
        window.location = "/";
    }

    render() {
        return (
            <div>
                <Helmet
                    title = "Error"
                />
                <h1>Error: {this.props.error}</h1>
                <button onClick={ this.reloadApp }>Go to root</button>
            </div>
        );
    }
}

function mapStateToProps(state, ownProps){
     return {
        error: state.site.errors.lastError
    };
}

function mapDispatchToProps(){
    return {

    };
}

export default connect(mapStateToProps, mapDispatchToProps)(ErrorPageCmpnt);
