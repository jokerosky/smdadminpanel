import * as React from 'react';
import { connect } from 'react-redux';
import * as _ from 'lodash';

import * as siteActions from '../../actions/siteActions';
import { EventType } from '../../enums/eventType';

export class SiteNotificationsCmpnt extends React.Component<any, any> {
    constructor(props, context) {
        super(props, context);
        this.state = {
        };

        this.handleClick = this.handleClick.bind(this);
        this.removeNotification = this.removeNotification.bind(this);
    }

    getClassName(type:EventType){
        let result = _.findKey(EventType, x => x == type);
        return result ? result : ''; 
    }

    handleClick(id:number, e:any){
        this.removeNotification(id);
    }

    removeNotification(id:number){
            this.props.removeNotification([id]);
    }

    render() {
        let lis = [];
        for(let i = 0; i < this.props.notifications.length; i++ ){
            lis.push(
            <li onClick={this.handleClick.bind(this, this.props.notifications[i].notificationId)}
                key={this.props.notifications[i].notificationId}
                className={`${this.getClassName(this.props.notifications[i].type)} smd-no-margin alert  alert-danger`}>
                {this.props.notifications[i].message}
            </li>);
        }

        if(lis.length > 0)
            return (
                <ul>
                    {lis}      
                </ul>
            );
        else return null;
    }

}

function mapStateToProps(state, ownProps) {
    return {
        notifications: state.site.notifications.notifications
    };
}

function mapDispatchToProps(dispatch) {
    return {
        removeNotification: (ids:number[]) => {dispatch(siteActions.removeNotifications(ids))} 
    };
}

export default connect(mapStateToProps, mapDispatchToProps)(SiteNotificationsCmpnt);
