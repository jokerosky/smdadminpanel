import React from 'react';
import { Link } from 'react-router';

class ToggleMenuButton extends React.Component {
    render() {
        return (
            <div className="header__menu">
                <a href="#" className="main-nav__toggle js-toggle-main-nav" data-bind="click:onNavClick">
                    <ins></ins>
                    <ins></ins>
                    <ins></ins>
                </a>
            </div>
        );
    }
}

export default ToggleMenuButton;
