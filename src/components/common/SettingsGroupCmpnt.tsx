import * as React from 'react';

import { ISetting } from '../../models/site/setting';
import { IMappedSetting } from '../../models/site/settingsMapping';

import { EditorType } from '../../enums/editorTypes';
import { EditorCreator } from '../../utilities/editorCreator';

import { ToggleButton } from '../common/inputs/ToggleButtonCmpnt';

export interface ISettingsGroupProps {
    settings?: IMappedSetting[];
    isCollapsed?: boolean;
    labelBackgroundClass?: string;
    title:string;

    onToggle?: (expanded: boolean) => void;
    onSettingChanged? : (newVal:any, setting: IMappedSetting, section:IMappedSetting[])=>void;
}

export class SettingsGroup extends React.Component<ISettingsGroupProps, any> {
    constructor(props) {
        super(props);

        let isCollapsed = this.props.isCollapsed;
        isCollapsed = typeof isCollapsed != undefined ? isCollapsed : true;
        this.state = {
            isCollapsed
        };

        this.togglePanel = this.togglePanel.bind(this);
        this.settingChaned = this.settingChaned.bind(this);
        this.createEditor = this.createEditor.bind(this);
    }

    componentWillReceiveProps(props){
        this.setState({ isCollapsed: props.isCollapsed });
    }

    settingChaned(val, setting:IMappedSetting, section:IMappedSetting[]){
        if(this.props.onSettingChanged){
            this.props.onSettingChanged(val, setting, section);
        }
        //e.value = newVal; 
        //not sure that is good idea to change value here, because settings should come from global state
    }
    
    createEditor(item: IMappedSetting, settingChaned?:(any:any)=>void) {
        let edit;
        
        if(!item.isActionRebinded){
            let emptyFunc =  (newVal:any, item: IMappedSetting, section:IMappedSetting[])=>{
                //setting.value = newVal;
                //lets change value of the setting inside mappedSetting action
            };
            let itemAction = item.action || emptyFunc;

            item.action = (val:any)=>{
                itemAction(val, item, this.props.settings);
                this.settingChaned(val, item, this.props.settings);
            }
            item.isActionRebinded = true;
        }
        
        edit = EditorCreator.getEditor(item);

        return edit;
    }

    togglePanel() {
        let isCollapsed = !this.state.isCollapsed;
        this.setState({ isCollapsed });
        if(typeof this.props.onToggle  == 'function'){
            this.props.onToggle(isCollapsed);
        }
    }

    renderWithChildren(setting: IMappedSetting){
        let children = [];
        let element = null;

        if(setting.children){
            
            for(let i in setting.children){
                if(setting.children[i].isVisible !== false)
                    children.push(this.renderWithChildren(setting.children[i]));
            }
            
        }
        element =
            <div key={setting.key} className='smd-full-width smd-no-h-padding col-12'>
                <div className='smd-no-h-padding d-flex'>
                    <div className={`smd-setting-label col-4 ${this.props.labelBackgroundClass || ''}`}>
                        {setting.label}
                    </div>
                    <div className='col'>
                        {this.createEditor(setting)}
                    </div>
                </div >
                {(children.length > 0) &&
                    <div className='smd-v-padding smd-setting-children col row d-flex'>
                        {children}
                    </div>}
            </div>;

        return element;
    }
 
    renderSettings(): any {
        let settingsArr = [];
        for (let index in this.props.settings) {
            let setting = this.props.settings[index];

            let elementWithChildren = this.renderWithChildren(setting);

            let item =
                <li key={setting.key}
                    className='list-group-item smd-transitions smd-no-h-padding'>
                    {elementWithChildren}
                </li>;
            settingsArr.push(item);
        }

        return settingsArr;
    }

    render() {
        return (
            <div className="card">
                <div className={`card-header smd-transitions  border-0 ${this.state.isCollapsed ?  'bg-light' :  'bg-primary' }`}
                    onClick={this.togglePanel}>
                    { this.props.title }
                </div>
                <div className={`card-body smd-no-padding smd-transitions`}>
                    <ul className={`list-group smd-transitions ${this.state.isCollapsed ?  'smd-collapsed' : 'smd-expanded' }`}>
                        { this.renderSettings() }
                    </ul>
                </div>
            </div>
        );
    }
}