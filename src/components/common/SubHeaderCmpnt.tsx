import * as React from 'react';


export interface ISubHeaderCmpntProps {
    subHeaderTitle: string;
    subHeaderModuleTitle: string;
    isHideSubHeader?: boolean;
    isSubHeaderModuleTitleVisible?: boolean;
    menuToggleItems: any;

    isMenuToggleVisible?: boolean;
    isAddButtonVisible?: boolean;
    isPanelToggleVisible?: boolean;
    addButtonAction?: () => void;
    panelToggleAction?: () => void;
}

export class SubHeaderCmpnt extends React.Component<ISubHeaderCmpntProps, any> {

    render() {
        return (
            !this.props.isHideSubHeader && <div className="row" >
                <div className="header-bar">
                    <ul className="page-toggle hide-mobile">
                        <li><h1>{this.props.subHeaderTitle || ''}</h1></li>
                        {!this.props.isHideSubHeader && <li className="page-toggle__module"><h1>{this.props.subHeaderModuleTitle || ''}</h1></li>}
                    </ul>

                    <ul className="menu-toggle">
                        { this.props.isMenuToggleVisible && <li className="menu-toggle__navigation">
                            { this.props.isAddButtonVisible && <a className="js-toggle-menu"
                                href="javascript:void(0)"
                                data-bind="events: { click: addButtonAction}, visible: isAddButtonVisible"
                                onClick={this.props.addButtonAction}>
                                <span className="icon icon_plus"></span>
                            </a> }
                            { this.props.isPanelToggleVisible && <a className="js-toggle-menu"
                                href="javascript:void(0)"
                                data-bind="events: { click: panelToggleAction}, visible: isPanelToggleVisible"
                                onClick={this.props.panelToggleAction}
                            >}
                                <span className="icon icon_list"></span>
                            </a>}
                        </li> }

                        {this.props.menuToggleItems}

                        {/*<li data-bind="className: {active: isLinkOneActive}, visible: islinkOneVisible">
                            <a href="#" data-bind="attr: { href: linkOnePath}, visible:linkOnePath" ><span data-bind="text: linkOneTitle">Link1</span></a>
                        </li>

                        <li data-bind="className: {is-active: isLinkTwoActive}">
                            <a href="#" data-bind="attr: { href: linkTwoPath}, visible:linkTwoPath"><span data-bind="text: linkTwoTitle">Link2</span></a>
                        </li>

                        <li data-bind="className: {is-active: isLinkThreeActive}">
                            <a href="#" data-bind="attr: { href: linkThreePath}, visible:linkThreePath"><span data-bind="text: linkThreeTitle">Link3</span></a>
                        </li>*/}
                    </ul>
                </div>
            </div>
        );
    }

}