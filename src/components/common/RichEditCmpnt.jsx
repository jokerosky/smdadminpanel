import * as React from 'react';
import ReactQuill from 'react-quill';
import {encodeHTML, decodeHTML} from '../../enums/encodeHTML';

export default class RichEditCmpnt extends React.Component{
    constructor(props){
        super(props);

        this.encodeHTML = this.encodeHTML.bind(this);
        this.decodeHTML = this.decodeHTML.bind(this);
    }
    colors(){
        let colors = [
            'black', 'red'
        ];
        return colors;
    }

    decodeHTML(str){
        let reg;
        for(let index in decodeHTML){
            if(index !== 'amp'){
                reg = new RegExp(decodeHTML[index], 'g');
                str = str.replace(reg, encodeHTML[index]);
            }
        }
        return str;
    }

    encodeHTML(str){
        let reg;
        for(let index in encodeHTML){
            reg = new RegExp(encodeHTML[index], 'g');
            str = str.replace(reg, decodeHTML[index]);
        }
        return str;
    }


    modules(){
        let modules = {
            toolbar: [
                [{'font':[]},{ 'header': [1, 2, 3, 4, 5, 6, false] }, {'size':['small', false, 'large', 'huge']}],
                [{'color': []}, {'background': []}],
                ['bold', 'italic', 'underline','strike', 'blockquote'],
                [{'script':'sub'}, {'script':'super'}],
                ['code', {'align':[]}, { 'direction': 'rtl' }],
                [{'list': 'ordered'}, {'list': 'bullet'}, {'indent': '-1'}, {'indent': '+1'}],
                ['link', 'image', 'video', 'formula'],
                ['clean']
            ],
        };
        return modules;
    }

  // the function returns an object which specify which corresponding button will be enabled and pressing its click actions
  /*  formats(){
        let formats= [
            'header',
            'bold', 'italic', 'underline', 'strike', 'blockquote',
            'list', 'bullet', 'indent',
            'link', 'image'
        ];
        return formats;
    }*/
    
    render(){
        return(
            <div 
                name='RichEdit'
                className='smd-display-flex'
            >
                    <ReactQuill 
                        theme='snow'
                        className='smd-flex-direction smd-z-index-1'
                        modules={this.modules()}
                        value={this.encodeHTML(this.props.text)}
                        onChange={(e)=>{
                            this.props.textEditChange(this.decodeHTML(e));
                        }}
                    />
            </div>
        )
    }
}