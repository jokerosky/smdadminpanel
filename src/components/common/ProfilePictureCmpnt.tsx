import * as  React from 'react';

export interface  IProfilePictureCmpntProps {
    isHideAvatarBtn?: boolean;
    profileImg: string; 
    toggleHeaderDD: ()=>void;
}


export class ProfilePictureCmpnt extends React.Component<IProfilePictureCmpntProps, any> {
        
        render(){
            return (
                <a
                  href="#" 
                  className = {`header__avatar  ${ this.props.isHideAvatarBtn ? "hide" : "" }`}
                  onClick = {this.props.toggleHeaderDD}
                  >
                    <img  src = {this.props.profileImg} />
                </a>
            );
        }
        
}