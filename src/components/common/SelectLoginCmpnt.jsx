import React from 'react';
import { Link } from 'react-router';
import { push } from 'react-router-redux';


import { connect } from 'react-redux';
import _ from 'lodash';

import { Endpoints, toAbsolutePath } from '../../enums/endpoints';
import * as userActions from '../../actions/userActions';
import * as userTypes from '../../enums/userTypes';


class SelectLoginCmpnt extends React.Component {
    constructor(props, context){
        super(props, context);
        this.state = {

        };
    }

    selectType(type){
        this.props.selectLogin(type);
        let loginPostfix = _.findKey(userTypes, x => x == type)
        this.props.goToRoute(push(toAbsolutePath(Endpoints.site.login.replace('%type%', loginPostfix))));
    }

    render() {
        return (
           <div className='cc-public'>
                <div id="divMainWrap" className="mainWrap">
                    <div id="page-wrap">

                        {/*<!--Header-->*/}
                        <header className="header cc-public__header" id="divHeader"><div className="centerWrap">
                            <h1 data-bind="text: brandName, attr: {title: brandName }" className="title" title={this.props.hospital.brandName}>{this.props.hospital.brandName}</h1>
                        </div></header>
                        {/*<!-- // End Header-->*/}
                        {/*<!--Content-->*/}
                        <section className="col5-sm col12-md col16-lg content cc-public__content cc-public__content--login active" id="divLogin">

                            <div className="col12-sm col12-md-8 col16-lg-6 box">
                               <table style={{width : '100%', height:'100px', textAlign:'center'}}>
                                   <tbody>
                                            <tr>
                                                {/* <td><button className='btn button__brand ' onClick={ (e) => { this.selectType(userTypes.patient)}} >Patient</button></td>
                                                <td><button className='btn button__brand ' onClick={ (e) => { this.selectType(userTypes.provider)}}>Provider</button></td>
                                                <td><button className='btn button__brand ' onClick={ (e) => { this.selectType(userTypes.admin)}}>Admin</button></td> */}
                                                <td><button className='btn button__brand ' onClick={ (e) => { this.selectType(userTypes.snapMdAdmin)}}>SnapMdAdmin</button></td>
                                            </tr>
                                            </tbody>
                                        </table>
                            </div>

                            <section className="cc-public__footer" id="secLoginFooter"><a href="/public/#/userTerms" data-target="#carousel-registration" data-slide-to="0" target="_blank">Terms &amp; Conditions</a></section>
                        </section>
                        {/*<!-- // End Content-->*/}

                    </div>
                    {/*<!-- //End Page Wrap -->*/}

                </div>
            </div>
        );
    }
}

function mapStateToProps(state, ownProps){
    return {
        userType:state.user.userType,
        hospital: state.hospital
    };
}

function mapDispatchToProps(dispatch){
    return {
        selectLogin: userType => dispatch(userActions.selectLogin(userType)),
        goToRoute: action => dispatch(action)
    };
}

export default connect(mapStateToProps,mapDispatchToProps)(SelectLoginCmpnt);
