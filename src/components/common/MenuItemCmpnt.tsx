import * as  React from 'react';
import { MenuItem, TopMenuItem } from '../../models/site/menuItems';
import { Link } from 'react-router';

export class MenuItemCmpnt extends React.Component<TopMenuItem, any> {

    render() {
        let action = () => { };
        return (
            <li key={this.props.key} onClick={this.props.action || action}
                className={this.props.activeClass || ''}
                // ↓ doing this because of our magic css, can`t just use {this.props.inVisible &&  <li>} for element, because of media queries for mobile
                style={{ display: this.props.inVisible ? "none" : "" }}
            >
                {this.props.additionalElements}
                <a href="#">
                    <span className={`icon ${this.props.icoClass}`}></span>
                    <span className="text">{this.props.text}</span>
                    {this.props.isFlagVisible && <span className={`header__flag is-visible`}>{this.props.flagText || ""}</span>}

                </a>
            </li>
        )
    }

    renderBootstrapListElement() {
        let action = () => { };
        return (
            <li
                className={`nav-item ${this.props.activeClass}`}
                key={this.props.key}
                onClick={this.props.action || action}
            >
                <Link
                    to={this.props.link || ''}
                    activeClassName={`nav-link ${this.props.linkClass || ''}`}
                    className={`nav-link ${this.props.linkClass || ''}`}
                >
                    {this.props.icoClass && <i className={this.props.icoClass}></i>}
                    <span>{this.props.text}</span>
                </Link>
            </li>
        );
    }



    renderLiElement(itemsMenu: any) {
        let action = () => { };
        let items: Array<any> = [];
        for (let index in itemsMenu) {
            let item =
                <Link
                    onClick={itemsMenu[index].action || action}
                    key={itemsMenu[index].key}
                    to={itemsMenu[index].link || ''}
                    activeClassName={`dropdown-item ${itemsMenu[index].linkClass || ''} ${itemsMenu[index].activeClass}`}
                    className={`dropdown-item ${itemsMenu[index].linkClass || ''} ${itemsMenu[index].activeClass === 'disabled' ? itemsMenu[index].activeClass + ' smd-cursor-not-allowed' : ''}`}
                >
                    {itemsMenu[index].icoClass && <i className={itemsMenu[index].icoClass}></i>}
                    <span className={``}>{itemsMenu[index].text}</span>
                </Link>


            items.push(item);
        }

        return items;
    }

    renderDropDownMenu() {
        let action = () => { };
        return (
            <li key={this.props.dropDown.key}
                onClick={this.props.action || action}
                className='nav-item dropdown'>

                <div className="nav-link dropdown-toggle smd-cursor-pointer active" id="navbarDropdownMenuLink" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                    {this.props.dropDown.name}
                </div>
                <div className='dropdown-menu' aria-labelledby="navbarDropdownMenuLink">
                    {this.renderLiElement(this.props.dropDown.items)}
                </div>

            </li>
        );
    }
}
