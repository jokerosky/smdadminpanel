import * as React from 'react';
import Files from 'react-files';

import * as Vocabulary from '../../localization/vocabulary';

const noImagePath = '/assets/images/no-image.png';

interface ILoadImageCmpntProps {
    title: string;
    imagePath: string;
    language: any;

    pathForFileCallback: (file: any, pathImage: string) => void;
}

export class LoadImageCmpnt extends React.Component<ILoadImageCmpntProps, any> {
    constructor(props) {
        super(props);

        this.state = {
            path: props.imagePath || ''
        };

        this.onFilesChange = this.onFilesChange.bind(this);
        this.onFilesError = this.onFilesError.bind(this);
        this.filesRemove = this.filesRemove.bind(this);
        this.filesLoad = this.filesLoad.bind(this);
    }

    componentWillReceiveProps(nextProps) {
        this.setState({
            path: nextProps.imagePath
        });
    }

    onFilesChange(files) {
        let path;
        let fileImage: any = new FormData();
        if (files.length > 0) {
            path = files[files.length - 1].preview.url;
            fileImage.append('', files[files.length - 1]);
        } else {
            path = '';
            let data = new Blob([path], { type: 'image/png' });
            let arrayOfBlob = new Array<Blob>();
            arrayOfBlob.push(data);
            let file = new File(arrayOfBlob, 'new.png', { type: 'image/png' });
            fileImage.append('', file);
        }

        this.setState({
            path: path
        });
        this.props.pathForFileCallback(fileImage, path);
    }

    onFilesError(error, file) {
        console.error('error code ' + error.code + ': ' + error.message)
    }

    filesRemove(file) {
        let remove: any = this.refs.files;
        remove.removeFiles()
    }

    filesLoad() {
        let fileChooser: any = this.refs.files;
        fileChooser.openFileChooser();
    }

    render() {
        return (
            <div className="files">
                <div>
                    <label>{this.props.title}</label>
                </div>
                <div className='col'>
                    <div className='row'>
                        <button
                            id={'saveImage' + this.props.title.replace(/ /g, '')}
                            className='btn smd-cursor-pointer'
                            onClick={this.filesLoad}
                        >
                            {this.props.language[Vocabulary.CLICK_TO_UPLOAD]}
                        </button>
                        <button
                            className='btn smd-clear-btn-image smd-cursor-pointer'
                            onClick={this.filesRemove}
                        >
                            {this.props.language[Vocabulary.SNAPMDADMIN_SETTINGSV2_CLEAR]}
                        </button>
                    </div>
                </div>
                <Files
                    ref={'files'}
                    className='files-dropzone'
                    onChange={this.onFilesChange}
                    onError={this.onFilesError}
                    accepts={['image/png', 'image/bmp', 'image/jpeg', 'image/jpg', 'image/gif']}
                    maxFileSize={10000000}
                    minFileSize={0}
                >
                    <div className='smd-div-image smd-cursor-pointer'>
                        <img ref={'image'} className='smd-image img-thumbnail' src={this.state.path || noImagePath}
                            alt={this.props.language[Vocabulary.DROP_FILES_HERE] + ' ' + this.props.language[Vocabulary.OR] + ' ' + this.props.language[Vocabulary.CLICK_TO_UPLOAD]}
                            onChange={(ch) => { debugger }}
                            onBlur={() => { debugger }}
                            onClick={() => { }} />
                    </div>
                </Files>
            </div>
        );
    }
}