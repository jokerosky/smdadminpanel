import * as React from 'react';

export interface IToggleButtonProps {
    name?: string;
    value?: string;
    isChecked?: boolean;
    onChange?: (any)=>void;
}

export class ToggleButton extends React.Component<IToggleButtonProps, any> {
    constructor(props) {
        super(props);

        let isOn = this.props.isChecked == true;

        this.state = {
            isOn
        };
        this.changeState = this.changeState.bind(this);
    }

    componentWillReceiveProps(nextProps:IToggleButtonProps){
        let isOn = nextProps.isChecked == true;
        this.setState({ isOn });
    }

    changeState() {
        let flag = !this.state.isOn;
        this.setState(
            { isOn: flag }
        );

        if( typeof this.props.onChange == "function" ){
            this.props.onChange(flag);
        }
    }

    render() {
        return (
            <div className={`btn btn-sm smd-toggle ${this.state.isOn ? 'btn-success' : 'btn-secondary'}`} id={this.props.name || ''}>
                <div className={`smd-toggle-item ${this.state.isOn ? '' : 'smd-off'}`}>
                    <span>On</span>
                </div>
                <button
                    className='smd-toggle-inner btn btn-sm btn-light'
                    onClick={this.changeState}>
                    &nbsp;
                    <input type="checkbox"
                            className='smd-toggle-checkbox'
                            onClick={(e:any)=>{
                                        e.target.parentElement.focus();
                                    }
                                }
                            defaultChecked = {this.state.isOn}
                            name={this.props.name || ''}
                            value={this.props.value || ''}
                            tabIndex={-1} />
                </button>
                <div className={`smd-toggle-item ${this.state.isOn ? 'smd-off' : ''}`} >
                    <span>Off</span>
                </div>
            </div>
        );
    }
}

export default ToggleButton;
