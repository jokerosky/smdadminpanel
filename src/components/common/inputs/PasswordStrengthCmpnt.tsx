import * as React from 'react';

//import * as googleMapsClient from  '@google/maps'; //seems that does not contain html components
// from google maps script
declare var google: any;

export class PasswordStrengthCmpnt extends React.Component<any, any> {
    constructor(props, context) {
        super(props, context);

        //functions binding
        this.onChange = this.onChange.bind(this);
        this.inputChanged = this.inputChanged.bind(this);
    }

    type:string = "text";

    inputChanged = function (e) {
        this.onChange(e.target.value);
    };

    onChange = function (val) {
        this.props.onChange(val);
    }

    render() {
        return (
            <input
                id={this.props.id}
                type={this.type}
                placeholder={this.props.placeholder}
                className={this.props.className}
                onChange={this.inputChanged}
            />
        );
    }
}
