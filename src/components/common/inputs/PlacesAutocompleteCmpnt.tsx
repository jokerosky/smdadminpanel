import * as React from 'react';

//import * as googleMapsClient from  '@google/maps'; //seems that does not contain html components
// from google maps script
declare var google: any;

export class PlacesAutocompleteCmpnt extends React.Component<any, any> {
    constructor(props, context) {
        super(props, context);
        //functions binding
        this.onChange = this.onChange.bind(this);
        this.inputChanged = this.inputChanged.bind(this);
    }

    autocomplete:any;

    inputChanged = function (e) {
        this.onChange(e.target.value);
    };

    onChange = function (val) {
        this.props.onChange(val);
    }

    render() {
        return (
            <input
                id={this.props.id}
                type="text"
                ref={(elm) => {
                    //let client = googleMapsClient.createClient({});
                    if (elm) {
                        if(! this.autocomplete)
                        {
                            this.autocomplete = new google.maps.places.Autocomplete(elm);
                            this.autocomplete.addListener('place_changed', () => {
                            let place = this.autocomplete.getPlace();
                            this.onChange(place.formatted_address);
                        });
                        }
                        
                        
                        
                    }
                }}
                placeholder={this.props.placeholder}
                className={this.props.className}
                defaultValue={this.props.defaultValue}
                onChange={this.inputChanged}
                
            />
        );
    }
}
