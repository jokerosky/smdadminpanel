import * as React from 'react';

export interface IModalWindow {
    title?: string;
    show?: string;
    display?: string;
    state?: any;
    visible?: boolean;
    dialogIsLarge?: boolean;
    btnSuccessShow?: boolean;
    id?:string;

    okBtnText?: string;
    cancaleBtnText?: string;

    success?: () => void;
    cancel?: () => void;
}

export class ModalWindow extends React.Component<IModalWindow, any>{
    constructor(props) {
        super(props);
        this.state = {
            btnUpdate: this.props.okBtnText || 'Update',
            btnClose: this.props.cancaleBtnText || 'Close'
        };
    }

    showModal = {
        display: 'block'
    }

    render() {
        if (this.props.visible) {
            return (
                <div 
                    id={this.props.id} 
                    className=''>
                    <div
                        className={`modal fade show`}
                        style={this.showModal}
                        role='dialog'
                        tabIndex={-1}>
                        <div className={`modal-dialog ${this.props.dialogIsLarge ? 'modal-lg' : ''}`} role='document'>
                            <div className='modal-content'>
                                <div className='modal-header'>
                                    <h4 className='modal-title'>{this.props.title}</h4>
                                </div>
                                <div className='modal-body'>
                                    {this.props.children || ''}
                                </div>
                                <div className='modal-footer'>
                                    { this.props.btnSuccessShow !== false ?
                                        <button
                                            name={'Success'}
                                            type='button'
                                            className='btn smd-cursor-pointer'
                                            onClick={() => {
                                                this.props.success();
                                            }}
                                        >{this.state.btnUpdate}</button>
                                        : null
                                    }
                                    <button
                                        name={'Cancel'}
                                        type='button'
                                        className='btn smd-cursor-pointer'
                                        onClick={() => {
                                            this.props.cancel();
                                        }}
                                    >{this.state.btnClose}</button>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div className={`modal-backdrop fade show`}
                        style={this.showModal}
                    >
                    </div>
                </div>
            )
        } else {
            return (<div>{''}</div>);
        }
    }
}