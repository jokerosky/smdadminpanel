import * as  React from 'react';

interface IBurgerMenuCmpntPprops {
    onNavClick: () => void;
}

export class BurgerMenuCmpnt extends React.Component<IBurgerMenuCmpntPprops, any> {
    render() {
        return (
            <a className="main-nav__toggle js-toggle-main-nav pull-left"
                style={{ cursor: 'pointer' }}
                onClick={this.props.onNavClick} >
                <ins></ins>
                <ins></ins>
                <ins></ins>
            </a>
        );
    }
}
