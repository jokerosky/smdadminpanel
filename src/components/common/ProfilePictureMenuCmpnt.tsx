import * as  React from 'react';

import { MenuItemCmpnt } from './MenuItemCmpnt';


export interface  IProfilePictureMenuCmpntProps {
    menuItems:MenuItemCmpnt[];
    isActive: boolean;
}


export class ProfilePictureMenuCmpnt extends React.Component<IProfilePictureMenuCmpntProps, any> {
        
        render(){
            return (
                <ul className={`header__dd-nav ${this.props.isActive ? 'is-active' : ''}`}>
                    { this.props.menuItems }
                </ul>
            );
        }
        
}