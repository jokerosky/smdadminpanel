import React from 'react';

class PageNotFoundCmpnt extends React.Component {
    reloadApp(){
        window.location = "/";
    }

    render() {
        return (
            <div>
                <h1>Page not Found</h1>
                <button onClick={ this.reloadApp }>Go to root</button>
            </div>
        );
    }
}

export default PageNotFoundCmpnt;
