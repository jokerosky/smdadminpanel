import * as React from 'react';
import { MenuItemCmpnt } from './MenuItemCmpnt';
import { MainNavUserInfoCmpnt } from './MainNavUserInfoCmpnt';

import { UserProfile } from '../../models/userProfile';
import { UserStaffProfile } from '../../models/userStaffProfile';

interface IMainNavCmpntProps {
    userProfile: UserProfile;
    goToAccount: () => void;

    menuItems:MenuItemCmpnt[];
}

export class MainNavCmpnt extends React.Component<IMainNavCmpntProps, any>
{
    constructor(){
        super()

        this.getUserInfo = this.getUserInfo.bind(this);
    }

    getUserInfo(profile:UserProfile):string  {
        if(Object.keys(this.props.userProfile).length < 3){

        }

        let userInfo = profile instanceof UserStaffProfile ? 
            profile.medicalSpeciality :            
            profile.gender + ' ' + profile.dob + ' ' + profile.mobilePhone;

        return userInfo;
    }

    render() {
        let userInfo = this.getUserInfo(this.props.userProfile); 

        return (
            <nav className="main-nav">

                <MainNavUserInfoCmpnt 
                    firstName= { this.props.userProfile.firstName }
                    lastName = { this.props.userProfile.lastName }
                    userInfo = { userInfo }
                    profileImgPath = {this.props.userProfile.profileImagePath}

                    goToAccount = {this.props.goToAccount}

                />

                <ul className="main-nav__list">
                    {this.props.menuItems}

                    {/*<li><a href="#" data-bind="click:goToHome"><span class="icon_dashboard"></span> Patient Queue</a></li>

        <li><a href="#" data-bind="click:goToAccount"><span class="icon_user"></span> My Account</a></li>

        <li><a href="#" data-bind="click:goToPatientRecords"><span class="icon_users2"></span> Patient Records</a></li>

        <li><a href="#" data-bind="click:goToScheduler, visible:canScheduleConsultation"><span class="icon_calendar"></span> Scheduler</a></li>

        <li><a href="#" data-bind="click:newAppointment, visible: canScheduleConsultation"><span class="icon_new_calendar_item"></span> New Appointment</a></li>

        <li><a href="#" data-bind="click:recordConsultation, visible:recordConsultationEnabled"><span class="icon_text"></span> Document Encounter</a></li>

        <li><a href="#" data-bind="click:goToOpenConsultation, visible:openConsultationEnabled"><span class="icon_video-camera"></span> Open Consultation</a></li>

        <li><a href="#" data-bind="click:goToConsultHistory"><span class="icon_history"></span> Encounter History</a></li>

        <li><a href="#" data-bind="click:goToFiles, visible:isMyFilesEnabled"><span class="icon_archive"></span> Files</a></li>

        <li><a href="#" data-bind="click:goToePrescrib, visible:ePrescriptionEnabled"><span class="icon_rx"></span> E-Prescribing</a></li>

        <li><a href="#" data-bind="click:goToHelpCenter"><span class="icon_help-with-circle"></span> Help Center</a></li>

        <li><a href="#" data-bind="click:goToTestConnect"><span class="icon_gauge"></span> Test Connectivity</a></li>

        <li><a href="#" data-bind="click:signOut"><span class="icon_log-out"></span>Log Out</a></li>*/}
                </ul>

                <div className="main-nav__bottom-aligner main-nav__bottom-aligner--clinician"></div>

                <a data-bind="click:terms" href="#" className="main-nav__toc">Terms &amp; Conditions</a>
            </nav>
        );
    }
}


