import * as React from 'react';
import { connect } from 'react-redux';
import { goBack } from 'react-router-redux';

import * as userActions from '../../actions/userActions';
import * as userTypes from '../../enums/userTypes';
import { Hospital } from '../../models/hospital';
import { IStorageService, LocalStorageService } from '../../services/storageService';
import { LocalizationService } from '../../localization/localizationService';
import * as Vocabulary from '../../localization/vocabulary';
import { Validator, ValidationSet, getCssFlags } from '../../utilities/validators';
import LoginRequestDTO from '../../models/DTO/loginRequestDTO';
import { BaseSnapComponent } from '../BaseSnapComponent';


export class ResetPasswordCmpnt extends BaseSnapComponent {
    constructor(props, context) {
        super(props, context);

        this.prepareValidationRules();

        //functions binding
        this.resetPassword = this.resetPassword.bind(this);
        this.submitClicked = this.submitClicked.bind(this);
    }

    emailId = "txtEmail";

    prepareValidationRules() {
        let emailValidation = {
            elementId: this.emailId,
            rules: [
                // if passed value is wrong we return error message, other case we return null or empty string
                (val) => {
                    if (!Validator.isEmail(val)) {
                        return this.props.language[Vocabulary.VALIDATION_INVALID_EMIAL];
                    }
                },
            ]
        } as ValidationSet;

        this.validationSets[this.emailId] = emailValidation;
    }

    resetPassword(e) {
        e.preventDefault();
        if (this.validate()) {
            let credentials = {
                hospitalId: this.props.hospital.hospitalId,
                email: this.validationSets[this.emailId].element.value,
                userTypeId: this.props.userType,
                redirectUrl: this.props.redirectUrl
            } as LoginRequestDTO;
            this.props.resetPassword(credentials);
        }
        return false;
    }
    
    static childContextTypes = {
    };

    render() {
        return (
            <div className='cc-public'>
                <div id="divMainWrap" className="mainWrap">
                    <div id="page-wrap">
                        {/*<!--Header-->*/}
                        <header className="header cc-public__header" id="divHeader"><div className="centerWrap">
                            <h1 className="title" title={this.props.hospital.brandName}> {this.props.hospital.brandName} </h1>
                        </div></header>
                        {/*<!-- // End Header-->*/}
                        {/*<!--Content-->*/}
                        {/*LocalizationService.getString(Vocabulary.COMPONENTS_RESET_PASSWORD_FORGOT_PASSWORD)*/}
                        <form className="col5-sm col12-md col16-lg content cc-public__content cc-public__content--login active" id="divLogin" onSubmit={this.resetPassword}>

                            <div className={`col12-sm col12-md-8 col16-lg-6 box ${this.props.cssFlags[BaseSnapComponent.shakeClass] || ''} `} >
                                <div className="box__header">
                                    <h2>Forgot Password</h2>
                                </div>
                                <div className="box__content" data-bind="css:{box__content--signupconfirm:isResetSuccessfull}">
                                    <div className="inputs inputs__icon" data-bind="invisible: isResetSuccessfull, attr:{class:errorClass}">
                                        <span className="icon_mail"></span>
                                        { !this.props.passwordResetSuccess && <input id={this.emailId}
                                            type="text"
                                            ref={(input)=>{this.validationSets[this.emailId].element = input;}}
                                            className={`k-input preventSpaces ${this.props.cssFlags[this.emailId] || ''}`}
                                            //disabled = {}
                                            onChange={this.cleanError}
                                            onBlur={this.validateInput}
                                            required
                                            data-bind="value: email, disabled: isInProgress, events: { keypress: onFocus }"
                                            placeholder="Enter Your Email Address" />}
                                    </div>
                                    { this.props.passwordResetSuccess && <div> 
                                        An email has been sent to you.
                                        <br />
                                        Please check your email for the link to reset your password.
                                    </div>}
                                </div>
                                <div className="box__footer">
                                    {!this.props.passwordResetSuccess && <button 
                                        className={`button button__brand--verify ${this.props.cssFlags[BaseSnapComponent.hasErrors] || 'button__brand'}`}
                                        tabIndex={4}
                                        data-bind="click: submitClicked, invisible: isResetSuccessfull"
                                        onClick={this.submitClicked}>
                                        <span>Email me a link</span>
                                    </button>}
                                    <div id="divNeedAccount" style={{ "display": "none" }}>
                                        <p>Need an Account? <a href="/public#/patientSignup">Register Here</a></p>
                                    </div>
                                    {(this.props.userType == userTypes.patient) && <p>Need an Account? <a className='registration' href="/public#/patientSignup">Register Here</a></p>}
                                    <p>Go <a onClick={this.props.goBack} className="back" href="#">Back</a></p>
                                </div>
                            </div>
                            <section className="cc-public__footer" id="secLoginFooter"><a href="/public/#/userTerms" data-target="#carousel-registration" data-slide-to="0" target="_blank">Terms &amp; Conditions</a></section>
                        </form>
                        {/*<!-- // End Content-->*/}

                    </div>
                    {/*<!-- //End Page Wrap -->*/}
                </div>
            </div>
        );
    }
}

function mapStateToProps(state, ownProps) {
   let flags = getCssFlags(ResetPasswordCmpnt.name, BaseSnapComponent.errClass, state.site.validation.validations);
    let willShakeIt = state.user.loginError || Object.keys(flags).length > 0;
    flags[BaseSnapComponent.shakeClass] = willShakeIt  ? BaseSnapComponent.shakeClass : '';
    flags[BaseSnapComponent.hasErrors] = willShakeIt  ? 'button__red error' : '';

    return {
        hospital: state.hospital as Hospital,
        userType: state.user.userType,
        userTypeName: state.user.userTypeName,
        passwordResetSuccess: state.user.passwordResetSuccess,
        cssFlags: flags,
        cmpntClassName: ResetPasswordCmpnt.name
    }
}

function mapDispatchToProps(dispatch) {
    return {
        resetPassword: credentials => dispatch(userActions.resetPassword(credentials)),
        goBack: () => dispatch(goBack())
    };
}

export default connect(mapStateToProps, mapDispatchToProps)(ResetPasswordCmpnt);
