import * as React from 'react';

export interface IMainNavUserInfoCmpntProps {
    profileImgPath: string;
    firstName: string;
    lastName: string;
    userInfo: string;

    goToAccount: () => void;
}

export class MainNavUserInfoCmpnt extends React.Component<IMainNavUserInfoCmpntProps, any>{

    render() {
        return (
            <a href="#" onClick={this.props.goToAccount} className="user-info user-info--main-nav">
                <div className="userpic userpic--main-nav">
                    <img id="imgProfile" src={this.props.profileImgPath} />
                </div>

                    <div className="user-info__text">
                        <div className="user-info__name" style={{display:'inline'}} >
                            <span className="user-info__fname" id="divFirstName">{`${this.props.firstName || ''}`}</span>
                            <span className="user-info__lname" id="divLastName">{`${this.props.lastName || ''}`}</span>
                        </div>
                        <div className="user-info__info">{this.props.userInfo}</div>
                    </div>
            </a>
                );
    }

}