/// Base class for connected or container components 
/// with validation functionality

import * as React from 'react';
import { connect } from 'react-redux';
import * as _ from 'lodash';


import { IStorageService, LocalStorageService } from '../services/storageService';
import { LocalizationService } from '../localization/localizationService';
import * as Vocabulary from '../localization/vocabulary';
import { IValidatable, Validator, ValidationError, ValidationSet, validateAll, validateSet, ValidationResult} from '../utilities/validators';

export class BaseSnapComponent extends React.Component<any, any> implements IValidatable {
    constructor(props, context) {
        super(props, context);

        this.state = {cssFlags:props.cssFlags};

        //functions binding
        this.cleanError = this.cleanError.bind(this);
        this.validate = this.validate.bind(this);
        this.validateInput = this.validateInput.bind(this);
        this.submitClicked = this.submitClicked.bind(this);
    }
    
    prepareValidationRules() {
    }

    cssFlags: object = {};
    static activeClass: string = 'is-active';
    static errClass: string = 'is-error';
    static shakeClass: string = 'shake';
    static hasErrors: string = 'hasErrors';

    validationSets = {};
    validationErrors = {};

    validateInput(event: any) {
        //if input is empty and it is not final validation we don`t want show error
        if (!event.target.value) {
            return;
        }
        let validationSet = this.validationSets[event.target.id] as ValidationSet;
        let result = validateSet(validationSet) as ValidationResult;
        
        if(! result.isValid)
        {
            this.cssFlags[result.errors[0].elementId] = BaseSnapComponent.errClass;    
            // dispatch an action about errors 
            this.validationErrors[event.target.id] = _.find(result.errors, (x:ValidationError)=>{return x.elementId == event.target.id});
        }
        else {
            delete this.cssFlags[event.target.id];    
            delete this.validationErrors[event.target.id];
        }
        
        this.setState({ cssFlags: this.cssFlags });
    }

    validate(notifyGlobally?:boolean): boolean {
        // validate all
        this.validationErrors = {};
        let cssFlags = this.cssFlags;
        _.each(cssFlags, (val, key)=>{
            delete cssFlags[key];
        });

        let vSets = _.flatMap(this.validationSets, (x)=>{ return x}) as ValidationSet[];
        let result =  validateAll(vSets);

        if( !result.isValid){
            _.each(result.errors,(err)=>{ 
                this.validationErrors[err.elementId] = err;
                this.cssFlags[err.elementId] = BaseSnapComponent.errClass;
            });
        }

        let hasErrors = Object.keys(this.validationErrors).length > 0;

        if(hasErrors){
             this.cssFlags[BaseSnapComponent.shakeClass] = BaseSnapComponent.shakeClass;
        }
        else{
            delete this.cssFlags[BaseSnapComponent.shakeClass];
        }
        this.setState({ cssFlags: this.cssFlags });
        if(notifyGlobally)
            this.props.showValidationErrors(result.errors, this.props.cmpntClassName);
        return !hasErrors;
    }

    cleanError(e) {
        delete this.cssFlags[e.target.id];
        this.setState({ cssFlags: this.cssFlags });
    }

    submitClicked() {
        delete this.cssFlags[BaseSnapComponent.shakeClass];
        this.setState({ cssFlags: this.cssFlags });
    }

    static childContextTypes = {
    };

    render() {
        return null;
    }
}