// This component handles the App template used on every page.
import React from 'react';
import { connect } from 'react-redux';
import Helmet from 'react-helmet';
import ReactCSSTransitionGroup from 'react-addons-css-transition-group';

import { SplashScreenCmpnt } from './common/SplashScreenCmpnt';
import LessStylesCmpnt from './common/LessStylesCmpnt';
import wireUpDependencies from '../wireUpDependencies';
import SiteNotificationsCmpnt from './common/SiteNotificationsCmpnt';

import * as userTypes from '../enums/userTypes';
import { Endpoints } from '../enums/endpoints';

import { SecurityService } from '../services/securityService';

var services = wireUpDependencies();

export class App extends React.Component {

  render() {
    //Dependency Injection imitation, add dependencies to props
    let children = React.Children.map(this.props.children, function (child) {
      return React.cloneElement(child, { dependencies: services });
    })

    return (
      <div>
        <Helmet
          htmlAttributes={{ lang: "en", amp: undefined }}
          defaultTitle="Snap Md"
          titleTemplate="Snap Md - %s"
        />
        {this.props.hospitalDataLoaded && this.props.enableHospitalStyles && <LessStylesCmpnt />}


        {/* Add messages service and component here, whethert it be toster or bars */}
        <ReactCSSTransitionGroup
          transitionName='splash'
          transitionAppear={true}
          transitionAppearTimeout={2000}
          transitionEnterTimeout={2000}
          transitionLeaveTimeout={2000}>
          <div style={{ position: 'absolute', zIndex: '1', width: '100%' }}>
            <SiteNotificationsCmpnt />
          </div>
        </ReactCSSTransitionGroup>

        {this.props.ajaxCall && <div style={{ background: 'red', color:'white',padding:'5px', position: 'absolute', zIndex: '1', width: '100%' }}>AJAX CALL IN PROGRESS</div>}

        <ReactCSSTransitionGroup
          transitionName='splash'
          transitionAppear={true}
          transitionAppearTimeout={1000}
          transitionEnterTimeout={100}
          transitionLeaveTimeout={2000}>
          {this.props.showSplashScreen  && <SplashScreenCmpnt key={0} />}
        </ReactCSSTransitionGroup>

        <ReactCSSTransitionGroup
          transitionName='splash'
          transitionAppear={true}
          transitionAppearTimeout={2000}
          transitionEnterTimeout={2000}
          transitionLeaveTimeout={2000}>
          {this.props.stylesLoaded  && children}
        </ReactCSSTransitionGroup>

      </div>
    );
  }
}

export function mapStateToProps(state, ownProps) {
  let isPublicPage = SecurityService.isPublicPage(ownProps.location.pathname);
  
  let publicPageDataLoaded = state.site.ajax.hospitalDataLoaded && 
                              state.site.ajax.stylesLoaded;
                            
  let showSplashScreen = true;

  // hide splash screeen when 
  // styles loaded + api keys loaded + hospital data loaded
  // significant data is loaded (like profile data)
  // or if critical error occured
 
  if(isPublicPage){
    showSplashScreen = !publicPageDataLoaded;
  }  
  else{
    showSplashScreen = ! (publicPageDataLoaded && state.site.ajax.dataLoaded);
  }
  showSplashScreen = showSplashScreen && !state.site.errors.isCriticalError;
  return {
    showSplashScreen: showSplashScreen,
    stylesLoaded: state.site.ajax.stylesLoaded,
    enableHospitalStyles: state.site.misc.enableHospitalStyles,
    hospitalDataLoaded: state.site.ajax.hospitalDataLoaded,
    ajaxCall: state.site.ajax.ajaxCallsInProgress > 0
  };
}

export default connect(mapStateToProps)(App);