import * as React from 'react';
import { Route, IndexRoute } from 'react-router';

class ClientAdminApp extends React.Component<any,any> {
    constructor(){
        super();
    }
    
    render() {
        return (
            <div className="CLIENT_ADMIN_PAGE">
                {this.props.children}
            </div>
        );
    }
}

export default ClientAdminApp;
