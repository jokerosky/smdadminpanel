import * as React from 'react';
import { Link } from 'react-router';

import * as Datetime from 'react-datetime';
import '../../../node_modules/react-datetime/css/react-datetime.css';

import { BaseSnapComponent } from '../BaseSnapComponent';
import { Endpoints } from '../../enums/endpoints';
import { LocalizationService } from '../../localization/localizationService';
import * as Vocabulary from '../../localization/vocabulary';
import { ValidationSet, Validator, getCssFlags } from '../../utilities/validators';
import * as converters from '../../utilities/converters';

export interface IRegistrationSecondStepData{
    email:string;
    dob:string;
    password:string;
    language:any;
}

export default class Registration2StepCmpnt extends BaseSnapComponent {
    constructor(props, context) {
        super(props, context);
        
        this.prepareValidationRules();

        this.setStep = this.setStep.bind(this);
        this.submit = this.submit.bind(this);
    }

    static emailId = 'txt-email';
    static birthDayId = 'txt-dob';
    static passwordId = 'txt-password';
    static passConfiramtionId = 'txt-confirmpassword';
    static chktermsId = 'chkterms';

    static dateFormat:string = 'MM/DD/YYYY';

    prepareValidationRules() {
        let that = this;

        let emailValidation = {
            elementId: Registration2StepCmpnt.emailId,
            rules: [
                // if passed value is wrong we return error message, other case we return null or empty string
                (val) => {
                    if (!Validator.isEmail(val)) {
                        return this.props.language[Vocabulary.VALIDATION_INVALID_EMIAL];
                    }
                },
            ]
        } as ValidationSet;

        let birthdayValidation = {
            elementId: Registration2StepCmpnt.birthDayId,
            rules: [(val) => {
                    if (!Validator.isDate(val, Registration2StepCmpnt.dateFormat)) {
                        return this.props.language[Vocabulary.VALIDATION_NOT_DATE];
                    }

                    if (!Validator.isOlderThanTwelve(val, Registration2StepCmpnt.dateFormat)) {
                        let ageDif = 12 - (converters.toDate(val, Registration2StepCmpnt.dateFormat).getFullYear() - new Date().getFullYear());
                        return this.props.language[Vocabulary.VALIDATION_SHOULD_BE_OLDER].replace("%AGE%", ageDif.toString());
                    }
                }]
        } as ValidationSet;

        let passwordValidation = {
            elementId: Registration2StepCmpnt.passwordId,
            rules: [(val) => {
                    if (!Validator.isNotEmptyOrWhitespace(val)) {
                        return this.props.language[Vocabulary.VALIDATION_INVALID_PASSWORD];
                    }
                }]
        } as ValidationSet;

        let passwordConfirmationValidation = {
            elementId: Registration2StepCmpnt.passConfiramtionId,
            rules: [(val) => {
                    if (!Validator.isNotEmptyOrWhitespace(val)) {
                        return this.props.language[Vocabulary.VALIDATION_EMPTY_STRING];
                    }

                    let pass = that.validationSets[Registration2StepCmpnt.passwordId].element.value;
                    if(pass != val){
                        return this.props.language[Vocabulary.VALIDATION_PASSWORDS_NOT_EQUAL];
                    }
                }]
        } as ValidationSet;

        let checkboxValidation = {
            elementId: Registration2StepCmpnt.chktermsId,
            rules: [() => {
                    if(!that.validationSets[Registration2StepCmpnt.chktermsId].element.checked){
                        return this.props.language[Vocabulary.VALIDATION_FLAG_NOT_CHECKED];
                    }
                }]
        } as ValidationSet;

        this.validationSets[Registration2StepCmpnt.emailId] = emailValidation;
        this.validationSets[Registration2StepCmpnt.birthDayId] = birthdayValidation;
        this.validationSets[Registration2StepCmpnt.passwordId] = passwordValidation;
        this.validationSets[Registration2StepCmpnt.passConfiramtionId] = passwordConfirmationValidation;
        this.validationSets[Registration2StepCmpnt.chktermsId] = checkboxValidation;
    }

    setStep() {
        this.props.setStep(1);
    }

    submit(e){
        e.preventDefault();
        let isValid =  this.validate(false);

        if(isValid)
        {
            let email = this.validationSets[Registration2StepCmpnt.emailId].element.value;
            let dob = this.validationSets[Registration2StepCmpnt.birthDayId].element.value;
            let password = this.validationSets[Registration2StepCmpnt.passwordId].element.value;

            let data = {
                email,
                dob,
                password
            } as IRegistrationSecondStepData;

            this.props.submit(data);
        }
        else{
            this.cssFlags[BaseSnapComponent.hasErrors] = 'button__red error';
            this.props.setErrors(this.validationErrors);
        }

        return false;
    }

    render() {
        return (
            <form onSubmit={this.submit}>
                <div className="box__content box__content--signup">
                    <h1 className="box__title">Hello, <span className="box__firstname" >{this.props.formData.firstName}</span></h1>
                    <h3 className="box__title box__title--sub-small">We just need a little more information to get you registered</h3>
                    <div className="inputs inputs__icon">
                        <span className="icon_mail"></span>
                        <input id={Registration2StepCmpnt.emailId}
                            type="text"
                            ref={(email) => { this.validationSets[Registration2StepCmpnt.emailId].element = email; }}
                            onChange={this.cleanError}
                            onBlur={this.validateInput}
                            placeholder="Email"
                            className={`k-input ${this.cssFlags[Registration2StepCmpnt.emailId] || ''}`}
                            defaultValue={this.props.formData.email} />
                    </div>
                    <div className="inputs inputs__icon inputs__icon--signup-label">
                        <span>Birthdate</span>
                        <div className="tooltip tooltip--signup">
                            <span>Why do we need this?</span>
                            <span style={{ float: "right" }} className="icon_help-with-circle tooltip-why-birthdate" title="Providing your date of birth helps ensure that you receive proper medical attention and care. Providing false information could lead to improper or ineffectual care. It also serves as a legal indicator that you have the right to use this system. Falsifying this information could result in legal action."></span>
                        </div>
                    </div>

                    <div className="inputs inputs__icon">
                        <span className="icon_calendar"></span>
                        <Datetime
                            dateFormat={Registration2StepCmpnt.dateFormat}
                            timeFormat={false}
                            inputProps={{
                                placeholder: "mm/dd/yyyy",
                                id: Registration2StepCmpnt.birthDayId,
                                className:` ${this.cssFlags[Registration2StepCmpnt.birthDayId] || ''}`,
                                onBlur:this.validateInput,
                                ref:(dob) => { this.validationSets[Registration2StepCmpnt.birthDayId].element = dob; }
                            }} />
                        {/*<input id="txt-dob" type="date" className="datepickerDOB" data-format="MM/dd/yyyy" data-bind="value: registrationInfo.birthdate, events:{change: datePickerChange}" autoComplete="off" />*/}
                    </div>

                    <div className="inputs inputs__icon">
                        <span className="icon_lock"></span>
                        <input id={Registration2StepCmpnt.passwordId}
                            type="password"
                            ref={(password) => { this.validationSets[Registration2StepCmpnt.passwordId].element = password; }}
                            onBlur={this.validateInput}
                            onChange={this.cleanError}
                            className={`k-input ${this.cssFlags[Registration2StepCmpnt.passwordId] || ''}`}
                            data-bind="value: registrationInfo.password, events: { focus: inputFocus }"
                            placeholder="Password" />
                    </div>

                    <div className="inputs inputs__icon">
                        <span className="icon_lock"></span>
                        <input id={Registration2StepCmpnt.passConfiramtionId}
                            name="confirm-password"
                            ref={(passConfiramtion) => { this.validationSets[Registration2StepCmpnt.passConfiramtionId].element = passConfiramtion; }}
                            onChange={this.cleanError}
                            onBlur={this.validateInput}
                            type="password"
                            className={`k-input ${this.cssFlags[Registration2StepCmpnt.passConfiramtionId] || ''}`}
                            data-bind="value: registrationInfo.confirmPassword, events: { focus: inputFocus }"
                            placeholder="Confirm Password" />
                    </div>

                    <div className="check-box clear">
                        <label>
                            <div className="checkbox">
                                <input
                                    id={Registration2StepCmpnt.chktermsId}
                                    ref={(chBox)=>{this.validationSets[Registration2StepCmpnt.chktermsId].element = chBox}}
                                    type="checkbox"
                                    data-bind="checked: registrationInfo.readTerms" />
                                <span className="checkbox__control"></span>
                                <span>By checking this box I acknowledge that I have read and agree to the <a href="/public/#/userTerms" target="_blank">terms & conditions</a> and <a href="/public/#/userTerms" target="_blank">privacy policy</a> for this platform.</span>
                            </div>
                        </label>
                    </div>

                </div>
                <div className="box__footer box__footer--signup">
                    <div className="row">
                        <button type="button" className="button one" onClick={this.setStep}><span>Back</span></button>&nbsp;
                        <button className={`button  button__brand--finish button__brand--margin-bottom ${this.props.hasErorr || this.cssFlags[BaseSnapComponent.hasErrors] ? 'button__red error' : 'button__brand'}`} onClick={this.props.submitClicked}><span>Continue</span></button>
                    </div>
                    <div className="row">
                        <p>Already have an acccount? <Link to={Endpoints.site.login.replace(/%type%/, this.props.userTypeName)}>LOGIN</Link></p>
                    </div>
                </div>
            </form>
        )
    }
}