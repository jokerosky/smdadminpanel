import * as React from 'react';

export default class RegistrationFailStepCmpnt extends React.Component<any, any> {
    constructor(props, context) {
        super(props, context);
    }

    render() {
        return (
            <div>
                <div className="box__content box__content--signupconfirm">
                    <h1 className="box__title">Sorry</h1>

                    <p>This service is not available in your area</p>
                    <p>Think we're mistaken?  Please contact: <span className="number">{this.props.contacNumber}</span></p>
                    <button className="button" onClick={this.props.goBack}><span>Back</span></button>

                </div>
            </div>
        )
    }
}