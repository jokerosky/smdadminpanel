import * as React from 'react';

export interface RegistrationSuccessStepCmpntProps {
    formData:any;
    logIn:()=>void;
}

export default class RegistrationSuccessStepCmpnt extends React.Component<RegistrationSuccessStepCmpntProps, any> {
    constructor(props, context) {
        super(props, context);
    }

    render() {
        return (
            <div>
                <div className="box__content box__content--signupconfirm">
                    <h1 className="box__title">You're all set<span className="box__firstname">{this.props.formData.firstName || ''}</span>!</h1>
                    <p>We’ve sent a verification email to the address you provided. Just be sure to verify your account with 24 hours to ensure uninterrupted access</p>
                    <button className="button button__brand button__brand--loginbtn" onClick={this.props.logIn}><span>Enter</span></button>
                </div> </div>
        )
    }

}