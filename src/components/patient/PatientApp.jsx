import React from 'react';
import { Route, IndexRoute } from 'react-router';

class PatientApp extends React.Component {
    constructor(props, context) {
        super(props, context);
    }

    render() {
        //Dependency Injection imitation, add dependencies to props
        let services = this.props.dependencies;
        let children = React.Children.map(this.props.children, function (child) {
            return React.cloneElement(child, { dependencies: services});
        })

        return (
            <div className="PATIENT_PAGE">
                {children}
            </div>
        );
    }
}

export default PatientApp;
