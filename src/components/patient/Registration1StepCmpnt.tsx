import * as React from 'react';
import { connect } from 'react-redux';
import { Link } from 'react-router';

import { BaseSnapComponent } from '../BaseSnapComponent';
import { PlacesAutocompleteCmpnt } from '../common/inputs/PlacesAutocompleteCmpnt';
import { Endpoints } from '../../enums/endpoints';

import { LocalizationService } from '../../localization/localizationService';
import * as Vocabulary from '../../localization/vocabulary';
import { ValidationSet, Validator, getCssFlags } from '../../utilities/validators';


export interface IRegistrationFirstStepData {
    name:{
        first:string,
        last:string
    };
    address:string;
    language:{};
}

export default class Registration1StepCmpnt extends BaseSnapComponent {
    constructor(props, context) {
        super(props, context);
        this.address = props.formData.address;

        this.prepareValidationRules();

        //functions binding
        this.onChange = this.onChange.bind(this);
        this.submit = this.submit.bind(this);
        
    }

    prepareValidationRules() {
         let firstNameValidation = {
            elementId: Registration1StepCmpnt.firstNameId,
            rules: [
                // if passed value is wrong we return error message, other case we return null or empty string
                (val) => {
                    if (!Validator.isNotEmptyOrWhitespace(val)) {
                        return this.props.language[Vocabulary.VALIDATION_EMPTY_STRING];
                    }
                },
            ]
        } as ValidationSet;

        let lastNameValidation = {
            elementId: Registration1StepCmpnt.lastNameId,
            rules: [
                (val) => {
                    if (!Validator.isNotEmptyOrWhitespace(val)) {
                        return this.props.language[Vocabulary.VALIDATION_EMPTY_STRING];
                    }
                },
            ]
        } as ValidationSet;

        let that = this;
        let addressValidation = {
            elementId: Registration1StepCmpnt.addressId,
            rules: [
                (val) => {
                    if (!Validator.isNotEmptyOrWhitespace(that.address)) {
                        return this.props.language[Vocabulary.VALIDATION_EMPTY_STRING];
                    }
                },
            ]
        } as ValidationSet;

        this.validationSets[Registration1StepCmpnt.firstNameId] = firstNameValidation;
        this.validationSets[Registration1StepCmpnt.lastNameId] = lastNameValidation;
        this.validationSets[Registration1StepCmpnt.addressId] = addressValidation;
    }

    static firstNameId = 'txt-firstname';
    static lastNameId = 'txt-lastname';
    static addressId = "txt-address";

    address:string;

    onChange(val:string){
        this.address = val;
        this.validateInput({target:{
                                value: val,
                                id:Registration1StepCmpnt.addressId}
                            });
    }

    submit(args){
        let isValid =  this.validate(false);

        if(isValid)
        {
            let first = this.validationSets[Registration1StepCmpnt.firstNameId].element.value;
            let last = this.validationSets[Registration1StepCmpnt.lastNameId].element.value;

            let data = {
                name:{
                    first,
                    last
                },
                address: this.address
            } as IRegistrationFirstStepData;

            this.props.submit(data);
            this.props.setErrors(this.validationErrors);
        }
        else{
            this.cssFlags[BaseSnapComponent.hasErrors] = 'button__red error';
            this.props.setErrors(this.validationErrors);
        }
    }

    render() {
        return (
            <div>
                <div className="box__content box__content--signup">
                    <h1 className="box__title">Welcome</h1>
                    <h3 className="box__title box__title--sub">Tell us who you are</h3>
                   
                    <div className="inputs inputs__icon">
                        <span className="icon_user"></span>
                        <input
                            id={Registration1StepCmpnt.firstNameId}
                            type="text"
                            ref={(x)=>{ this.validationSets[Registration1StepCmpnt.firstNameId].element = x}}
                            maxLength={24}
                            onChange={this.validateInput}
                            className={`k-input preventSpaces ${this.cssFlags[Registration1StepCmpnt.firstNameId] || ''}`}
                            placeholder="First Name"
                            defaultValue={this.props.formData.name ? this.props.formData.name.first : "" } />
                    </div>
                    <div className="inputs inputs__icon">
                        <span className="icon_user"></span>
                        <input
                            id={Registration1StepCmpnt.lastNameId}
                            ref={(x)=>{ this.validationSets[Registration1StepCmpnt.lastNameId].element = x}}
                            type="text"
                            maxLength={24}
                            onChange={this.validateInput}
                            className={`k-input preventSpaces ${this.cssFlags[Registration1StepCmpnt.lastNameId] || ''}`}
                            placeholder="Last Name" 
                            defaultValue={this.props.formData.name ? this.props.formData.name.last : ""} />
                    </div>

                    <h3 className="box__title box__title--sub">From</h3>
                    <div className="inputs inputs__icon">
                        <span className="icon_location-pin"></span>
                        <PlacesAutocompleteCmpnt
                            id={Registration1StepCmpnt.addressId}
                            className={`k-input preventSpaces ${this.cssFlags[Registration1StepCmpnt.addressId] || ''}`}
                            placeholder="Full Address"
                            onChange={ this.onChange }
                            defaultValue={this.props.formData.address} />
                        {/*<input id="txt-address" type="text" className="k-input" placeholder="Full Address" data-bind="value: registrationInfo.address, geoAddressInput:value, events: { focus: inputFocus }" />*/}
                    </div>
                </div>
                <div className="box__footer box__footer--signup">
                    <div className="row">
                        <button 
                            className={`button button__green button__green--margin-bottom button__brand--verify ${this.cssFlags[BaseSnapComponent.hasErrors]}`}
                            onClick={this.submit}>
                            <span>Get Started</span>
                        </button>
                    </div>
                    <div className="row">
                        <p>Already have an acccount? <Link to={Endpoints.site.login.replace(/%type%/, this.props.userTypeName)}>LOGIN</Link></p>
                    </div>
                </div>
            </div>
        )
    }
}
