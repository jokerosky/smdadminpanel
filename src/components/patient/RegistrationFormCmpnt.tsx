import * as React from 'react';
import { connect } from 'react-redux';
import { Link } from 'react-router';

import { Hospital } from '../../models/hospital';
import { Endpoints } from '../../enums/endpoints';
import * as  actions from '../../enums/actionTypes';
import * as  userTypes from '../../enums/userTypes';
import { BaseSnapComponent } from '../BaseSnapComponent';
import { NewPatientRequestDTO, PatientName } from '../../models/DTO/newPatientRequestDTO';
import LoginRequestDTO from '../../models/DTO/loginRequestDTO';

import { LocalizationService } from '../../localization/localizationService';
import * as Vocabulary from '../../localization/vocabulary';
import { ValidationSet, Validator, getCssFlags } from '../../utilities/validators';

import * as userActions from '../../actions/userActions';

import Registration1StepCmpnt, { IRegistrationFirstStepData } from './Registration1StepCmpnt';
import Registration2StepCmpnt, { IRegistrationSecondStepData } from './Registration2StepCmpnt';
import RegistrationSuccessStepCmpnt from './RegistrationSuccessStepCmpnt';
import RegistrationFailStepCmpnt from './RegistrationFailStepCmpnt';


export class RegistrationFormData extends NewPatientRequestDTO {
    passwordConfirmation?: string;
    language:any;
}

export class RegistrationFormCmpnt extends BaseSnapComponent {
    constructor(props, context) {
        super(props, context);
        this.state = {
            step: props.step,
            registrationData: props.registrationData as RegistrationFormData
        };

        this.prepareValidationRules();

        //functions binding
        this.submitClicked = this.submitClicked.bind(this);
        this.goBackForFailure = this.goBackForFailure.bind(this);
        this.setErrors = this.setErrors.bind(this);
        this.closeNotification = this.closeNotification.bind(this);
        this.validateAddress = this.validateAddress.bind(this);
        this.registerUser = this.registerUser.bind(this);
        this.setStep = this.setStep.bind(this);
        this.logIn = this.logIn.bind(this);
    }

    notificationText: string;

    componentWillReceiveProps(nextProps) {
        if(nextProps.errMessage)
        {
            this.cssFlags[BaseSnapComponent.hasErrors] = BaseSnapComponent.activeClass;
            this.cssFlags[BaseSnapComponent.shakeClass] = BaseSnapComponent.shakeClass;
            this.notificationText = nextProps.errMessage;
        }

        this.setState({
            step: nextProps.step,
            registrationData: nextProps.registrationData
        });
    }

    setErrors(validationErrors) {
        this.validationErrors = validationErrors;
        let errKeys = Object.keys(validationErrors);
        if (errKeys.length > 0) {
            this.cssFlags[BaseSnapComponent.hasErrors] = BaseSnapComponent.activeClass;
            this.cssFlags[BaseSnapComponent.shakeClass] = BaseSnapComponent.shakeClass;

            if (validationErrors[Registration2StepCmpnt.chktermsId] && errKeys.length == 1) {
                this.notificationText = this.props.language[Vocabulary.VALIDATION_ACCEPT_TERMS];
            } else {
                //this.notificationText = LocalizationService.getString(Vocabulary.VALIDATION_PLEASE_CORRECT_ERRORS);
                this.notificationText = validationErrors[errKeys[0]].messages[0];
            }
                
        }
        else {
            this.closeNotification();
        }

        this.setState({ cssFlags: this.cssFlags });
    }

    closeNotification() {
        delete this.cssFlags[BaseSnapComponent.hasErrors];
        this.setState({ cssFlags: this.cssFlags });
    }

    setStep(step: number) {
        this.props.setStep(step);
    }

    goBackForFailure() {
        
        this.props.setStep(1);
    }

    registerUser(formData: IRegistrationSecondStepData) {
        this.closeNotification();
        let newData = {
            providerId: (this.props.hospital as Hospital).hospitalId, 

        } as NewPatientRequestDTO ;
        Object.assign(newData, this.state.registrationData, formData);
        this.setState({ registrationData: newData });
        this.props.setRegistrationFormData(newData);
        this.props.registerUser(newData);
    }

    logIn() {
        this.closeNotification();
        let loginData:LoginRequestDTO = {
            email: this.state.registrationData.email,
            password: this.state.registrationData.password,
            hospitalId:this.props.hospital.hospitalId,
            userTypeId: userTypes.patient,
            redirectUrl: this.props.redirectUrl
        };
        this.props.loginUser(loginData);
    }

    validateAddress(formData: IRegistrationFirstStepData) {
        let newData = {};
        Object.assign(newData, this.state.registrationData, formData);
        this.setState({ registrationData: newData });
        this.props.setRegistrationFormData(newData);
        this.props.validateAddress(formData.address, this.props.hospital.hospitalId);
        //this.setState({step: 2});
    }

    render() {
        return (
            <div className='cc-public'>
                <header className="header cc-public__header" id="divHeader">
                    <div className="centerWrap">
                        <h1 className="title" title={this.props.hospital.brandName}> {this.props.hospital.brandName} </h1>
                    </div>
                </header>

                <section className={`col5-sm col12-md col16-lg content cc-public__content cc-public__content--signup`}>
                    <div className={`col12-sm col12-md-8 col16-lg-6 box ${this.cssFlags[BaseSnapComponent.shakeClass] || ''} `}>
                        <div className="box__header box__header--signup">
                            <h2>Registration</h2>

                            <ul className="box__steps">
                                <li className={this.state.step == 1 ? 'is-active' : ''}><span>1</span></li>
                                <li className={this.state.step == 2 ? 'is-active' : ''}><span>2</span></li>
                                <li className={this.state.step == 3 ? 'is-active' : ''}><span>3</span></li>
                            </ul>
                        </div>

                        <div className={`notification-bar notification-bar--registration notification-bar--error ${this.cssFlags[BaseSnapComponent.hasErrors]}`}>
                            <span className="notification-bar__icon icon_emoji-sad"></span>
                            <span className="notification-bar__title">Error:</span>
                            <span className="notification-bar__desc" >{this.notificationText}</span>
                            <button className="notification-bar__close" onClick={this.closeNotification}><span className="icon_cross"></span></button>
                        </div>

                        <div className="register-panel">
                            {(this.state.step == 1) && <Registration1StepCmpnt
                                onChange={(val) => { }}
                                submit={this.validateAddress}
                                userTypeName={this.props.userTypeName}
                                setErrors={this.setErrors}
                                submitClicked={this.submitClicked}
                                formData={this.state.registrationData}
                                language = {this.props.language}
                            />}

                            {(this.state.step == 2) && <Registration2StepCmpnt
                                userTypeName={this.props.userTypeName}
                                submit={this.registerUser}
                                setStep={this.setStep}
                                setErrors={this.setErrors}
                                formData={this.state.registrationData}
                                submitClicked={this.submitClicked}
                                hasErorr = {this.cssFlags[BaseSnapComponent.hasErrors]}
                                language = {this.props.language}
                            />}
                            {(this.state.step == 3) && <RegistrationSuccessStepCmpnt
                                formData={this.state.registrationData}
                                logIn={this.logIn}
                            />}
                            {(this.state.step == 4) && <RegistrationFailStepCmpnt
                                goBack={this.goBackForFailure} 
                                contactNumber={this.props.hospital.contactNumber}
                             />}
                        </div>
                    </div>
                    <section className="cc-public__footer cc-public__footer--signup" id="secLoginFooter">
                        <a href="/public/#/userTerms" data-target="#carousel-registration" data-slide-to="0" target="_blank">Terms &amp; Conditions</a>
                    </section>
                </section>
            </div>
        )
    }
}

export function mapStateToProps(state, ownProps) {
    let cssFlags = {};
    let errMessage = "";

    if(state.user.loginError){

    }

    if (state.site.errors.lastErrorEventName == actions.REGISTRATION_NEW_PATIENT_ERROR) {
        errMessage = state.site.errors.lastError;
    };
    return {
        hospital: state.hospital as Hospital,
        userType: state.user.userType,
        userTypeName: state.user.userTypeName,
        cssFlags: cssFlags,
        errMessage: errMessage,
        step: state.user.registrationStep,
        registrationData: state.user.registrationData,
        redirectUrl: state.user.redirectUrl,
        language: state.site.misc.vocabulary as any
    }
}

function mapDispatchToProps(dispatch) {
    return {
        validateAddress: (address, hospitalId) => dispatch(userActions.validateRegistrationAddress(address, hospitalId)),
        registerUser: (data) => dispatch(userActions.registerPatient(data)),
        loginUser: (data) => dispatch(userActions.logIn(data)) ,
        setStep: (step) => dispatch(userActions.setUserRegistrationStep(step)),
        setRegistrationFormData: (data) => dispatch(userActions.setRegistrationFormData(data))
    };
}

export default connect(mapStateToProps, mapDispatchToProps)(RegistrationFormCmpnt);