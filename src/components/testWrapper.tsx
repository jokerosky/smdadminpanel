import * as React from 'react';
import { SiteConstants } from '../enums/siteConstants';

import { PatientQueueHeaderCmpnt } from './provider/PatientQueueHeaderCmpnt';


export class TestWrapperCmpnt extends React.Component<any,any>{
    constructor() {
        super();

        this.state = {
            actionLog : this.actionLog
        }

        this.clearLog = this.clearLog.bind(this);

    }

    actionLog:string[] = [];
    key:number = 0;

    mockFunc(action:string){
        this.actionLog.push(action);
        this.setState({actionLog: this.actionLog});
    }

    renderLog(log:string[]) {
        let result:any[] = [];
        log.forEach(elm => {
            result.push(<br key = {++this.key} />);
            result.push(<span key = {++this.key}> {elm} </span>);
        });

        return result;
    }

    clearLog(){
        this.actionLog = [];
        this.setState({actionLog: this.actionLog});
    }

    render(){
        return (
            <div>
                <div className='col16-lg'>
                <PatientQueueHeaderCmpnt 
                                toggleHistory = { ()=>{this.mockFunc('toggleHistoryTab')}} 
                                toggleTabsMobile = { ()=>{this.mockFunc('toggleTabsMobile')}}
                                isScheduledActiveCssFlag = { true ? SiteConstants.css.isActive : '' }
                                onDemandActiveCssFlag = { true ? '' : SiteConstants.css.isActive }

                                onDemandCount = { 1 }
                                scheduledCount = { 2 }
                                />
                </div>
                <div className='col16-lg'>
                    <button onClick={this.clearLog}>Clear Log</button>
                    { this.renderLog(this.actionLog) }
                </div>
               
            </div>
        )
    }
}