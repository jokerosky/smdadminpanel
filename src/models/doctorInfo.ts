export class DoctorInfo
    {
        medicalSpeciality?: string;
        subSpeciality?: string;
        name?: string;
        profileImage?: string;
        userId?: number;
        personId :string 
        userName? :string; 
        statesLicenced? :string;
    }