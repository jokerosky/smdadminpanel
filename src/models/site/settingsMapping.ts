import * as _ from 'lodash';
import { HospitalSettingDTO } from '../DTO/hospitalSettingDTO';

import { ISetting } from './setting';
import { EditorType } from '../../enums/editorTypes';
import { ObjectHelper } from '../../utilities/objectHelper';

export interface IMappedSetting extends ISetting {
    key: string;
    action?: (newValue?: any, item?: IMappedSetting,  section?: IMappedSetting[]) => any;
    isActionRebinded?: boolean;
    hasChanged?: boolean;
    isVisible?: boolean;

    children?: IMappedSetting[];
}

export class ToggleMappedSetting implements IMappedSetting{
    constructor(    key:string,
                    label:string,
                    children?: IMappedSetting[],
                    action?:(newValue?: any, item?: IMappedSetting,  section?: IMappedSetting[]) => any){

        let defaultAction = (value:any, item:IMappedSetting, section:IMappedSetting[])=>{
            item.value = ObjectHelper.dataBaseStringFromBool(value);
        };
        
        this.key = key,
        this.label = label,
        this.children = children ? children : null;
        this.action = action ? action: defaultAction;
    }

    key: string;
    action?: (newValue?: any, item?: IMappedSetting, section?: IMappedSetting[]) => any ;
    isActionRebinded?: boolean;
    hasChanged?: boolean;
    children?: IMappedSetting[];
    label?: string;
    value?: any;
    name?: string;
    editorType?: EditorType = EditorType.toggle;
}

export class SettingsMapping {
    constructor(keys: IMappedSetting[], settings: Object) {
        
        settings = settings || {};
        this.mappedSettings = _.cloneDeep(keys);

        this.mappedSettings.forEach(x => {
            this.mapKeyToValue(x, settings);
        });
    }

    mappedSettings: IMappedSetting[];

    mapKeyToValue(mapedSetting: IMappedSetting, settings: Object) {
        let setting = settings[mapedSetting.key];

        //TODO: verify this ↓ and cover with test
        if(setting){
            if(setting.value)
                mapedSetting.value = setting.value;
            else
                mapedSetting.value = setting;
        }

        if (mapedSetting.children) {
            mapedSetting.children.forEach(x => {
                this.mapKeyToValue(x, settings);
            });
        }
    }

    // method that returns array of key value pairs which can be used to send to the server 
    getSettingsArrayToSave(): any[] {
        let result = [];

        let composeObject = (mappedSettings: IMappedSetting[]) => {
            for (let item in mappedSettings) {
                if(typeof(mappedSettings[item].value) != 'undefined')
                    result.push({
                        key: mappedSettings[item].key,
                        value: mappedSettings[item].value,
                    });

                if (mappedSettings[item].children &&
                    mappedSettings[item].children.length > 0) {
                    composeObject(mappedSettings[item].children);
                }
            }
        };

        composeObject(this.mappedSettings);

        return result;
    }

}