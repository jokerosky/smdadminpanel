export class MenuItem {
    text: string;
    id?: number;
    key?: string;
    title?: string;
    image?: string;
    activeClass?: string;
    inVisible?: boolean;
    action?: (any) => any;
    link?: string;
    linkClass?: string;
    icoClass?: string;
    additionalElements?: any;
}


export class TopMenuItem extends MenuItem {
    isFlagVisible?: boolean;
    flagText?: string;
    dropDown?: any;
}