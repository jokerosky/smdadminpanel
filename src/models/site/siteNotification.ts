import { EventType } from '../../enums/eventType';

export default class SiteNotification{
    notificationId:number; 
    comment?:string;
    message:string;
    type:EventType;
}