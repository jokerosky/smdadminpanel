import { EditorType } from '../../enums/editorTypes';

export interface ISetting{
    label?:string;
    value?:any;
    name?:string;
    editorType?:EditorType;    
}