import {HospitalSubscribeNotificationEventsKeysDTO, HospitalAddressKeysDTO, HospitalBrandingKeysDTO,
    HospitalClientKeysDTO, HospitalConsulatationParametersKeysDTO, HospitalCustomAppLinksKeysDTO, 
    HospitalEHRIntegrationKeysDTO, HospitalePrescriptionPartnerKeysDTO, HospitalInternalAPIIntegrationKeysDTO,
    HospitalPaymentPartnerKeysDTO, HospitalPlatformSeatsKeysDTO, HospitalRequiredCustomerFieldsKeysDTO,
    HospitalModulesKeysDTO, HospitalSingleSignOnKeysDTO} from '../DTO/hospitalSettingDTO';

export class HospitalSettings {
    client: HospitalClientKeysDTO;
    clientAddress: HospitalAddressKeysDTO;
    modules: HospitalModulesKeysDTO;
    mobile: ''
    platformSeats: HospitalPlatformSeatsKeysDTO;
    branding: HospitalBrandingKeysDTO;
    paymentPartner: HospitalPaymentPartnerKeysDTO;
    prescriptionPartner: HospitalePrescriptionPartnerKeysDTO;
    singleSignOn: HospitalSingleSignOnKeysDTO;
    consultationParameters: HospitalConsulatationParametersKeysDTO;
    reqiredCustomerFields: HospitalRequiredCustomerFieldsKeysDTO;
    customAppLinks: HospitalCustomAppLinksKeysDTO;
    ehrIntegration: HospitalEHRIntegrationKeysDTO;
    internalAPIIntegration: HospitalInternalAPIIntegrationKeysDTO;
    subscribeNotificationEvents: HospitalSubscribeNotificationEventsKeysDTO;
}