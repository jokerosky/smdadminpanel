import { Details } from './snapMDAdminDetails';
import { CodeSets } from './snapMDAdminCodeSets';
import { HospitalDocument } from './snapMDAdminHospitalDocument';
import { HospitalSettings } from './snapMDAdminSettings';

export class Client {
    details: Details;
    organizations:Array<any> = [];
    settings: HospitalSettings;
    smsTemplates: Array<any> = [];
    soapCodes: string;
    codeSets: CodeSets;
    operatingHours: Array<any> = [];
    hospitalDocument: HospitalDocument;
    locations: Array<any> = [];
};