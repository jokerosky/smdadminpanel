import { ClientDataDTO, SeatsDTO, ServicesDTO } from '../DTO/snapmdAdminStateDTO';
import { AddressDTO, AddressObject } from '../DTO/addressDTO';


export class Details {
    clientData: ClientDataDTO;
    seats: SeatsDTO;
    seatsAssigned: SeatsDTO;
    address: AddressDTO;
};