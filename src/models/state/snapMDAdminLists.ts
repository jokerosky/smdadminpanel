export class Lists {
    clients: Array<any> = [];
    clientsTypes: Array<any> = [];
    codeSets: Array<any> = [];
    documentTypes: Array<any> = [];
    organizationTypes: Array<any> = [];
    partner: Array<any> = [];
    partnerOnlyActive: Array<any> = [];
    countries: Array<any> = [];
};