import { Misc } from './snapMDAdminMisc';
import { Client } from './snapMDAdminClient';
import { Lists } from './snapMDAdminLists';
import { ServicesDTO } from '../DTO/snapmdAdminStateDTO';
import { IntegrationLogsDTO } from '../DTO/IntegrationLogsDTO';

export class SnapAdmin {
    misc: Misc;
    client: Client;
    lists: Lists;
    consultationMeetings: Array<any> = [];
    logMessages: Array<any> = [];
    services: Array<ServicesDTO> = [];
    integrationLogs: Array<IntegrationLogsDTO> = [];
};