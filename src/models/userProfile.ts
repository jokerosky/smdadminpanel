export class UserProfile {
    email: string;
    firstName: string;
    lastName: string;
    fullName: string;
    gender: string; //The gender: "M" for Male, "F" for Female.
    profileId: number;
    profileImage: string;
    profileImagePath: string;
    organizationId?: number;
    locationId?: number;
    timeZone: string;
    timeZoneId?: number;
    timeZoneSystemId?: string // Gets or sets time zone string identifier. Example: 'Eastern Standard Time'.
    userId: number;
    dob?: Date; // date of birth
    address: string;
    weight: string;
    height: string;
    homePhone: string;
    mobilePhone: string;
    hairColor?: number;
    eyeColor?: number;
    bloodType?: number;
    ethnicity?: number;
    hasRequiredFields: boolean; //<c>true</c> if this instance has required fields; otherwise, <c>false</c>.
    personId?: string;
    isDependent: boolean;
    guardianName: string; // Guardian full name (for dependent patients).
    guardianUserId?: number; 
    userRoleDescription: string;
}