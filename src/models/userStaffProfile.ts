import {UserProfile} from './userProfile';

export class UserStaffProfile extends UserProfile{
    medicalLicense: string;
    medicalSchool: string;
    medicalSpeciality: string;
    roles: string; //     Comma-separated list of roles.
    statesLicenced: string;
    subSpeciality: string;
}