import { PersonRecord } from './personRecord';

export class  AppointmentParticipant{
    participantId: string;
    person: PersonRecord;
    status: number;

    attendenceCode: number;
    personId: string;
    participantTypeCode: number;

}