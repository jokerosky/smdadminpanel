export class  PersonName{
    given: string;
    prefix: string;
    suffix: string;
    text: string;
    family: string;
}