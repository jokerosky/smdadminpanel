export class SeatsDTO {
    modAdminEHRERx: number = 0;
    modAdminPMFull: number = 0;
    modAdminPMPart: number = 0;
    modClSubTotalSeats: number = 0;
    modEHRERx: number = 0;
    modTotalSeats: number = 0;
    snAdmin: number = 0;
    snClSubTotalSeats: number = 0;
    snEnterprise: number = 0;
    snRestricted: number = 0;
    snTotalSeats: number = 0;
    snUnRestricted: number = 0;
};

export class ClientDataDTO {
    appointmentsContactNumber: string = '';
    brandName: string = '';
    brandTitle: string = '';
    children: number = 0;
    cliniciansCount: number = 0;
    consultationCharge: number = 0;
    contactNumber: string = '';
    email: string = '';
    hospitalCode: string = '';
    hospitalDomainName: string = '';
    hospitalId: number = 0;
    hospitalImage: string = '';
    hospitalName: string = '';
    hospitalType: number = 0;
    isActive: string = '';
    itDeptContactNumber: string = '';
    nPINumber: string = '';
    seats: number = 0;
    totalUsersCount: number = 0;
};

export class ServicesDTO {
    Description: string = '';
    Status: string = '';
    LastRun: string = '';
    LoggingLevel: number = null;
    LoggingLevelString: string = '';
    ScheduleTypeid: number = null;
    ServiceId: number = null;
    StatusId: number = null;
    VersionLastRun: string = '';
    CycleMS: number = null;
};