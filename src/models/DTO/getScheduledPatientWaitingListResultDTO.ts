import { DoctorInfo } from '../doctorInfo';
import { AppointmentNotificationModel } from '../appointmentNotificationModel';

export class GetScheduledPatientWaitingListResultDTO{
    hospitalId?: number;
    consultationId?: number;
    patientId?: number;
    consultantUserId?: number;
    phoneNumber?: string; 
    assignedDoctorId?: number; 
    patientName?: string; 

    guardianName?: string;

    patientFirstName?: string; 
    patientLastName?: string; 

    gender?: string;

    dOB?: Date;
    ageInfo?: string;

    profileImagePath?: string;
    homePhone?: string; 
    mobilePhone?: string;
    consultationStatus?: number; 
    rxNTPatientId?: number; 
    primaryConsern?: string; 
    secondaryConsern?: string; 
    additionNotes?: string; 
    consultationDateInfo?: Date; 
    consultationTimeInfo?: Date; 
    
    consultationTime?: string;
    consultationDate?: string;

    isScheduled?: boolean;
    remainTimeInfo?: string; 
    waitTime?: string;

    waitTimeInfo: string;
    waitTimeInfoMinute?: number;
    waitTimeInfoSecond?: number;

    patientQueueEntryId?: string;
    personId?: string;
    meetingId?: string;
    meetingConversationCount?: number;

    state?: string;
    stateCode?: string; 
    country?: string;
    countryCode?: string; 
    zip?: string; 
    city?: string; 

    appointmentId?: string;
    appointmentType?: string; // actually int, because it is enum

    encounterTypeCode?: string; // actually int, because it is enum

    flag?: number;
    notifications?: AppointmentNotificationModel
    doctorProfile?: DoctorInfo;
    isPatientInMobile?: boolean;
}