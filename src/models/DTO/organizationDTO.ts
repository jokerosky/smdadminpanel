export class OrganizationDTO{
    addresses?:Array<any>;
    hospitalId?:number;
    locations?:Array<LocationsDTO>;
    name?:string;
    organizationTypeId?:number;
    id?:number;
    createdDate?:string;
    modifiedDate?:string;
    createdByUserId?:number;
    modifiedByUserId?:number;
}

export class LocationsDTO{
    name?:string;
    organizationId?:number;
    id?:number;
    statusCode?:number;
    createdDate?:string;
    modifiedDate?:string;
    createdByUserId?:number;
    modifiedByUserId?:number;
}