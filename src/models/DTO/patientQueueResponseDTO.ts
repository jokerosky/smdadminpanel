import { DoctorInfo } from '../doctorInfo';
import { GetScheduledPatientWaitingListResultDTO } from '../../models/DTO/getScheduledPatientWaitingListResultDTO';

export class PatientQueueResponseDTO{
    avgPastConsultation?: string;
    physicianSlotAvailable?: boolean;
    doctorProfile? : DoctorInfo;
    patientAvgWaitTime?: string;
    schedulPatientWaitingList?: GetScheduledPatientWaitingListResultDTO[];
}