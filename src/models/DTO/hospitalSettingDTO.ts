import { MODULE_KEYS } from '../../enums/clientSettingsKeys';

export class HospitalSettingDTO {
    id: number;
    hospitalId: number;
    key: string;
    value: string;
}

export class HospitalModulesKeysDTO {
    GenericMobileAppEnabled: string;
    mAddressValidation: string;
    mAdminMeetingReport: string;
    mAnnotation: string;
    mClinicianSearch: string;
    mConsultationVideoArchive: string;
    mDisablePhoneConsultation: string;
    mECommerce: string;
    mEncounterGeoLocation: string;
    mShowAppointmentWizard: string;
    mShowNowButton: string;
    mEPerscriptions: string;
    mEPSchedule1: string;
    mFileSharing: string;
    mHideDrToDrChat: string;
    mHideForgotPasswordLink: string;
    mHideOpenConsultation: string;
    mHidePaymentBeforeWaiting: string;
    mIFOnDemand: string;
    mIFScheduled: string;
    mIncludeDirections: string;
    mInsVerification: string;
    mInsVerificationBeforeWaiting: string;
    mInsVerificationDummy: string;
    mIntakeForm: string;
    mKubi: string;
    mMedicalCodes: string;
    mOnDemand: string;
    mOnDemandHourly: string;
    mOrganizationLocation: string;
    mRxNTEHR: string;
    mRxNTPM: string;
    mShowCTTOnScheduled: string;
    mTextMessaging: string;
    mVideoBeta: string;
    mDrToDrChatInAdmin: string;
}

export class AdditionalSettingsKeysDTO {
    BrandBackgroundColor: string;
    BrandBackgroundImage: string;
    BrandBackgroundLoginImage: string;
    BrandTextColor: string;
    ContactUsImage: string;
    language: string;
}

export class HospitalAddressKeysDTO {
    line1: string;
    line2: string;
    city: string;
    state: string;
    stateCode: string;
    postalCode: string;
    country: string;
    countryCode: string;
    addressText: string;
}

export class HospitalClientKeysDTO {
    hospitalName: string;
    consultationCharge: string;
    contactNumber: string;
    itDeptContactNumber: string;
    appointmentsContactNumber: string;
    npiNumber: string;
    hospitalType: string;
    children: string;
    ePrescriptionGateWay: string;
    email: string;
    hospitalCode: string;
    hospitalId: string;
    insuranceValidationGateWay: string;
    isActive: string;
    paymentGateWay: string;
    smsGateway: string;

    language: string;
    useAsProductName: string;
}

export class HospitalPlatformSeatsKeysDTO {
    snAdmin: string;
    snAdminPending: string;
    snEnterprise: string;
    snEnterprisePending: string;
    snTotalSeats: string;

    snAdminAssigned: string;
    snAdminPendingAssigned: string;
    snEnterpriseAssigned: string;
    snEnterprisePendingAssigned: string;
    snTotalSeatsAssigned: string;
}

export class HospitalBrandingKeysDTO {
    brandName: string;
    brandTitle: string;
    hospitalDomainName: string;
    brandColor: string;
    BrandTextColor: string;
    BrandBackgroundColor: string;
    BrandBackgroundImage: string;
    BrandBackgroundLoginImage: string;
    ContactUsImage: string;
    hospitalImage: string;
}

export class HospitalPaymentPartnerKeysDTO {
    AuthorizeNet_BaseUrl: string;
    AuthorizeNet_PostURL: string;
    AuthorizeNet_TransactionKey: string;
    AuthorizeNet_LoginID: string;
    Chargify_Subdomain: string;
    Chargify_V1Key: string;
    Chargify_V2ApiId: string;
    Chargify_V2SharedSecret: string;
    Chargify_V2Password: string;

    paymentPartnerSelect: string;
}

export class HospitalePrescriptionPartnerKeysDTO {
    RxNTEPrescriptionSingleSignonUrl: string;
    PrescriptionWebServiceUrl: string;
    MDToolbox_ServiceUrl: string;
    MDToolbox_SsoUrl: string;
    MDToolbox_AccountId: string;
    MDToolbox_PracticeId: string;
    MDToolbox_AuthenticationKey: string;

    prescriptionPartnerSelect: string;
}

export class HospitalSingleSignOnKeysDTO {
    oAuthClientId: string;
    oAuthClientSecret: string;
    oAuthUrl: string;
    oAuthCallback: string;
    ssoExternalLoginUrl: string;
    ssoReturnUrl: string;
    customerSSO: string;
    customerSSOButtonText: string;
    ssoClinicianExternalLoginUrl: string;
    ssoClinicianReturnUrl: string;
    clinicianSSO: string;
    clinicianSSOButtonText: string;
    ssoAdminExternalLoginUrl: string;
    adminConsultEndUrl: string;
    adminSSO: string;
    adminSSOButtonText: string;
    jwtIssuer: string;
    patientTokenApi: string;
    patientRegistrationApi: string;
    patientForgotPasswordApi: string;
}

export class HospitalConsulatationParametersKeysDTO {
    DefaultAvailabilityBlockDuration: string;
    CbMinClinicansGood: string;
}

export class HospitalRequiredCustomerFieldsKeysDTO {
    IsBloodTypeRequired: string;
    IsHairColorRequired: string;
    IsEthnicityRequired: string;
    IsEyeColorRequired: string;
    IsHeightRequired: string;
    IsWeightRequired: string;
    IsPharmacyRequired: string;
    IsPreferredProviderRequired: string;
}

export class HospitalCustomAppLinksKeysDTO {
    iOSSchemaUrl: string;
    androidSchemaUrl: string;
    iOSAppStoreUrl: string;
    androidPlayStoreUrl: string;
}

export class HospitalEHRIntegrationKeysDTO {
    externalHospitalId: string;
    partnerId: string;
    ehrFailureNotificationEmails: string;
    FailureNotificationEmails: string;
    PracticeMaxEcwSvcUrl: string;
    PracticeMaxEcwSvcLogin: string;
    PracticeMaxEcwSvcPassword: string;
    AthenaHealthAppointmentType: string;
    AthenaHealthConsultationType: string;
    AthenaHealthDefaultDepartment: string;
    AthenaHealthSearchDepartments: string;
    AthenaHealthSvcLogin: string;
    AthenaHealthSvcPassword: string;
    AthenaHealthSvcUrl: string;
    AthenaHealthSvcAuthPrefix: string;
    AthenaHealthSvcApiPrefix: string;
    AthenaHealthHasProviderGroups: string;
    AthenaHealthHidePatientProfile: string;
    AthenaHealthSyncPatientSurgeries: string;
    AthenaHealthSyncPatientAllergies: string;
    AthenaHealthSyncPatientMedicalHistory: string;
    AthenaHealthSyncPatientHeadClinician: string;
    AthenaHealthSyncPatientPharmacies: string;

    ehrPartnerSelect: string;
}

export class HospitalInternalAPIIntegrationKeysDTO {
    AdminPassword: string;
    AdminUserName: string;
    externalHospitalId: string;
    partnerId: string;

    internalAPIIntegrationSelect: string;
}

export class HospitalSubscribeNotificationEventsKeysDTO {
    secret: string;
    callback: string;
}

export class SaveHospitalClientDTO {
    appointmentsContactNumber: string;
    brandColor: string;
    brandName: string;
    brandTitle: string;
    children: string;
    cliniciansCount: string;
    consultationCharge: string;
    contactNumber: string;
    ePrescriptionGateWay: string;
    email: string;
    hospitalCode: string;
    hospitalDomainName: string;
    hospitalId: string;
    hospitalImage: string;
    hospitalName: string;
    hospitalType: string;
    iTDeptContactNumber: string;
    insuranceValidationGateWay: string;
    isActive: string;
    nPINumber: string;
    paymentGateWay: string;
    smsGateway: string;
    totalUsersCount: string;
}

export class HospitalClientInListsKeyDTO {
    children: string;
    hospitalId: string;
    hospitalName: string;
    hospitalType: string;
    hospitalTypeName: string;
    isActive: string;
    seats: string;
}

export class HospitalImagesDTO{
    fileImage: FormData;
    pathSaveImage: string;
    fieldNameToSave: string;
}

export function getDefaultHospitalSettings(): HospitalModulesKeysDTO {
    let hospitalSettings: any = {};
    for (let index in MODULE_KEYS) {
        hospitalSettings[MODULE_KEYS[index]] = false;
    }
    return hospitalSettings;
}

export function getDefaultAdditionalSettings(): AdditionalSettingsKeysDTO {
    let additionalSettings: AdditionalSettingsKeysDTO = {
        BrandBackgroundColor: "#fff",
        BrandBackgroundImage: "",
        BrandBackgroundLoginImage: "",
        BrandTextColor: "#fff",
        ContactUsImage: "",
        language: "en"
    };
    return additionalSettings;
}

export function getDefaultHospitalKeys(): HospitalModulesKeysDTO {
    let hospitalSettings: any = {};
    for (let index in MODULE_KEYS) {
        hospitalSettings[MODULE_KEYS[index]] = index;
    }
    return hospitalSettings;
}

export function getHospitalAddressKeys(): HospitalAddressKeysDTO {
    return {
        line1: 'line1',
        line2: 'line2',
        city: 'city',
        state: 'state',
        stateCode: 'stateCode',
        postalCode: 'postalCode',
        country: 'country',
        countryCode: 'countryCode',
        addressText: 'addressText',
    } as HospitalAddressKeysDTO;
}

export function getHospitalModulesKeys(): HospitalModulesKeysDTO {
    return {
        mECommerce: 'mECommerce',
        mHidePaymentBeforeWaiting: 'mHidePaymentBeforeWaiting',
        mInsVerification: 'mInsVerification',
        mInsVerificationDummy: 'mInsVerificationDummy',
        mInsVerificationBeforeWaiting: 'mInsVerificationBeforeWaiting',
        mEPerscriptions: 'mEPerscriptions',
        mEPSchedule1: 'mEPSchedule1',
        mOnDemand: 'mOnDemand',
        mIntakeForm: 'mIntakeForm',
        mIFOnDemand: 'mIFOnDemand',
        mIFScheduled: 'mIFScheduled',
        mMedicalCodes: 'mMedicalCodes',
        mVideoBeta: 'mVideoBeta',
        mClinicianSearch: 'mClinicianSearch',
        mTextMessaging: 'mTextMessaging',
        mRxNTEHR: 'mRxNTEHR',
        mRxNTPM: 'mRxNTPM',
        mShowCTTOnScheduled: 'mShowCTTOnScheduled',
        mFileSharing: 'mFileSharing',
        mOrganizationLocation: 'mOrganizationLocation',
        mAddressValidation: 'mAddressValidation',
        mHideOpenConsultation: 'mHideOpenConsultation',
        mHideDrToDrChat: 'mHideDrToDrChat',
        mDisablePhoneConsultation: 'mDisablePhoneConsultation',
        mDrToDrChatInAdmin: 'mDrToDrChatInAdmin',
        mAnnotation: 'mAnnotation',
        mKubi: 'mKubi',
        mConsultationVideoArchive: 'mConsultationVideoArchive',
        mAdminMeetingReport: 'mAdminMeetingReport',
        mIncludeDirections: 'mIncludeDirections',
        mHideForgotPasswordLink: 'mHideForgotPasswordLink',
        mEncounterGeoLocation: 'mEncounterGeoLocation',
        GenericMobileAppEnabled: 'GenericMobileAppEnabled',
        mOnDemandHourly: 'mOnDemandHourly',
        mShowAppointmentWizard: 'mShowAppointmentWizard',
        mShowNowButton: 'mShowNowButton'
    } as HospitalModulesKeysDTO;
}

export function getHospitalClientKeys(): HospitalClientKeysDTO {
    return {
        hospitalName: 'hospitalName',
        consultationCharge: 'consultationCharge',
        contactNumber: 'contactNumber',
        itDeptContactNumber: 'itDeptContactNumber',
        appointmentsContactNumber: 'appointmentsContactNumber',
        npiNumber: 'npiNumber',
        hospitalType: 'hospitalType',
        language: 'language',
        useAsProductName: 'useAsProductName',
        children: 'children',
        ePrescriptionGateWay: 'ePrescriptionGateWay',
        email: 'email',
        hospitalCode: 'hospitalCode',
        hospitalId: 'hospitalId',
        insuranceValidationGateWay: 'insuranceValidationGateWay',
        isActive: 'isActive',
        paymentGateWay: 'paymentGateWay',
        smsGateway: 'smsGateway'
    } as HospitalClientKeysDTO;
}

export function getHospitalPlatformSeatsKeys(): HospitalPlatformSeatsKeysDTO {
    return Object.assign({}, getHospitalSeatsKeys(), getHospitalSeatsAssignedKeys()) as HospitalPlatformSeatsKeysDTO;
}

export function getHospitalSeatsKeys() {
    return {
        snAdmin: 'snAdmin',
        snAdminPending: 'snAdminPending',
        snEnterprise: 'snEnterprise',
        snEnterprisePending: 'snEnterprisePending',
        snTotalSeats: 'snTotalSeats'
    }
}

export function getHospitalSeatsAssignedKeys() {
    return {
        snAdminAssigned: 'snAdminAssigned',
        snAdminPendingAssigned: 'snAdminPendingAssigned',
        snEnterpriseAssigned: 'snEnterpriseAssigned',
        snEnterprisePendingAssigned: 'snEnterprisePendingAssigned',
        snTotalSeatsAssigned: 'snTotalSeatsAssigned'
    }
}

export function getHospitalBrandingKeys(): HospitalBrandingKeysDTO {
    return {
        brandName: 'brandName',
        brandTitle: 'brandTitle',
        hospitalDomainName: 'hospitalDomainName',
        brandColor: 'brandColor',
        BrandTextColor: 'BrandTextColor',
        BrandBackgroundColor: 'BrandBackgroundColor',
        BrandBackgroundImage: 'BrandBackgroundImage',
        BrandBackgroundLoginImage: 'BrandBackgroundLoginImage',
        ContactUsImage: 'ContactUsImage',
        hospitalImage: 'hospitalImage'
    } as HospitalBrandingKeysDTO;
}

export function getHospitalPaymentPartnerKeys(): HospitalPaymentPartnerKeysDTO {
    return {
        AuthorizeNet_BaseUrl: 'AuthorizeNet_BaseUrl',
        AuthorizeNet_PostURL: 'AuthorizeNet_PostURL',
        AuthorizeNet_TransactionKey: 'AuthorizeNet_TransactionKey',
        AuthorizeNet_LoginID: 'AuthorizeNet_LoginID',
        Chargify_Subdomain: 'Chargify_Subdomain',
        Chargify_V1Key: 'Chargify_V1Key',
        Chargify_V2ApiId: 'Chargify_V2ApiId',
        Chargify_V2SharedSecret: 'Chargify_V2SharedSecret',
        Chargify_V2Password: 'Chargify_V2Password',

        paymentPartnerSelect: 'paymentPartnerSelect'
    } as HospitalPaymentPartnerKeysDTO;
}

export function getHospitalePrescriptionPartnerKeys(): HospitalePrescriptionPartnerKeysDTO {
    return {
        RxNTEPrescriptionSingleSignonUrl: 'RxNTEPrescriptionSingleSignonUrl',
        PrescriptionWebServiceUrl: 'PrescriptionWebServiceUrl',
        MDToolbox_ServiceUrl: 'MDToolbox_ServiceUrl',
        MDToolbox_SsoUrl: 'MDToolbox_SsoUrl',
        MDToolbox_AccountId: 'MDToolbox_AccountId',
        MDToolbox_PracticeId: 'MDToolbox_PracticeId',
        MDToolbox_AuthenticationKey: 'MDToolbox_AuthenticationKey',

        prescriptionPartnerSelect: 'prescriptionPartnerSelect'
    } as HospitalePrescriptionPartnerKeysDTO;
}

export function getHospitalSingleSignOnKeys(): HospitalSingleSignOnKeysDTO {
    return {
        oAuthClientId: 'oAuthClientId',
        oAuthClientSecret: 'oAuthClientSecret',
        oAuthUrl: 'oAuthUrl',
        oAuthCallback: 'oAuthCallback',
        ssoExternalLoginUrl: 'ssoExternalLoginUrl',
        ssoReturnUrl: 'ssoReturnUrl',
        customerSSO: 'customerSSO',
        customerSSOButtonText: 'customerSSOButtonText',
        ssoClinicianExternalLoginUrl: 'ssoClinicianExternalLoginUrl',
        ssoClinicianReturnUrl: 'ssoClinicianReturnUrl',
        clinicianSSO: 'clinicianSSO',
        clinicianSSOButtonText: 'clinicianSSOButtonText',
        ssoAdminExternalLoginUrl: 'ssoAdminExternalLoginUrl',
        adminConsultEndUrl: 'adminConsultEndUrl',
        adminSSO: 'adminSSO',
        adminSSOButtonText: 'adminSSOButtonText',
        jwtIssuer: 'jwtIssuer',
        patientTokenApi: 'patientTokenApi',
        patientRegistrationApi: 'patientRegistrationApi',
        patientForgotPasswordApi: 'patientForgotPasswordApi'
    } as HospitalSingleSignOnKeysDTO;
}

export function getHospitalConsulatationParametersKeys(): HospitalConsulatationParametersKeysDTO {
    return {
        DefaultAvailabilityBlockDuration: 'DefaultAvailabilityBlockDuration',
        CbMinClinicansGood: 'CbMinClinicansGood'
    } as HospitalConsulatationParametersKeysDTO;
}

export function getHospitalRequiredCustomerFieldsKeys(): HospitalRequiredCustomerFieldsKeysDTO {
    return {
        IsBloodTypeRequired: 'IsBloodTypeRequired',
        IsHairColorRequired: 'IsHairColorRequired',
        IsEthnicityRequired: 'IsEthnicityRequired',
        IsEyeColorRequired: 'IsEyeColorRequired',
        IsHeightRequired: 'IsHeightRequired',
        IsWeightRequired: 'IsWeightRequired',
        IsPharmacyRequired: 'IsPharmacyRequired',
        IsPreferredProviderRequired: 'IsPreferredProviderRequired'
    } as HospitalRequiredCustomerFieldsKeysDTO;
}

export function getHospitalCustomAppLinksKeys(): HospitalCustomAppLinksKeysDTO {
    return {
        iOSSchemaUrl: 'iOSSchemaUrl',
        androidSchemaUrl: 'androidSchemaUrl',
        iOSAppStoreUrl: 'iOSAppStoreUrl',
        androidPlayStoreUrl: 'androidPlayStoreUrl'
    } as HospitalCustomAppLinksKeysDTO;
}

export function getHospitalEHRIntegrationKeys(): HospitalEHRIntegrationKeysDTO {
    return {
        externalHospitalId: 'externalHospitalId',
        partnerId: 'partnerId',
        ehrFailureNotificationEmails: 'ehrFailureNotificationEmails',
        FailureNotificationEmails: 'FailureNotificationEmails',
        PracticeMaxEcwSvcUrl: 'PracticeMaxEcwSvcUrl',
        PracticeMaxEcwSvcLogin: 'PracticeMaxEcwSvcLogin',
        PracticeMaxEcwSvcPassword: 'PracticeMaxEcwSvcPassword',
        AthenaHealthAppointmentType: 'AthenaHealthAppointmentType',
        AthenaHealthConsultationType: 'AthenaHealthConsultationType',
        AthenaHealthDefaultDepartment: 'AthenaHealthDefaultDepartment',
        AthenaHealthSearchDepartments: 'AthenaHealthSearchDepartments',
        AthenaHealthSvcLogin: 'AthenaHealthSvcLogin',
        AthenaHealthSvcPassword: 'AthenaHealthSvcPassword',
        AthenaHealthSvcUrl: 'AthenaHealthSvcUrl',
        AthenaHealthSvcAuthPrefix: 'AthenaHealthSvcAuthPrefix',
        AthenaHealthSvcApiPrefix: 'AthenaHealthSvcApiPrefix',
        AthenaHealthHasProviderGroups: 'AthenaHealthHasProviderGroups',
        AthenaHealthHidePatientProfile: 'AthenaHealthHidePatientProfile',
        AthenaHealthSyncPatientSurgeries: 'AthenaHealthSyncPatientSurgeries',
        AthenaHealthSyncPatientAllergies: 'AthenaHealthSyncPatientAllergies',
        AthenaHealthSyncPatientMedicalHistory: 'AthenaHealthSyncPatientMedicalHistory',
        AthenaHealthSyncPatientHeadClinician: 'AthenaHealthSyncPatientHeadClinician',
        AthenaHealthSyncPatientPharmacies: 'AthenaHealthSyncPatientPharmacies',

        ehrPartnerSelect: 'ehrPartnerSelect'
    } as HospitalEHRIntegrationKeysDTO;
}

export function getHospitalInternalAPIIntegrationKeys(): HospitalInternalAPIIntegrationKeysDTO {
    return {
        AdminPassword: 'AdminPassword',
        AdminUserName: 'AdminUserName',
        externalHospitalId: 'externalHospitalId',
        partnerId: 'partnerId',

        internalAPIIntegrationSelect: 'internalAPIIntegrationSelect'
    } as HospitalInternalAPIIntegrationKeysDTO;
}

export function getHospitalSubscribeNotificationEventsKeys(): HospitalSubscribeNotificationEventsKeysDTO {
    return {
        secret: 'secret',
        callback: 'callback'
    } as HospitalSubscribeNotificationEventsKeysDTO;
}

export function getSaveHospitalClientKeys(): SaveHospitalClientDTO {
    return {
        appointmentsContactNumber: 'appointmentsContactNumber',
        brandColor: 'brandColor',
        brandName: 'brandName',
        brandTitle: 'brandTitle',
        children: 'children',
        cliniciansCount: 'cliniciansCount',
        consultationCharge: 'consultationCharge',
        contactNumber: 'contactNumber',
        ePrescriptionGateWay: 'ePrescriptionGateWay',
        email: 'email',
        hospitalCode: 'hospitalCode',
        hospitalDomainName: 'hospitalDomainName',
        hospitalId: 'hospitalId',
        hospitalImage: 'hospitalImage',
        hospitalName: 'hospitalName',
        hospitalType: 'hospitalType',
        iTDeptContactNumber: 'iTDeptContactNumber',
        insuranceValidationGateWay: 'insuranceValidationGateWay',
        isActive: 'isActive',
        nPINumber: 'nPINumber',
        paymentGateWay: 'paymentGateWay',
        smsGateway: 'smsGateway',
        totalUsersCount: 'totalUsersCount'
    } as SaveHospitalClientDTO;
}

export function getHospitalClientInListsKey(): HospitalClientInListsKeyDTO {
    return {
        children: 'children',
        hospitalId: 'hospitalId',
        hospitalName: 'hospitalName',
        hospitalType: 'hospitalType',
        hospitalTypeName: 'hospitalTypeName',
        isActive: 'isActive',
        seats: 'seats'
    } as HospitalClientInListsKeyDTO;
}

export function getSaveHospitalAddressKey(): HospitalAddressKeysDTO {
    let hospitalAddress: HospitalAddressKeysDTO = getHospitalAddressKeys();
    delete hospitalAddress['AddressText'];
    return hospitalAddress;
}

export function getSaveHospitalModulesKeys(): HospitalModulesKeysDTO {
    let modules: HospitalModulesKeysDTO = getHospitalModulesKeys();
    delete modules['mDrToDrChatInAdmin'];
    return modules;
}

export function getDefaultState() {
    let state = {};
    let tempState = {};
    let fields: Array<any> = [
        getHospitalAddressKeys(),
        getHospitalClientKeys(),
        getDefaultHospitalKeys(),
        getHospitalPlatformSeatsKeys(),
        getHospitalBrandingKeys(),
        getHospitalPaymentPartnerKeys(),
        getHospitalePrescriptionPartnerKeys(),
        getHospitalSingleSignOnKeys(),
        getHospitalConsulatationParametersKeys(),
        getHospitalRequiredCustomerFieldsKeys(),
        getHospitalCustomAppLinksKeys(),
        getHospitalSubscribeNotificationEventsKeys(),
        getHospitalEHRIntegrationKeys(),
        getHospitalInternalAPIIntegrationKeys(),
        getHospitalClientInListsKey()
    ];
    for (let index in fields) {
        for (let field in fields[index]) {
            if (field.search(/[a-zA-Z]+Select/i) === 0) {
                tempState[field] = 0;
            } else {
                tempState[field] = '';
            }
        }
    }

    state = Object.assign(state, tempState);

    return state;
}