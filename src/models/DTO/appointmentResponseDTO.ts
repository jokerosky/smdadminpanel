import { AppointmentParticipant } from '../appointmentParticipant';

export class AppointmentResponse {
    appointmentId: string;
    patientId: number;
    clinicianId: number;
    appointmentStatusCode: string;

    consultationId: number;
    dismissed: boolean;
    dismissedReason: string;
    serviceTypeName: string;
    
    participants: AppointmentParticipant[];
    
    availabilityBlockId: string;
    waiveFee: boolean;
    startTime: string;
    endTime: string;
    patientQueueId: string;
    appointmentTypeCode: number;
    encounterTypeCode: number;
    where: string;
    whereUse: number;
    serviceTypeId: number;
    zonedTime: any;
}