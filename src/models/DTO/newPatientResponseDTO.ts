 export class NewPatientResponseDTO {
    address: string;
    dob?: string;
    timeZoneId: number;
    email: string;
    name: any;
    patientId: number;
    providerId: number;
    userLoginId: number;
};