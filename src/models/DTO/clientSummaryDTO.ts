export class ClientSummaryDTO{
    hospitalName:string;
    hospitalType:number;
    hospitalId:number;
    hospitalTypeName:string;
    isActive:string; // 'N', 'A' 
    children:number;
    seats:number;
}