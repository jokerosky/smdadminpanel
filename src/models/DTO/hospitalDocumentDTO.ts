export default class HospitalDocumentDTO{
    documentText:string;
    documentType:number;
    hospitalId:number;
}