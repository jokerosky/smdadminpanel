export class PatientName{
    first:string;
    last:string;
}

export class NewPatientRequestDTO {
   address:string;
   email:string;
   dob:string;
   name:PatientName;
   //hospitalId:number;
   providerId:number;
   password:string;
   userTypeId:number;
   redirectUrl?:string;
}