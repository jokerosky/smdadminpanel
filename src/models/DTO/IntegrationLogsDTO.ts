export class IntegrationLogsDTO {
    IntegrationLogId: number = null;
    LogType: number = null;
    DateCreated: string = '';
    PatientId?: number = null;
    HospitalStaffProfileId?: number = null;
    PartnerId?: number = null;
    LogMessage: string = '';
    HospitalId?: number = null;
    CommandName: string = '';
}