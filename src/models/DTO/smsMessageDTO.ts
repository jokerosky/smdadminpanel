export default class SmsMessageDTO{
    id:number;
    hospitalId:number;
    type:string;
    smsText:string;
    isActive:string;
}