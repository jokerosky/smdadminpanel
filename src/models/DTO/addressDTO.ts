export class AddressObject{
    line1: string;
    line2: string;
    city: string;
    state: string;
    stateCode: string;
    postalCode: string;
    country: string;
    countryCode: string;
    addressText: string;
}

export class AddressDTO {
    hospitalId: number;
    address: string;
    addressObject: AddressObject;
}