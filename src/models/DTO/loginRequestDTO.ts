export default class LoginRequestDTO {
   email:string;
   hospitalId:number;
   password:string;
   userTypeId:number;
   redirectUrl?:string;
}