export class SettingsDTO{
    id:number;
    hospitalId:number;
    key:string;
    value:string;
}