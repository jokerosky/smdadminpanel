export class CodeSetsDTO{
    CodeId: number;
    CodeSetId: number;
    Description: string;
    DisplayOrder: number;
    HospitalId: number;
    IsActive: boolean;
    PartnerCode: string;
    codeSetId?: string;
    displayOrder?: number;
    hospitalId?: number;
}