
export interface IRegistrationAvailabilityResponse{
    isAvailable: boolean;
    message: string;
    category: number;
    hospitalId: number;
}
