export class AdditionalHospitalSettings {
    language?: string;
    defaultAvailabilityBlockDuration?: number;
    brandTextColor?: string;
    brandBackgroundImage?: string;
    brandBackgroundColor?: string;
    mDiagnosticCodingSystem?: string;
    brandBackgroundLoginImage?: string;
    mobileApp_MinSupportedVersion?: string;
    defaultAppointmentDuration?: number;
    onDemandAppointmentDuration?: number;
    selfScheduledAppointmentDuration?: number;
}

export class Hospital {
    customerSso?: string;
    customerSsoLinkText?: string;
    enabledModules?: string[];
    hospitalId?: number
    hospitalName?: string;
    hospitalImage?: string;
    hospitalDomainName?: string;
    patientConsultEndUrl?: string;
    patientLogin?: string;
    settings?: AdditionalHospitalSettings;
    clinicianSso?: string;
    address?: string;
    appointmentsContactNumber?: string;
    brandColor?: string;
    brandName?: string;
    brandTitle?: string;
    contactNumber?: string;
    itDepartmentContactNumber?: string;
    locale?: string
}
