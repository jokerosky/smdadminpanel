import { PersonName } from './personName';
import { SmallTelecom } from './smallTelecom';

export class PersonRecord{
    id: string;
    name: PersonName;
    photoUrl: string;
    providerId: number;
    statusCode: number;
    contactTypeCode: number;
    phones: SmallTelecom[];
}