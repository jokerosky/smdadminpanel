import * as webdriver from 'selenium-webdriver';
import { assert } from 'chai';

import * as vars from './e2e.variables';
import { waitForSeconds } from './e2e.utils';
var By = webdriver.By,
    until = webdriver.until;

export function loginSnapMdAdmin(driver) {
    const elements = {
        nameInput: By.css('')
    };

    return {
        loginAsSnapMdAdmin: function (done) {
            driver.get(vars.DEV_SERVER);
            driver.wait(until.elementLocated(By.css('div.box')), vars.SHORT_WAIT);
            driver.wait(waitForSeconds(vars.LONG_WAIT), vars.LONG_WAIT, 'Splash screen shoudl go away within 4 seconds');
            driver.findElement(By.css('td:nth-child(4) button')).click();
            driver.findElement(By.id('txtloginemail')).sendKeys(vars.ADMIN_EMAIL, webdriver.Key.TAB);
            driver.findElement(By.id('txtPassword')).sendKeys(vars.USER_PASSWORD);
            return  driver.findElement(By.css('button.button__brand')).click();          
        }
    }
};