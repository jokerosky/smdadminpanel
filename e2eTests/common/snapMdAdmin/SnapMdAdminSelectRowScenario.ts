import * as webdriver from 'selenium-webdriver';
import { assert } from 'chai';

import * as vars from '../../e2e.variables';
import { waitForSeconds } from '../../e2e.utils';

var By = webdriver.By,
    until = webdriver.until;
var driver = new webdriver.Builder()
    .forBrowser('chrome')
    .build();

import { loginSnapMdAdmin } from '../../objectPageSnapMdAdmin';

describe('SnapMdAdmin scenario', () => {


    beforeEach(() => {

    });

    afterEach(() => {
        driver.quit();
    });

    it('Selected Client', (done) => {
        loginSnapMdAdmin(driver).loginAsSnapMdAdmin(done);

        driver.wait(until.elementLocated(By.css('#tableListClient')), vars.SHORT_WAIT);
        driver.findElement(By.css('table tr:nth-child(2)')).click();
        driver.wait(until.elementLocated(By.css('#tableListClient .table-success')), vars.SHORT_WAIT)
        driver.wait(until.elementLocated(By.css('#contentView ul li:nth-child(1) .active')), vars.SHORT_WAIT)
        for(let index =1; index < 6; index++){
            driver.findElement(By.css('#contentView ul li:nth-child(2) a')).click();
            driver.wait(until.elementLocated(By.css('#contentView ul li:nth-child(2) .active')), vars.SHORT_WAIT)
        }
        
        /*
        driver.findElement(By.css('#contentView ul li:nth-child(3) a')).click();
        driver.wait(until.elementLocated(By.css('#contentView ul li:nth-child(3) .active')), vars.SHORT_WAIT)
        driver.findElement(By.css('#contentView ul li:nth-child(4) a')).click();
        driver.wait(until.elementLocated(By.css('#contentView ul li:nth-child(4) .active')), vars.SHORT_WAIT)
        driver.findElement(By.css('#contentView ul li:nth-child(5) a')).click();
        driver.wait(until.elementLocated(By.css('#contentView ul li:nth-child(5) .active')), vars.SHORT_WAIT)*/
        /*    .then((msg) => {
                assert.isTrue(true);
                done();
            }, (msg) => {
                console.log(msg);
                assert.fail('no redirection to dashboard happend');
                done();
            });*/
    }).timeout(15000); //

});