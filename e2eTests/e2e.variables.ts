var
DEV_SERVER = "http://localhost:3000",
SHORT_WAIT = 2000,
MEDIUM_WAIT = 5000,
LONG_WAIT = 7000,
ADMIN_EMAIL = "hi@example.com",
USER_EMAIL = "jokerosky@gmail.com",
USER_PASSWORD = "Password@123",
USER_NAME = "Test",
SNAPMD_ADMIN_DASHBOARD = "/snapmdadmin/dashboard";


export {
    DEV_SERVER,
    SHORT_WAIT,
    MEDIUM_WAIT,
    LONG_WAIT,
    USER_EMAIL,
    USER_PASSWORD,
    ADMIN_EMAIL,
    SNAPMD_ADMIN_DASHBOARD
}