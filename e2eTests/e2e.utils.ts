export function waitForSeconds(seconds){
    return new Promise((resolve, reject)=>{
        setTimeout(()=>{
            resolve();
        }, seconds);
    });
}